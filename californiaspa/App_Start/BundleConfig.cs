using System;
using System.Web.Optimization;

namespace californiaspa
{
	public class BundleConfig
	{
		public BundleConfig()
		{
		}

		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add((new ScriptBundle("~/bundles/jqueryval")).Include(new string[] { "~/Scripts/jquery.unobtrusive*", "~/Scripts/jquery.validate*" }));
		}
	}
}