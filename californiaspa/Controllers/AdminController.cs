﻿using californiaspa.DataBase;
using californiaspa.Models;
using californiaspa.Models.Account;
using californiaspa.Models.AdminAudio;
using californiaspa.Models.Appointment;
using californiaspa.Models.booking;
using californiaspa.Models.Chat;
using californiaspa.Models.Client;
using californiaspa.Models.therapist;
using californiaspa.Models.user;
using californiaspa.utilis;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace californiaspa.Controllers
{
    public class AdminController : Controller
    {

        [AuthorizeVerifiedAdmin]
        public ActionResult pending_approval_appt(int? page, string tokenid = "")
        {
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            AccountModel accountModel = new AccountModel();
            int userid = (base.Session["ADId"] == null ? 1 : Convert.ToInt32(base.Session["ADId"].ToString()));
            if (!string.IsNullOrEmpty(tokenid))
            {
                var id = Convert.ToInt32(new Crypto().DecryptStringAES(tokenid).ToString());
                bookingServices _bks = new bookingServices();
                _bks.rejectAppointmentRequestonly(id);
                string str1 = Server.UrlEncode("https://www.californiamassage.in/appointment/reject_request?token" + tokenid);
                string str2 = Server.UrlEncode("https://www.californiamassage.in/admin/reject_request?token" + tokenid);
                Task.Factory.StartNew(() => _bks.rejectAppointmentRequestold(id, "Reject Request Approved by Admin at " + DateTime.Now.ToString(), str1, str2));
                accountModel.ErrorText = "Appointment is Canceled";
            }
            AccountServices accountService = new AccountServices();

            if (base.TempData["user"] != null)
            {
                accountModel.ErrorText = base.TempData["user"].ToString();
            }
            accountModel = accountService.GetCustomerpendingapprovalappt(userid, num, num1, ref total);
            string str = string.Concat(new object[] { "1" });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "admin", "pending_approval_appt", str, new decimal(10)));
            ViewBag.paging = htmlString;
            return base.View(accountModel);
        }


        [AuthorizeVerifiedAdmin]
        public ActionResult approve(int? page)
        {
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            int userid = (base.Session["ADId"] == null ? 1 : Convert.ToInt32(base.Session["ADId"].ToString()));
            AccountServices accountService = new AccountServices();
            AccountModel accountModel = new AccountModel();
            if (base.TempData["user"] != null)
            {
                accountModel.ErrorText = base.TempData["user"].ToString();
            }
            accountModel = accountService.GetCustomerpendingapprovalappt(userid, num, num1, ref total);
            string str = string.Concat(new object[] { "1" });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "admin", "pending_approval_appt", str, new decimal(10)));
            ViewBag.paging = htmlString;
            return base.View(accountModel);
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult add(int? _xid)
        {
            if (!this.haspermission(16))
            {
                return base.View("acess");
            }
            HotelServices hotelService = new HotelServices();
            spaHotelItem _spaHotelItem = new spaHotelItem();
            _spaHotelItem.type = 1;
            if (_xid.HasValue)
            {
                if (!this.haspermission(18))
                {
                    return base.View("acess");
                }
                _spaHotelItem = hotelService.GetHotelsById(_xid.Value);
                _spaHotelItem.hImages = (
                    from a in hotelService.GetAllHotelsImages()
                    where a.ImgId == _xid.Value
                    select a).ToList<imagesItem>();
            }
            if (base.TempData["msg"] != null)
            {
                _spaHotelItem.ErrorText = base.TempData["msg"].ToString();
            }
            return base.View(_spaHotelItem);
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult addadmin()
        {
            if (!this.haspermission(12))
            {
                return base.View("acess");
            }
            userService _userService = new userService();
            base.ViewBag.roles = _userService.getAllRoles();
            if (base.Session["ADId"] != null)
            {
                Convert.ToInt32(base.Session["ADId"].ToString());
            }
            return base.View(new AdminProfile());
        }

        [HttpPost]
      //  [ValidateAntiForgeryToken]
        public ActionResult addadmin(AdminProfile _model)
        {
            userService _userService = new userService();
           ViewBag.roles = _userService.getAllRoles();
            int num = (base.Session["ADId"] == null ? 1 : Convert.ToInt32(base.Session["ADId"].ToString()));
            Crypto crypto = new Crypto();
            if (_model.confirmPassword != _model.Password)
            {
                _model.ErrorText = "Password and Confirm password must match";
                return base.View(_model);
            }
            string str = (new AdminServices()).CreateAdminXML(_model, num);
            base.TempData["msg"] = str;
            return base.RedirectToAction("user");
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult getotp(int? page, string search = "", int searchtype = -1)
        {
          
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            userService _userService = new userService();
            otpmodel _model = new otpmodel();
            _model = new userService().getotpmodel(num, num1, ref total, search, searchtype);
            string str = string.Concat(new object[] { "1&search=", search, "&searchtype=", searchtype });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "admin", "getotp", str, new decimal(10)));
            ViewBag.paging = htmlString;
            return View("getotp", _model);
        }


        [AuthorizeVerifiedAdmin]
        public ActionResult appointment_reminder(int? page, string therapistid = "0", string startDate = null, string endDate = null, int? deletId = null)
        {
            if (!this.haspermission(43))
            {
                return base.View("acess");
            }
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            string empty = string.Empty;
            ChatServices chatService = new ChatServices();
            NotificationModel notificationModel = new NotificationModel();
            DropDownServices dropDownService = new DropDownServices();
            if (deletId.HasValue)
            {
                if (!this.haspermission(44))
                {
                    return base.View("acess");
                }
                empty = chatService.UpdateNotification(deletId.Value);
            }
            int? nullable = null;
            notificationModel = chatService.getAllNotifications(num, num1, ref total, nullable, new int?(Convert.ToInt32(therapistid)), startDate, endDate);
            ViewBag.therapist = dropDownService.bindTherapistDropDown(therapistid, 0);
            ViewBag.startDate = (string.IsNullOrEmpty(startDate) ? DateTime.Now.ToString("dd-MMM-y") : startDate);
            ViewBag.endDate = (string.IsNullOrEmpty(endDate) ? DateTime.Now.ToString("dd-MMM-y") : endDate);
            ViewBag.dtchange = (string.IsNullOrEmpty(startDate) ? 0 : 1);
            notificationModel.ErrorText = empty;
            string str = string.Concat(new object[] { "1&therapistid=", therapistid, "&startDate=", startDate, "&endDate=", endDate });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "admin", "appointment_reminder", str, new decimal(10)));
            ViewBag.paging = htmlString;
            return base.View(notificationModel);
        }

        public void changestatus(string val, int token)
        {
            if (new cookiechecker().isadmincookieexist())
            {
                (new HotelServices()).updateStatusbyhId(token, val);
            }
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult Chat()
        {
            if (!this.haspermission(38))
            {
                return base.View("acess");
            }
            ChatServices chatService = new ChatServices();
            DropDownServices dropDownService = new DropDownServices();
            ViewBag.therapist = dropDownService.bindTherapistDropDown("0", 0);
            ViewBag.client = dropDownService.bindClientDropDown("0", 0);
            int num = 1;
            AdminChat adminChat = new AdminChat()
            {
                _ClientChatlist = chatService.GetAllChatByAdminIdForClient(num, ""),
                _TherapistChatlist = chatService.GetAllChatByAdminIdForTherspist(num, ""),
                _clientProfiles = chatService.GetAllClientProfiles(),
                _therapistProfiles = chatService.GetAllTherspistProfiles(),
                _ClientTherpistChat = chatService.GetAllClientTherapistChatForLoad()
            };
            if (base.TempData["user"] != null)
            {
                adminChat.ErrorText = TempData["user"].ToString();
            }
            return base.View(adminChat);
        }

        public ActionResult client_profiles(string searchBy = "", string searchItem = "", int pageindex = 0, int pagesize = 10, int idx = 0)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return View();
            }
            if (!this.haspermission(8))
            {
                return base.View("acess");
            }
            AdminServices adminService = new AdminServices();
            Profile profile = new Profile();
            if (TempData["msg"] != null)
            {
                profile.ErrorText = TempData["msg"].ToString();
            }
            ViewBag.pageindex = pageindex;
            ViewBag.idx = idx;
            ViewBag.searchby = searchBy;
            ViewBag.searchitem = searchItem;
            return base.View(profile);
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult ClientAccount(string _xid = "")
        {
            Crypto crypto = new Crypto();
            ViewBag.xid = _xid;
            _xid = _xid.Replace(" ", "+");
            int num = Convert.ToInt32(crypto.DecryptStringAES(_xid));
            AccountServices accountService = new AccountServices();
            AccountModel accountModel = new AccountModel();
            if (TempData["user"] != null)
            {
                accountModel.ErrorText = TempData["user"].ToString();
            }
            accountModel = accountService.GetCustomer(num, false);
            return base.View(accountModel);
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult Confirm_Appointments(string date = "", int t = 0, int c = 0, int h = 0)
        {
            DropDownServices dropDownService = new DropDownServices();
            ViewBag.therapist = dropDownService.bindTherapistDropDown("0", t);
            ViewBag.client = dropDownService.bindClientDropDown("0", c);
            ViewBag.hotel = (new HotelServices()).GetAllHotelsForDropDown();
            ViewBag.date = date;
            ViewBag.hotelvalue = h;
            ViewBag.t = t;
            ViewBag.c = c;
            if (!this.haspermission(22, 24))
            {
                return base.View("acess");
            }
            string str = "GetAllConfirmAppointments";
            return base.View(new AppointmentModel()
            {
                appt = (new therapistService()).GetAllConfirmAppointments(str, date, h, t, c)
            });
        }

        [HttpPost]
        public ActionResult CreateorUpdateClient(RegisterModel _model, int index = 0, int idx = 0, HttpPostedFileBase files = null)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return View();
            }
            HotelServices hotelService = new HotelServices();
            ViewBag.hotels = hotelService.GetAllHotelsForDropDown();
            Crypto crypto = new Crypto();
            ViewBag.index = index;
            AdminServices adminService = new AdminServices();
            if (string.IsNullOrEmpty(_model._exid))
            {
                string str = adminService.AddNewCustomer(_model);
                int num = 0;
                if (str.Contains<char>('\u005F'))
                {
                    num = Convert.ToInt32(str.Split(new char[] { '\u005F' })[0]);
                    str = str.Split(new char[] { '\u005F' })[1];
                }
                ViewBag.userId = num;
                _model.ErrorText = str;
                if (str.Contains("Error"))
                {
                    return base.View("EditClientAccount", _model);
                }
                TempData["msg"] = str;
                return base.RedirectToAction("client_profiles");
            }
            RegisterModel registerModel = _model;
            registerModel._exid = registerModel._exid.Replace(" ", "+");
            int num1 = Convert.ToInt32(crypto.DecryptStringAES(_model._exid));
            if (files != null)
            {
                string str1 = files.FileName.Replace(' ', '\u005F');
                string str2 = Path.Combine(Server.MapPath("~/profileImages"), str1);
                files.SaveAs(str2);
                _model.photoidurl = (new bookingServices()).GetShortUrl(string.Concat("https://www.californiamassage.in/main/getId?name=", str1), false);
            }
            string str3 = adminService.updateCustomer(_model, num1);
            _model.ErrorText = str3;
            ViewBag.userId = num1;
            if (str3.Contains("Error"))
            {
                return base.View("EditClientAccount", _model);
            }
            TempData["msg"] = str3;
            return base.RedirectToAction("client_profiles", new { pageindex = index, idx = idx, searchBy = _model.searchBy, searchItem = _model.searchItem });
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult dashboard()
        {
            return base.View();
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult delete_reminder(string therapistid = "0", string startDate = null, string endDate = null, string deletId = null)
        {
            if (!this.haspermission(44))
            {
                return base.View("acess");
            }
            ChatServices chatService = new ChatServices();
            NotificationModel notificationModel = new NotificationModel();
            DropDownServices dropDownService = new DropDownServices();
            string empty = string.Empty;
            if (!string.IsNullOrEmpty(deletId))
            {
                string[] strArrays = deletId.Split(new char[] { '\u005F' });
                for (int i = 0; i < (int)strArrays.Length; i++)
                {
                    string str = strArrays[i];
                    empty = chatService.UpdateNotification(Convert.ToInt16(str));
                }
            }
            int? nullable = null;
            // notificationModel = chatService.getAllNotifications(nullable, new int?(Convert.ToInt32(therapistid)), startDate, endDate);
            ViewBag.therapist = dropDownService.bindTherapistDropDown(therapistid, 0);
            ViewBag.startDate = (string.IsNullOrEmpty(startDate) ? DateTime.Now.ToString("dd-MMM-y") : startDate);
            ViewBag.endDate = (string.IsNullOrEmpty(endDate) ? DateTime.Now.ToString("dd-MMM-y") : endDate);
            ViewBag.dtchange = (string.IsNullOrEmpty(startDate) ? 0 : 1);
            return RedirectToAction("appointment_reminder", "admin", new { therapistid = therapistid, startDate = startDate, endDate = endDate });
        }

        [HttpPost]
        public string DeleteAccount(string _xid)
        {
            if (!this.haspermission(4))
            {
                return "Error : You are not authorize for this action.";
            }
            int num = Convert.ToInt32((new Crypto()).DecryptStringAES(_xid));
            return (new therapistService()).DeleteTherapistById(num);
        }

        [HttpPost]
        public string Deleteclient(string _xid)
        {
            Crypto crypto = new Crypto();
            if (!this.haspermission(9))
            {
                return " Error : you are not authorized for this action";
            }
            int num = Convert.ToInt32(crypto.DecryptStringAES(_xid));
            return (new therapistService()).DeleteUserById(num);
        }

        [HttpPost]
        public void DeleteClientmessage(int[] data)
        {
            if (new cookiechecker().isadmincookieexist())
            {
                (new ChatServices()).deleteChatMessage(data);
            }
            
        }

        [HttpPost]
        public string DeleteHotelImage(int _xid)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return "";
            }
                return (new HotelServices()).deleteImage(_xid);
        }

        [HttpPost]
        public string DeleteImage(int _xid)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return "";
            }
            return (new therapistService()).DeleteTherapistImageById(_xid);
        }

        [HttpPost]
        public string DeleteimageId(string _xid)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return "";
            }
            int num = Convert.ToInt32((new Crypto()).DecryptStringAES(_xid));
            return (new therapistService()).DeleteUserPhotoIdByUserId(num);
        }

        [HttpPost]
        public void Deletemessage(int _xid)
        {
            if (new cookiechecker().isadmincookieexist())
            {

                (new ChatServices()).deleteChatMessage(_xid);
            }
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult deletuser(string token = "")
        {
            if (!this.haspermission(14))
            {
                return base.View("acess");
            }
            int num = Convert.ToInt32((new Crypto()).DecryptStringAES(token));
            string str = (new AdminServices()).deleteUser(num);
            TempData["msg"] = str;
            return base.RedirectToAction("user");
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult Edit_ClientSurvey(string token)
        {
            if (!this.haspermission(36))
            {
                return base.View("acess");
            }
            bookingServices bookingService = new bookingServices();
            Crypto crypto = new Crypto();
            int num = 0;
            if (!string.IsNullOrEmpty(token))
            {
                token = token.Replace(" ", "+");
                num = Convert.ToInt32(crypto.DecryptStringAES(token));
            }
            return base.View(bookingService.GetClientSurveyByIdSurvey(num, token));
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult Edit_Survey(string token)
        {
            if (!this.haspermission(30))
            {
                return base.View("acess");
            }
            bookingServices bookingService = new bookingServices();
            Crypto crypto = new Crypto();
            int num = 0;
            if (!string.IsNullOrEmpty(token))
            {
                token = token.Replace(" ", "+");
                num = Convert.ToInt32(crypto.DecryptStringAES(token));
            }
            return base.View(bookingService.GetTerapistSurveyByIdSurvey(num, token));
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult editadmin(string _xid = "")
        {
            if (!this.haspermission(13))
            {
                return base.View("acess");
            }
            userService _userService = new userService();
            ViewBag.roles = _userService.getAllRoles();
            int num = Convert.ToInt32((new Crypto()).DecryptStringAES(_xid));
            return base.View((new AdminServices()).readAdminxml(num));
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult editadmin(AdminProfile _model)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return View();
            }
            userService _userService = new userService();
            ViewBag.roles = _userService.getAllRoles();
            Crypto crypto = new Crypto();
            if (_model.confirmPassword != _model.Password)
            {
                _model.ErrorText = "Password and Confirm password must match";
                return base.View(_model);
            }
            _model.Password = crypto.EncryptStringAES(_model.Password);
            string str = (new AdminServices()).updateAdminXMLwithRole(_model);
            TempData["msg"] = str;
            return base.RedirectToAction("user");
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult EditAdminAccount(string _xid = "")
        {
            int num = Convert.ToInt32(Session["ADId"].ToString());
            return base.View((new AdminServices()).readAdminxml(num));
        }

        [HttpPost]
        public ActionResult EditAdminAccount(AdminProfile _model)
        {
            _model.Id = Convert.ToInt32(Session["ADId"].ToString());
            Crypto crypto = new Crypto();
            if (_model.confirmPassword != _model.Password)
            {
                _model.ErrorText = "Password and Confirm password must match";
                return base.View(_model);
            }
            _model.ErrorText = (new AdminServices()).updateAdminXML(_model);
            return base.View(_model);
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult EditClientAccount(string _xid = "", int index = 1, int idx = 0, string searchBy = "", string searchItem = "")
        {
            if (!this.haspermission(6))
            {
                return base.View("acess");
            }
            Crypto crypto = new Crypto();
            AccountServices accountService = new AccountServices();
            RegisterModel registerModel = new RegisterModel();
            registerModel.usertype = "New";
            HotelServices hotelService = new HotelServices();
            ViewBag.hotels = hotelService.GetAllHotelsForDropDown();
            ViewBag.userId = 0;
            ViewBag.index = index;
            ViewBag.idx = 0;
            registerModel.searchBy = searchBy;
            registerModel.searchItem = searchItem;
            if (!string.IsNullOrEmpty(_xid))
            {
                if (!this.haspermission(7))
                {
                    return base.View("acess");
                }
                _xid = _xid.Replace(" ", "+");
                int num = Convert.ToInt32(crypto.DecryptStringAES(_xid));
                ViewBag.userId = num;
                AccountModel customer = accountService.GetCustomer(num, false);
                registerModel.userName = customer.userName;
                registerModel.usertype = customer.usertype;
                registerModel.mobile = customer.mobile;
                registerModel.CountryCode = customer.CountryCode;
                registerModel._exid = customer._exid;
                registerModel.name = customer.Name;
                registerModel.password = customer.password;
                registerModel.email = customer.emailAddress;
                registerModel.block = customer.block;
                registerModel.mngrVerfiy = customer.Verifiedbymg;
                registerModel.blockReason = customer.blockResaon;
                registerModel.HouseNumber = customer.houseNumber;
                registerModel.AptNumber = customer.aptNumber;
                registerModel.HotelName = customer.hotelName;
                registerModel.FloorNumber = customer.floorNumber;
                registerModel.StreetName = customer.streetName;
                registerModel.CrossStreetName = customer.crossstreetName;
                registerModel.Landmark = customer.landmark;
                registerModel.LocalityArea = customer.localityArea;
                registerModel.AptName = customer.aptName;
                registerModel.RoomNumber = customer.roomNumber;
                registerModel.ltd = customer.ltd;
                registerModel.lth = customer.lth;
                registerModel.spahotelAddress = customer.spahotelAddress;
                registerModel.spahotelname = customer.spahotelname;
                registerModel.locationtypeId = customer.locationtypeId;
                registerModel.SpaHotelId = customer.SpaHotelId;
                registerModel.photoidurl = customer.photoidurl;
                if (TempData["user"] != null)
                {
                    registerModel.ErrorText = TempData["user"].ToString();
                }
                ViewBag.idx = customer.counter;
            }
            return base.View(registerModel);
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult Etherapist_profiles(string token = null)
        {
            if (string.IsNullOrEmpty(token))
            {
                if (!this.haspermission(1))
                {
                    return base.View("acess");
                }
                return base.View(new therapistprofile());
            }
            if (!this.haspermission(3))
            {
                return base.View("acess");
            }
            therapistService _therapistService = new therapistService();
            AdminServices adminService = new AdminServices();
            Crypto crypto = new Crypto();
            token = token.Replace(" ", "+");
            int num = Convert.ToInt32(crypto.DecryptStringAES(token));
            therapistprofile allTherapistById = _therapistService.GetAllTherapistById(num, true);
            allTherapistById.images = (
                from a in _therapistService.GetAllTherapistImages()
                where a.thId == num
                select a).ToList<imagesItem>();
            if (TempData["msg"] != null)
            {
                allTherapistById.ErrorText = TempData["msg"].ToString();
            }
            return base.View(allTherapistById);
        }

        public JsonResult Fetchusers(int start = 0, int length = 0, int draw = 1, string searchBy = "", string searchItem = "", int pageindex = 1)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
               return Json("Session out ,Please login agin",JsonRequestBehavior.AllowGet);
            }
            int num;
            if (pageindex != 0)
            {
                num = pageindex;
            }
            else
            {
                num = (start == 0 ? start : start / 10 + 1);
            }
            start = num;
            AdminServices adminService = new AdminServices();
            AdminController.TableDate<profile_item> tableDate = new AdminController.TableDate<profile_item>();
            List<string[]> strArrays = new List<string[]>();
            int num1 = 0;
            List<profile_item> allClientsPageWise = adminService.GetAllClientsPageWise(ref num1, searchBy, searchItem, start, length)._profiles;
            tableDate.draw = draw;
            tableDate.data = allClientsPageWise;
            tableDate.recordsFiltered = (allClientsPageWise.Count<profile_item>() > 0 ? allClientsPageWise[0].total : 0);
            tableDate.recordsTotal = num1;
            return base.Json(tableDate, 0);
        }

        private DataTable FormatclientDtForExcel(SummaryModel _model)
        {
            DataTable dataTable = new DataTable();
            dataTable.Clear();
            dataTable.Columns.Add("ClientName");
            dataTable.Columns.Add("TherapistName");
            dataTable.Columns.Add("bookingdate");
            dataTable.Columns.Add("confirmTime");
            dataTable.Columns.Add("Amount");
            dataTable.Columns.Add("Discount");
            dataTable.Columns.Add("TotalAmount");
            Dictionary<string, List<SummaryViewModel>> dictionary = (
                from a in _model._list
                orderby a.ClientName
                group a by a.ClientName).ToDictionary<IGrouping<string, SummaryViewModel>, string, List<SummaryViewModel>>((IGrouping<string, SummaryViewModel> a) => a.Key, (IGrouping<string, SummaryViewModel> a) => a.ToList<SummaryViewModel>());
            int num = 0;
            double num1 = 0;
            double discountAmount = 0;
            int num2 = 0;
            double num3 = 0;
            double discountAmount1 = 0;
            int num4 = 0;
            double num5 = 0;
            double discountAmount2 = 0;
            foreach (string key in dictionary.Keys)
            {
                Dictionary<string, List<SummaryViewModel>> strs = (
                    from a in dictionary[key]
                    group a by a.Name).ToDictionary<IGrouping<string, SummaryViewModel>, string, List<SummaryViewModel>>((IGrouping<string, SummaryViewModel> a) => a.Key, (IGrouping<string, SummaryViewModel> a) => a.ToList<SummaryViewModel>());
                num2 = 0;
                num3 = 0;
                discountAmount1 = 0;
                foreach (string str in strs.Keys)
                {
                    num = 0;
                    num1 = 0;
                    discountAmount = 0;
                    foreach (SummaryViewModel item in strs[str])
                    {
                        num++;
                        num1 += Convert.ToDouble(item.totalamountPerClient);
                        discountAmount += item.DiscountAmount;
                        num2++;
                        num3 += Convert.ToDouble(item.totalamountPerClient);
                        discountAmount1 += item.DiscountAmount;
                        num4++;
                        num5 += Convert.ToDouble(item.totalamountPerClient);
                        discountAmount2 += item.DiscountAmount;
                        DataRow clientName = dataTable.NewRow();
                        clientName["ClientName"] = item.ClientName;
                        clientName["TherapistName"] = item.Name;
                        clientName["bookingdate"] = item.bookingDate;
                        clientName["confirmTime"] = item.bookingtime;
                        clientName["Amount"] = item.totalamountPerClient;
                        clientName["Discount"] = item.DiscountAmount;
                        clientName["TotalAmount"] = item.Amount;
                        dataTable.Rows.Add(clientName);
                    }
                    string str1 = string.Concat(key, " Sub-Total with ", str);
                    DataRow dataRow = dataTable.NewRow();
                    dataRow["TotalAmount"] = str1;
                    dataTable.Rows.Add(dataRow);
                    str1 = string.Concat(new object[] { key, " total massage with ", str, ":", num });
                    DataRow dataRow1 = dataTable.NewRow();
                    dataRow1["TotalAmount"] = str1;
                    dataTable.Rows.Add(dataRow1);
                    str1 = string.Concat(new object[] { key, " sub-total amount with ", str, ":", num1 });
                    DataRow dataRow2 = dataTable.NewRow();
                    dataRow2["TotalAmount"] = str1;
                    dataTable.Rows.Add(dataRow2);
                    str1 = string.Concat(new object[] { key, " total discount with ", str, ":", discountAmount });
                    DataRow dataRow3 = dataTable.NewRow();
                    dataRow3["TotalAmount"] = str1;
                    dataTable.Rows.Add(dataRow3);
                    str1 = string.Concat(new object[] { key, " Net Amount with ", str, ":", num1, "-", discountAmount, "=", num1 - discountAmount });
                    DataRow dataRow4 = dataTable.NewRow();
                    dataRow4["TotalAmount"] = str1;
                    dataTable.Rows.Add(dataRow4);
                }
                string str2 = string.Concat(key, " Total With All Therapist");
                DataRow dataRow5 = dataTable.NewRow();
                dataRow5["TotalAmount"] = str2;
                dataTable.Rows.Add(dataRow5);
                str2 = string.Concat(key, " total massage :", num2);
                DataRow dataRow6 = dataTable.NewRow();
                dataRow6["TotalAmount"] = str2;
                dataTable.Rows.Add(dataRow6);
                str2 = string.Concat(key, " sub-total amount :", num3);
                DataRow dataRow7 = dataTable.NewRow();
                dataRow7["TotalAmount"] = str2;
                dataTable.Rows.Add(dataRow7);
                str2 = string.Concat(key, " total discount:", discountAmount1);
                DataRow dataRow8 = dataTable.NewRow();
                dataRow8["TotalAmount"] = str2;
                dataTable.Rows.Add(dataRow8);
                str2 = string.Concat(new object[] { key, " Net Amount :", num3, "-", discountAmount1, "=", num3 - discountAmount1 });
                DataRow dataRow9 = dataTable.NewRow();
                dataRow9["TotalAmount"] = str2;
                dataTable.Rows.Add(dataRow9);
            }
            string str3 = " Total All clients";
            DataRow dataRow10 = dataTable.NewRow();
            dataRow10["TotalAmount"] = str3;
            dataTable.Rows.Add(dataRow10);
            str3 = string.Concat(" total massage :", num4);
            DataRow dataRow11 = dataTable.NewRow();
            dataRow11["TotalAmount"] = str3;
            dataTable.Rows.Add(dataRow11);
            str3 = string.Concat(" sub-total amount :", num5);
            DataRow dataRow12 = dataTable.NewRow();
            dataRow12["TotalAmount"] = str3;
            dataTable.Rows.Add(dataRow12);
            str3 = string.Concat(" total discount:", discountAmount2);
            DataRow dataRow13 = dataTable.NewRow();
            dataRow13["TotalAmount"] = str3;
            dataTable.Rows.Add(dataRow13);
            str3 = string.Concat(new object[] { " Net Amount :", num5, "-", discountAmount2, "=", num5 - discountAmount2 });
            DataRow dataRow14 = dataTable.NewRow();
            dataRow14["TotalAmount"] = str3;
            dataTable.Rows.Add(dataRow14);
            dataTable.AcceptChanges();
            return dataTable;
        }

        private DataTable FormattherapistDtForExcel(SummaryModel _model, string level1, string level2)
        {
            DataTable dataTable = new DataTable();
            dataTable.Clear();
            dataTable.Columns.Add("TherapistName");
            dataTable.Columns.Add("ClientName");
            dataTable.Columns.Add("bookingdate");
            dataTable.Columns.Add("confirmTime");
            dataTable.Columns.Add("Amount");
            dataTable.Columns.Add("Discount");
            dataTable.Columns.Add("TotalAmount");
            string sorttype = "Date";
            var _orderbylist = _model._list.OrderBy(a => a.bookingDate_2);
            var list = _orderbylist.GroupBy(a => a.Name).ToDictionary(a => a.Key, a => a.ToList());
            var _datelist = _orderbylist.GroupBy(a => a.bookingDate_2).ToDictionary(a => a.Key, a => a.ToList());

            if (level1 == "1d")
            {
                _orderbylist = _model._list.OrderBy(a => a.bookingDate_2);
                if (level2 == "2d")
                {
                    _datelist = _orderbylist.GroupBy(a => a.bookingDate_2).ToDictionary(a => a.Key, a => a.ToList().OrderBy(p => p.bookingDate_2).ToList());
                }
                else
                {
                    _datelist = _orderbylist.GroupBy(a => a.bookingDate_2).ToDictionary(a => a.Key, a => a.ToList().OrderBy(p => p.Name).ToList());

                }
            }
            else
            {
                sorttype = "Name";
                _orderbylist = _model._list.OrderBy(a => a.Name);
                if (level2 == "2d")
                {
                    list = _orderbylist.GroupBy(a => a.Name).ToDictionary(a => a.Key, a => a.ToList().OrderBy(p => p.bookingDate_2).ToList());
                }
                else
                {
                    list = _orderbylist.GroupBy(a => a.Name).ToDictionary(a => a.Key, a => a.ToList().OrderBy(p => p.Name).ToList());
                }
            }
            int msgCount = 0;
            Double totalAmount = 0.0;
            Double TotalDiscount = 0.0;
            int tmsgCount = 0;
            Double ttotalAmount = 0.0;
            Double tTotalDiscount = 0.0;
            int atmsgCount = 0;
            Double attotalAmount = 0.0;
            Double atTotalDiscount = 0.0;
            int i = 0;
            int j = 0;
            if (sorttype == "Date")
            {
                foreach (var item in _datelist.Keys)
                {
                    i = i + 1;
                    j = j + 1;
                    var _item1 = _datelist[item];
                    var _list1 = _item1.GroupBy(a => a.ClientName).ToDictionary(a => a.Key, a => a.ToList());
                    tmsgCount = 0;
                    ttotalAmount = 0.0;
                    tTotalDiscount = 0.0;
                    foreach (var itemval in _list1.Keys)
                    {
                        msgCount = 0;
                        totalAmount = 0.0;
                        TotalDiscount = 0.0;
                        foreach (var _items in _list1[itemval])
                        {
                            msgCount += 1;
                            totalAmount += Convert.ToDouble(_items.totalamountPerClient);
                            TotalDiscount += _items.DiscountAmount;
                            tmsgCount += 1;
                            ttotalAmount += Convert.ToDouble(_items.totalamountPerClient);
                            tTotalDiscount += _items.DiscountAmount;
                            atmsgCount += 1;
                            attotalAmount += Convert.ToDouble(_items.totalamountPerClient);
                            atTotalDiscount += _items.DiscountAmount;
                            DataRow clientName = dataTable.NewRow();
                            clientName["ClientName"] = _items.ClientName;
                            clientName["TherapistName"] = _items.Name;
                            clientName["bookingdate"] = _items.bookingDate_2.ToString("d-MMM-yy");
                            clientName["confirmTime"] = _items.bookingtime;
                            clientName["Amount"] = _items.totalamountPerClient;
                            clientName["Discount"] = _items.DiscountAmount;
                            clientName["TotalAmount"] = _items.Amount;
                            dataTable.Rows.Add(clientName);
                        }
                    }

                    string str2 = string.Concat(item.ToString("d-MMM-yy"), " Total With All clients");
                    DataRow dataRow5 = dataTable.NewRow();
                    dataRow5["TotalAmount"] = str2;
                    dataTable.Rows.Add(dataRow5);
                    str2 = string.Concat(item.ToString("d-MMM-yy"), " total massage :", tmsgCount);
                    DataRow dataRow6 = dataTable.NewRow();
                    dataRow6["TotalAmount"] = str2;
                    dataTable.Rows.Add(dataRow6);
                    str2 = string.Concat(item.ToString("d-MMM-yy"), " sub-total amount :", ttotalAmount);
                    DataRow dataRow7 = dataTable.NewRow();
                    dataRow7["TotalAmount"] = str2;
                    dataTable.Rows.Add(dataRow7);
                    str2 = string.Concat(item.ToString("d-MMM-yy"), " total discount:", tTotalDiscount);
                    DataRow dataRow8 = dataTable.NewRow();
                    dataRow8["TotalAmount"] = str2;
                    dataTable.Rows.Add(dataRow8);
                    str2 = string.Concat(new object[] { item.ToString("d-MMM-yy"), " Net Amount :", ttotalAmount, "-", tTotalDiscount, "=", ttotalAmount - tTotalDiscount
    });
                    DataRow dataRow9 = dataTable.NewRow();
                    dataRow9["TotalAmount"] = str2;
                    dataTable.Rows.Add(dataRow9);

                }
                string str3 = " Total All therapist";
                DataRow dataRow10 = dataTable.NewRow();
                dataRow10["TotalAmount"] = str3;
                dataTable.Rows.Add(dataRow10);
                str3 = string.Concat(" total massage :", atmsgCount);
                DataRow dataRow11 = dataTable.NewRow();
                dataRow11["TotalAmount"] = str3;
                dataTable.Rows.Add(dataRow11);
                str3 = string.Concat(" sub-total amount :", attotalAmount);
                DataRow dataRow12 = dataTable.NewRow();
                dataRow12["TotalAmount"] = str3;
                dataTable.Rows.Add(dataRow12);
                str3 = string.Concat(" total discount:", atTotalDiscount);
                DataRow dataRow13 = dataTable.NewRow();
                dataRow13["TotalAmount"] = str3;
                dataTable.Rows.Add(dataRow13);
                str3 = string.Concat(new object[] { " Net Amount :", attotalAmount, "-", atTotalDiscount, "=", attotalAmount - atTotalDiscount
    });
                DataRow dataRow14 = dataTable.NewRow();
                dataRow14["TotalAmount"] = str3;
                dataTable.Rows.Add(dataRow14);
                dataTable.AcceptChanges();
                return dataTable;
            }
            else
            {
                foreach (var item in list.Keys)
                {
                    i = i + 1;
                    j = j + 1;
                    var _item1 = list[item];
                    var _list1 = _item1.GroupBy(a => a.ClientName).ToDictionary(a => a.Key, a => a.ToList());
                    tmsgCount = 0;
                    ttotalAmount = 0.0;
                    tTotalDiscount = 0.0;
                    foreach (var itemval in _list1.Keys)
                    {
                        msgCount = 0;
                        totalAmount = 0.0;
                        TotalDiscount = 0.0;
                        foreach (var _items in _list1[itemval])
                        {
                            msgCount += 1;
                            totalAmount += Convert.ToDouble(_items.totalamountPerClient);
                            TotalDiscount += _items.DiscountAmount;
                            tmsgCount += 1;
                            ttotalAmount += Convert.ToDouble(_items.totalamountPerClient);
                            tTotalDiscount += _items.DiscountAmount;
                            atmsgCount += 1;
                            attotalAmount += Convert.ToDouble(_items.totalamountPerClient);
                            atTotalDiscount += _items.DiscountAmount;
                            DataRow clientName = dataTable.NewRow();
                            clientName["ClientName"] = _items.ClientName;
                            clientName["TherapistName"] = _items.Name;
                            clientName["bookingdate"] = _items.bookingDate_2.ToString("d-MMM-yy");
                            clientName["confirmTime"] = _items.bookingtime;
                            clientName["Amount"] = _items.totalamountPerClient;
                            clientName["Discount"] = _items.DiscountAmount;
                            clientName["TotalAmount"] = _items.Amount;
                            dataTable.Rows.Add(clientName);
                        }
                    }

                    string str2 = string.Concat(item, " Total With All clients");
                    DataRow dataRow5 = dataTable.NewRow();
                    dataRow5["TotalAmount"] = str2;
                    dataTable.Rows.Add(dataRow5);
                    str2 = string.Concat(item, " total massage :", tmsgCount);
                    DataRow dataRow6 = dataTable.NewRow();
                    dataRow6["TotalAmount"] = str2;
                    dataTable.Rows.Add(dataRow6);
                    str2 = string.Concat(item, " sub-total amount :", ttotalAmount);
                    DataRow dataRow7 = dataTable.NewRow();
                    dataRow7["TotalAmount"] = str2;
                    dataTable.Rows.Add(dataRow7);
                    str2 = string.Concat(item, " total discount:", tTotalDiscount);
                    DataRow dataRow8 = dataTable.NewRow();
                    dataRow8["TotalAmount"] = str2;
                    dataTable.Rows.Add(dataRow8);
                    str2 = string.Concat(new object[] { item, " Net Amount :", ttotalAmount, "-", tTotalDiscount, "=", ttotalAmount - tTotalDiscount
    });
                    DataRow dataRow9 = dataTable.NewRow();
                    dataRow9["TotalAmount"] = str2;
                    dataTable.Rows.Add(dataRow9);

                }
                string str3 = " Total All therapist";
                DataRow dataRow10 = dataTable.NewRow();
                dataRow10["TotalAmount"] = str3;
                dataTable.Rows.Add(dataRow10);
                str3 = string.Concat(" total massage :", atmsgCount);
                DataRow dataRow11 = dataTable.NewRow();
                dataRow11["TotalAmount"] = str3;
                dataTable.Rows.Add(dataRow11);
                str3 = string.Concat(" sub-total amount :", attotalAmount);
                DataRow dataRow12 = dataTable.NewRow();
                dataRow12["TotalAmount"] = str3;
                dataTable.Rows.Add(dataRow12);
                str3 = string.Concat(" total discount:", atTotalDiscount);
                DataRow dataRow13 = dataTable.NewRow();
                dataRow13["TotalAmount"] = str3;
                dataTable.Rows.Add(dataRow13);
                str3 = string.Concat(new object[] { " Net Amount :", attotalAmount, "-", atTotalDiscount, "=", attotalAmount - atTotalDiscount
    });
                DataRow dataRow14 = dataTable.NewRow();
                dataRow14["TotalAmount"] = str3;
                dataTable.Rows.Add(dataRow14);
                dataTable.AcceptChanges();
                return dataTable;
            }


        }

        [AuthorizeVerifiedAdmin]
        public ActionResult Get_hotel_Report(int? _xid)
        {
            if (!this.haspermission(17))
            {
                return base.View("acess");
            }
            HotelServices hotelService = new HotelServices();
            spaHotel _spaHotel = new spaHotel();
            if (_xid.HasValue)
            {
                if (!this.haspermission(19))
                {
                    return base.View("acess");
                }
                _spaHotel.ErrorText = hotelService.deleteHotel(_xid.Value);
            }
            _spaHotel = hotelService.GetAllHotels(true);
            _spaHotel.statuspermission = this.haspermission(20);
            return base.View(_spaHotel);
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult Get_Punctuality_Report(int? page, string therapistid = "0", string startDate = null, string endDate = null, int? deletId = null)
        {
            if (!this.haspermission(39))
            {
                return base.View("acess");
            }
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            therapistService _therapistService = new therapistService();
            AppointmentModel appointmentModel = new AppointmentModel();
            DropDownServices dropDownService = new DropDownServices();
            if (deletId.HasValue)
            {
                if (!this.haspermission(40))
                {
                    return base.View("acess");
                }
                appointmentModel.ErrorText = _therapistService.UpdateAppointmentOnReport(deletId.Value);
            }
            List<AppointmentModel_item> allPunctualityReport = _therapistService.GetAllPunctualityReport(num, num1, ref total, Convert.ToInt32(therapistid), startDate, endDate);
            appointmentModel.lstPunctualityReports = allPunctualityReport;
            ViewBag.therapist = dropDownService.bindTherapistDropDown(therapistid, 0);
            ViewBag.startDate = (string.IsNullOrEmpty(startDate) ? DateTime.Now.ToString("dd-MMM-y") : startDate);
            ViewBag.endDate = (string.IsNullOrEmpty(endDate) ? DateTime.Now.ToString("dd-MMM-y") : endDate);
            ViewBag.dtchange = (string.IsNullOrEmpty(startDate) ? 0 : 1);
            string str = string.Concat(new object[] { "1&startDate=", startDate, "&endDate=", endDate, "&therapistid=", therapistid });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "admin", "get_punctuality_report", str, new decimal(10)));
            ViewBag.paging = htmlString;
            return base.View(appointmentModel);
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult GetAudioReports(int? page, string therapistid = "0", string startDate = null, string endDate = null, int? deletId = null)
        {
            if (!this.haspermission(42))
            {
                return base.View("acess");
            }
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            AdminServices adminService = new AdminServices();
            AudioReport audioReport = new AudioReport();
            DropDownServices dropDownService = new DropDownServices();
            if (deletId.HasValue)
            {
                if (!this.haspermission(41))
                {
                    return base.View("acess");
                }
                audioReport.ErrorText = adminService.UpdateAudioReport(deletId.Value);
            }
            audioReport.audioReportList = adminService.GetAllAudioReports(num, num1, ref total, Convert.ToInt32(therapistid), startDate, endDate);
            ViewBag.therapist = dropDownService.bindTherapistDropDown(therapistid, 0);
            ViewBag.startDate = (string.IsNullOrEmpty(startDate) ? DateTime.Now.ToString("dd-MMM-y") : startDate);
            ViewBag.endDate = (string.IsNullOrEmpty(endDate) ? DateTime.Now.ToString("dd-MMM-y") : endDate);
            ViewBag.dtchange = (string.IsNullOrEmpty(startDate) ? 0 : 1);
            string str = string.Concat(new object[] { "1&startDate=", startDate, "&endDate=", endDate, "&therapistid=", therapistid });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "admin", "getAudioReports", str, new decimal(10)));
            ViewBag.paging = htmlString;
            return base.View("AudioReport", audioReport);
        }

        [HttpPost]
        public ActionResult GetChatbySearch(int therapistId = 0, int clientid = 0)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            ChatServices chatService = new ChatServices();
            return this.PartialView("SearchAllChats", new AdminChat()
            {
                _keywordSearch = chatService.GetAllChatBySerachForAdmin(therapistId, clientid, "", 0, 0)
            });
        }

        [HttpPost]
        public PartialViewResult getclient_profiles(string searchBy = "", string searchItem = "", int pageindex = 1, int pagesize = 10)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return PartialView();
            }
            AdminServices adminService = new AdminServices();
            return this.PartialView("getclient_profiles", new Profile()
            {
                idnex = 10 * pageindex - 10,
                pageidnex = pageindex
            });
        }

        [AuthorizeVerifiedAdmin]
        [HttpPost]
        public ActionResult GetClientChat(string searchToken = "")
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            ChatServices chatService = new ChatServices();
            int num = 1;
            return this.PartialView("SearchClientsChats", new AdminChat()
            {
                _ClientChatlist = chatService.GetAllChatByAdminIdForClient(num, searchToken),
                ClientsearchToken = searchToken
            });
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult GetClientFeedBackByTherapist(int? page, int therapistid = 0, string startDate = "", string endDate = "", string deleteId = "")
        {
            if (!haspermission(35))
            {
                return View("acess");
            }
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            ChatServices chatService = new ChatServices();
            string strq = "";
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (!string.IsNullOrEmpty(deleteId))
            {
                string[] strArrays = deleteId.Split(new char[] { '\u005F' });
                for (int i = 0; i < (int)strArrays.Length; i++)
                {
                    strq = strArrays[i];
                    chatService.deleteclientsurvey(Convert.ToInt16(strq));
                }
            }
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            AdminServeyModel adminServeyModel = new AdminServeyModel();
            DropDownServices dropDownService = new DropDownServices();
            int total = 0;
            ViewBag.page = page ?? 1;
            adminServeyModel._Summarylist = chatService.GetAllSurveyForClient(num, num1, startDate, endDate, therapistid);
            if (adminServeyModel._Summarylist.Count<SurveyViewModel>() > 0)
            {
                total = adminServeyModel._Summarylist[0].Total;
            }
            if (base.TempData["msg"] != null)
            {
                adminServeyModel.ErrorText = base.TempData["msg"].ToString();
            }
            string str = string.Concat(new object[] { "1&startDate=", startDate, "&endDate=", endDate, "&therapistid=", therapistid });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "Admin", "GetClientFeedBackByTherapist", str, new decimal(10)));
            ViewBag.paging = htmlString;
            ViewBag.therapist = dropDownService.bindTherapistDropDown(therapistid.ToString(), 0);
            base.ViewBag.startDate = (string.IsNullOrEmpty(startDate) ? DateTime.Now.ToString("dd-MMM-y") : startDate);
            base.ViewBag.endDate = (string.IsNullOrEmpty(endDate) ? DateTime.Now.ToString("dd-MMM-y") : endDate);
            base.ViewBag.dtchange = (string.IsNullOrEmpty(startDate) ? 0 : 1);
            return base.View(adminServeyModel);

        }

        [HttpPost]
        public int getclients(string searchBy = "", string searchItem = "", string countryCode = "", int pageindex = 1, int pagesize = 10)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return 1;
            }
            int item = 1;
            AdminServices adminService = new AdminServices();
            Profile profile = new Profile();
            if (profile._profiles.Count<profile_item>() > 0)
            {
                item = profile._profiles[0].counter;
            }
            return item;
        }
        [AuthorizeVerifiedAdmin]
        public ActionResult GetClientSummarybySearch()
        {
            if (!this.haspermission(26))
            {
                return base.View("acess");
            }
            DropDownServices dropDownService = new DropDownServices();
            ViewBag.client = dropDownService.bindClientDropDown("0", 0);
            ViewBag.therapist = dropDownService.bindTherapistDropDown("0", 0);
            ViewBag.exportclient = this.haspermission(27);
            ViewBag.print = this.haspermission(28);
            return base.View();
        }

        [HttpPost]
        public ActionResult GetClientViewSummarybySearch(string token, string startDate = "", string enddate = "", int print = 0, int therapist = 0, string type = "", string serachValue = "")
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            ChatServices chatService = new ChatServices();
            SummaryModel summaryModel = new SummaryModel();
            int num = Convert.ToInt32(token);
            summaryModel._list = chatService.GetViewSummaryForAdmin1(therapist, startDate, enddate, 0, 0, 0, num, type, serachValue);
            ViewBag.start = startDate;
            ViewBag.end = enddate;
            return this.PartialView("SearchClientViewSummryReport", summaryModel);
        }

        public ActionResult GetClientViewSummaryForExport(string token, string startDate = "", string enddate = "", int print = 0)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            ChatServices chatService = new ChatServices();
            SummaryModel summaryModel = new SummaryModel();
            int num = Convert.ToInt32(token);
            DataTable dataTable = new DataTable();
            summaryModel._list = chatService.GetViewSummaryForAdmin1(0, startDate, enddate, 0, 0, 0, num);
            if (print != 1)
            {
                return this.PartialView("SearchClientViewSummryReport", summaryModel);
            }
            DataTable dataTable1 = this.FormatclientDtForExcel(summaryModel);
            return this.GetExcelByDt(dataTable1, "Summary Report", string.Concat(new string[] { "Report for (", startDate, " to ", enddate, ")" }));
        }

        public string getdate()
        {
            return DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").ToString();
        }

        public object GetDynamicSortProperty(object item, string propName)
        {
            return item.GetType().GetProperty(propName).GetValue(item, null);
        }

        public ActionResult GetEvents(int hid, string start = null, string end = null, int byClient = 0, int cnt = 0)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            DateTime dateTime;
            HotelServices hotelService = new HotelServices();
            DateTime dateTime1 = (!string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time"));
            if (string.IsNullOrEmpty(end))
            {
                DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                dateTime = currentTimeByTimeZone.AddDays(30);
            }
            else
            {
                dateTime = Convert.ToDateTime(end);
            }
            Events[] array = hotelService.GethotelEvents(hid, dateTime1, dateTime).ToArray();
            return base.Json(array, 0);
        }

        [HttpPost]
        public string GetEventsByDate(int thid, string start = null, string end = null, string date = null)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            DateTime dateTime = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", "10:00 AM"));
            if (thid != 4 && thid != 12)
            {
                DateTime datetime = Convert.ToDateTime(date);
                return (new HotelServices()).GethotelAvailbilityScheduleIdbydate2(thid, datetime, false);
            }
            return string.Concat("Available <br/>", (currentTimeByTimeZone < dateTime ? "10:00 AM" : currentTimeByTimeZone.AddHours(1).AddMinutes(4).ToString("hh:mm tt")), "- 12:00 PM");
        }

        [HttpPost]
        public string GetEventTimeByDate(int thid, DateTime datetime)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            DateTime dateTime = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", "10:00 AM"));
            if (thid != 4 && thid != 12)
            {
                return (new HotelServices()).GethotelAvailbilityScheduleIdbydate2(thid, datetime, false);
            }
            return string.Concat("Available <br/>", (currentTimeByTimeZone < dateTime ? "10:00 AM" : currentTimeByTimeZone.AddHours(1).AddMinutes(4).ToString("hh:mm tt")), "- 12:00 PM");
        }



        private FileContentResult GetExcel(DataTable dt, string FileName)
        {
            double num = 0;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    num += Convert.ToDouble(dt.Rows[i]["Amount"].ToString());
                }
            }
            DataRow dataRow = dt.NewRow();
            dataRow["CLientName"] = "Total";
            dataRow["Amount"] = num;
            dt.Rows.Add(dataRow);
            dt.AcceptChanges();
            ExcelExportHelper excelExportHelper = new ExcelExportHelper();
            byte[] numArray = excelExportHelper.ExportExcel(dt, "Report", true, 4, new string[] { "name", "Bookingdate", "CLientName", "Amount" });
            return this.File(numArray, excelExportHelper.ExcelContentType, "Report.xlsx");
        }

        private FileContentResult GetExcel<T1>(List<T1> dt, string FileName)
        {
            ExcelExportHelper excelExportHelper = new ExcelExportHelper();
            DataTable dataTable = excelExportHelper.ListToDataTable<T1>(dt);
            double num = 0;
            if (dataTable.Rows.Count > 0)
            {
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    num += Convert.ToDouble(dataTable.Rows[i]["Amount"].ToString());
                }
            }
            DataRow dataRow = dataTable.NewRow();
            dataRow["totalamountPerClient"] = "Total";
            dataRow["totalAmountpertherapist"] = num;
            dataTable.Rows.Add(dataRow);
            dataTable.AcceptChanges();
            byte[] numArray = excelExportHelper.ExportExcel(dataTable, "Report", true, 8, new string[] { "Name", "bookingDate", "ClientName", "totalMassage", "totalamountPerClient", "totalAmountpertherapist", "rate", "DiscountAmount", "Amount" });
            return this.File(numArray, excelExportHelper.ExcelContentType, "Report.xlsx");
        }

        private FileContentResult GetExcel1<T1>(List<T1> dt, string FileName, string heading)
        {
            ExcelExportHelper excelExportHelper = new ExcelExportHelper();
            DataTable dataTable = excelExportHelper.ListToDataTable<T1>(dt);
            double num = 0;
            double num1 = 0;
            if (dataTable.Rows.Count > 0)
            {
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    num += Convert.ToDouble(dataTable.Rows[i]["TotalAmount"].ToString());
                    num1 += Convert.ToDouble(dataTable.Rows[i]["DiscountAmount"].ToString());
                }
            }
            DataRow dataRow = dataTable.NewRow();
            dataRow["TotalAmount"] = string.Concat("Sub-Total :", num, " Rs");
            dataTable.Rows.Add(dataRow);
            DataRow dataRow1 = dataTable.NewRow();
            dataRow1["TotalAmount"] = string.Concat("Discount :", num1, " Rs");
            dataTable.Rows.Add(dataRow1);
            DataRow dataRow2 = dataTable.NewRow();
            dataRow2["TotalAmount"] = string.Concat("Total Amount:", num - num1, " Rs");
            dataTable.Rows.Add(dataRow2);
            dataTable.AcceptChanges();
            byte[] numArray = excelExportHelper.ExportExcel(dataTable, heading, true, 8, new string[] { "Name", "ClientName", "TotalAmount", "totalMassage", "DiscountAmount", "Amount" });
            return this.File(numArray, excelExportHelper.ExcelContentType, "Report.xlsx");
        }

        private FileContentResult GetExcelByDt(DataTable _dt, string FileName, string heading)
        {
            ExcelExportHelper excelExportHelper = new ExcelExportHelper();
            byte[] numArray = excelExportHelper.ExportExcel(_dt, heading, true, 9, new string[] { "ClientName", "TherapistName", "bookingdate", "confirmTime", "Amount", "Discount", "TotalAmount" });
            return this.File(numArray, excelExportHelper.ExcelContentType, "Report.xlsx");
        }

        [HttpPost]
        public string GetHotelMapById(int id)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return "";
            }
            return (new HtmlString((new HotelServices()).GetHotelsById(id).Map)).ToString();
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult Getrequest(string token, string name = "C")
        {
            if (!this.haspermission(22, 25))
            {
                return base.View("acess");
            }
            Crypto crypto = new Crypto();
            bookingServices bookingService = new bookingServices();
            int num = Convert.ToInt32(crypto.DecryptStringAES(token));
            if (name == "C")
            {
                ViewBag.Name = "Confirmed Appointment";
            }
            else if (name == "PA")
            {
                ViewBag.Name = "Pending Appointment Approval";
            }
            else
            {
                ViewBag.Name = "Pending Appointment";
            }
            return base.View(bookingService.GetAppointMentDetailById(num, false));
        }

        [HttpPost]
        public string getsubsummary(int clientId, int therapistId, string startDate = "", string enddate = "")
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return "";
            }
            ChatServices chatService = new ChatServices();
            DataTable dataTable = new DataTable();
            return chatService.GettherapistAppointmentSummary(clientId, therapistId, startDate, enddate, ref dataTable);
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult GetSummarybySearch()
        {
            if (!this.haspermission(32))
            {
                return base.View("acess");
            }
            DropDownServices dropDownService = new DropDownServices();
            ViewBag.therapist = dropDownService.bindTherapistDropDown("0", 0);
            ViewBag.client = dropDownService.bindClientDropDown("0", 0);
            ViewBag.export = this.haspermission(33);
            ViewBag.print = this.haspermission(34);
            return base.View();
        }

        [HttpPost]
        public ActionResult GetSummarybySearch(int? page, string startDate = "", string enddate = "")
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return View();
            }
            utility _utility = new utility();
            int num = 1;
            page = page ?? 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            ChatServices chatService = new ChatServices();
            SummaryModel summaryModel = new SummaryModel();
            int total = 0;
            summaryModel._Summarylist = chatService.GetAllSummaryForAdmin(startDate, enddate, num, num1);
            if (summaryModel._Summarylist.Count<SummaryListModel>() > 0)
            {
                total = summaryModel._Summarylist[0].Total;
            }
            HtmlString htmlString = new HtmlString(_utility.create_paging_ajax(total, one, "GetMainSummryReport"));
            ViewBag.paging = htmlString;
            return this.PartialView("SearchSummryReport", summaryModel);
        }




        [AuthorizeVerifiedAdmin]
        [HttpPost]
        public ActionResult GetTherapistChat(string searchToken = "")
        {
            ChatServices chatService = new ChatServices();
            int num = 1;
            return this.PartialView("SearchTherapistChats", new AdminChat()
            {
                _TherapistChatlist = chatService.GetAllChatByAdminIdForTherspist(num, searchToken),
                TherapistsearchToken = searchToken
            });
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult GetTherapistFeedBackByClient(int? page, int therapistid = 0, int clientid = 0, string startDate = "", string endDate = "", string deleteId = "")
        {
            page = page ?? 1;
            if (!haspermission(29))
            {
                return View("acess");
            }
            ChatServices chatService = new ChatServices();
            utility _utility = new utility();
            int num = 1;
            string strq = "";
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (!string.IsNullOrEmpty(deleteId))
            {
                string[] strArrays = deleteId.Split(new char[] { '\u005F' });
                for (int i = 0; i < (int)strArrays.Length; i++)
                {
                    strq = strArrays[i];
                    chatService.deletetherapistsurvey(Convert.ToInt16(strq));
                }
            }
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            ViewBag.page = page ?? 1;
            AdminServeyModel adminServeyModel = new AdminServeyModel();
            DropDownServices dropDownService = new DropDownServices();
            int total = 0;
            adminServeyModel._Summarylist = chatService.GetAllSurveyForTherapist(num, num1, startDate, endDate, therapistid, clientid);
            if (adminServeyModel._Summarylist.Count<SurveyViewModel>() > 0)
            {
                total = adminServeyModel._Summarylist[0].Total;
            }
            if (base.TempData["msg"] != null)
            {
                adminServeyModel.ErrorText = base.TempData["msg"].ToString();
            }
            string str = string.Concat(new object[] { "1&startDate=", startDate, "&endDate=", endDate, "&therapistid=", therapistid, "&clientid=", clientid });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "Admin", "GetTherapistFeedBackByClient", str, new decimal(10)));
            ViewBag.paging = htmlString;
            ViewBag.therapist = dropDownService.bindTherapistDropDown(therapistid.ToString(), 0);
            ViewBag.client = dropDownService.bindClientDropDown(clientid.ToString());
            base.ViewBag.startDate = (string.IsNullOrEmpty(startDate) ? DateTime.Now.ToString("dd-MMM-y") : startDate);
            base.ViewBag.endDate = (string.IsNullOrEmpty(endDate) ? DateTime.Now.ToString("dd-MMM-y") : endDate);
            base.ViewBag.dtchange = (string.IsNullOrEmpty(startDate) ? 0 : 1);
            return base.View(adminServeyModel);
        }



        [HttpPost]
        public string GetuserLocationMapById(int id)
        {
            return (new AccountServices()).getUserLocationById(id);
        }

        [HttpPost]
        public ActionResult GetViewSummarybySearch(string token, int clientid = 0, string startDate = "", string enddate = "", int print = 0, string level1 = "", string level2 = "")
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            ChatServices chatService = new ChatServices();
            SummaryModel summaryModel = new SummaryModel();
            int num = Convert.ToInt32(token);
            summaryModel._list = chatService.GetViewSummaryForAdmin1(num, startDate, enddate, 0, 0, 0, clientid);
            ViewBag.start = startDate;
            ViewBag.end = enddate;
            ViewBag.level1 = level1;
            ViewBag.level2 = level2;
            return this.PartialView("SearchViewSummryReport", summaryModel);
        }

        public ActionResult GetViewSummaryForExport(string token, string startDate = "", string enddate = "", int print = 0, string level1 = "", string level2 = "")
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            ChatServices chatService = new ChatServices();
            SummaryModel summaryModel = new SummaryModel();
            int num = Convert.ToInt32(token);
            DataTable dataTable = new DataTable();
            summaryModel._list = chatService.GetViewSummaryForAdmin(num, startDate, enddate, 0, 0, 1);
            if (print != 1)
            {
                return this.PartialView("SearchViewSummryReport", summaryModel);
            }
            return this.GetExcel<SummaryViewModel>(summaryModel._list, "Summary Report");
        }

        public ActionResult GetViewSummaryForExport1(string token, string startDate = "", string enddate = "", int print = 0, string level1 = "", string level2 = "")
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            ChatServices chatService = new ChatServices();
            SummaryModel summaryModel = new SummaryModel();
            int num = Convert.ToInt32(token);
            DataTable dataTable = new DataTable();
            summaryModel._list = chatService.GetViewSummaryForAdmin1(num, startDate, enddate, 0, 0, 1, 0);
            if (print != 1)
            {
                return this.PartialView("SearchViewSummryReport", summaryModel);
            }
            DataTable dataTable1 = this.FormattherapistDtForExcel(summaryModel, level1, level2);
            return this.GetExcelByDt(dataTable1, "Summary Report", string.Concat(new string[] { "Report for (", startDate, " to ", enddate, ")" }));
        }

        public ActionResult GetViewSummaryForExport2(int cid, int tid, string startDate = "", string enddate = "", int print = 0)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            ChatServices chatService = new ChatServices();
            DataTable dataTable = new DataTable();
            chatService.GettherapistAppointmentSummary(cid, tid, startDate, enddate, ref dataTable);
            return this.GetExcelByDt(dataTable, "Summary Report", string.Concat(new string[] { "Report for (", startDate, " to ", enddate, ")" }));
        }

        public bool haspermission(int id)
        {
            int num;
            num = (Session["ADId"] == null ? 0 : Convert.ToInt32(Session["ADId"]));
            return (new userService()).getPermissionByuserIdd(num).Contains(id);
        }

        public bool haspermission(int id, int id1)
        {
            int num;
            num = (Session["ADId"] == null ? 0 : Convert.ToInt32(Session["ADId"]));
            List<int> permissionByuserIdd = (new userService()).getPermissionByuserIdd(num);
            if (!permissionByuserIdd.Contains(id))
            {
                return false;
            }
            return permissionByuserIdd.Contains(id1);
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult Pending_Appointments()
        {
            if (!this.haspermission(22))
            {
                return base.View("acess");
            }
            return base.View(new AppointmentModel()
            {
                appt = (new therapistService()).getAllPendingAppointment()
            });
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult reject_request(string token)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            int num = Convert.ToInt32((new Crypto()).DecryptStringAES(token));
            Crypto crypto = new Crypto();
            bookingServices bookingService = new bookingServices();
            ViewBag.Name = "Reject Appointment";
            AppointmentModel_item appointmentModelItem = new AppointmentModel_item();
            return base.View("reject_request", bookingService.GetAppointMentDetailById(num, false));
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult retrunAppointement(int? page, string token = "")
        {
            AccountServices accountService = new AccountServices();
            string message = "";
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            if (!string.IsNullOrEmpty(token))
            {
                int aptid = Convert.ToInt16((new Crypto()).DecryptStringAES(token));
                message = accountService.Deleterequest(aptid);
            }
            AppointmentModel _model = new Models.Appointment.AppointmentModel();
            _model.appt = accountService.GetAllAdminRejectedAppointment(num, num1, ref total);
            _model.ErrorText = message;
            string str = string.Concat(new object[] { "1" });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "admin", "retrunAppointement", str, new decimal(10)));
            ViewBag.paging = htmlString;
            return View("GetRejected_Appointement", _model);
        }



        [HttpPost]
        public string SaveClientNewUserName(string newName, int id)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return "";
            }
            return (new therapistService()).UpdateClientNameById(id, newName);
        }

        private List<T> SortList<T>(List<T> collection, string order, string propertyName)
        {
            if (order != "asc")
            {
                (from n in collection
                 orderby GetDynamicSortProperty(n, propertyName) ascending
                 select n).ToList<T>();
            }
            return (
                from n in collection
                orderby GetDynamicSortProperty(n, propertyName) descending
                select n).ToList<T>();
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult therapist_profiles()
        {
            if (!this.haspermission(2))
            {
                return base.View("acess");
            }
            therapistService _therapistService = new therapistService();
            _therapist allTherapist = _therapistService.GetAllTherapist(true);
            allTherapist.images = _therapistService.GetAllTherapistImages();
            if (TempData["msg"] != null)
            {
                allTherapist.ErrorText = TempData["msg"].ToString();
            }
            allTherapist.permission = this.haspermission(5);
            return base.View(allTherapist);
        }

        [HttpPost]
        public ActionResult UpdateClientSurvey(ClientSurveymodel _model)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            bookingServices bookingService = new bookingServices();
            Crypto crypto = new Crypto();
            int num = 0;
            if (!string.IsNullOrEmpty(_model.token))
            {
                ClientSurveymodel clientSurveymodel = _model;
                clientSurveymodel.token = clientSurveymodel.token.Replace(" ", "+");
                num = Convert.ToInt32(crypto.DecryptStringAES(_model.token));
            }
            bookingService.UpdateClientSurvey(_model, num);
            TempData["msg"] = "Survey Feedback Updated suceesfully";
            return base.RedirectToAction("GetClientFeedBackByTherapist");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Updatehotel(spaHotelItem _model, HttpPostedFileBase file)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            HotelServices hotelService = new HotelServices();
            Crypto crypto = new Crypto();
            int? nullable = null;
            if (!string.IsNullOrEmpty(_model.token))
            {
                _model.token = _model.token.Replace(" ", "+");
                nullable = new int?(Convert.ToInt32(crypto.DecryptStringAES(_model.token)));
            }
            if (file != null)
            {
                string str = DateTime.Now.Ticks.ToString();
                string str1 = Path.Combine(Server.MapPath("~/hotelImage"), string.Concat(str, file.FileName));
                file.SaveAs(str1);
                _model.ImgPath = string.Concat("~/hotelImage/", str, file.FileName);
            }
            string str2 = hotelService.AddUpdateHotel(_model, nullable);
            _model.ErrorText = str2.Split(new char[] { '_' })[0];
            int hotelid = Convert.ToInt32(str2.Split(new char[] { '_' })[1]);
            return RedirectToAction("add", new { _xid = hotelid });
        }

        [HttpPost]
        public ActionResult UpdateHotelImage(string token, HttpPostedFileBase[] files)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            AdminServices adminService = new AdminServices();
            Crypto crypto = new Crypto();
            int num = 0;
            if (!string.IsNullOrEmpty(token))
            {
                token = token.Replace(" ", "+");
                num = Convert.ToInt32(crypto.DecryptStringAES(token));
            }
            string str = "";
            string str1 = "";
            string str2 = "";
            if (files.Count<HttpPostedFileBase>() > 0)
            {
                HttpPostedFileBase[] httpPostedFileBaseArray = files;
                for (int i = 0; i < (int)httpPostedFileBaseArray.Length; i++)
                {
                    HttpPostedFileBase httpPostedFileBase = httpPostedFileBaseArray[i];
                    long ticks = DateTime.Now.Ticks;
                    str2 = string.Concat(ticks.ToString(), httpPostedFileBase.FileName);
                    string str3 = Path.Combine(Server.MapPath("~/hotelImage"), str2);
                    httpPostedFileBase.SaveAs(str3);
                    str = string.Concat("~/hotelImage/", str2);
                    str1 = adminService.UpdateHotelImages(str, num);
                }
            }
            TempData["msg"] = str1;
            return base.RedirectToAction("add", new { _xid = num });
        }

        [HttpPost]
        public ActionResult UpdateImage(string token, HttpPostedFileBase[] files)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            AdminServices adminService = new AdminServices();
            Crypto crypto = new Crypto();
            int num = 0;
            if (!string.IsNullOrEmpty(token))
            {
                token = token.Replace(" ", "+");
                num = Convert.ToInt32(crypto.DecryptStringAES(token));
            }
            string str = "";
            string str1 = "";
            string str2 = "";
            int num1 = (new Random(1000)).Next(800);
            string str3 = "";
            if (files.Count<HttpPostedFileBase>() > 0)
            {
                HttpPostedFileBase[] httpPostedFileBaseArray = files;
                for (int i = 0; i < (int)httpPostedFileBaseArray.Length; i++)
                {
                    HttpPostedFileBase httpPostedFileBase = httpPostedFileBaseArray[i];
                    int num2 = num1 + 1;
                    str3 = string.Concat(num2.ToString(), httpPostedFileBase.FileName);
                    str2 = Path.Combine(Server.MapPath("~/profileImages"), str3);
                    httpPostedFileBase.SaveAs(str2);
                    str = string.Concat("~/profileImages/", str3);
                    str1 = adminService.UpdateImages(str, num);
                }
            }
            TempData["msg"] = str1;
            return base.RedirectToAction("Etherapist_profiles", new { token = token });
        }

        [HttpPost]
        public ActionResult UpdateProfile(therapistprofile _model, HttpPostedFileBase file)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            AdminServices adminService = new AdminServices();
            therapistService _therapistService = new therapistService();
            Crypto crypto = new Crypto();
            int num = 0;
            if (!string.IsNullOrEmpty(_model.token))
            {
                therapistprofile _therapistprofile = _model;
                _therapistprofile.token = _therapistprofile.token.Replace(" ", "+");
                num = Convert.ToInt32(crypto.DecryptStringAES(_model.token));
            }
            if (file != null)
            {
                int num1 = (new Random(1000)).Next(800) + 29;
                string str = string.Concat(num1.ToString(), file.FileName);
                string str1 = Path.Combine(Server.MapPath("~/profileImages"), str);
                file.SaveAs(str1);
                _model.imagePath = string.Concat("~/profileImages/", str);
            }
            _model.ErrorText = adminService.UpdateOrCreate(_model, num);
            _model.imagePath = _therapistService.GetAllTherapistById(num, true).imagePath;
            _model.images = (
                from a in _therapistService.GetAllTherapistImages()
                where a.thId == num
                select a).ToList<imagesItem>();
            if (num == 0)
            {
                TempData["msg"] = _model.ErrorText;
                return base.RedirectToAction("therapist_profiles");
            }
            TempData["msg"] = _model.ErrorText;
            return base.View("Etherapist_profiles", _model);
        }

        [HttpPost]
        public string UpdateSchedule(DateTime date, string start, string end, int tid)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return "";
            }
            therapistService _therapistService = new therapistService();
            string str = string.Concat(new object[] { date.Month, "/", date.Day, "/", date.Year });
            DateTime dateTime = Convert.ToDateTime(string.Concat(str, " ", start));
            if (Convert.ToDateTime(string.Concat(str, " ", end)) < dateTime)
            {
                return "Error : Last Appointment start time can not grater then first Appointment start time ";
            }
            return _therapistService.UpdateSchedule(date, start, end, tid);
        }

        [HttpPost]
        public ActionResult UpdateSurvey(Surveymodel _model)
        {
            if (!new cookiechecker().isadmincookieexist())
            {
                return Json("Session out ,Please login agin", JsonRequestBehavior.AllowGet);
            }
            bookingServices bookingService = new bookingServices();
            Crypto crypto = new Crypto();
            int num = 0;
            if (!string.IsNullOrEmpty(_model.token))
            {
                Surveymodel surveymodel = _model;
                surveymodel.token = surveymodel.token.Replace(" ", "+");
                num = Convert.ToInt32(crypto.DecryptStringAES(_model.token));
            }
            bookingService.UpdateSurvey(_model, num);
            TempData["msg"] = "Survey Feedback Updated suceesfully";
            return base.RedirectToAction("GetTherapistFeedBackByClient");
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult user()
        {
            if (!this.haspermission(11))
            {
                return base.View("acess");
            }
            adminlist _adminlist = new adminlist()
            {
                adminprofiles = (new AdminServices()).getalladmin()
            };
            if (TempData["msg"] != null)
            {
                _adminlist.ErrorText = TempData["msg"].ToString();
            }
            return base.View(_adminlist);
        }

        public class TableDate<T>
        {
            public List<T> data
            {
                get;
                set;
            }

            public int draw
            {
                get;
                set;
            }

            public int recordsFiltered
            {
                get;
                set;
            }

            public int recordsTotal
            {
                get;
                set;
            }

            public TableDate()
            {
            }
        }
    }
}