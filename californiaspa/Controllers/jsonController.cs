﻿using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace californiaspa.Controllers
{
    public class jsonController : Controller
    {
       public jsonController()
        {//9148555500
            new whatsapphelper().sendSMS("+9177760555500","this is test from site with APIWHA API");
        }

        public string DataTableToJSON(DataTable table)
        {
            List<Dictionary<string, object>> dictionaries = new List<Dictionary<string, object>>();
            foreach (DataRow row in table.Rows)
            {
                Dictionary<string, object> strs = new Dictionary<string, object>();
                foreach (DataColumn column in table.Columns)
                {
                    strs[column.ColumnName] = Convert.ToString(row[column]);
                }
                dictionaries.Add(strs);
            }
            return (new JavaScriptSerializer()).Serialize(dictionaries);
        }

        public string Index()
        {
            DataTable dataTable = new DataTable();
            dataTable = (new SqlHelper()).fillDataTable("GetSmsForSent", "", null);
            return this.DataTableToJSON(dataTable);
        }

        public void update(int num, string empty)
        {
            SortedList sortedLists = new SortedList();
            SqlHelper sqlHelper = new SqlHelper();
            sortedLists.Add("@Id", num);
            sortedLists.Add("@Status", empty);
            sqlHelper.executeNonQuery("UpdateSmsStatus", "", sortedLists);
        }
    }
}