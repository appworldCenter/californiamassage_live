using californiaspa.DataBase;
using californiaspa.Models;
using californiaspa.Models.Account;
using californiaspa.Models.Appointment;
using californiaspa.utilis;
using System;
using System.Web;
using System.Web.Mvc;

namespace californiaspa.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult AdminForgetPassword()
        {
            return base.View(new loginmodel());
        }

        [HttpPost]
        public ActionResult AdminForgetPassword(loginmodel _model)
        {
            utility _utility = new utility();
            AdminProfile adminProfile = (new AdminServices()).readAdminxml(_model.username);
            if (_model.username.ToLower() != adminProfile.EmailAddress.ToLower())
            {
                _model.ErrorText = "Username can't find ";
                return base.View(_model);
            }
            string str = string.Concat("Your Secure password is ", adminProfile.Password);
            _utility.Sendemail(adminProfile.EmailAddress, "admin@californiaspa.in", "Admin password Recovery", str);
            _model.ErrorText = "Password has been sent to your mobile number";
            return base.View("TForgetPasswordalert", _model);
        }

        public ActionResult Adminlogin(string ReturnUrl = "xyz")
        {
            base.Session.Abandon();
            if (!base.Request.Url.AbsoluteUri.Contains("https") && !base.Request.Url.AbsoluteUri.Contains("localhost"))
            {
                return this.Redirect(string.Concat("https://www.californiamassage.in/account/Adminlogin?ReturnUrl=", ReturnUrl));
            }
            return base.View(new loginmodel()
            {
                returnUrl = ReturnUrl
            });
        }

        [HttpPost]
        public ActionResult Adminlogin(loginmodel _model)
        {
         // AdminProfile adminProfile = (new AdminServices()).readAdminxml(_model.username);
            int id = 0;
            if (_model.username == "neiladvani.consult@gmail.com")
            {
                id = 2;
            }
            else if(_model.username == "admin@admin.com")
            {
                id = 9;
            }
            Session["ADId"] = id;
            Session.Timeout = 1440;
            new utility().createCookie(id.ToString(), utility.usertype.admin);
            if (_model.returnUrl != "xyz")
            {
                return this.Redirect(_model.returnUrl);
            }
            return base.RedirectToAction("dashboard", "Admin");
        }

        public ActionResult Adminlogout()
        {
            Session.Abandon();
            new utility().createCookie("123", utility.usertype.admin,-5);
            return base.RedirectToAction("Adminlogin", "Account");
        }

        [AuthorizeVerifieduser]
        public ActionResult appointment_request()
        {
            bookingServices bookingService = new bookingServices();
            int num = Convert.ToInt32(base.Session["cluserId"].ToString());
            return base.View(bookingService.GetAllAppointMentDetailByClientId(num));
        }

        [AuthorizeVerifieduser]
        public ActionResult chat(int token = 0)
        {
            int num = Convert.ToInt32(base.Session["cluserId"].ToString());
            ChatServices chatService = new ChatServices();
            return base.View(new AccountModel()
            {
                clientId = num,
                chatList = chatService.GetAllChatByClientId(num, ""),
                AdminchatList = chatService.GetAdminChatByClientId(num, false),
                chatDivId = token
            });
        }

        [AuthorizeVerifieduser]
        public ActionResult confirmed_appt(int? page)
        {
            int userid = Convert.ToInt32(base.Session["cluserId"].ToString());
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            AccountServices accountService = new AccountServices();
            AccountModel accountModel = new AccountModel();
            if (base.TempData["user"] != null)
            {
                accountModel.ErrorText = base.TempData["user"].ToString();
            }
            accountModel = accountService.GetCustomerconfirmappt(userid, num, num1, ref total);
            string str = string.Concat(new object[] { "1" });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "account", "confirmed_appt", str, new decimal(10)));
            ViewBag.paging = htmlString;
            return base.View(accountModel);
        }

        [AuthorizeVerifieduser]
        public ActionResult editpassword()
        {
            AccountServices accountService = new AccountServices();
            int num = Convert.ToInt32(base.Session["cluserId"].ToString());
            return base.View(new PasswordModel()
            {
                OldPassword = accountService.GetPassword(num)
            });
        }

        [HttpPost]
        public ActionResult editpassword(PasswordModel _model1)
        {
            AccountServices accountService = new AccountServices();
            int num = Convert.ToInt32(base.Session["cluserId"].ToString());
            string str = accountService.updatePassword(_model1.OldPassword, _model1.NewPassword, num, _model1.confirmPassword);
            _model1.ErrorText = str;
            if (str.Contains("Error"))
            {
                return base.View(_model1);
            }
            base.TempData["user"] = str;
            return base.RedirectToAction("Myaccount");
        }

        [HttpPost]
        public ActionResult edituser(EditAccount _model1)
        {
            //PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

            //var tel = phoneUtil.parse("+" + _model1.CountryCode + _model1.newUserName, "");

            //if (!phoneUtil.isValidNumber(tel))
            //{
            //    _model1.ErrorText = "Plese enter valid mobile number";
            //    return base.View(_model1);
            //}
            AccountServices accountService = new AccountServices();
            int num = Convert.ToInt32(base.Session["cluserId"].ToString());
            string str = accountService.verifyEmail(_model1.newUserName, _model1.newEmailId, num);
            if (str.Contains("Error"))
            {
                _model1.ErrorText = str;
                return base.View(_model1);
            }
            AccountModel customer = accountService.GetCustomeraccount(num, false);
            utility _utility = new utility();
            Crypto crypto = new Crypto();
            string randomalphanumeric = _utility.GetRandomalphanumeric(new int?(6), "0123456789", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
            string randomalphanumeric1 = _utility.GetRandomalphanumeric(new int?(8), "9876542130", "ABCDEFGHIabcdefghijklJKLMNOPQRSTUVWXYZmnopqrstuvwxyz");
            customer.userName = _model1.newUserName;
            customer.emailAddress = _model1.newEmailId;
            smshelper _smshelper = new smshelper();
            //  _smshelper.SendSMSWith91(_model1.newUserName, "CAL SPA : CLIENT <br/>OTP :" + randomalphanumeric1);

            customer.userName = _model1.CountryCode + _model1.newUserName;
            _smshelper.SendSms("+" + _model1.CountryCode + _model1.newUserName, "CAL SPA : CLIENT <br/> SMS OTP :" + randomalphanumeric1, "+14177089506", "CALSPA: SMS OTP : " + randomalphanumeric1);

           // _smshelper.SendSms(_model1.newUserName, "CAL SPA : CLIENT <br/> SMS OTP :" + randomalphanumeric1, "+14177089506", "CALSPA: SMS OTP : " + randomalphanumeric1);
            _utility.Sendemail(_model1.newEmailId, "admin@californiaspa.in", "verification code", string.Concat("your EMAIL OTP is ", randomalphanumeric));
            customer._exid = crypto.EncryptStringAES(randomalphanumeric);
            customer._mxid = crypto.EncryptStringAES(randomalphanumeric1);
            return base.View("editusername", customer);
        }

        [AuthorizeVerifieduser]
        public ActionResult editUser()
        {
            AccountServices accountService = new AccountServices();
            int num = Convert.ToInt32(base.Session["cluserId"].ToString());
            AccountModel customer = accountService.GetCustomeraccount(num, false);
            return base.View(new EditAccount()
            {
                OldUserName = customer.userName,
                OldEmailId = customer.emailAddress
            });
        }

        [HttpPost]
        public ActionResult editusername(AccountModel _model)
        {
            int num = Convert.ToInt32(base.Session["cluserId"].ToString());
            _model.Id = num;
            AccountServices accountService = new AccountServices();
            Crypto crypto = new Crypto();
            AccountModel accountModel = _model;
            accountModel._exid = accountModel._exid.Replace(" ", "+");
            AccountModel accountModel1 = _model;
            accountModel1._mxid = accountModel1._mxid.Replace(" ", "+");
            if (crypto.DecryptStringAES(_model._exid) != _model.emailVerification)
            {
                _model.ErrorText = "Email verification code doesn't match, please check Inbox";
                return base.View(_model);
            }
            if (crypto.DecryptStringAES(_model._mxid) != _model.mobileVerification)
            {
                _model.ErrorText = "Mobile code doesn't match, please check mobile inbox";
                return base.View(_model);
            }
            string str = accountService.UpdateUserName(_model);
            if (str.Contains("Error"))
            {
                _model.ErrorText = str;
                return base.View(_model);
            }
            base.TempData["user"] = "Your Account Info updated Succesfully";
            return base.RedirectToAction("MyAccount");
        }

        public ActionResult ForgetPassword()
        {
            return base.View(new loginmodel());
        }

        [HttpPost]
        public ActionResult ForgetPassword(loginmodel _model)
        {
            utility _utility = new utility();
            string passwordByUserName = (new AccountServices()).getPasswordByUserName(_model);
            if (passwordByUserName.Contains("Error"))
            {
                _model.ErrorText = passwordByUserName;
                return base.View(_model);
            }
            string[] strArrays = passwordByUserName.Split(new char[] { '\u005F' });
            string str = string.Concat("CAL SPA : CLIENT<br/>Your password is ", strArrays[1]);
            smshelper _smshelper = new smshelper();
           // _smshelper.SendSMSWith91(strArrays[0], str);
           _smshelper.SendSms(strArrays[0], str, "+14177089506", "CALSPA : Your password is "+ strArrays[1]);
            _model.ErrorText = "Password has been sent to your mobile number";
            return base.View("ForgetPasswordalert", _model);
        }

        [AuthorizeVerifieduser]
        [HttpPost]
        public ActionResult GetChat(string searchToken = "")
        {
            int num = Convert.ToInt32(base.Session["cluserId"].ToString());
            ChatServices chatService = new ChatServices();
            return this.PartialView("SearchClientChats", new AccountModel()
            {
                chatList = chatService.GetAllChatByClientId(num, searchToken)
            });
        }

        public ActionResult getMainView()
        {
            return base.View();
        }

        [AuthorizeVerifiedtherapistchrome]
        public ActionResult login(string ReturnUrl = "xyz")
        {
            base.Session.Abandon();
            if (!base.Request.Url.AbsoluteUri.Contains("https") && !base.Request.Url.AbsoluteUri.Contains("localhost"))
            {
                return this.Redirect(string.Concat("https://www.californiamassage.in/account/login?ReturnUrl=", ReturnUrl));
            }
            return base.View(new loginmodel()
            {
                returnUrl = ReturnUrl
            });
        }

        [HttpPost]
        public ActionResult login(loginmodel _model)
        {
            string str = (new AccountServices()).validateLogin(_model);
            _model.ErrorText = str;
            if (str.Contains("Error"))
            {
                return base.View(_model);
            }
            Session["cluserId"] = str;
            Session.Timeout = 1440;
            new utility().createCookie(str, utility.usertype.user);
            if (_model.returnUrl != "xyz")
            {
                return this.Redirect(_model.returnUrl);
            }
            return base.RedirectToAction("dashboard", "main");
        }

        public ActionResult logout()
        {
            base.Session.Abandon();
            new utility().createCookie("123", utility.usertype.user, -5);
            return base.RedirectToAction("Index", "main");
        }

        [AuthorizeVerifieduser]
        public ActionResult my_review(int? page)
        {
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            int userid = Convert.ToInt32(base.Session["cluserId"].ToString());
            AccountServices accountService = new AccountServices();
            AccountModel accountModel = new AccountModel();
            if (base.TempData["user"] != null)
            {
                accountModel.ErrorText = base.TempData["user"].ToString();
            }
            accountModel = accountService.GetCustomerreviews(userid, num, num1, ref total);
            string str = string.Concat(new object[] { "1" });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "account", "my_review", str, new decimal(10)));
            ViewBag.paging = htmlString;
            return base.View(accountModel);
        }

        [AuthorizeVerifieduser]
        public ActionResult MyAccount()
        {
            int num = Convert.ToInt32(base.Session["cluserId"].ToString());
            AccountServices accountService = new AccountServices();
            ChatServices chatService = new ChatServices();
            AccountModel accountModel = new AccountModel();

            if (base.TempData["user"] != null)
            {
                accountModel.ErrorText = base.TempData["user"].ToString();
            }
            accountModel = accountService.GetCustomeraccount(num, false);
            accountModel.chatList = chatService.GetAllChatByClientId(num, "");
            accountModel.AdminchatList = chatService.GetAdminChatByClientId(num, false);
            return base.View(accountModel);
        }

        [AuthorizeVerifieduser]
        public ActionResult pending_appt(int? page)
        {
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            int userid = Convert.ToInt32(base.Session["cluserId"].ToString());
            AccountServices accountService = new AccountServices();
            AccountModel accountModel = new AccountModel();
            if (base.TempData["user"] != null)
            {
                accountModel.ErrorText = base.TempData["user"].ToString();
            }
            accountModel = accountService.GetCustomerpendingappt(userid, num, num1, ref total);
            string str = string.Concat(new object[] { "1" });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "account", "pending_appt", str, new decimal(10)));
            ViewBag.paging = htmlString;
            return base.View(accountModel);
        }

        public ActionResult Register()
        {
            return base.View(new RegisterModel());
        }

        [HttpPost]
        public ActionResult Register(RegisterModel _model)
        {
            //PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

            //var tel = phoneUtil.parse("+" + _model.CountryCode + _model.mobile, "");

            //if (!phoneUtil.isValidNumber(tel))
            //{
            //    _model.ErrorText = "Plese enter valid mobile number";
            //    return base.View("Register", _model);
            //}

            utility _utility = new utility();
            Crypto crypto = new Crypto();
            string randomalphanumeric = "";
            string str = "";
            (new AccountServices()).getOldOTP(_model.mobile, _model.email, ref randomalphanumeric, ref str);
            if (string.IsNullOrEmpty(randomalphanumeric) && string.IsNullOrEmpty(str))
            {
                randomalphanumeric = _utility.GetRandomalphanumeric(new int?(6), "0123456789", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
                str = _utility.GetRandomalphanumeric(new int?(8), "9876542130", "ABCDEFGHIabcdefghijklJKLMNOPQRSTUVWXYZmnopqrstuvwxyz");
            }
            string str1 = (new AccountServices()).AddNewCustomer(_model, randomalphanumeric, str);
            if (str1.Contains("Error"))
            {
                _model.ErrorText = str1;
                return base.View("Register", _model);
            }
            RegisterModel registerModel = _model;
            registerModel.userName = registerModel.mobile;
            _model.ErrorText = str1;
            smshelper _smshelper = new smshelper();
            var body = "sms otp is for " + _model.mobile + " is : " + str + ".  Email otp is " + _model.email + " is : " + randomalphanumeric;
            // _smshelper.addOtpMessage(_model.mobile,);
            // _smshelper.addOtpMessage(_model.mobile, string.Concat("CAL SPA : CLIENT <br/>Your Requested OTP :", str));
       _utility.Sendemail(_model.email, "admin@californiaspa.in", "verification code", string.Concat(new string[] { randomalphanumeric }));
            registerModel.userName = registerModel.CountryCode + registerModel.mobile;
            _smshelper.SendSms("+" + _model.CountryCode + _model.mobile, string.Concat("CAL SPA : CLIENT<br/>Your Requested  SMS OTP :", str), "+14177089506", "CLASPA : Your SMS OTP :" + str);

            // _smshelper.SendSms(_model.mobile, string.Concat("CAL SPA : CLIENT<br/>Your Requested  SMS OTP :", str), "+14177089506", "CLASPA : Your SMS OTP :"+ str);
            _smshelper.addOtpMessage(_model.email, string.Concat("CAL SPA : CLIENT<br/>Your Requested EMAIL OTP :", randomalphanumeric));
_model._exid = crypto.EncryptStringAES(randomalphanumeric);
            _model._mxid = crypto.EncryptStringAES(str);
           // _smshelper.SendSms("9591895700", body);
            return base.View("Verification", _model);
        }

        [AuthorizeVerifieduser]
        public ActionResult retrunAppointement(int? page, string token = "")
        {
            int userid = Convert.ToInt32(base.Session["cluserId"].ToString());
            AccountServices accountService = new AccountServices();
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            string message = "";
            if (!string.IsNullOrEmpty(token))
            {
                int apptid = Convert.ToInt16((new Crypto()).DecryptStringAES(token));
                message = accountService.Deleterequest(apptid);
            }
            AppointmentModel _model = new Models.Appointment.AppointmentModel();
            _model.appt = accountService.GetAllRejectedAppointmentByUserId(userid, num, num1, ref total);
            _model.ErrorText = message;
            string str = string.Concat(new object[] { "1" });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "account", "retrunAppointement", str, new decimal(10)));
            ViewBag.paging = htmlString;
            return View("GetRejected_Appointement", _model);
        }

        //public void testSms()
        //{
        //    (new smshelper()).SendSms("9813151090", "your request please add this is is click here to leasve a blank space with me abnd you https://goo.gl/s8KumY", "+19282373824");
        //}

        public ActionResult TForgetPassword()
        {
            return base.View(new loginmodel());
        }

        [HttpPost]
        public ActionResult TForgetPassword(loginmodel _model)
        {
            utility _utility = new utility();
            string tPasswordByUserName = (new AccountServices()).getTPasswordByUserName(_model);
            if (tPasswordByUserName.Contains("Error"))
            {
                _model.ErrorText = tPasswordByUserName;
                return base.View(_model);
            }
            string[] strArrays = tPasswordByUserName.Split(new char[] { '\u005F' });
            string str = string.Concat("CAL SPA : THERAPIST <br/> Your Secure password is ", strArrays[1]);
            smshelper _smshelper = new smshelper();
           // _smshelper.SendSMSWith91(strArrays[0], str);
            _smshelper.SendSms(strArrays[0], str, "+14177089506", "CLASPA : Your Secure password is "+strArrays[1]);
            _model.ErrorText = "Password has been sent to your mobile number";
            return base.View("TForgetPasswordalert", _model);
        }

        [AuthorizeVerifiedtherapistchrome]
        public ActionResult Tlogin(string ReturnUrl = "xyz")
        {
            base.Session.Abandon();
            if (!base.Request.Url.AbsoluteUri.Contains("https") && !base.Request.Url.AbsoluteUri.Contains("localhost"))
            {
                return this.Redirect(string.Concat("https://www.californiamassage.in/account/Tlogin?ReturnUrl=", ReturnUrl));
            }
            return base.View(new loginmodel()
            {
                returnUrl = ReturnUrl
            });
        }

        [HttpPost]
        public ActionResult Tlogin(loginmodel _model)
        {
            string str = (new AccountServices()).validateTLogin(_model);
            _model.ErrorText = str;
            if (str.Contains("Error"))
            {
                return base.View(_model);
            }
            Session["name"] = _model.username;
            Session["thId"] = str;
            Session.Timeout = 1440;
            new utility().createCookie(str, utility.usertype.therapist);
            if (_model.returnUrl != "xyz")
            {
                return this.Redirect(_model.returnUrl);
            }
            return base.RedirectToAction("dashboard", "Taccount");
        }

        public ActionResult Tlogout()
        {
            base.Session.Abandon();
            new utility().createCookie("123", utility.usertype.therapist, -5);
            return base.RedirectToAction("Tlogin", "Account");
        }

        public ActionResult TRegister()
        {
            return base.View(new TRegisterModel());
        }

        [HttpPost]
        public ActionResult TRegister(TRegisterModel _model)
        {
            utility _utility = new utility();
            Crypto crypto = new Crypto();
            string randomalphanumeric = _utility.GetRandomalphanumeric(new int?(8), "9876542130", "ABCDEFGHIabcdefghijklJKLMNOPQRSTUVWXYZmnopqrstuvwxyz");
            string str = (new AccountServices()).AddNewTherapist(_model, randomalphanumeric);
            if (str.Contains("Error"))
            {
                _model.ErrorText = str;
                return base.View("Register", _model);
            }
            TRegisterModel tRegisterModel = _model;
            tRegisterModel.userName = tRegisterModel.mobile;
            _model.ErrorText = str;
            _utility.sendsms(_model.mobile, randomalphanumeric);
            _model._mxid = crypto.EncryptStringAES(randomalphanumeric);
            return base.View("TVerification", _model);
        }

        [HttpPost]
        public ActionResult TVerification(TRegisterModel _model)
        {
            AccountServices accountService = new AccountServices();
            if ((new Crypto()).DecryptStringAES(_model._mxid) != _model.mobileVerification)
            {
                _model.ErrorText = "Mobile code doesn't match, please check mobile inbox";
                return base.View(_model);
            }
            string str = accountService.UpdateTCustomer(_model);
            if (str.Contains("Error"))
            {
                _model.ErrorText = str;
                return base.View("Verification", _model);
            }
            base.TempData["user"] = str;
            _model.ErrorText = "your verification is complate , contact with admin to start your services";
            return base.View("Tverifyalert", _model);
        }

        [HttpPost]
        public ActionResult Verification(RegisterModel _model)
        {
            AccountServices accountService = new AccountServices();
            Crypto crypto = new Crypto();
            if (_model.password.ToLower() != _model.verifypassword.ToLower())
            {
                _model.ErrorText = "Password and Confirm password doesn't match, please review and enter again";
                return base.View(_model);
            }
            if (crypto.DecryptStringAES(_model._exid) != _model.emailVerification)
            {
                _model.ErrorText = "Email verification code doesn't match, please check Inbox";
                return base.View(_model);
            }
            if (crypto.DecryptStringAES(_model._mxid) != _model.mobileVerification)
            {
                _model.ErrorText = "Mobile code doesn't match, please check mobile inbox";
                return base.View(_model);
            }
            string str = accountService.UpdateCustomer(_model);
            if (str.Contains("Error"))
            {
                _model.ErrorText = str;
                return base.View("Verification", _model);
            }
            base.TempData["user"] = str;
            return base.RedirectToAction("index", "therapist");
        }
    }
}