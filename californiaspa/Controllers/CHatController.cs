using californiaspa.DataBase;
using californiaspa.Models.Account;
using californiaspa.Models.Chat;
using californiaspa.utilis;
using System;
using System.Collections;
using System.Web.Mvc;

namespace californiaspa.Controllers
{
    public class CHatController : Controller
    {
         [HttpPost]
        public string checkstatus(int _xid, string number)
        {
            return (new ChatServices()).Getcallstatus(_xid, number);
        }

        public string checkstatus1(int _xid, string number)
        {
            return (new ChatServices()).Getcallstatus(_xid, number);
        }

        [HttpPost]
        public string GetCall(string clientNumber, string therapistNumber, int _xid, string number)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            (new ChatServices()).forwardCall(string.Concat("+91", therapistNumber), "+19282373824", string.Concat("+91", clientNumber), _xid, number);
            return "Call is intiating , Please wait...";
        }

        public string GetCall(string clientNumber, string therapistNumber)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            return (new ChatServices()).forwardCall(string.Concat("+91", therapistNumber), "+19282373824", string.Concat("+91", clientNumber), 0, "0");
        }

        public JsonResult GetClientTherapistUpdateChat()
        {
            if (!new cookiechecker().iscookieexist())
            {
                return Json("",JsonRequestBehavior.AllowGet);
            }
            return base.Json(new AccountModel()
            {
                chatList = (new ChatServices()).GetAllClientTherapistChat()
            }, 0);
        }

        public JsonResult GetClientUpdateChat()
        {
            if (!new cookiechecker().iscookieexist())
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            return base.Json(new AccountModel()
            {
                chatList = (new ChatServices()).GetClientUpdateChat()
            }, 0);
        }

        [HttpPost]
        public string GetDay(string _date)
        {
            return Convert.ToDateTime(_date).DayOfWeek.ToString();
        }

        public JsonResult GetTherapistUpdateChat()
        {
            if (!new cookiechecker().iscookieexist())
            {
                return Json("chat", JsonRequestBehavior.AllowGet);
            }
            return base.Json(new AccountModel()
            {
                chatList = (new ChatServices()).GetTherapistUpdateChat()
            }, 0);
        }

        [HttpPost]
        public JsonResult GetUpdatedAdminChatByClientId(int clientId)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return Json("chat", JsonRequestBehavior.AllowGet);
            }
            return base.Json(new AccountModel()
            {
                chatList = (new ChatServices()).GetUpdatedAdminChatByClientId(clientId)
            }, 0);
        }

        public JsonResult GetUpdatedAdminChatByTherapistId(int therapistId)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return Json("chat", JsonRequestBehavior.AllowGet);
            }
            return base.Json(new AccountModel()
            {
                chatList = (new ChatServices()).GetUpdatedAdminChatByTherapistId(therapistId)
            }, 0);
        }
        
        [HttpPost]
        public JsonResult GetUpdatedClientChatByClientId(int clientId)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return Json("chat", JsonRequestBehavior.AllowGet);
            }
            return base.Json(new AccountModel()
            {
                chatList = (new ChatServices()).GetUpdatedClientChatByClientId(clientId)
            }, 0);
        }
        [HttpPost]
        public JsonResult GetUpdatedTherapistChatByTherapistId(int therapistId)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return Json("chat", JsonRequestBehavior.AllowGet);
            }
            return base.Json(new AccountModel()
            {
                chatList = (new ChatServices()).GetUpdatedTherapistChatByTherapistId(therapistId)
            }, 0);
        }

        [HttpPost]
        public string SaveAdminChat(string ctoken, string text)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            Crypto crypto = new Crypto();
            ChatServices chatService = new ChatServices();
            ctoken = ctoken.Replace(" ", "+");
            int num = Convert.ToInt32(crypto.DecryptStringAES(ctoken));
            return chatService.SaveAdminChat(num, text);
        }

        [HttpPost]
        public string SaveAdminChatByClientId(string ctoken, string text, string ChatReply = "")
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            Crypto crypto = new Crypto();
            ChatServices chatService = new ChatServices();
            ctoken = ctoken.Replace(" ", "+");
            int num = Convert.ToInt32(crypto.DecryptStringAES(ctoken));
            return chatService.SaveAdminChatByClientId(num, text, ChatReply);
        }

        [HttpPost]
        public string SaveAdminChatByTherapist(string ctoken, string text)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            Crypto crypto = new Crypto();
            ChatServices chatService = new ChatServices();
            ctoken = ctoken.Replace(" ", "+");
            int num = Convert.ToInt32(crypto.DecryptStringAES(ctoken));
            return chatService.SaveAdminChatByTherapist(num, text);
        }

        [HttpPost]
        public string SaveAdminChatByTherapistId(string ctoken, string text)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            Crypto crypto = new Crypto();
            ChatServices chatService = new ChatServices();
            ctoken = ctoken.Replace(" ", "+");
            int num = Convert.ToInt32(crypto.DecryptStringAES(ctoken));
            return chatService.SaveAdminChatByTherapistId(num, text);
        }

        [HttpPost]
        public string SaveClientChat(string ctoken, string ttoken, string text)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            Crypto crypto = new Crypto();
            ChatServices chatService = new ChatServices();
            ttoken = ttoken.Replace(" ", "+");
            ctoken = ctoken.Replace(" ", "+");
            int num = Convert.ToInt32(crypto.DecryptStringAES(ttoken));
            int num1 = Convert.ToInt32(crypto.DecryptStringAES(ctoken));
            return chatService.SaveClientChat(num1, num, text);
        }

        [HttpPost]
        public string SaveTherapistChat(string ctoken, string ttoken, string text)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            Crypto crypto = new Crypto();
            ChatServices chatService = new ChatServices();
            ttoken = ttoken.Replace(" ", "+");
            ctoken = ctoken.Replace(" ", "+");
            int num = Convert.ToInt32(crypto.DecryptStringAES(ttoken));
            int num1 = Convert.ToInt32(crypto.DecryptStringAES(ctoken));
            return chatService.SaveTherapistChat(num1, num, text);
        }

        [HttpPost]
        public PartialViewResult SearchAllClientChatByAdminWithKeyword(string text = "")
        {
            if (!new cookiechecker().iscookieexist())
            {
                return PartialView();
            }
            return this.PartialView("_keywordSearchAllClient", (new ChatServices()).GetAllChatByAllClientIdForAdminBYKeywordSearch(text));
        }

        [HttpPost]
        public PartialViewResult SearchAllExpertChatByAdminWithKeyword(string text = "")
        {
            if (!new cookiechecker().iscookieexist())
            {
                return PartialView();
            }
            return this.PartialView("_keywordSearchAllExpert", (new ChatServices()).GetAllChatByAllTherapistIdForAdminBYKeywordSearch(text));
        }

        [HttpPost]
        public PartialViewResult SearchClientChatByAdminWithKeyword(int Clientid, string text = "")
        {
            if (!new cookiechecker().iscookieexist())
            {
                return PartialView();
            }
            chatModel allChatByClientIdForAdminBYKeywordSearch = (new ChatServices()).GetAllChatByClientIdForAdminBYKeywordSearch(Clientid, text);
            return this.PartialView("_keywordSearchClient", allChatByClientIdForAdminBYKeywordSearch);
        }

        [HttpPost]
        public PartialViewResult SearchTherapistChatByAdminWithKeyword(int tid, string text = "")
        {
            if (!new cookiechecker().iscookieexist())
            {
                return PartialView();
            }
            chatModel allChatByTherapistIdForAdminBYKeywordSearch = (new ChatServices()).GetAllChatByTherapistIdForAdminBYKeywordSearch(tid, text);
            return this.PartialView("_keywordSearchTherapist", allChatByTherapistIdForAdminBYKeywordSearch);
        }
        private void AddUrl(string status, int Aid, string Number)
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();
                SortedList sortedLists = new SortedList()
                {
                    { "@Aid", Aid },
                    { "@status", Number }
                };
                sqlHelper.executeNonQuery("UpdateStatus", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, "", "", 2, "", Enums.LogType.Info)).LogWrite();
            }
        }

        public void statusurl()
        {
            string status = base.Request.Params["CallStatus"].ToString();
            int num = Convert.ToInt32(base.Request.QueryString["Aid"].ToString());
            string number = base.Request.QueryString["number"].ToString();
            AddUrl(status, num, number);
        }

        public void texttospeech()
        {
            try
            {
                SqlHelper _sql = new utilis.SqlHelper();
                string message = string.Empty;
                string callStatus = Request.Params["CallStatus"];
                int SID = Convert.ToInt32(Request.Params["SID"]);
                int CID = Request.Params["cid"] != null ? Convert.ToInt32(Request.Params["cid"]) : 0;
                int tid = Request.Params["tid"] != null ? Convert.ToInt32(Request.Params["tid"]) : 0;
                var twiml = new Twilio.TwiML.TwilioResponse();
                if (callStatus == "in-progress")
                {
                    string text = "";
                    if (tid > 0)
                    {
                        text = _sql.executeScaler("Select isnull(chatmassage,'') from  tblchat WITH(NOLOCK) Where msgid=" + tid).ToString();
                    }
                    else if (CID > 0)
                    {
                        text = _sql.executeScaler("Select isnull(NotificationMessage,'') from  tblappointement WITH(NOLOCK) Where Aid=" + CID).ToString();
                    }
                    else
                    {
                        text = _sql.executeScaler("Select Message from  tblNotification WITH(NOLOCK) Where Id=" + SID).ToString();
                    }
                    text = text.Replace(":", " : ").Replace("Chat Reminder-Hello", "Hello ").Replace("Appointment Reminder-Hello", "Hello ").Replace("from", " from");
                    twiml.Say(text, new { voice = "woman", language = "en-gb" });
                    twiml.Hangup();
                    Response.ContentType = "text/xml";
                    Response.Write(twiml.ToString());
                    Response.End();
                }
            }
            catch (Exception ex)
            {
            }

        }
    }
}