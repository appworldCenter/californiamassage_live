﻿using californiaspa.DataBase;
using californiaspa.Models.user;
using californiaspa.utilis;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;

namespace californiaspa.Controllers
{
    public class MangeController : Controller
    {
        public bool haspermission(int id)
        {
            int num = (Session["ADId"] == null ? 0 : Convert.ToInt32(Session["ADId"]));
            return (new userService()).getPermissionByuserIdd(num).Contains(id);
        }

        [AuthorizeVerifiedAdmin]
        public ActionResult Index(int? roleId)
        {
            if (!this.haspermission(15))
            {
                return base.View("acess");
            }
            
            userService _userService = new userService();
            ViewBag.roles = _userService.getAllRoles();
            RoleModal roleModal1 = new RoleModal();
           int? nullable = roleId;
            roleModal1.roleId = (nullable.HasValue ? nullable.GetValueOrDefault() : 1);
            roleModal1._permissions = _userService.getAllPermissions();
             nullable = roleId;
            ViewBag.rolePermission = _userService.getPermissionByRoleId((nullable.HasValue ? nullable.GetValueOrDefault() : 1));
            return base.View(roleModal1);
        }

        [HttpPost]
        public ActionResult Index(int roleId, int[] chk)
        {
            if (!this.haspermission(15))
            {
                return base.View("acess");
            }
            userService _userService = new userService();
            ViewBag.roles = _userService.getAllRoles();
            RoleModal roleModal = new RoleModal();
            roleModal.roleId = roleId;
            roleModal._permissions = _userService.getAllPermissions();
            roleModal.ErrorText = _userService.updatePermissionByRole(chk, roleId); 
            ViewBag.rolePermission = _userService.getPermissionByRoleId(roleId);
            return base.View(roleModal);
        }
    }
}