using californiaspa.DataBase;
using californiaspa.Models.Appointment;
using californiaspa.utilis;
using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace californiaspa.Controllers
{
    public class AppointmentController : Controller
    {
        public string confirm2()
        {
            TimeZoneInfo curTimeZone = TimeZoneInfo.Local;
            return curTimeZone.Id;
        }

        public string getclientterms(int id)
        {
            string str = "";
            string str1 = "<p class='paragraphColor'>1)Booking made under CLIENTS  name by ANIL\v\r\nDO NOT MENTION SPA \r\nJUST SAY ANILS BOOKING</p>\r\n<p class='paragraphColor'>2)Talk to any attendant at reception or to Vishwas \r\nShow him I'd \r\nPAN CARD NOT ACCEPTED DL VOTER I'D ADHAAR OK\r\nBOOKING NAME WITH SPA MUST MATCH ID</p>\r\n <p class='paragraphColor'>3)PAY1000  TO THERAPIST AS PMT HAS ALREADY BEEN MADE TO SERVICE APT IN ADVANCE\r\nDO NOT PAY THEM</p>\r\n<p class='paragraphColor'>4)If\u00a0 the room is dirty contact me.Therapist will come direct to room</p>\r\n<p class='paragraphColor'>3)Stayvel Inn Service Apt\r\nIn same lane as Chalo Punjab Restaurant \r\nOpposite Carrots restaurant \r\nSame lane as Ganapati wines \r\n80ft road \r\nEjipura \r\n2 min from Sony Signal</p>\r\n<p class='paragraphColor'>4)Go to reception \r\nTell them Booking room number\r\nGo to 4th floor sit on chair. Therapist has room key</p>\r\n<p class='paragraphColor'>5)Geyser switch is there for hot water You have to put on 15 min ahead.  Ask therapist  to show you which is geyser switch</p>\r\n<p class='paragraphColor'>\v6)IF YOU ARE LATE THEN MASSAGE TIME WILL BE PROPORTIONATELY REDUCED\r\nBOOKINGS HERE ARE VERY BUSY SO PLEASE COME 15 MIN BEFORE</p>\r\n<p class='paragraphColor'>7)NO ALCOHOL SMOKING DRUGS</p>\r\n<p class='paragraphColor'>8)Towels Soap Shampoo given by Therapist </p>\r\n<p class='paragraphColor'>9)\vRegular water is free Mineral water is rs 25 extra. Make sure the room boy gives change back to you.</p>\r\n<p class='paragraphColor'>10)Any issues with hotel talk to Vishwas</p>";
            string str2 = "<p class='paragraphColor'>1)Go to Front Desk Ask for Sushma if not there say Booking for California SPA Pay rs 1500 including taxes </p>\r\n<p class='paragraphColor'>2)Check in 15 mins early\r\nTalk to Mgr Sushma\r\nShow her  I'd \r\nNO PAN DL ADHAAR OR VOTER ID</p>\r\n<p class='paragraphColor'>3)SHe will chk you into room \r\n1500 ac Whatsapp room number to me Therapist will come direct to room </p>\r\n<p class='paragraphColor'>4)Skyland hotel\r\nSampige rd\r\nNear Al bek restaurant \r\nNear mantri mall \r\nMalleswaram</p>\r\n<p class='paragraphColor'>5)IF YOU ARE MORE THAN 15 MIN LATE THEN MASSAGE TIME WILL BE REDUCED  PROPORTIONATELY </p>\r\n<p class='paragraphColor'>6)Please check if room.and bath are clean,  clean bedsheets,  2 clean towels and 2 soaps and drinking water and glassRegular water is free Mineral water is rs 25 extra. Make sure the room boy gives change back to you. \r\nAny issues with hotel talk to Mgr  Sushma  If still problems whatsapp me</p>\r\n<p class='paragraphColor'>7) NO SMOKING DRUGS ALCOHOL CONDOMS</p>";
            if (id == 4)
            {
                str = str2;
            }
            else if (id == 7 || id == 9)
            {
                str = str1;
            }
            return (new HtmlString(str)).ToString();
        }

        [AuthorizeVerifieduser]
        public ActionResult Getrequest(string token, string name = "C")
        {
            Crypto crypto = new Crypto();
            bookingServices bookingService = new bookingServices();
            token = token.Replace(" ", "+");
            int num = Convert.ToInt32(crypto.DecryptStringAES(token));
            if (name == "C")
            {
                ViewBag.Name = "Confirmed Appointment";
            }
            else
            {
                ViewBag.Name = "Appointment Request";
            }
            return base.View(bookingService.GetAppointMentDetailById(num, false));
        }

        [AuthorizeVerifieduser]
        public ActionResult GetrequestById(int? _xid)
        {
            Crypto crypto = new Crypto();
            bookingServices bookingService = new bookingServices();
            ViewBag.Name = "Confirmed Appointment";
            AppointmentModel_item appointmentModelItem = new AppointmentModel_item();
            appointmentModelItem = bookingService.GetAppointMentDetailById(_xid.Value, false);
            return base.View("Getrequest", appointmentModelItem);
        }

        public string gettherapistterms(int id)
        {
            string str = "";
            string str1 = " <p class='paragraphColor'>1)TAKE 1000  FROM CLIENT AS PMT HAS ALREADY BEEN MADE TO SERVICE APT IN ADVANCE\r\nDO NOT PAY THEM</p>\r\n<p class='paragraphColor'>2)If\u00a0 the room is dirty contact me.</p>\r\n<p class='paragraphColor'>3)Stayvel Inn Service Apt\r\nIn same lane as Chalo Punjab Restaurant \r\nOpposite Carrots restaurant \r\nSame lane as Ganapati wines \r\n80ft road \r\nEjipura \r\n2 min from Sony Signal</p>\r\n<p class='paragraphColor'>4)Geyser switch is there for hot water You have to put on 15 min ahead.</p>\r\n<p class='paragraphColor'>5)If Client is more than 15 min late then reduce time proportionately </p>\r\n<p class='paragraphColor'>6)NO ALCOHOL SMOKING DRUGS</p>\r\n<p class='paragraphColor'>7)Towels Soap Shampoo given by Therapist to Client </p>\r\n<p class='paragraphColor'>8)\vRegular water is free Mineral water is rs 25 extra. Make sure the room boy gives change back to you.</p>\r\n<p class='paragraphColor'>9)Any issues with hotel talk to Vishwas</p>";
            string str2 = "<p class='paragraphColor'>1)Go to Front Desk Ask for Room number under CLIENTS name  If any question say Booking for California SPA </p>\r\n<p class='paragraphColor'>2) If you don't have room number whstsapp Anil</p>\r\n<p class='paragraphColor'>3)Skyland hotel\r\nSampige rd\r\nNear Al bek restaurant \r\nNear mantri mall \r\nMalleswaram</p>\r\n<p class='paragraphColor'>4)IF CLIENT IS MORE THAN 15 MIN LATE THEN MASSAGE TIME WILL BE REDUCED  PROPORTIONATELY </p>\r\n<p class='paragraphColor'>5)Please check if room.and bath are clean,  clean bedsheets,  2 clean towels and 2 soaps and drinking water and glassRegular water is free Mineral water is rs 25 extra. Make sure the room boy gives change back to you. \r\nAny issues with hotel talk to Mgr  Sushma  If still problems whatsapp me</p>\r\n<p class='paragraphColor'>6) NO SMOKING DRUGS ALCOHOL CONDOMS</p>";
            if (id == 4)
            {
                str = str2;
            }
            else if (id == 7 || id == 9)
            {
                str = str1;
            }
            return (new HtmlString(str)).ToString();
        }

        [AuthorizeVerifieduser]
        public ActionResult reject_request(string token)
        {
            int num = Convert.ToInt32((new Crypto()).DecryptStringAES(token));
            Crypto crypto = new Crypto();
            bookingServices bookingService = new bookingServices();
            ViewBag.Name = "Reject Appointment";
            AppointmentModel_item appointmentModelItem = new AppointmentModel_item();
            return base.View("reject_request", bookingService.GetAppointMentDetailById(num, false));
        }

        [AuthorizeVerifieduser]
        public ActionResult request()
        {
            bookingServices bookingService = new bookingServices();
            int num = 0;
            if (Session["Aid"] != null)
            {
                num = Convert.ToInt32(Session["Aid"].ToString());
                Session["Aid"] = null;
            }
            return base.View(bookingService.GetAppointMentDetailById(num, false));
        }

        [AuthorizeVerifieduser]
        public ActionResult sendRequest(string token)
        {
           
            string str = token;
            Crypto crypto = new Crypto();
            bookingServices bookingService = new bookingServices();
            str = str.Replace(" ", "+");
            Task.Factory.StartNew(() => bookingService.SendAppointmentRequest(Convert.ToInt32(crypto.DecryptStringAES(str))));
            TempData["mesg"] = "check home page chat  for messages from therapist  You can communicate with therapist through web";
            return base.RedirectToAction("index", "therapist");
        }
    }
}