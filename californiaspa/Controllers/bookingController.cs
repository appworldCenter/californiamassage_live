using californiaspa.DataBase;
using californiaspa.Models;
using californiaspa.Models.booking;
using californiaspa.Models.therapist;
using californiaspa.utilis;
using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;

namespace californiaspa.Controllers
{
    public class bookingController : Controller
    {
        [AuthorizeVerifieduser]
        public ActionResult cancel(string tokenId)
        {
                  Crypto crypto = new Crypto();
            bookingServices bookingService = new bookingServices();
            bookingModel _bookingModel = new bookingModel();
            if (string.IsNullOrEmpty(tokenId))
            {
                return base.View(_bookingModel);
            }
            tokenId = tokenId.Replace(" ", "+");
            int num = Convert.ToInt32(crypto.DecryptStringAES(tokenId));
            TempData["user"] = bookingService.CancelAppointMentById(num);
            return base.RedirectToAction("myaccount", "account");
        }

        [HttpPost]
        public ActionResult CheckBookingDateIsExist(int thId, string bookingDate, string startTime, string endTime)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return View();
            }
            string str = (new bookingServices()).CheckBookingDateIsExist(thId, bookingDate, startTime, endTime);
            return base.Json(str);
        }

        public string checkempty(string value, string name, ref string error)
        {
            if (string.IsNullOrEmpty(value))
            {
                error = string.Concat(error, " Error : ", name, " can't empty, if not exist , please use N/A");
            }
            if (!string.IsNullOrEmpty(value))
            {
                return value;
            }
            return string.Concat("Error : ", name, " can't empty, if not exist , please use N/A");
        }

        [HttpPost]
        public ActionResult CheckhotelDateIsExist(int hId, string bookingDate, string startTime, string endTime)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return View();
            }
            var out_hotels = new HotelServices().getoutsidehotels();
            if (hId == 4 || out_hotels.Contains(hId))
            {
                return base.Json("Availabel");
            }
            string str = (new bookingServices()).CheckhotelBookingDateIsExist(hId, bookingDate, startTime, endTime);
            return base.Json(str);
        }

        [HttpPost]
        public PartialViewResult GetChart()
        {
            return base.PartialView("AvavilableBar");
        }

        [AuthorizeVerifieduser]
        public ActionResult Index(string token = null, string tokenId = null)
        {
            if (string.IsNullOrEmpty(token))
            {
                return base.RedirectToAction("index", "therapist");
            }
            Crypto crypto = new Crypto();
            token = token.Replace(" ", "+");
            int num = Convert.ToInt32(Session["cluserId"]);
            HttpCookie userInfo = new HttpCookie("userInfo");
            userInfo.Value = num.ToString();
            userInfo.Expires = DateTime.Now.AddHours(10);
            Response.Cookies.Add(userInfo);
            int num1 = Convert.ToInt32(crypto.DecryptStringAES(token));
            bookingServices bookingService = new bookingServices();
            therapistService _therapistService = new therapistService();
            HotelServices hotelService = new HotelServices();
            bookingModel _bookingModel = new bookingModel();
            if (!string.IsNullOrEmpty(tokenId))
            {
                tokenId = tokenId.Replace(" ", "+");
                int num2 = Convert.ToInt32(crypto.DecryptStringAES(tokenId));
                _bookingModel.AID = num2;
                _bookingModel = bookingService.GetAppointMentById(num2);
            }
            else
            {
                _bookingModel = bookingService.GetAppointMentBythidId(num);
                if (_bookingModel.loc == "4")
                {
                    _bookingModel.spaHotelId = 0;
                }
            }
            _bookingModel.isNew = _therapistService.checkServiceTaken(num);
            _bookingModel.profile = _therapistService.GetAllTherapistById(num1, false);
            _bookingModel.hotels = hotelService.GetAllHotels(false,!_bookingModel.isNew);
            _bookingModel.images = _therapistService.GetAllTherapistImages().Where(a => a.thId == num1).ToList<imagesItem>();
            _bookingModel.hImages = hotelService.GetAllHotelsImages();
            ViewBag.date = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            return base.View(_bookingModel);
        }

        public JsonResult gethoteltermbyid(int hid)
        {
            var res = new HotelServices().gethotelterm(hid);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult saveAppointment(bookingModel _model, HttpPostedFileBase file, string pickTime2H = "", string pickTime2M = "", string pickTime2AM = "")
        {
            if (Session["cluserId"] == null)
            {
                HttpCookie aCookie = Request.Cookies["userInfo"];
                if (aCookie != null)
                {
                    Session["cluserId"] = aCookie.Value;
                }
            }
            ViewBag.date = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            HotelServices hotelService = new HotelServices();
            therapistService _therapistService = new therapistService();
            _model.profile = _therapistService.GetAllTherapistById(_model.profile.id, false);
            _model.pickUpTime = string.Concat(new string[] { pickTime2H, ":", pickTime2M, " ", pickTime2AM });
            int num = Convert.ToInt32(Session["cluserId"]);
            _model.isNew = _therapistService.checkServiceTaken(num);
            _model.hotels = hotelService.GetAllHotels(false,!_model.isNew);
            _model.images = (
                from a in _therapistService.GetAllTherapistImages()
                where a.thId == _model.profile.id
                select a).ToList<imagesItem>();
            _model.hImages = hotelService.GetAllHotelsImages();
            _model.isNew = _therapistService.checkServiceTaken(_model.profile.id);
            var status = new AccountServices().checkstatus(num);
            if (!status)
            {
                var error = "Error :" + status.ToString() + num.ToString() + " Since you are a new client please contact SPA manager at 9591895700 to validate and proceed. Do not allow to complete booking";
               // new smshelper().SendSms("9813151090", error);
                _model.ErrorText = "Error : Since you are a new client please contact SPA manager at 9591895700 to validate and proceed. Do not allow to complete booking";
                return base.View("Index", _model);
            }
            string str = "";
            if (_model.loc == "4" && _model.spaHotelId == 0)
            {
                _model.ErrorText = "Error : You need to select spa hotel again";
                return base.View("Index", _model);
            }
            if (_model.loc != "4" && (_model.ltd == "0" || _model.lth == "0"))
            {
                _model.ErrorText = "Error : You need to allow your location";
                return base.View("Index", _model);
            }
            if (_model.loc == null || _model.locPick == null)
            {
                _model.ErrorText = "Error : please select location and transport type";
                return base.View("Index", _model);
            }
            if (_model.loc == "4" && file != null)
            {
                string str1 = file.FileName.Replace(' ', '\u005F');
                string str2 = Path.Combine(Server.MapPath("~/profileImages"), str1);
                file.SaveAs(str2);
                str = str1;
            }
            string str3 = "";
            if (_model.loc == "1")
            {
                str3 = "Clients House";
                _model.housecrossStreetName = this.checkempty(_model.housecrossStreetName, "Cross Street Name", ref str3);
                _model.houseFloorNUmber = this.checkempty(_model.houseFloorNUmber, "Floor Number", ref str3);
                _model.housestreetname = this.checkempty(_model.housestreetname, "Street Name", ref str3);
                _model.houseNUmber = this.checkempty(_model.houseNUmber, "House Number", ref str3);
                _model.houselandmark = this.checkempty(_model.houselandmark, "Landmark", ref str3);
                _model.houselacalityArea = this.checkempty(_model.houselacalityArea, "Locality Area", ref str3);
                _model.houseFlatNUmber = this.checkempty(_model.houseFlatNUmber, "Flat Number", ref str3);
                str3 = string.Concat(str3, (string.IsNullOrEmpty(_model.ltd) ? " Error : Please select location on map" : ""));
            }
            else if (_model.loc == "3")
            {
                str3 = "Clients Apartment";
                _model.AptcrossStreetName = this.checkempty(_model.AptcrossStreetName, "Cross Street Name", ref str3);
                _model.AptFloorNUmber = this.checkempty(_model.AptFloorNUmber, "Floor Number", ref str3);
                _model.Aptstreetname = this.checkempty(_model.Aptstreetname, "Street Name", ref str3);
                _model.ApartmentNUmber = this.checkempty(_model.ApartmentNUmber, "Apt Number", ref str3);
                _model.Aptlandmark = this.checkempty(_model.Aptlandmark, "Landmark", ref str3);
                _model.ApartmentName = this.checkempty(_model.ApartmentName, "Apt Name", ref str3);
                _model.AptlacalityArea = this.checkempty(_model.AptlacalityArea, "Locality Area", ref str3);
                str3 = string.Concat(str3, (string.IsNullOrEmpty(_model.ltd) ? " Error : Please select location on map" : ""));
            }
            else if (_model.loc == "2")
            {
                str3 = "Clients Hotel";
                _model.HotelStreetname = this.checkempty(_model.HotelStreetname, "Street Name", ref str3);
                _model.HotelRoomNUmber = this.checkempty(_model.HotelRoomNUmber, "Room Number", ref str3);
                _model.HotelcrossStreetName = this.checkempty(_model.HotelcrossStreetName, "Cross Street Name", ref str3);
                _model.Hotellandmark = this.checkempty(_model.Hotellandmark, "Hotel Landmark", ref str3);
                _model.HotalName = this.checkempty(_model.HotalName, "Hotel Name", ref str3);
                str3 = string.Concat(str3, (string.IsNullOrEmpty(_model.ltd) ? " Error : Please select location on map" : ""));
            }
            if (str3.Contains("Error"))
            {
                _model.ErrorText = str3;
                return base.View("Index", _model);
            }
            string str4 = (new bookingServices()).appointmentRequest(_model, num, str);
            if (str4.Contains("Error"))
            {
                _model.ErrorText = str4;
                return base.View("Index", _model);
            }
            Session["Aid"] = str4;
            return base.RedirectToAction("request", "appointment");
        }
    }
}