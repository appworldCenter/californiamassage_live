﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using californiaspa.utilis;

namespace californiaspa.Controllers
{
    public class browserController : Controller
    {
        // GET: browser
        public ActionResult Index(string ReturnUrl="")
        {
            var useragent = Request.UserAgent;
            ViewBag.url = ReturnUrl;
            //(new Logger(useragent, this.GetType().Namespace, this.GetType().Name, 0, useragent, Enums.LogType.Error)).LogWrite();
            return View();
        }
    }
}