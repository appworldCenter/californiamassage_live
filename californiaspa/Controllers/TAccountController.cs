using californiaspa.DataBase;
using californiaspa.Models;
using californiaspa.Models.Account;
using californiaspa.Models.Appointment;
using californiaspa.Models.Chat;
using californiaspa.Models.therapist;
using californiaspa.utilis;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace californiaspa.Controllers
{
    public class TAccountController : Controller
    {
        [AuthorizeVerifiedtherapist]
        public ActionResult appointment_reminder(int? deleteId,int? page)
        {
            int thid = Convert.ToInt32(Session["thId"]);
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            ChatServices chatService = new ChatServices();
            NotificationModel notificationModel = new NotificationModel();
            string empty = string.Empty;
            if (deleteId.HasValue)
            {
                empty = chatService.UpdateNotification(deleteId.Value);
            } 
            int? nullable = null;
            notificationModel = chatService.getAllNotifications(num, num1, ref total, nullable, new int?(thid), "", "");
            notificationModel.ErrorText = empty;
            string str = string.Concat(new object[] { "1" });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "taccount", "appointment_reminder", str, new decimal(10)));
            ViewBag.paging = htmlString;
            return base.View(notificationModel);
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult appointment_request()
        {
            ViewBag.token = Convert.ToInt32(Session["thId"]);
            therapistService _therapistService = new therapistService();
            Schedule schedule = new Schedule();
            schedule.tid = Convert.ToInt32(Session["thId"]);
            string[] strArrays = _therapistService.GetTherapistScheduleId(schedule.tid, false).Split(new char[] { '-' });
            schedule.firstaptTime = strArrays[0];
            schedule.LastaptTime = strArrays[1];
            return base.View(schedule);
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult callschedule(int _xid)
        {
            ViewBag.token = Convert.ToInt32(Session["thId"]);
            therapistService _therapistService = new therapistService();
            Schedule schedule = new Schedule()
            {
                tid = _xid
            };
            string[] strArrays = _therapistService.GetTherapistcallScheduleId(schedule.tid).Split(new char[] { '-' });
            schedule.firstaptTime = strArrays[0];
            schedule.LastaptTime = strArrays[1];
            return base.View(schedule);
        }

        [HttpPost]
        public ActionResult callschedule(Schedule _model)
        {
            ViewBag.token = Convert.ToInt32(Session["thId"]);
            therapistService _therapistService = new therapistService();
            object[] month = new object[5];
            DateTime now = DateTime.Now;
            month[0] = now.Month;
            month[1] = "/";
            now = DateTime.Now;
            month[2] = now.Day;
            month[3] = "/";
            now = DateTime.Now;
            month[4] = now.Year;
            string str = string.Concat(month);
            DateTime dateTime = Convert.ToDateTime(string.Concat(str, " ", _model.firstaptTime));
            if (Convert.ToDateTime(string.Concat(str, " ", _model.LastaptTime)) < dateTime)
            {
                _model.ErrorText = "Latest  time can not grater then Earliest time ";
                return base.View(_model);
            }
            string str1 = _therapistService.UpdateScheduleForcallandmessage(_model.firstaptTime, _model.LastaptTime, _model.tid);
            string[] strArrays = _therapistService.GetTherapistScheduleId(_model.tid, true).Split(new char[] { '-' });
            string[] strArrays1 = _therapistService.GetTherapistcallScheduleId(_model.tid).Split(new char[] { '-' });
            _model.firstaptTime = strArrays[0];
            _model.LastaptTime = strArrays[1];
            _model.callfirstaptTime = strArrays1[0];
            _model.callLastaptTime = strArrays1[1];
            _model.ErrorText = str1;
            return base.View("edit_schedule", _model);
        }

        public void changestatus(bool status, string token)
        {
            if (new cookiechecker().iscookieexist())
            {
                int num = Convert.ToInt32((new Crypto()).DecryptStringAES(token).ToString());
                (new bookingServices()).updateStatusbyThId(num, status);
            }
            
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult Chat(string searchToken = "", int token = 0)
        {
            ViewBag.token = Convert.ToInt32(Session["thId"]);
            ChatServices chatService = new ChatServices();
            Crypto crypto = new Crypto();
            int num = Convert.ToInt32(Session["thId"]);
            TherapistChat therapistChat = new TherapistChat()
            {
                therapistId = num,
                _AdminChatlist = chatService.GetAdminChatByTherapistId(num),
                _TherapistChatlist = chatService.GetAllChatByTherapistId(num, searchToken),
                chatDivId = token
            };
            if (string.IsNullOrEmpty(searchToken))
            {
                return base.View(therapistChat);
            }
            return this.PartialView("SearchChats", therapistChat);
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult ClientAccount(int _xid, int editid)
        {
            ViewBag.token = Convert.ToInt32(Session["thId"]);
            int num = _xid;
            AccountServices accountService = new AccountServices();
            AccountModel accountModel = new AccountModel();
            if (TempData["user"] != null)
            {
                accountModel.ErrorText = TempData["user"].ToString();
            }
            ViewBag.edit = editid;
            accountModel = accountService.GetCustomer(num, true);
            return base.View(accountModel);
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult confirmed_request(string token = "")
        {
            ViewBag.token = Convert.ToInt32(Session["thId"]);
            bookingServices bookingService = new bookingServices();
            int num = Convert.ToInt32(Session["thId"]);
            AppointmentModel confirmedAppointMentDetailByTherapistId = bookingService.GetConfirmedAppointMentDetailByTherapistId(num);
            if (TempData["error"] != null)
            {
                confirmedAppointMentDetailByTherapistId.ErrorText = TempData["error"].ToString();
            }
            return base.View(confirmedAppointMentDetailByTherapistId);
        }

        [HttpPost]
        public string confirmrequest(int id, string res, string lth, string ltd, string time, string confirmDate = "", int clientId = 0, int Aid = 0, string pcktime = "")
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            bookingServices bookingService = new bookingServices();
            therapistService _therapistService = new therapistService();
            
            return bookingService.UpdateConfirmedSTatusByAID(id, res, lth, ltd, time, pcktime);
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult dashboard()
        {
            ViewBag.token = Convert.ToInt32(Session["thId"]);
            return base.View();
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult delete_reminder(string deleteId)
        {
            int num = Convert.ToInt32(Session["thId"]);
            ChatServices chatService = new ChatServices();
            NotificationModel notificationModel = new NotificationModel();
            string empty = string.Empty;
            if (!string.IsNullOrEmpty(deleteId))
            {
                string[] strArrays = deleteId.Split(new char[] { '\u005F' });
                for (int i = 0; i < (int)strArrays.Length; i++)
                {
                    string str = strArrays[i];
                    empty = chatService.UpdateNotification(Convert.ToInt16(str));
                }
            }
            int? nullable = null;
            int total = 0;
            notificationModel = chatService.getAllNotifications(1, 2, ref total, nullable, new int?(num), "", "");
            notificationModel.ErrorText = empty;
            return RedirectToAction("appointment_reminder");

            //return base.View("appointment_reminder", notificationModel);
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult edit_schedule()
        {
            ViewBag.token = Convert.ToInt32(Session["thId"]);
            therapistService _therapistService = new therapistService();
            Schedule schedule = new Schedule()
            {
                tid = Convert.ToInt32(Session["thId"])
            };
            string[] strArrays = _therapistService.GetTherapistScheduleId(schedule.tid, true).Split(new char[] { '-' });
            string[] strArrays1 = _therapistService.GetTherapistcallScheduleId(schedule.tid).Split(new char[] { '-' });
            schedule.firstaptTime = strArrays[0];
            schedule.LastaptTime = strArrays[1];
            schedule.callfirstaptTime = strArrays1[0];
            schedule.callLastaptTime = strArrays1[1];
            return base.View(schedule);
        }

        [HttpPost]
        public string endMassage(int _xid)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
                return (new bookingServices()).SaveEndMassageNotification(_xid);
        }

        [AuthorizeVerifiedtherapist]
        [HttpPost]
        public ActionResult GetChat(string searchToken = "")
        {
            ViewBag.token = Convert.ToInt32(Session["thId"]);
            ChatServices chatService = new ChatServices();
            int num = Convert.ToInt32(Session["thId"]);
            return this.PartialView("SearchChats", new TherapistChat()
            {
                _TherapistChatlist = chatService.GetAllChatByTherapistId(num, searchToken)
            });
        }

        [HttpPost]
        public string getclientSurvey()
        {
            if (Session["cluserId"] == null)
            {
                return "Your session is time out , Please refresh page ";
            }
            bookingServices bookingService = new bookingServices();
            AccountServices accountService = new AccountServices();
            int num = Convert.ToInt32(Session["cluserId"]);
            if (!accountService.checkstatus(num))
            {
                return "Error : NotVerified";
            }
            return bookingService.getclientsurvey(num);
        }

        [HttpPost]
        public string getConfirmrequest(int id, string time)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            return (new HtmlString((new bookingServices()).getconfirmTime(time, id))).ToString();
        }

        public ActionResult getRequest(string token = "")
        {
            ViewBag.token = Convert.ToInt32(token);
            bookingServices bookingService = new bookingServices();
            int num = Convert.ToInt32(Session["thId"]);
            return base.View("confirmed_request", bookingService.GetConfirmedAppointMentDetailByTherapistId(num));
        }

        public string IPRequestHelper(string url)
        {
            StreamReader streamReader = new StreamReader(((HttpWebResponse)((HttpWebRequest)WebRequest.Create(url)).GetResponse()).GetResponseStream());
            string str = streamReader.ReadToEnd().Replace("\n", string.Empty);
            streamReader.Close();
            streamReader.Dispose();
            return str;
        }

        [HttpPost]
        public string leftInCab(int _xid, string leftCabTime)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            (new bookingServices()).SendCabNotification(_xid, leftCabTime);
            return " request is rejected by therapist";
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult Myprofile(int? page)
        {
            page = page ?? 1;
            utility _utility = new utility();
            int num = 1;
            int num1 = (page.HasValue ? page.Value : 10);
            decimal one = decimal.One;
            if (page.HasValue)
            {
                num = (Convert.ToInt32(page) - 1) * 10;
                one = Convert.ToInt32(page);
                num++;
            }
            num1 = (Convert.ToInt32(page)) * 10;
            int total = 0;
            therapistService _therapistService = new therapistService();
            AdminServices adminService = new AdminServices();
            int therapistid = Convert.ToInt32(Session["thId"]);
            therapistprofile allTherapistById = _therapistService.GetAllTherapistById(therapistid, true);
            allTherapistById.Comments = _therapistService.GettopAllComments(therapistid,num,num1,ref total);
            allTherapistById.images = _therapistService.GetAllTherapistImages().Where(a => a.thId == therapistid).ToList();
            if (TempData["msg"] != null)
            {
                allTherapistById.ErrorText = TempData["msg"].ToString();
            }
            string str = string.Concat(new object[] { "1" });
            HtmlString htmlString = new HtmlString(_utility.create_paging(total, one, "taccount", "myprofile", str, new decimal(10)));
            ViewBag.paging = htmlString;
            ViewBag.page = num;
            return base.View(allTherapistById);
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult Notification(string token = "")
        {
            ViewBag.token = Convert.ToInt32(Session["thId"]);
            return base.View();
        }

        [HttpPost]
        public string rejectRequest(int _xid, string res)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            bookingServices _bks = new bookingServices();
            string str = (new Crypto()).EncryptStringAES(_xid.ToString());
            string str1 = Server.UrlEncode("https://www.californiamassage.in/appointment/reject_request?token" + str);
            string str2 = Server.UrlEncode("https://www.californiamassage.in/admin/reject_request?token" + str);
            Task.Factory.StartNew(() => _bks.rejectAppointmentRequest(_xid, res, str1, str2));
            return "Please Send Whatsapp Message Manager for approval, YOU CAN't CANCEL WITHOUT MANAGER APPROVAL";
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult request(string token = null, string confirmDate = "", int clientId = 0, int Aid = 0)
        {
            int num = Convert.ToInt32(Session["thId"]);
            ViewBag.token = num;
            therapistService _therapistService = new therapistService();
            bookingServices bookingService = new bookingServices();
            string str = "";
            if (string.IsNullOrEmpty(token))
            {
                AppointmentModel appointMentDetailByTherapistId = bookingService.GetAppointMentDetailByTherapistId(num);
                if (!string.IsNullOrEmpty(token))
                {
                    appointMentDetailByTherapistId.ErrorText = str;
                }
                return base.View(appointMentDetailByTherapistId);
            }
            string[] strArrays = token.Split(new char[] { '\u005F' });
            strArrays[1] = (strArrays[1].Length == 1 ? string.Concat("0", strArrays[1]) : strArrays[1]);
            DateTime dateTime = Convert.ToDateTime(string.Concat(new string[] { strArrays[1], ":", strArrays[2], " ", strArrays[3] }));
            string shortTimeString = dateTime.AddMinutes(120).ToShortTimeString();
            shortTimeString = (shortTimeString.Length == 7 ? string.Concat("0", shortTimeString) : shortTimeString);
            Convert.ToDateTime(confirmDate);
            str = bookingService.UpdateConfirmedSTatusByAID(Convert.ToInt32(strArrays[0]), "", "", "", string.Concat(new string[] { strArrays[1], ":", strArrays[2], " ", strArrays[3] }), "");
            TempData["error"] = str;
            return base.RedirectToAction("confirmed_request");
        }

        [HttpPost]
        public string return_request(int id, string res)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            bookingServices _bks = new bookingServices();
            string str = (new Crypto()).EncryptStringAES(id.ToString());
            string str1 = Server.UrlEncode("https://www.californiamassage.in/appointment/reject_request?token" + str);
            string str2 = Server.UrlEncode("https://www.californiamassage.in/admin/reject_request?token" + str);
            Task.Factory.StartNew(() => _bks.rejectAppointmentRequest(id, res, str1, str2));
            return "Please Send Whatsapp Message Manager for approval, YOU CAN't CANCEL WITHOUT MANAGER APPROVAL";
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult schedule(int _xid)
        {
            ViewBag.token = Convert.ToInt32(Session["thId"]);
            therapistService _therapistService = new therapistService();
            Schedule schedule = new Schedule()
            {
                tid = _xid
            };
            string[] strArrays = _therapistService.GetTherapistScheduleId(schedule.tid, true).Split(new char[] { '-' });
            schedule.firstaptTime = strArrays[0];
            schedule.LastaptTime = strArrays[1];
            return base.View(schedule);
        }

        [HttpPost]
        public ActionResult schedule(Schedule _model)
        {
            ViewBag.token = Convert.ToInt32(Session["thId"]);
            therapistService _therapistService = new therapistService();
            object[] month = new object[5];
            DateTime now = DateTime.Now;
            month[0] = now.Month;
            month[1] = "/";
            now = DateTime.Now;
            month[2] = now.Day;
            month[3] = "/";
            now = DateTime.Now;
            month[4] = now.Year;
            string str = string.Concat(month);
            DateTime dateTime = Convert.ToDateTime(string.Concat(str, " ", _model.firstaptTime));
            if (Convert.ToDateTime(string.Concat(str, " ", _model.LastaptTime)) <= dateTime)
            {
                _model.ErrorText = "Last Appointment start time can not grater then Or equal to first Appointment start time ";
                return base.View(_model);
            }
            now = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            _model.ErrorText = _therapistService.UpdateScheduleForAll(now.Date, _model.firstaptTime, _model.LastaptTime, _model.tid, "Y");
            return base.View("edit_schedule", _model);
        }

        [HttpPost]
        public void sendNotification(int _xid, string ltd, string lth)
        {
            if (new cookiechecker().iscookieexist())
            {


                bookingServices bookingService = new bookingServices();
                string str = string.Concat("http://maps.google.com/?q=", ltd, ",", lth);
                if (str == "https://goo.gl/GPRM2")
                {
                    string empty = string.Empty;
                    if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                    {
                        empty = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    else if (Request.UserHostAddress.Length != 0)
                    {
                        empty = Request.UserHostAddress;
                    }
                    string str1 = string.Concat("http://ipinfo.io/", empty, "/loc");
                    str = this.IPRequestHelper(str1);
                    str = string.Concat("http://maps.google.com/?q=", str);
                }
                bookingService.setNotification(_xid, str);
            }
        }

        [HttpPost]
        public string startMassage(int _xid, string startTime)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            (new bookingServices()).SaveStartMassageNotification(_xid, startTime);
            return "";
        }

        [HttpPost]
        public ActionResult UpdateImage(HttpPostedFileBase[] files)
        {
            int num = Convert.ToInt32(Session["thId"]);
            AdminServices adminService = new AdminServices();
            Crypto crypto = new Crypto();
            string str = "";
            string str1 = "";
            if (files.Count<HttpPostedFileBase>() > 0)
            {
                HttpPostedFileBase[] httpPostedFileBaseArray = files;
                for (int i = 0; i < (int)httpPostedFileBaseArray.Length; i++)
                {
                    HttpPostedFileBase httpPostedFileBase = httpPostedFileBaseArray[i];
                    string str2 = Path.Combine(Server.MapPath("~/profileImages"), httpPostedFileBase.FileName);
                    httpPostedFileBase.SaveAs(str2);
                    str = string.Concat("~/profileImages/", httpPostedFileBase.FileName);
                    str1 = adminService.UpdateImages(str, num);
                }
            }
            TempData["msg"] = str1;
            return base.RedirectToAction("Myprofile");
        }

        [HttpPost]
        public ActionResult UpdateProfile(HttpPostedFileBase file)
        {
            AdminServices adminService = new AdminServices();
            therapistService _therapistService = new therapistService();
            int num = Convert.ToInt32(Session["thId"]);
            string str = "";
            if (file != null)
            {
                string str1 = Path.Combine(Server.MapPath("~/profileImages"), file.FileName);
                file.SaveAs(str1);
                str = string.Concat("~/profileImages/", file.FileName);
            }
            TempData["msg"] = adminService.UpdateOrCreateImage(str, num);
            return base.RedirectToAction("Myprofile");
        }

        [HttpPost]
        public string UpdateSchedule(DateTime date, string start, string end, int tid, string working)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            therapistService _therapistService = new therapistService();
            string str = string.Concat(new object[] { date.Month, "/", date.Day, "/", date.Year });
            DateTime dateTime = Convert.ToDateTime(string.Concat(str, " ", start));
            if (Convert.ToDateTime(string.Concat(str, " ", end)) < dateTime)
            {
                return "Error : Last Appointment start time can not grater then first Appointment start time ";
            }
            return _therapistService.UpdateSchedule(date, start, end, tid, working);
        }
    }
}