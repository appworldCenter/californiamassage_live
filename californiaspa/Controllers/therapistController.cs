using californiaspa.DataBase;
using californiaspa.Models;
using californiaspa.Models.therapist;
using californiaspa.utilis;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace californiaspa.Controllers
{
    public class therapistController : Controller
    {

        public ActionResult GetEvents(int thid, string start = null, string end = null, int byClient = 0, int cnt = 0)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return Json("",JsonRequestBehavior.AllowGet);
            }
            DateTime dateTime;
            therapistService _therapistService = new therapistService();
            DateTime dateTime1 = (!string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time"));
            if (string.IsNullOrEmpty(end))
            {
                DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                dateTime = currentTimeByTimeZone.AddDays(30);
            }
            else
            {
                dateTime = Convert.ToDateTime(end);
            }
            DateTime dateTime2 = dateTime;
            Events[] array = _therapistService.GetEvents(thid, dateTime1, dateTime2, byClient, cnt).ToArray();
            return base.Json(array, 0);
        }

        [HttpPost]
        public string GetEventsByDate(int thid, string start = null, string end = null, string date = null)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return "";
            }
            return (new therapistService()).GetAvailablityWithAppointmentByDate(thid, start, end, date);
        }

        [HttpPost]
        public string GetEventTimeByDate(int thid, DateTime datetime)
        {
           if(!new cookiechecker().iscookieexist())
            {
                return "";
            }
            return (new therapistService()).GetTherapistScheduleId(thid, datetime.Date, false);
        }

        [AuthorizeVerifieduser]
        public ActionResult Index()
        {
            therapistService _therapistService = new therapistService();
            int userid = Convert.ToInt32(Session["cluserId"]);
            bool isnew= _therapistService.checkServiceTaken(userid);
            _therapist allTherapist = _therapistService.GetAllTherapist(false, !isnew);
            allTherapist.images = _therapistService.GetAllTherapistImages();
            if (TempData["mesg"] != null)
            {
                allTherapist.ErrorText = TempData["mesg"].ToString();
            }
            return base.View(allTherapist);
        }
    }
}