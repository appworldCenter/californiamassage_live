using californiaspa.DataBase;
using californiaspa.Models;
using californiaspa.Models.booking;
using californiaspa.utilis;
using System;
using System.Web;
using System.Web.Mvc;

namespace californiaspa.Controllers
{
    public class MainController : Controller
    {
        public ActionResult AboutUs()
        {
            return base.View();
        }

        [AuthorizeVerifiedtherapist]
        public ActionResult ClientSurvey(int _xid, int aid)
        {
            ClientSurveymodel clientSurveymodel = new ClientSurveymodel();
            bookingServices bookingService = new bookingServices();
            if (Convert.ToInt32(Session["thId"].ToString()) == _xid)
            {
                clientSurveymodel = bookingService.CheckCLientPendingSurveryByAId(aid);
                clientSurveymodel.AID = aid;
                if (!clientSurveymodel.pendingServey)
                {
                    clientSurveymodel.ErrorText = string.Concat("Survery already completed for this appointement date :", clientSurveymodel.appointment_date, ";", clientSurveymodel.appointment_time);
                }
            }
            else
            {
                clientSurveymodel.ErrorText = "No Survey exist currently";
            }
            return base.View(clientSurveymodel);
        }
        
        [HttpPost]
        public ActionResult completeClientSurvey(ClientSurveymodel _model)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return base.View("ClientSurvey", _model);
            }
            bookingServices bookingService = new bookingServices();
            smshelper _smshelper = new smshelper();
            string str = bookingService.SaveClientSurvey(_model);
            if (str.Contains("Error"))
            {
                _model.ErrorText = str;
            }
            else
            {
                string[] strArrays = str.Split(new char[] { '-' });
                //string str1 = strArrays.Length>1? string.Concat(string.Concat("CLSPA :", strArrays[1], " completed survey feedback for "), strArrays[0]): "CLSPA : Appointment Id : "+ _model.AID + "  completed survey feedback by therapist" ;
                string str1 = strArrays.Length > 1 ? string.Concat(string.Concat("CAL SPA : ADMIN <br/><br/>", strArrays[1], " completed survey feedback for "), strArrays[0]) : "CAL SPA : ADMIN<br/><br/>  Appointment Id : " + _model.AID + "  completed survey feedback by therapist";
               // _smshelper.SendSms("9591895700", str1, "+14177089506");
                _model.ErrorText = "Thank you for participating in survey , We appreciate your valuable feedback";
            }
            return base.View("ClientSurvey", _model);
        }

        [HttpPost]
        public ActionResult completeSurvey(Surveymodel _model)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return base.View("Survey", _model);
            }
            bookingServices bookingService = new bookingServices();
            smshelper _smshelper = new smshelper();
            string str = bookingService.SaveSurvey(_model);
            if (str.Contains("Error"))
            {
                _model.ErrorText = str;
            }
            else
            {
                string[] strArrays = str.Split(new char[] { '-' });
                string str1 = strArrays.Length > 1 ? string.Concat(string.Concat("CAL SPA : ADMIN <br/>", strArrays[0], " complete survey feedback for "), strArrays[1]) : "CAL SPA : ADMIN<br/> Appointment Id : " + _model.AID + "  completed survey feedback by therapist";
               // _smshelper.SendSms("9591895700", str1, "+14177089506");
                _model.ErrorText = "Thank you for participating in survey , We appreciate your valuable feedback";
            }
            return base.View("Survey", _model);
        }

        [AuthorizeVerifieduser]
        public ActionResult dashboard()
        {
            return base.View();
        }

        public ActionResult FemaleMassage()
        {
            return base.View();
        }

        public PartialViewResult GetClientSurvey(string token)
        {
            if (!new cookiechecker().iscookieexist())
            {
                return base.PartialView("Survey");
            }
            bookingServices bookingService = new bookingServices();
            Crypto crypto = new Crypto();
            int num = 0;
            if (!string.IsNullOrEmpty(token))
            {
                token = token.Replace(" ", "+");
                num = Convert.ToInt32(crypto.DecryptStringAES(token));
            }
            return base.PartialView(bookingService.GetClientSurveyByIdSurvey(num, token));
        }

        public FileResult getId(string name)
        {
            string str = Server.MapPath(string.Concat("~/profileImages/", name));
            string empty = string.Empty;
            if (name.Contains(".pdf"))
            {
                empty = "application/pdf";
            }
            else
            {
                empty = (!name.Contains(".docx") ? "image/jpeg" : "application/docx");
            }
            return this.File(str, empty, name);
        }

        public PartialViewResult GetSurvey(string token, string name = "")
        {
            if (!new cookiechecker().iscookieexist())
            {
                return base.PartialView("Survey");
            }
            bookingServices bookingService = new bookingServices();
            Crypto crypto = new Crypto();
            int num = 0;
            if (!string.IsNullOrEmpty(token))
            {
                token = token.Replace(" ", "+");
                num = Convert.ToInt32(crypto.DecryptStringAES(token));
            }
            Surveymodel terapistSurveyByIdSurvey = bookingService.GetTerapistSurveyByIdSurvey(num, token);
            if (!string.IsNullOrEmpty(name))
            {
                terapistSurveyByIdSurvey.therapistName = name;
            }
            return base.PartialView(terapistSurveyByIdSurvey);
        }

        public ActionResult HomeMassage()
        {
            return base.View();
        }

        public ActionResult Index()
        {
            utility _utility = new utility();
            if (Request.Url.AbsoluteUri.Contains("https") && !base.Request.Url.AbsoluteUri.Contains("localhost"))
            {
                return base.View();
            }
            return this.Redirect("https://www.californiamassage.in/main/index");
            //return View();
        }

        public ActionResult Jobs()
        {
            return base.View();
        }

        public ActionResult MassageStyle()
        {
            return base.View();
        }

        [AuthorizeVerifieduser]
        public ActionResult Survey(int _xid, int aid)
        {
            Surveymodel surveymodel = new Surveymodel();
            bookingServices bookingService = new bookingServices();
            if (Convert.ToInt32(Session["cluserId"].ToString()) == _xid)
            {
                surveymodel = bookingService.CheckPendingSurveryByAId(aid);
                if (!surveymodel.pendingServey)
                {
                    surveymodel.ErrorText = string.Concat("Survery already completed for this appointement date :", surveymodel.appointment_date, ";", surveymodel.appointment_time);
                }
                surveymodel.AID = aid;
            }
            else
            {
                surveymodel.ErrorText = "No Survey exist currently ";
            }
            return base.View(surveymodel);
        }

        public ActionResult Testimonials()
        {
            return base.View();
        }
    }
}