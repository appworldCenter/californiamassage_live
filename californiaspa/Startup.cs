﻿using californiaspa.Controllers;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(californiaspa.Startup))]
namespace californiaspa
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //GlobalConfiguration.Configuration.UseSqlServerStorage("con");
            //// app.UseHangfireDashboard();
            //hangfireController _hngfire = new hangfireController();
            //RecurringJob.AddOrUpdate(()=> _hngfire.check_request_status(), Cron.MinuteInterval(5));
            //RecurringJob.AddOrUpdate(() => _hngfire.check_request_status1z(), Cron.MinuteInterval(5));
           // app.UseHangfireServer();
        }
    }
}
