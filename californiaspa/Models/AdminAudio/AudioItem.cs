using californiaspa.Models;
using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.AdminAudio
{
	public class AudioItem : ErrorModel
	{
		public int Aid
		{
			get;
			set;
		}
        public int srno
        {
            get;
            set;
        }

        public int Audioid
		{
			get;
			set;
		}

		public string audioUrl
		{
			get;
			set;
		}

		public string ClientName
		{
			get;
			set;
		}

		public string recordedDate
		{
			get;
			set;
		}

		public string therapistName
		{
			get;
			set;
		}

		public AudioItem()
		{
		}
	}
}