using californiaspa.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.AdminAudio
{
	public class AudioReport : ErrorModel
	{
		public List<AudioItem> audioReportList
		{
			get;
			set;
		}

		public AudioReport()
		{
		}
	}
}