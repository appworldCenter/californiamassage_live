using californiaspa.Models;
using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.therapist
{
	public class Schedule : ErrorModel
	{
		public string firstaptTime
		{
			get;
			set;
		}

		public string LastaptTime
		{
			get;
			set;
		}

        public string callfirstaptTime
        {
            get;
            set;
        }

        public string callLastaptTime
        {
            get;
            set;
        }

        public int tid
		{
			get;
			set;
		}

		public Schedule()
		{
		}
	}
}