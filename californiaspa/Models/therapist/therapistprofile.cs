using californiaspa.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.therapist
{
	public class therapistprofile : ErrorModel
	{
		public string Adminreview
		{
			get;
			set;
		}

		public int Age
		{
			get;
			set;
		}

		public int bookingLimit
		{
			get;
			set;
		}

		public double ClientRating
		{
			get;
			set;
		}

		public List<comments> Comments
		{
			get;
			set;
		}

		public string FirstAptTime
		{
			get;
			set;
		}

        public string ErliestcallTime
        {
            get;
            set;
        }

        public string latestcallTime
        {
            get;
            set;
        }

        public int id
		{
			get;
			set;
		}

		public int ImageCount
		{
			get;
			set;
		}

		public string imagePath
		{
			get;
			set;
		}

		public List<imagesItem> images
		{
			get;
			set;
		}

		public string joiningDate
		{
			get;
			set;
		}

		public string languages
		{
			get;
			set;
		}

		public string LastAptTime
		{
			get;
			set;
		}

		public string location
		{
			get;
			set;
		}

		public string MassageStyles
		{
			get;
			set;
		}

		public string mobile
		{
			get;
			set;
		}

		public string name
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}

		public decimal rate
		{
			get;
			set;
		}

		public bool showprofile
		{
			get;
			set;
		}

		public string TodaysAvailabilty
		{
			get;
			set;
		}

		public string token
		{
			get;
			set;
		}

		public int TotalRating
		{
			get;
			set;
		}

		public string Username
		{
			get;
			set;
		}

		public therapistprofile()
		{
		}
	}
}