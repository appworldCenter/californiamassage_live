using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.therapist
{
	public class comments
	{
		public string Comment
        {
            get;
            set;
        }

        public string Commentdate
        {
            get;
            set;
        }

        public int srno
        {
            get;
            set;
        }

        public string aptdate
        {
            get;
            set;
        }

        public string name
		{
			get;
			set;
		}

		public int rating
		{
			get;
			set;
		} 
	}
}