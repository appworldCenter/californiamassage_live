using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.therapist
{
	public class Events
	{
		public bool allDay
		{
			get;
			set;
		}

		public string backgroundColor
		{
			get;
			set;
		}

		public string className
		{
			get;
			set;
		}

		public string date
		{
			get;
			set;
		}

		public string end
		{
			get;
			set;
		}

		public string id
		{
			get;
			set;
		}

		public string start
		{
			get;
			set;
		}

		public string title
		{
			get;
			set;
		}

		public string url
		{
			get;
			set;
		}

		public Events()
		{
		}
	}
}