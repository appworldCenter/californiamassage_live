using californiaspa.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.therapist
{
	public class _therapist : ErrorModel
	{
		public List<imagesItem> images
		{
			get;
			set;
		}
        public bool permission { get; set; }
        public List<therapistprofile> profile
		{
			get;
			set;
		}

		public _therapist()
		{
		}
	}
}