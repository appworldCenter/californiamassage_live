﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace californiaspa.Models.user
{
    public class RoleModal:ErrorModel
    {
        public List<permissionitem> _permissions { get; set; }
        public int roleId { get; set; }
    }

    public class Roles
    {
        public string name { get; set; }
        public int roleId { get; set; }
    }
}