using californiaspa.Models.therapist;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Client
{
	public class profile_item
	{
		public int _xid
		{
			get;
			set;
		}

		public int alltotal
		{
			get;
			set;
		}

		public string BlockStatus
		{
			get;
			set;
		}

		public List<comments> clientReview
		{
			get;
			set;
		}

		public int counter
		{
			get;
			set;
		}

		public string email
		{
			get;
			set;
		}

		public string mobile
		{
			get;
			set;
		}

        public string usertype
        {
            get;
            set;
        }

        public string name
		{
			get;
			set;
		}

		public int pgindex
		{
			get;
			set;
		}

		public List<comments> review
		{
			get;
			set;
		}

		public string reviewToken
		{
			get;
			set;
		}

		public int therapistVisitedCount
		{
			get;
			set;
		}

		public string token
		{
			get;
			set;
		}

		public int total
		{
			get;
			set;
		}

		public int totalMassage
		{
			get;
			set;
		}

		public profile_item()
		{
		}
	}
}