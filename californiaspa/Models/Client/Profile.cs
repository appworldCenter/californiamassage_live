using californiaspa.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Client
{
	public class Profile : ErrorModel
	{
		public List<profile_item> _profiles
		{
			get;
			set;
		}

		public int idnex
		{
			get;
			set;
		}

		public int pageidnex
		{
			get;
			set;
		}

		public Profile()
		{
		}
	}
}