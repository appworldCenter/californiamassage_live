using californiaspa.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Account
{
	public class AdminProfile : ErrorModel
	{
		public string confirmPassword
		{
			get;
			set;
		} 

        public int roleId
        {
            get;
            set;
        }

        public string EmailAddress
		{
			get;
			set;
		}

		public int Id
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}
        public string rolename
        {
            get;
            set;
        } 
        public string UpdatedDate
		{
			get;
			set;
		}
        public string  token { get; set; }
    }

    public class adminlist:ErrorModel
    {
        public List<AdminProfile> adminprofiles  { get; set; }
    }
}