using californiaspa.Models;
using californiaspa.Models.Chat;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Account
{
	public class AccountModel : ErrorModel
    {
        public string CountryCode
        {
            get;
            set;
        }
        public string _exid
		{
			get;
			set;
		}
        public string usertype
        {
            get;
            set;
        }
        public string photoidurl
        {
            get;
            set;
        }

        public string _mxid
		{
			get;
			set;
		}

		public string Address
		{
			get;
			set;
		}

		public chatModel AdminchatList
		{
			get;
			set;
		}

		public string AdminUserName
		{
			get;
			set;
		}

		public List<Appontments> appointmentList
		{
			get;
			set;
		}

		public string aptName
		{
			get;
			set;
		}

		public string aptNumber
		{
			get;
			set;
		}

		public bool block
		{
			get;
			set;
		}

		public string blockResaon
		{
			get;
			set;
		}

		public bool canEdit
		{
			get;
			set;
		}

		public int chatDivId
		{
			get;
			set;
		}

		public chatModel chatList
		{
			get;
			set;
		}

		public int clientId
		{
			get;
			set;
		}

		public List<californiaspa.Models.Account.Reviews> ClientReviews
		{
			get;
			set;
		}

		public int counter
		{
			get;
			set;
		}

		public string crossstreetName
		{
			get;
			set;
		}

		public string emailAddress
		{
			get;
			set;
		}

		public string emailVerification
		{
			get;
			set;
		}

		public string floorNumber
		{
			get;
			set;
		}

		public string hotelName
		{
			get;
			set;
		}

		public string houseNumber
		{
			get;
			set;
		}

		public int Id
		{
			get;
			set;
		}

		public string landmark
		{
			get;
			set;
		}

		public string localityArea
		{
			get;
			set;
		}

		public int locationtypeId
		{
			get;
			set;
		}

		public string ltd
		{
			get;
			set;
		}

		public string lth
		{
			get;
			set;
		}

		public string Map
		{
			get;
			set;
		}

		public int MassagesCount
		{
			get;
			set;
		}

		public string mobile
		{
			get;
			set;
		}

		public string mobileVerification
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string password
		{
			get;
			set;
		}

		public List<Appontments> pendingAppointment
		{
			get;
			set;
		}

		public double rating
		{
			get;
			set;
		}

		public List<californiaspa.Models.Account.Reviews> Reviews
		{
			get;
			set;
		}

		public string roomNumber
		{
			get;
			set;
		}

		public string searchtoken
		{
			get;
			set;
		}

		public bool serviceTaken
		{
			get;
			set;
		}

		public string spahotelAddress
		{
			get;
			set;
		}

		public int SpaHotelId
		{
			get;
			set;
		}

		public string spahotelname
		{
			get;
			set;
		}

		public string streetName
		{
			get;
			set;
		}

		public string Tharepisttried
		{
			get;
			set;
		}

		public string userName
		{
			get;
			set;
		}

		public bool Verifiedbymg
		{
			get;
			set;
		}

		public AccountModel()
		{
		}
	}
}