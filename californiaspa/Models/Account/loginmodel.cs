using californiaspa.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Account
{
	public class loginmodel : ErrorModel
	{
		[DataType(DataType.Password)]
		[Display(Name="Password")]
		[RegularExpression("^[a-zA-Z0-9]+$", ErrorMessage="special characters are not allowed")]
		[Required(ErrorMessage="Please Enter Password.", AllowEmptyStrings=false)]
		public string password
		{
			get;
			set;
		}

		public string returnUrl
		{
			get;
			set;
		}

         public string username
		{
			get;
			set;
		}
         
	}
}