using californiaspa.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Account
{
    public class RegisterModel : ErrorModel
    {
        [Required(ErrorMessage = "Your must provide a Code")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Country code must be numeric ")]
        public string CountryCode
        {
            get;
            set;
        }
        public string _exid
        {
            get;
            set;
        }
        public string usertype
        {
            get;
            set;
        }
        public string photoidurl
        {
            get;
            set;
        }

        public string _mxid
        {
            get;
            set;
        }

        public string AptName
        {
            get;
            set;
        }

        public string AptNumber
        {
            get;
            set;
        }

        public bool block
        {
            get;
            set;
        }

        public string blockReason
        {
            get;
            set;
        }

        public string CrossStreetName
        {
            get;
            set;
        }

        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string email
        {
            get;
            set;
        }

        public string emailVerification
        {
            get;
            set;
        }

        public string FloorNumber
        {
            get;
            set;
        }

        public string hearAbout
        {
            get;
            set;
        }

        public string HotelName
        {
            get;
            set;
        }

        public string HouseNumber
        {
            get;
            set;
        }

        public string Landmark
        {
            get;
            set;
        }

        public string LocalityArea
        {
            get;
            set;
        }

        public int locationtypeId
        {
            get;
            set;
        }

        public string ltd
        {
            get;
            set;
        }

        public string lth
        {
            get;
            set;
        }

        public string Map
        {
            get;
            set;
        }

        public bool mngrVerfiy
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Your must provide a Mobile Number")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Mobile Number must be numeric ")]
        [DataType(DataType.PhoneNumber)]
        //TODO - JP Add below attribute to mobile field
        [UIHint("Mobile")]
        public string mobile
        {
            get;
            set;
        }

        public string mobileVerification
        {
            get;
            set;
        }

        public string name
        {
            get;
            set;
        }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [RegularExpression("^[a-zA-Z0-9]+$", ErrorMessage = "special characters are not allowed")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string password
        {
            get;
            set;
        }

        public string RoomNumber
        {
            get;
            set;
        }

        public string searchBy
        {
            get;
            set;
        }

        public string searchItem
        {
            get;
            set;
        }

        public string spahotelAddress
        {
            get;
            set;
        }

        public int SpaHotelId
        {
            get;
            set;
        }

        public string spahotelname
        {
            get;
            set;
        }

        public string StreetName
        {
            get;
            set;
        }

        public string userName
        {
            get;
            set;
        }

        [Compare("password", ErrorMessage = "The password and verify password does not match")]
        [DataType(DataType.Password)]
        [RegularExpression("^[a-zA-Z0-9]+$", ErrorMessage = "special characters are not allowed")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string verifypassword
        {
            get;
            set;
        }

        public RegisterModel()
        {
        }
    }


    public class otpmodel : ErrorModel
    {
        public int searchtype { get; set; }
        public string searchvalue { get; set; }
        public List<otpnumbers> body { get; set; }
    }
    public class otpnumbers
    {
       
        public string phone { get; set; }
        public string body { get; set; }
    }
}