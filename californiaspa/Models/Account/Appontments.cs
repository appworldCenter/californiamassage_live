using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Account
{
	public class Appontments
	{
		public string address
		{
			get;
			set;
		}
        public int srno { get; set; }
        public decimal amount
		{
			get;
			set;
		}

		public string comments
		{
			get;
			set;
		}

		public string date
		{
			get;
			set;
		}

		public int Id
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string time
		{
			get;
			set;
		}

		public string tltd
		{
			get;
			set;
		}

		public string tlth
		{
			get;
			set;
		}

		public string token
		{
			get;
			set;
		}

		public string tokenId
		{
			get;
			set;
		}
        public string uname
        {
            get;
            set;
        }

    }
}