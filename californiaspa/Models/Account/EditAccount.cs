using californiaspa.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Account
{
	public class EditAccount : ErrorModel
	{
        [Required(ErrorMessage = "Your must provide a Code")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Country code must be numeric ")]
        public string CountryCode
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Your must provide a E-mail Address")]
         [DataType(DataType.EmailAddress)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string newEmailId
		{
			get;
			set;
		}

        [Required(ErrorMessage = "Your must provide a Mobile Number")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Mobile Number must be numeric")]
        [DataType(DataType.PhoneNumber)]

        //TODO - JP add below attribute to newUsername
        [UIHint("Mobile")]
        public string newUserName
		{
			get;
			set;
		}

		public string OldEmailId
		{
			get;
			set;
		}

		public string OldUserName
		{
			get;
			set;
		} 
	}
}