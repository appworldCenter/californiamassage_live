using californiaspa.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Account
{
	public class TRegisterModel : ErrorModel
	{
		public string _exid
		{
			get;
			set;
		}

		public string _mxid
		{
			get;
			set;
		}

		public int age
		{
			get;
			set;
		}

		public string email
		{
			get;
			set;
		}

		public string emailVerification
		{
			get;
			set;
		}

		public string imagePath
		{
			get;
			set;
		}

		public string languages
		{
			get;
			set;
		}

		public string location
		{
			get;
			set;
		}

		public string massageStyle
		{
			get;
			set;
		}

		public string mobile
		{
			get;
			set;
		}

		public string mobileVerification
		{
			get;
			set;
		}

		public string name
		{
			get;
			set;
		}

		public List<images> profileImages
		{
			get;
			set;
		}

		public decimal rate
		{
			get;
			set;
		}

		public string token
		{
			get;
			set;
		}

		public string userName
		{
			get;
			set;
		}

		public TRegisterModel()
		{
		}
	}
}