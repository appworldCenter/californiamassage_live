using californiaspa.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Account
{
	public class PasswordModel : ErrorModel
	{
		public string confirmPassword
		{
			get;
			set;
		}

        [DataType(DataType.Password)]
        [RegularExpression("^[a-zA-Z0-9]+$", ErrorMessage = "special characters are not allowed")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
public string NewPassword
		{
			get;
			set;
		}

		public string OldPassword
		{
			get;
			set;
		}

		public PasswordModel()
		{
		}
	}
}