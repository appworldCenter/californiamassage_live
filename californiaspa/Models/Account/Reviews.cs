using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Account
{
	public class Reviews
	{
		public string Name
		{
			get;
			set;
		}

		public int rating
		{
			get;
			set;
		}

		public string Review
		{
			get;
			set;
		}

		public string tokenId
		{
			get;
			set;
		}

        public int srno
        {
            get;
            set;
        }
    }
}