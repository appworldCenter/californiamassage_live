using californiaspa.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.booking
{
	public class ClientSurveymodel : ErrorModel
	{
		public int AID
		{
			get;
			set;
		}

		public string appointment_date
		{
			get;
			set;
		}

		public string appointment_time
		{
			get;
			set;
		}

		public string Pay
		{
			get;
			set;
		}

		public bool pendingServey
		{
			get;
			set;
		}

		[Required(ErrorMessage="Was Clients address location given accurate?")]
		public string Q1
		{
			get;
			set;
		}

		public string Q10
		{
			get;
			set;
		}

		public string Q10Comment
		{
			get;
			set;
		}

		public string Q11
		{
			get;
			set;
		}

		public string Q11Comment
		{
			get;
			set;
		}

		public string Q12
		{
			get;
			set;
		}

		public string Q12Comment
		{
			get;
			set;
		}

		public string Q13
		{
			get;
			set;
		}

		public string Q13Comment
		{
			get;
			set;
		}

		public string Q14
		{
			get;
			set;
		}

		public string Q14Comment
		{
			get;
			set;
		}

		public string Q1Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Did client delay start of session ?")]
		public string Q2
		{
			get;
			set;
		}

		public string Q2Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Was Clients Location reasonably clean?")]
		public string Q3
		{
			get;
			set;
		}

		public string Q3Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Was Clients neighbourhood safe?")]
		public string Q4
		{
			get;
			set;
		}

		public string Q4Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Was client polite and respectful?")]
		public string Q5
		{
			get;
			set;
		}

		public string Q5Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Did client tip you reasonably?")]
		public string Q6
		{
			get;
			set;
		}

		public string Q6Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Would you like to repeat the client in future?")]
		public string Q7
		{
			get;
			set;
		}

		public string Q7Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Did he ask for your number or contact info or if you could come direct?")]
		public string Q8
		{
			get;
			set;
		}

		public string Q8Comment
		{
			get;
			set;
		}

		public string Q9
		{
			get;
			set;
		}

		public string Q9Comment
		{
			get;
			set;
		}

		public int rating
		{
			get;
			set;
		}

		public string therapistName
		{
			get;
			set;
		}

		public string tname
		{
			get;
			set;
		}

		public string token
		{
			get;
			set;
		}

		public ClientSurveymodel()
		{
		}
	}
}