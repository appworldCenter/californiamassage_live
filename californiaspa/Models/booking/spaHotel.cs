using californiaspa.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.booking
{
	public class spaHotel : ErrorModel
	{
		public List<spaHotelItem> _hotel
		{
			get;
			set;
		}

		public bool statuspermission
		{
            get;
            set;
        }
	}


    public class hotelterm
    {
        public string name { get; set; }
        public string des { get; set; }
        public int type { get; set; }
    }
}