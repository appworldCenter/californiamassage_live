using californiaspa.Models;
using californiaspa.Models.therapist;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.booking
{
	public class bookingModel : ErrorModel
	{
		public int AID
		{
			get;
			set;
		}

		public string ApartmentName
		{
			get;
			set;
		}

		public string ApartmentNUmber
		{
			get;
			set;
		}

		public string AptcrossStreetName
		{
			get;
			set;
		}

		public string AptFloorNUmber
		{
			get;
			set;
		}

		public string AptlacalityArea
		{
			get;
			set;
		}

		public string Aptlandmark
		{
			get;
			set;
		}

		public string Aptstreetname
		{
			get;
			set;
		}

		public string bookingdate
		{
			get;
			set;
		}

		public string color
		{
			get;
			set;
		}

        [Required(AllowEmptyStrings =false,ErrorMessage = "Custom Message is required")]
        public string CustomeMessage
		{
			get;
			set;
		}

		public string EArrivalTime
		{
			get;
			set;
		}

		public List<imagesItem> hImages
		{
			get;
			set;
		}

		public string HotalName
		{
			get;
			set;
		}

		public string HotelcrossStreetName
		{
			get;
			set;
		}

		public string HotellacalityArea
		{
			get;
			set;
		}

		public string Hotellandmark
		{
			get;
			set;
		}

		public string HotelRoomNUmber
		{
			get;
			set;
		}

		public spaHotel hotels
		{
			get;
			set;
		}

		public string HotelStreetname
		{
			get;
			set;
		}

		public string housecrossStreetName
		{
			get;
			set;
		}

		public string houseFlatNUmber
		{
			get;
			set;
		}

		public string houseFloorNUmber
		{
			get;
			set;
		}

		public string houselacalityArea
		{
			get;
			set;
		}

		public string houselandmark
		{
			get;
			set;
		}

		public string houseNUmber
		{
			get;
			set;
		}

		public string housestreetname
		{
			get;
			set;
		}

		public List<imagesItem> images
		{
			get;
			set;
		}

		public bool isNew
		{
			get;
			set;
		}

		public string LArrivalTime
		{
			get;
			set;
		}

		public string licenceNumber
		{
			get;
			set;
		}

		public string loc
		{
			get;
			set;
		}

		public string locPick
		{
			get;
			set;
		}

		public string ltd
		{
			get;
			set;
		}

		public string lth
		{
			get;
			set;
		}

		public string make
		{
			get;
			set;
		}

		public string ModelNumber
		{
			get;
			set;
		}

		public string pickUpTime
		{
			get;
			set;
		}

		public therapistprofile profile
		{
			get;
			set;
		}

		public int spaHotelId
		{
			get;
			set;
		}

		public string SpaMap
		{
			get;
			set;
		}

		public string vehicalType
		{
			get;
			set;
		}

		public bookingModel()
		{
		}
	}
}