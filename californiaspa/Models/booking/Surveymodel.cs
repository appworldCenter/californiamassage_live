using californiaspa.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.booking
{
	public class Surveymodel : ErrorModel
	{
		public int AID
		{
			get;
			set;
		}

		public string appointment_date
		{
			get;
			set;
		}

		public string appointment_time
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please fill Total paid including tips?")]
		public string Pay
		{
			get;
			set;
		}

		public bool pendingServey
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please choose Did she come on time?")]
		public string Q1
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please Choose Cover all areas?")]
		public string Q10
		{
			get;
			set;
		}

		public string Q10Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please Choose Ask if you wanted any area repeated?")]
		public string Q11
		{
			get;
			set;
		}

		public string Q11Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please Choose Any suggestions for Improvement?")]
		public string Q12
		{
			get;
			set;
		}

		public string Q12Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Did you like Oil therapist used?")]
		public string Q13
		{
			get;
			set;
		}

		public string Q13Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Was therapist on phone a lot?")]
		public string Q14
		{
			get;
			set;
		}

		public string Q14Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please enter feedback summary")]
		public string Q15Comment
		{
			get;
			set;
		}

		public string Q1Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please choose Greet you ?")]
		public string Q2
		{
			get;
			set;
		}

		public string Q2Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please Choose Asked how you were Doing?")]
		public string Q3
		{
			get;
			set;
		}

		public string Q3Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please Choose Asked if you had any ache or pains?")]
		public string Q4
		{
			get;
			set;
		}

		public string Q4Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please Choose Inquire if pressure was OK?")]
		public string Q5
		{
			get;
			set;
		}

		public string Q5Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please Choose Play relaxing Music?")]
		public string Q6
		{
			get;
			set;
		}

		public string Q6Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please Choose Burn Incense?")]
		public string Q7
		{
			get;
			set;
		}

		public string Q7Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please Choose Use Ayurvedic Ointment for Pains?")]
		public string Q8
		{
			get;
			set;
		}

		public string Q8Comment
		{
			get;
			set;
		}

		[Required(ErrorMessage="Please Choose Do full 90 mins?")]
		public string Q9
		{
			get;
			set;
		}

		public string Q9Comment
		{
			get;
			set;
		}

		public int rating
		{
			get;
			set;
		}

		public string therapistName
		{
			get;
			set;
		}

		public string tname
		{
			get;
			set;
		}

		public string token
		{
			get;
			set;
		}

		public Surveymodel()
		{
		}
	}
}