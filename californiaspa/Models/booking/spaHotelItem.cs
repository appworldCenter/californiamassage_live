using californiaspa.Models;
using californiaspa.Models.therapist;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.booking
{
	public class spaHotelItem : ErrorModel
	{
		public string address
		{
			get;
			set;
		}

		public bool availfornew
		{
			get;
			set;
		}

		public string descriptio
		{
			get;
			set;
		}

        public string bookingdescriptio
        {
            get;
            set;
        }

        public string EndTime
		{
			get;
			set;
		}

		public List<imagesItem> hImages
		{
			get;
			set;
		}

		public int hotalId
		{
			get;
			set;
		}

		public string ImgPath
		{
			get;
			set;
		}

		public string isshow
		{
			get;
			set;
		}

		public string Map
		{
			get;
			set;
		}

		public string name
		{
			get;
			set;
		}

		public string paymentType
		{
			get;
			set;
		}

		public string rate
		{
			get;
			set;
		}

		public string startTime
		{
			get;
			set;
		}

		public string todayavailabe
		{
			get;
			set;
		}

		public string token
		{
			get;
			set;
		}

		public string WebPath
		{
			get;
			set;
		}

        public int type
        {
            get;
            set;
        }
    }
}