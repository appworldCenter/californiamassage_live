using californiaspa.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Appointment
{
    public class AppointmentModel_item : ErrorModel
    {
        public string address
        {
            get;
            set;
        }

        public string locationurl
        {
            get;
            set;
        }

        public int Aid
        {
            get;
            set;
        }

        public int SpaHotelid
        {
            get;
            set;
        }

        public decimal amount
        {
            get;
            set;
        }

        public List<AppointmentModel_item> appt
        {
            get;
            set;
        }

        public string appTime
        {
            get;
            set;
        }

        public string aptName
        {
            get;
            set;
        }

        public string aptNumber
        {
            get;
            set;
        }

        public string arrivalTime
        {
            get;
            set;
        }

        public int ClientId
        {
            get;
            set;
        }

        public string ClientMobile
        {
            get;
            set;
        }

        public string ClientName
        {
            get;
            set;
        }

        public string ClientPickUptime
        {
            get;
            set;
        }

        public string comments
        {
            get;
            set;
        }

        public string createdDate
        {
            get;
            set;
        }

        public string crossstreetName
        {
            get;
            set;
        }

        public string custommessage
        {
            get;
            set;
        }

        public string finishTime
        {
            get;
            set;
        }

        public string floorNumber
        {
            get;
            set;
        }

        public string hotelName
        {
            get;
            set;
        }

        public string houseNumber
        {
            get;
            set;
        }

        public bool isLeftCabTime
        {
            get;
            set;
        }

        public bool isstartmassage
        {
            get;
            set;
        }

        public string landmark
        {
            get;
            set;
        }

        public string localityArea
        {
            get;
            set;
        }

        public string location
        {
            get;
            set;
        }

        public string ltd
        {
            get;
            set;
        }

        public string lth
        {
            get;
            set;
        }

        public string pickdrop
        {
            get;
            set;
        }

        public string rate
        {
            get;
            set;
        }

        public string roomNumber
        {
            get;
            set;
        }

        public string SpaMap
        {
            get;
            set;
        }

        public string startDate
        {
            get;
            set;
        }

        public string startTime
        {
            get;
            set;
        }

        public string status
        {
            get;
            set;
        }

        public string streetName
        {
            get;
            set;
        }

        public string therapistMobile
        {
            get;
            set;
        }

        public string therapistName
        {
            get;
            set;
        }

        public string therapistTime
        {
            get;
            set;
        }

        public int thid
        {
            get;
            set;
        }

        public string tltd
        {
            get;
            set;
        }

        public int srno
        {
            get;
            set;
        }

        public string tlth
        {
            get;
            set;
        }

        public string token
        {
            get;
            set;
        }

        public string tokenId
        {
            get;
            set;
        }

        public int TotalVaritaion
        {
            get;
            set;
        }

        public string variation
        {
            get;
            set;
        }

        public VehicalModel vehical
        {
            get;
            set;
        }
    }
}