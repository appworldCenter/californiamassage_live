using californiaspa.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Appointment
{
	public class AppointmentModel : ErrorModel
	{
		public List<AppointmentModel_item> appt
		{
			get;
			set;
		}

		public List<AppointmentModel_item> lstPunctualityReports
		{
			get;
			set;
		} 
	}
}