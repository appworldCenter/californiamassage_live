using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Appointment
{
	public class VehicalModel
	{
		public string vcolor
		{
			get;
			set;
		}

		public string vlicenceNumber
		{
			get;
			set;
		}

		public string vmake
		{
			get;
			set;
		}

		public string vmodelNumber
		{
			get;
			set;
		}

		public string vtype
		{
			get;
			set;
		}

		public VehicalModel()
		{
		}
	}
}