using californiaspa.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Chat
{
	public class SummaryModel : ErrorModel
	{
		public List<SummaryViewModel> _list
		{
			get;
			set;
		}

		public List<SummaryListModel> _Summarylist
		{
			get;
			set;
		}

		public SummaryModel()
		{
		}
	}
}