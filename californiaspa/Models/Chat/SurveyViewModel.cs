using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Chat
{
	public class SurveyViewModel
	{
		public string bookingDate
		{
			get;
			set;
		}

        public int id
        {
            get;
            set;
        }
        public string ClientName
		{
			get;
			set;
		}

		public string comments
		{
			get;
			set;
		}

		public string feedbackDate
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public int rating
		{
			get;
			set;
		}

		public string token
		{
			get;
			set;
		}

		public int Total
		{
			get;
			set;
		}

		public SurveyViewModel()
		{
		}
	}
}