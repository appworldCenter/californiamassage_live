using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Chat
{
	public class SummaryViewModel
	{
		public double Amount
		{
			get;
			set;
		}

		public string bookingDate
		{
			get;
			set;
		}
        public DateTime bookingDate_2 { get; set; }

        public string bookingtime
		{
			get;
			set;
		}

		public int clientId
		{
			get;
			set;
		}

		public string ClientName
		{
			get;
			set;
		}

		public double DiscountAmount
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public double rate
		{
			get;
			set;
		}

		public int therapistId
		{
			get;
			set;
		}

		public int Total
		{
			get;
			set;
		}

		public string totalamountPerClient
		{
			get;
			set;
		}

		public double totalAmountpertherapist
		{
			get;
			set;
		}

		public int totalMassage
		{
			get;
			set;
		} 
	}
}