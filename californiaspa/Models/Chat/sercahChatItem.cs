using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Chat
{
	public class sercahChatItem
	{
		public int Clientid
		{
			get;
			set;
		}

		public string date
		{
			get;
			set;
		}

		public string message
		{
			get;
			set;
		}

		public int messageId
		{
			get;
			set;
		}

		public string name
		{
			get;
			set;
		}

		public string time
		{
			get;
			set;
		}

		public sercahChatItem()
		{
		}
	}
}