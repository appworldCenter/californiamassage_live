using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Chat
{
	public class AppointementReminder
	{
		public string _ctoken
		{
			get;
			set;
		}

		public string _ttoken
		{
			get;
			set;
		}

		public string apptCreatedTime
		{
			get;
			set;
		}

		public int ClientId
		{
			get;
			set;
		}

		public string ClientName
		{
			get;
			set;
		}

		public string message
		{
			get;
			set;
		}

		public int MessageId
		{
			get;
			set;
		}

        public int srno
        {
            get;
            set;
        }


        public string sendTime
		{
			get;
			set;
		}

		public int TherapistId
		{
			get;
			set;
		}

		public string TherapistName
		{
			get;
			set;
		}

		public AppointementReminder()
		{
		}
	}
}