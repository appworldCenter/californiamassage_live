using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Chat
{
	public class TherapistChat
	{
		public chatModel _AdminChatlist
		{
			get;
			set;
		}

		public chatModel _TherapistChatlist
		{
			get;
			set;
		}

		public int chatDivId
		{
			get;
			set;
		}

		public string searchToken
		{
			get;
			set;
		}

		public int therapistId
		{
			get;
			set;
		}

		public TherapistChat()
		{
		}
	}
}