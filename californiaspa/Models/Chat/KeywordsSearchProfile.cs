using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Chat
{
	public class KeywordsSearchProfile
	{
		public string Admin
		{
			get;
			set;
		}

		public string client
		{
			get;
			set;
		}

		public int clientId
		{
			get;
			set;
		}

		public string date
		{
			get;
			set;
		}

		public string Message
		{
			get;
			set;
		}

		public int Sender
		{
			get;
			set;
		}

		public string therapist
		{
			get;
			set;
		}

		public int therapistId
		{
			get;
			set;
		}

		public string time
		{
			get;
			set;
		}

		public int total
		{
			get;
			set;
		}

		public KeywordsSearchProfile()
		{
		}
	}
}