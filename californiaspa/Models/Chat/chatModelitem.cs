using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Chat
{
	public class chatModelitem
	{
		public string _ctoken
		{
			get;
			set;
		}

		public string _ttoken
		{
			get;
			set;
		}

		public int ClientId
		{
			get;
			set;
		}

		public string ClientName
		{
			get;
			set;
		}

		public string CreatedDate
		{
			get;
			set;
		}

		public string LastMassage
		{
			get;
			set;
		}

		public int MessageId
		{
			get;
			set;
		}

		public int SenderId
		{
			get;
			set;
		}

		public int TherapistId
		{
			get;
			set;
		}

		public string TherapistName
		{
			get;
			set;
		}

		public string time
		{
			get;
			set;
		}

		public chatModelitem()
		{
		}
	}
}