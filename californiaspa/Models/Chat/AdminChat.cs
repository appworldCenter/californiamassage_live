using californiaspa.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Chat
{
	public class AdminChat : ErrorModel
	{
		public chatModel _ClientChatlist
		{
			get;
			set;
		}

		public profileList _clientProfiles
		{
			get;
			set;
		}

		public chatModel _ClientTherpistChat
		{
			get;
			set;
		}

		public List<KeywordsSearchProfile> _keywordSearch
		{
			get;
			set;
		}

		public chatModel _TherapistChatlist
		{
			get;
			set;
		}

		public profileList _therapistProfiles
		{
			get;
			set;
		}

		public string ClientsearchToken
		{
			get;
			set;
		}

		public string GLobalsearchToken
		{
			get;
			set;
		}

		public string TherapistsearchToken
		{
			get;
			set;
		}

		public AdminChat()
		{
		}
	}
}