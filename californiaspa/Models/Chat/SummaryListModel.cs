using californiaspa.Models;
using System;
using System.Runtime.CompilerServices;

namespace californiaspa.Models.Chat
{
	public class SummaryListModel : ErrorModel
	{
		public double Amount
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string tokenId
		{
			get;
			set;
		}

		public int Total
		{
			get;
			set;
		}

		public SummaryListModel()
		{
		}
	}
}