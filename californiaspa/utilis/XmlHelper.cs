using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace californiaspa.utilis
{
    public class XmlHelper
    { 
        public bool allowTocommunicate(int thid)
        {
            bool flag = false;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            { string str = "";
                    string str1 = "";
                    string[] strArrays = new string[5];
                    string[] strArrays1 = new string[5];
                    sortedLists.Add("@id", thid);
                    DataTable dataTable = sqlHelper.fillDataTable("GetcallScheduleByTherapistId", "", sortedLists);
                    if (dataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            str = dataTable.Rows[0]["Starttime"].ToString();
                            str1 = dataTable.Rows[0]["EndTime"].ToString();
                            strArrays = str.Split(new char[] { ':' });
                            str = (strArrays[0].Length < 2 ? string.Concat("0", strArrays[0], ":", strArrays[1]) : string.Concat(strArrays[0], ":", strArrays[1]));
                            strArrays1 = str1.Split(new char[] { ':' });
                            str1 = (strArrays1[0].Length < 2 ? string.Concat("0", strArrays1[0], ":", strArrays1[1]) : string.Concat(strArrays1[0], ":", strArrays1[1]));
                        }
                    }
                    dataTable.Dispose();
                    DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                    string str2 = string.Concat(new object[] { currentTimeByTimeZone.Month, "/", currentTimeByTimeZone.Day, "/", currentTimeByTimeZone.Year });
                    DateTime dateTime = Convert.ToDateTime(string.Concat(str2, string.Concat(" ", str)));
                    DateTime dateTime1 = Convert.ToDateTime(string.Concat(str2, string.Concat(" ", str1)));
                    if (currentTimeByTimeZone > dateTime && currentTimeByTimeZone < dateTime1)
                    {
                        flag = true;
                    }
                }
                catch (Exception exception)
                {
                     string str3 = string.Format("{0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.FullName, MethodBase.GetCurrentMethod().Name);
                    (new Logger(exception.Message, this.GetType().Namespace, str3, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return flag;
        }

        public bool allowTocommunicate(int thid, ref DateTime dateTime)
        {
            bool flag = false;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                string str = "";
                string str1 = "";
                string[] strArrays = new string[5];
                string[] strArrays1 = new string[5];
                sortedLists.Add("@id", thid);
                DataTable dataTable = sqlHelper.fillDataTable("GetcallScheduleByTherapistId_v2", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        str = dataTable.Rows[0]["Starttime"].ToString();
                        str1 = dataTable.Rows[0]["EndTime"].ToString();
                        strArrays = str.Split(new char[] { ':' });
                        str = (strArrays[0].Length < 2 ? string.Concat("0", strArrays[0], ":", strArrays[1]) : string.Concat(strArrays[0], ":", strArrays[1]));
                        strArrays1 = str1.Split(new char[] { ':' });
                        str1 = (strArrays1[0].Length < 2 ? string.Concat("0", strArrays1[0], ":", strArrays1[1]) : string.Concat(strArrays1[0], ":", strArrays1[1]));
                    }
                }
                dataTable.Dispose();
                DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                string str2 = string.Concat(new object[] { currentTimeByTimeZone.Month, "/", currentTimeByTimeZone.Day, "/", currentTimeByTimeZone.Year });
                dateTime = Convert.ToDateTime(string.Concat(str2, string.Concat(" ", str)));
                DateTime dateTime1 = Convert.ToDateTime(string.Concat(str2, string.Concat(" ", str1)));
                if (currentTimeByTimeZone > dateTime && currentTimeByTimeZone < dateTime1)
                {
                    flag = true;
                }
            }
            catch (Exception exception)
            {
                string str3 = string.Format("{0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.FullName, MethodBase.GetCurrentMethod().Name);
                (new Logger(exception.Message, this.GetType().Namespace, str3, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return flag;
        }

        public void CreateChatRequestCheckerXML(string value)
        {
            Task.Factory.StartNew(() => this.updateVal("ChatRequestChecker", value));
        }

        public void CreateChatRequestCheckerXML1(string value)
        {
            Task.Factory.StartNew(() => this.updateVal("ChatRequestChecker1", value));
        }

        public void CreateRequestCheckerXML(string value)
        {
            Task.Factory.StartNew(() => this.updateVal("RequestChecker", value));
        }

        public void CreateRequestCheckerXML1(string value)
        {
            Task.Factory.StartNew(() => this.updateVal("RequestChecker1", value));
        }

        public void CreateSmsSenderXML(string value)
        {
            Task.Factory.StartNew(() => this.updateVal("SmsChecker", value));
        }

        public void CreatetherapistRequestCheckerXML1(string value)
        {
            Task.Factory.StartNew(() => this.updateVal("therapistchat", value));
        }

        public void CreatetherapistRequestCheckerXML2(string value)
        {
            Task.Factory.StartNew(() => this.updateVal("therapistchat1", value));
        }

        public string readChatRequestCheckerXML()
        {
            return this.readVal("ChatRequestChecker");
        }

        public string readChatRequestCheckerXML1()
        {
            return this.readVal("ChatRequestChecker1");
        }

        public string readRequestCheckerXML()
        {
            return this.readVal("RequestChecker");
        }

        public string readRequestCheckerXML1()
        {
            return this.readVal("RequestChecker1");
        }

        public string readSMSSenderXML()
        {
            return this.readVal("SmsChecker");
        }

        public string readtherapistRequestCheckerXML1()
        {
            return this.readVal("therapistchat");
        }

        public string readtherapistRequestCheckerXML2()
        {
            return this.readVal("therapistchat1");
        }

        private string readVal(string name)
        {
            string str = "";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            { 
                    str = sqlHelper.executeScaler(string.Concat("Select top(1)[value] as val from  tblxmlChaecker WITH(NOLOCK) Where Name='", name, "'")).ToString();
                }
                catch (Exception exception)
                {
                     string str1 = string.Format("{0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.FullName, MethodBase.GetCurrentMethod().Name);
                    (new Logger(exception.Message, this.GetType().Namespace, str1, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                    str = string.Concat("Error :", exception.Message);
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        private void updateVal(string name, string _val)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            { sortedLists.Add("@name", name);
                    sortedLists.Add("@val", _val);
                    sqlHelper.executeNonQuery("UpdatecheckerVal", "", sortedLists);
                }
                catch (Exception exception)
                {
                     string str = string.Format("{0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.FullName, MethodBase.GetCurrentMethod().Name);
                    (new Logger(exception.Message, this.GetType().Namespace, str, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
        }
    }
}