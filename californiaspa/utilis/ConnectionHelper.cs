using System;
using System.Configuration;
using System.Data.SqlClient;

namespace californiaspa.utilis
{
    public class ConnectionHelper
    {
        public SqlConnection con; 
        public ConnectionHelper()
        {
           con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ToString());
        }
    }
}