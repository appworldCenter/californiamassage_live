using System;
using System.Runtime.CompilerServices;

namespace californiaspa.utilis
{
    public static class DateTimeExtensions
    {
        public static DateTime staticdatetime =Convert.ToDateTime("04/13/2020");
        public static DateTime GetCurrentTimeByTimeZone(this DateTime _time, string Timezone = "India Standard Time")
        {
            if (_time > staticdatetime)
            {
                if (Timezone == "Pacific Standard Time")
                {
                    Timezone = "UTC";
                    _time = _time.AddMinutes(4);
                }
               
            }
            return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow.AddMinutes(4), TimeZoneInfo.FindSystemTimeZoneById(Timezone));
        }

        public static DateTime GetIndianTimeFromPST(this DateTime _date)
        {
            string Timezone = "Pacific Standard Time";
            if (_date > staticdatetime)
            {
                Timezone = "UTC";
                _date = _date.AddMinutes(4);
            }
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_date, Timezone, "India Standard Time");
        }

        public static DateTime GetPSTFromIndianTime(this DateTime _date)
        {
            string Timezone = "Pacific Standard Time";
            if (_date > staticdatetime)
            {
                Timezone = "UTC";
                _date = _date.AddMinutes(4);
            }
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_date, "India Standard Time", Timezone);
        }

        public static string GetMonth(string date, char splitcharcter, int dateposition, int monthposition, int yearposition, bool withzero = false)
        {
            var dates = date.Split(splitcharcter);
            string monthName = dates[monthposition];
            string num = "0";
            switch (monthName.ToUpper())
            {
                case "JAN":
                    {
                        num = withzero ? num + "1" : "1";
                        break;
                    }
                case "FEB":
                    {
                        num = withzero ? num + "2" : "2";
                        break;
                    }
                case "MAR":
                    {
                        num = withzero ? num + "3" : "3";
                        break;
                    }
                case "APR":
                    {
                        num = withzero ? num + "4" : "4";
                        break;
                    }
                case "MAY":
                    {
                        num = withzero ? num + "5" : "5";
                        break;
                    }
                case "JUN":
                    {
                        num = withzero ? num + "6" : "6";
                        break;
                    }
                case "JUL":
                    {
                        num = withzero ? num + "7" : "7";
                        break;
                    }
                case "AUG":
                    {
                        num = withzero ? num + "8" : "8";
                        break;
                    }
                case "SEP":
                    {
                        num = withzero ? num + "9" : "9";
                        break;
                    }
                case "OCT":
                    {
                        num = "10";
                        break;
                    }
                case "NOV":
                    {
                        num = "11";
                        break;
                    }
                case "DEC":
                    {
                        num = "12";
                        break;
                    }
            }
            num = num + "-" + dates[dateposition] + "-" + dates[yearposition];
            return num;
        }
    }
}