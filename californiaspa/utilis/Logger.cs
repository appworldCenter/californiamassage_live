using System;
using System.Collections;

namespace californiaspa.utilis
{
    public class Logger : IDisposable
    {
        private string message; 
        private string className; 
        private string method; 
        private int personId; 
        private string stackTrace; 
        private Enums.LogType type; 
        public Logger(string _message, string _className, string _method, int _personId, string _stackTrace, Enums.LogType _type)
        {
            this.message = _message;
            this.className = _className;
            this.method = _method;
            this.personId = _personId;
            this.stackTrace = _stackTrace;
            this.type = _type;
        }

        public void Dispose()
        {
            this.message = null;
            this.className = null;
            this.method = null;
            this.stackTrace = null;
        }

        public void LogWrite()
        {
            SqlHelper sqlHelper = new SqlHelper();
            sqlHelper.executeNonQuery("spp_insertlogs", "", new SortedList()
            {
                { "@LogMessage", this.message },
                { "@method", this.method },
                { "@Class", this.className },
                { "@stacktrace", this.stackTrace },
                { "@personid", this.personId },
                { "@LogType", this.type.ToString() }
            });
            sqlHelper.Dispose();
        }
    }
}