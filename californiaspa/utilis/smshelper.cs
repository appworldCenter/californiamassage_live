using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

namespace californiaspa.utilis
{
    public class smshelper
    { 
        public string addOtpMessage(string to, string message)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList()
            {
                { "@mobile", to },
                { "@body", message }
            };
            try
            {
                sqlHelper.executeNonQuery("AddOTPMessage", "", sortedLists);
            }
            catch (Exception exception)
            {
            }
            return Enums.smsStatus.pending.ToString();
        }

        public string GetLinks(string message)
        {
            string value = "";
            foreach (object obj in (new Regex("((https?|ftp|file)\\://|www.)[A-Za-z0-9\\.\\-]+(/[A-Za-z0-9\\?\\&\\=;\\+!'\\(\\)\\*\\-\\._~%]*)*", RegexOptions.IgnoreCase)).Matches(message))
            {
                value = ((Match)obj).Value;
            }
            return value;
        }

        public string SendSms(string to, string message, string from = "+14177089506",string text="")
        {
            message = message.Replace("Appointment Reminder", "Reminder").Replace("Appointment", "appt").Replace("CALIFORNIA SPA", "CLSPA").Replace("confirmed", "confirm").GetNameFromstring();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList()
            {
                { "@mobile", to },
                 { "@text", text },
                { "@body", message }
            };
            try
            {
                sqlHelper.executeNonQuery("AddNewSMS_v2", "", sortedLists);
            }
            catch (Exception exception)
            {
            }
            return Enums.smsStatus.pending.ToString();
        }

        public void SendSMSWith91(string to, string body)
        {
            SendSms(to, body);
        } 
      
    }
}