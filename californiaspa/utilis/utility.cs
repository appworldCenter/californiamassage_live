using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace californiaspa.utilis
{
    public class utility
    {
        public string create_paging(decimal total_records, decimal active, string contollerName, string action, string SearchType, decimal _pagesize)
        {
            decimal num;
            decimal num1 = Math.Ceiling(total_records / _pagesize);
            string str = "<div class='pager skew-25'><ul>";
            if (total_records <= _pagesize)
            {
                return "";
            }
            int num2 = Convert.ToInt32(num1);
            int num3 = 2;
            if (num1 > new decimal(10))
            {
                num2 = 10;
            }
            if (active > new decimal(10))
            {
                num3 = Convert.ToInt32(active) / 10 * 10;
                num2 = ((num1 - num3) > new decimal(10) ? num3 + 10 : Convert.ToInt32(num1));
            }
            if (active < new decimal(2))
            {
                str = string.Concat(str, "<li class='first", (active == decimal.One ? " selected" : ""), "' ><a>First</a></li>");
                str = string.Concat(str, "<li><a>Pre</a></li>");
            }
            else if (SearchType != string.Empty)
            {
                string[] searchType = new string[] { str, "<li class='first", null, null, null, null, null, null, null, null };
                searchType[2] = (active == decimal.One ? " selected" : "");
                searchType[3] = "' ><a class='skew25' href='https://www.californiamassage.in/";
                searchType[4] = contollerName;
                searchType[5] = "/";
                searchType[6] = action;
                searchType[7] = "?uname=";
                searchType[8] = SearchType;
                searchType[9] = "&page=1'>First</a></li>";
                str = string.Concat(searchType);
                string[] strArrays = new string[] { str, "<li  ><a class='skew25' href='https://www.californiamassage.in/", contollerName, "/", action, "?uname=", SearchType, "&page=", null, null };
                num = active - decimal.One;
                strArrays[8] = num.ToString();
                strArrays[9] = "'>Pre</a></li>";
                str = string.Concat(strArrays);
            }
            else
            {
                string[] strArrays1 = new string[] { str, "<li class='first", null, null, null, null, null, null };
                strArrays1[2] = (active == decimal.One ? " selected" : "");
                strArrays1[3] = "' ><a class='skew25' href='https://www.californiamassage.in/";
                strArrays1[4] = contollerName;
                strArrays1[5] = "/";
                strArrays1[6] = action;
                strArrays1[7] = "?page=1'>First</a></li>";
                str = string.Concat(strArrays1);
                string[] str1 = new string[] { str, "<li  ><a class='skew25' href='https://www.californiamassage.in/", contollerName, "/", action, "?page=", null, null };
                num = active - decimal.One;
                str1[6] = num.ToString();
                str1[7] = "'>Pre</a></li>";
                str = string.Concat(str1);
            }
            for (int i = num3; i <= num2; i++)
            {
                if (SearchType != string.Empty)
                {
                    str = string.Concat(new string[] { str, "<li class=", (i == active ? " selected" : ""), "><a class='skew25' href='https://www.californiamassage.in/", contollerName, "/", action, "?uname=", SearchType, "&page=", i.ToString(), "'>", i.ToString(), "</a></li>" });
                }
                else
                {
                    string[] str2 = new string[] { str, "<li class=", null, null, null, null, null, null, null, null, null, null };
                    str2[2] = (i == active ? " selected" : "");
                    str2[3] = "><a class='skew25' href='https://www.californiamassage.in/";
                    str2[4] = contollerName;
                    str2[5] = "/";
                    str2[6] = action;
                    str2[7] = "?page=";
                    str2[8] = i.ToString();
                    str2[9] = "'>";
                    str2[10] = i.ToString();
                    str2[11] = "</a></li>";
                    str = string.Concat(str2);
                }
            }
            if (active == num1)
            {
                str = string.Concat(str, "<li><a>Next</a></li>");
                str = string.Concat(str, "<li class='last", (active == num1 ? " selected" : ""), "'><a>Last</a></li>");
            }
            else if (SearchType != string.Empty)
            {
                string[] searchType1 = new string[] { str, "<li class='", null, null, null, null, null, null, null, null, null, null };
                searchType1[2] = (active == num1 ? " selected" : "");
                searchType1[3] = "'><a class='skew25' href='https://www.californiamassage.in/";
                searchType1[4] = contollerName;
                searchType1[5] = "/";
                searchType1[6] = action;
                searchType1[7] = "?uname=";
                searchType1[8] = SearchType;
                searchType1[9] = "&page=";
                num = active + decimal.One;
                searchType1[10] = num.ToString();
                searchType1[11] = "'>Next</a></li>";
                str = string.Concat(searchType1);
                string[] searchType2 = new string[] { str, "<li class='last", null, null, null, null, null, null, null, null, null, null };
                searchType2[2] = (active == num1 ? " selected" : "");
                searchType2[3] = "'><a class='skew25' href='https://www.californiamassage.in/";
                searchType2[4] = contollerName;
                searchType2[5] = "/";
                searchType2[6] = action;
                searchType2[7] = "?uname=";
                searchType2[8] = SearchType;
                searchType2[9] = "&page=";
                searchType2[10] = num1.ToString();
                searchType2[11] = "'>Last</a></li>";
                str = string.Concat(searchType2);
            }
            else
            {
                string[] strArrays2 = new string[] { str, "<li class='", null, null, null, null, null, null, null, null };
                strArrays2[2] = (active == num1 ? " selected" : "");
                strArrays2[3] = "'><a class='skew25' href='https://www.californiamassage.in/";
                strArrays2[4] = contollerName;
                strArrays2[5] = "/";
                strArrays2[6] = action;
                strArrays2[7] = "?page=";
                num = active + decimal.One;
                strArrays2[8] = num.ToString();
                strArrays2[9] = "'>Next</a></li>";
                str = string.Concat(strArrays2);
                string[] str3 = new string[] { str, "<li class='last", null, null, null, null, null, null, null, null };
                str3[2] = (active == num1 ? " selected" : "");
                str3[3] = "'><a class='skew25' href='https://www.californiamassage.in/";
                str3[4] = contollerName;
                str3[5] = "/";
                str3[6] = action;
                str3[7] = "?page=";
                str3[8] = num1.ToString();
                str3[9] = "'>Last</a></li>";
                str = string.Concat(str3);
            }
            return string.Concat(str, "</ul></div>");
        }

        public string create_paging_ajax(decimal total_records, decimal active, string paging_function)
        {
            decimal num;
            decimal num1 = Math.Ceiling(total_records / new decimal(10));
            string str = "<div class='pager skew-25'><ul>";
            if (total_records <= new decimal(10))
            {
                return "";
            }
            int num2 = Convert.ToInt32(num1);
            int num3 = 2;
            if (num1 > new decimal(10))
            {
                num2 = 10;
            }
            if (active > new decimal(10))
            {
                num3 = Convert.ToInt32(active) / 10 * 10;
                num2 = ((num1 - num3) > new decimal(10) ? num3 + 10 : Convert.ToInt32(num1));
            }
            if (active < new decimal(2))
            {
                str = string.Concat(str, "<li class='first", (active == decimal.One ? " selected" : ""), "' ><a>First</a></li>");
                str = string.Concat(str, "<li><a>Pre</a></li>");
            }
            else
            {
                string[] pagingFunction = new string[] { str, "<li class='first", null, null, null, null };
                pagingFunction[2] = (active == decimal.One ? " selected" : "");
                pagingFunction[3] = "' ><a class='skew25' onclick='";
                pagingFunction[4] = paging_function;
                pagingFunction[5] = "(1)'   href='javascript:;'>First</a></li>";
                str = string.Concat(pagingFunction);
                string[] strArrays = new string[] { str, "<li  ><a class='skew25'  onclick='", paging_function, "(", null, null };
                num = active - decimal.One;
                strArrays[4] = num.ToString();
                strArrays[5] = ")'   href='javascript:;'>Pre</a></li>";
                str = string.Concat(strArrays);
            }
            for (int i = num3; i <= num2; i++)
            {
                string[] pagingFunction1 = new string[] { str, "<li class=", null, null, null, null, null, null, null, null };
                pagingFunction1[2] = (i == active ? " selected" : "");
                pagingFunction1[3] = "><a class='skew25' onclick='";
                pagingFunction1[4] = paging_function;
                pagingFunction1[5] = "(";
                pagingFunction1[6] = i.ToString();
                pagingFunction1[7] = ")' href='javascript:;'>";
                pagingFunction1[8] = i.ToString();
                pagingFunction1[9] = "</a></li>";
                str = string.Concat(pagingFunction1);
            }
            if (active == num1)
            {
                str = string.Concat(str, "<li><a>Next</a></li>");
                str = string.Concat(str, "<li class='last", (active == num1 ? " selected" : ""), "'><a>Last</a></li>");
            }
            else
            {
                string[] str1 = new string[] { str, "<li class='", null, null, null, null, null, null };
                str1[2] = (active == num1 ? " selected" : "");
                str1[3] = "'><a class='skew25' onclick='";
                str1[4] = paging_function;
                str1[5] = "(";
                num = active + decimal.One;
                str1[6] = num.ToString();
                str1[7] = ")' href='javascript:;'>Next</a></li>";
                str = string.Concat(str1);
                object[] objArray = new object[] { str, "<li class='last", null, null, null, null, null, null };
                objArray[2] = (active == num1 ? " selected" : "");
                objArray[3] = "'><a  class='skew25' onclick='";
                objArray[4] = paging_function;
                objArray[5] = "(";
                objArray[6] = num1;
                objArray[7] = ")'  href='javascript:;'>Last</a></li>";
                str = string.Concat(objArray);
            }
            return string.Concat(str, "</ul></div>");
        }

        public string GetRandomalphanumeric(int? charcount, string numerics = "0123456789", string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
        {
            char[] chrArray = new char[4];
            Random random = new Random();
            for (int i = 0; i < (int)chrArray.Length; i++)
            {
                chrArray[i] = numerics[random.Next(numerics.Length)];
            }
            return string.Concat(new string[] { new string(chrArray) });
        }

        public string GetTime(string time)
        {
            string[] strArrays = time.Split(new char[] { ':' });
            int num = Convert.ToInt32(strArrays[0]);
            string str = " AM";
            if (num >= 12)
            {
                num -= 12;
                str = " PM";
            }
            return string.Concat(num.ToString(), ":", strArrays[1].ToString(), str);
        }

        public int ParseMonthToInt(string month)
        {
            return DateTime.ParseExact(month, "MMM", CultureInfo.InvariantCulture).Month;
        }

        public string RemoveSpecialchar(string text)
        {
            return this.RemoveSpecialchar(text, new char[] { '~', '@', '#', '$', '%', '\u005E', '&', '*', '(', ')', '|', '{', '}', '<', '>', '?', '+', '=', '\u005F', '?', '>', '<' });
        }

        public string RemoveSpecialchar(string text, char[] characters = null)
        {
            for (int i = 0; i < (int)characters.Length; i++)
            {
                if (text.Contains(characters[i].ToString()))
                {
                    text = text.Replace(characters[i], ' ');
                }
            }
            return text;
        }

        public string Sendemail(string _to, string _from, string _subject, string _text)
        {
            string str = "email sent";
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.To.Add(new MailAddress(_to));
                mailMessage.From = new MailAddress("noreply@californiamassage.in");
                mailMessage.Subject = _subject;
                mailMessage.IsBodyHtml = true;
                mailMessage.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(_text, null, "text/html"));
                SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587))
                {
                    EnableSsl = false,
                    Credentials = new NetworkCredential("apikey", "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")
                };
                smtpClient.Send(mailMessage);
                mailMessage = null;

            }
            catch (SmtpException smtpException)
            {
                str = string.Concat("Error : ", smtpException.Message);
                (new Logger(str, this.GetType().FullName, smtpException.Source, 0, smtpException.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            catch (WebException webException)
            {
                using (WebResponse response = webException.Response)
                {
                    HttpWebResponse httpWebResponse = (HttpWebResponse)response;
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream))
                        {
                            str = string.Concat("Error : ", streamReader.ReadToEnd());
                        }
                    }
                }
                (new Logger(str, this.GetType().FullName, this.ToString(), 0, webException.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            return str;
        }

        public void sendsms(string to, string body)
        {
            (new smshelper()).SendSms(string.Concat("+91", to), body, "+19282373824");
        }

        public bool iscookieExist(string name)
        {
            bool isexist = false;
            if (HttpContext.Current.Response.Cookies[name] != null)
            {
                isexist = true;
            }
            return isexist;
        }




        public void createCookie(string _encyrptedkey, usertype _type,int time=5)
        {
            string name = "_xid";
            switch (_type)
            {
                case usertype.user:
                    name = "_xid";
                    break;
                case usertype.admin:
                    name = "_yid";
                    break;
                case usertype.therapist:
                    name = "_zid";
                    break;
            }
            HttpCookie httpCookie = new HttpCookie(name, _encyrptedkey)
            {
                Expires = DateTime.Now.AddHours(time)
            };
            HttpContext.Current.Response.Cookies.Add(httpCookie);

        }

        public enum usertype
        {
            user = 1,
            therapist = 2,
            admin = 3
        }

    }
}