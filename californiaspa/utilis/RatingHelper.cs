using System;
using System.Collections;
using System.Data;
using System.Reflection;

namespace californiaspa.utilis
{
    public class RatingHelper
    { 
        public double GetAverageRating(int Thid, ref int count)
        {
            double num = 0;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            { sortedLists.Add("@thid", Thid);
                    DataTable dataTable = sqlHelper.fillDataTable("GetRatinByTherapistid", "", sortedLists);
                    count = dataTable.Rows.Count;
                    if (dataTable.Rows.Count > 0)
                    {
                        int num1 = 0;
                        int num2 = 0;
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            num1 = num1 + Convert.ToInt16(dataTable.Rows[i]["rating"].ToString()) * Convert.ToInt16(dataTable.Rows[i]["RatingCount"].ToString());
                            num2 += Convert.ToInt16(dataTable.Rows[i]["RatingCount"].ToString());
                        }
                        num = Convert.ToDouble(num1) / Convert.ToDouble(num2);
                        num = Math.Round(num, 1, MidpointRounding.AwayFromZero);
                    }
                }
                catch (Exception exception)
                {
                    (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return num;
        }

        public double GetAverageRatingByClientId1(int Clientid, ref int count)
        {
            double num = 0;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                  sortedLists.Add("@ClientId", Clientid);
                    DataTable dataTable = sqlHelper.fillDataTable("GetRatinByClientId1", "", sortedLists);
                    count = dataTable.Rows.Count;
                    if (dataTable.Rows.Count > 0)
                    {
                        int num1 = 0;
                        int num2 = 0;
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            num1 = num1 + Convert.ToInt16(dataTable.Rows[i]["rating"].ToString()) * Convert.ToInt16(dataTable.Rows[i]["RatingCount"].ToString());
                            num2 += Convert.ToInt16(dataTable.Rows[i]["RatingCount"].ToString());
                        }
                        num = Convert.ToDouble(num1) / Convert.ToDouble(num2);
                        num = Math.Round(num, 1, MidpointRounding.AwayFromZero);
                    }
                }
                catch (Exception exception)
                {
                   (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return num;
        }
    }
}