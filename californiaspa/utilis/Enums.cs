using System;

namespace californiaspa.utilis
{
    public class Enums
    {
         public enum AppointmentStatus
        {
            pending,
            confirm
        }

        public enum location
        {
            clientHoues = 1,
            clientHotel = 2,
            ClientApt = 3,
            SpaHotel = 4
        }

        public enum LogType
        {
            Error,
            Info
        }

        public enum smsStatus
        {
            pending,
            sent,
            failed
        }

        public enum transport
        {
            pickAndDrop = 1,
            PayTransport = 2
        }
    }
}