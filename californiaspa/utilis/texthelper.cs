﻿using System;
using System.Runtime.CompilerServices;

namespace californiaspa.utilis
{
    public static class texthelper
    {
        private static string[] characters; 
        static texthelper()
        {
            texthelper.characters = new string[] { "PR", "EU", "EP", "HSM", "GGM", "GGK", "HOTEL", "UR", "MC", "AU", "SV", "K", "HSR", "PU", "OS", "AU", "SK", "H", "ES", "P", "PI" };
        } 
        public static string GetNameFromstring(this string _completeName)
        {
            return texthelper.splitnameString(_completeName);
        }

        private static string splitnameString(string name)
        {
            char[] chrArray = new char[] { ' ' };
            string str = "";
            int num = 0;
            string[] strArrays = name.Split(chrArray);
            for (int i = 0; i < (int)strArrays.Length; i++)
            {
                string str1 = strArrays[i];
                num = 0;
                int num1 = 0;
                while (num1 < (int)texthelper.characters.Length)
                {
                    if (str1.ToUpper() != texthelper.characters[num1].ToString())
                    {
                        num1++;
                    }
                    else
                    {
                        num = 1;
                        break;
                    }
                }
                str = (num == 0 ? string.Concat(str, str1, " ") : str);
            }
            return str;
        }
    }
}