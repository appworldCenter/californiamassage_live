using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;

namespace californiaspa.utilis
{
    public class ExcelExportHelper
    {
        public string ExcelContentType
        {
            get
            {
                return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            }
        } 
        public byte[] ExportExcel(DataTable dataTable, string heading = "", bool showSrNo = false, int Lastcoulmn = 8, params string[] columnsToTake)
        {
            byte[] asByteArray = null;
            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add(string.Format("{0} Data", heading));
                int num = (string.IsNullOrEmpty(heading) ? 1 : 3);
                int num1 = (string.IsNullOrEmpty(heading) ? 1 : 3);
                if (showSrNo)
                {
                    dataTable.Columns.Add("Sr No.", typeof(int)).SetOrdinal(0);
                    int num2 = 1;
                    foreach (object row in dataTable.Rows)
                    {
                        ((DataRow)row)[0] = num2;
                        num2++;
                        if (num2 != dataTable.Rows.Count - 4)
                        {
                            continue;
                        }
                        goto Label0;
                    }
                }
                Label0:
                excelWorksheet.Cells[string.Concat("A", num)].LoadFromDataTable(dataTable, true);
                int num3 = 1;
                foreach (DataColumn column in dataTable.Columns)
                {
                    ExcelRange item = excelWorksheet.Cells[excelWorksheet.Dimension.Start.Row, num3, excelWorksheet.Dimension.End.Row, num3];
                    try
                    {
                        if (item.Max<ExcelRangeBase>((ExcelRangeBase cell) => cell.Value.ToString().Count<char>()) < 150)
                        {
                            excelWorksheet.Column(num3).AutoFit();
                        }
                    }
                    catch (Exception exception)
                    {
                    }
                    num3++; 
                }
                using (ExcelRange excelRange = excelWorksheet.Cells[num, 1, num, dataTable.Columns.Count])
                {
                    excelRange.Style.Font.Color.SetColor(Color.White);
                    excelRange.Style.Font.Bold = true;
                    excelRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    excelRange.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#1fb5ad"));
                } 
                using (ExcelRange item1 = excelWorksheet.Cells[num + 1, 1, num + dataTable.Rows.Count, dataTable.Columns.Count])
                {
                    item1.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    item1.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    item1.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    item1.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    item1.Style.Border.Top.Color.SetColor(Color.Black);
                    item1.Style.Border.Bottom.Color.SetColor(Color.Black);
                    item1.Style.Border.Left.Color.SetColor(Color.Black);
                    item1.Style.Border.Right.Color.SetColor(Color.Black);
                } 
                for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
                {
                    if (i != 0 | !showSrNo && !columnsToTake.Contains<string>(dataTable.Columns[i].ColumnName))
                    {
                        excelWorksheet.DeleteColumn(i + 1);
                    }
                }
                if (!string.IsNullOrEmpty(heading))
                {
                    excelWorksheet.Cells["A1"].Value = heading;
                    excelWorksheet.Cells["A1"].Style.Font.Size = 20f;
                    excelWorksheet.InsertColumn(1, 1);
                    excelWorksheet.InsertRow(1, 1);
                    excelWorksheet.Column(1).Width = 5;
                }
                using (ExcelRange excelRange1 = excelWorksheet.Cells[dataTable.Rows.Count + num1 - 3, Lastcoulmn, dataTable.Rows.Count + num1 + 1, Lastcoulmn])
                {
                    excelRange1.Style.Font.Color.SetColor(Color.White);
                    excelRange1.Style.Font.Bold = true;
                    excelRange1.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    excelRange1.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#1fb5ad"));
                }
                asByteArray = excelPackage.GetAsByteArray();
            }
            return asByteArray;
        }

        public byte[] ExportExcel<T>(List<T> data, string Heading = "", bool showSlno = false, int lastcolumn = 8, params string[] ColumnsToTake)
        {
            return this.ExportExcel(this.ListToDataTable<T>(data), Heading, showSlno, lastcolumn, ColumnsToTake);
        }

        public DataTable ListToDataTable<T>(List<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable dataTable = new DataTable();
            for (int i = 0; i < properties.Count; i++)
            {
                PropertyDescriptor item = properties[i];
                string str = (item.Name == "totalamountPerClient" ? "TotalAmount" : item.Name);
                dataTable.Columns.Add(str, Nullable.GetUnderlyingType(item.PropertyType) ?? item.PropertyType);
            }
            object[] value = new object[properties.Count];
            foreach (T datum in data)
            {
                for (int j = 0; j < (int)value.Length; j++)
                {
                    value[j] = properties[j].GetValue(datum);
                }
                dataTable.Rows.Add(value);
            }
            return dataTable;
        }
    }
}