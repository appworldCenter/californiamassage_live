using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace californiaspa.utilis
{
    public class AuthorizeVerifieduserAttribute : FilterAttribute, IAuthorizationFilter
    {
        protected void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result=(new RedirectToRouteResult(new RouteValueDictionary()
            {
                { "action", "login" },
                { "controller", "account" },
                { "returnUrl", filterContext.HttpContext.Request.Url.ToString() }
            }));
        }

        void System.Web.Mvc.IAuthorizationFilter.OnAuthorization(AuthorizationContext filterContext)
        {
            bool flag = false;
            if (filterContext.HttpContext.Request.Cookies["_xid"] != null)
            {
                filterContext.HttpContext.Session["cluserId"] = filterContext.HttpContext.Request.Cookies["_xid"].Value;
            }

            if (filterContext.HttpContext.Session["cluserId"] != null)
            {
                flag = true;
            }
            if (!flag)
            {
                this.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}