using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace californiaspa.utilis
{
    public class AuthorizeVerifiedtherapistAttribute : FilterAttribute, IAuthorizationFilter
    { 
        protected void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result=(new RedirectToRouteResult(new RouteValueDictionary()
            {
                { "action", "tlogin" },
                { "controller", "Account" },
                { "ReturnUrl", filterContext.HttpContext.Request.Url.ToString() }
            }));
        }
        protected void HandleUnauthorizedchromeRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = (new RedirectToRouteResult(new RouteValueDictionary()
            {
                { "action", "Index" },
                { "controller", "browser" },
                { "ReturnUrl", filterContext.HttpContext.Request.Url.ToString() }
            }));
        }

        void System.Web.Mvc.IAuthorizationFilter.OnAuthorization(AuthorizationContext filterContext)
        {
            bool flag = true;
            var browserName = filterContext.HttpContext.Request.UserAgent;
            if (browserName.ToLower().Contains("brave") || browserName.ToLower().Contains("samsung") || browserName.ToLower().Contains("opr") || browserName.ToLower().Contains("ucbrowser") || browserName.ToLower().Contains("rocket") || browserName.ToLower().Contains("safari/604.1") || browserName.ToLower().Contains("firefox") || browserName.ToLower().Contains("trident") || browserName.ToLower().Contains("edge"))
            {
                if(browserName.ToLower().Contains("crios"))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
               
            }
            if (!flag)
            {
                this.HandleUnauthorizedchromeRequest(filterContext);
            }
            if (filterContext.HttpContext.Request.Cookies["_zid"] != null)
            {
                filterContext.HttpContext.Session["thId"] = filterContext.HttpContext.Request.Cookies["_zid"].Value;
            }
            if (filterContext.HttpContext.Session["thId"] != null)
            {
                flag = false;
            }
            if (flag)
            {
                this.HandleUnauthorizedRequest(filterContext);
            }
        }
    }

    public class AuthorizeVerifiedtherapistchromeAttribute : FilterAttribute, IAuthorizationFilter
    {
        protected void HandleUnauthorizedchromeRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = (new RedirectToRouteResult(new RouteValueDictionary()
            {
                { "action", "Index" },
                { "controller", "browser" },
                { "ReturnUrl", filterContext.HttpContext.Request.Url.ToString() }
            }));
        }

        void System.Web.Mvc.IAuthorizationFilter.OnAuthorization(AuthorizationContext filterContext)
        {
            bool flag = true;
            var browserName = filterContext.HttpContext.Request.UserAgent;
            if (browserName.ToLower().Contains("brave") || browserName.ToLower().Contains("samsung") || browserName.ToLower().Contains("opr") || browserName.ToLower().Contains("ucbrowser") || browserName.ToLower().Contains("rocket") || browserName.ToLower().Contains("safari/604.1") || browserName.ToLower().Contains("firefox") || browserName.ToLower().Contains("trident") || browserName.ToLower().Contains("edge"))
            {
                if (browserName.ToLower().Contains("crios"))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            if (!flag)
            {
                this.HandleUnauthorizedchromeRequest(filterContext);
            }
        }
    }
}