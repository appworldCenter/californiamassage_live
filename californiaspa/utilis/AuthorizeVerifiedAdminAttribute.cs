using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace californiaspa.utilis
{
    public class AuthorizeVerifiedAdminAttribute : FilterAttribute, IAuthorizationFilter
    {
        protected void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = (new RedirectToRouteResult(new RouteValueDictionary()
            {
                { "action", "adminlogin" },
                { "controller", "account" },
                { "ReturnUrl", filterContext.HttpContext.Request.Url.ToString() }
            }));
        }

        void System.Web.Mvc.IAuthorizationFilter.OnAuthorization(AuthorizationContext filterContext)
        {
            bool flag = false;
            if (filterContext.HttpContext.Request.Cookies["_yid"] != null)
            {
                filterContext.HttpContext.Session["ADId"] = filterContext.HttpContext.Request.Cookies["_yid"].Value;
            }
            if (filterContext.HttpContext.Session["ADId"] != null)
            {
                flag = true;
            }
            if (!flag)
            {
                this.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}