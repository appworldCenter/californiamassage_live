﻿function ScroolDiv(Id) {
    $("#" + Id).animate({ scrollTop: $("#" + Id)[0].scrollHeight }, 1000);
}

function ValidateNumber(e) {
    var evt = (e) ? e : window.event;
    var charCode = (evt.keyCode) ? evt.keyCode : evt.which;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105)) {
        return false;
    }
    return true;
}; 

$(document).ready(setup);

function setup() {
     
    $("form").on("submit", function () { 
        if ($(".error1").html() != '' && $(".error1").html() != null)
        {
            $(this).find(":submit").removeAttr("disabled");
            $(this).find(":submit").addClass("btn-action");
            $(this).find(":submit").removeClass("btn-success");
        }
      else  if ($("form").valid()) {
            $(this).find(":submit").removeClass("btn-action");
            $(this).find(":submit").addClass("btn-success");
            $(this).find(":submit").attr("disabled",true);
      } else {
          var el = $('.input-validation-error').eq(0);
          if (el.length > 0)
          {
              var Id = el[0].id;
              $("#" + Id).animate({ scrollTop: $("#" + Id)[0].scrollHeight }, 1000);
          } 
          $(this).find(":submit").addClass("btn-action");
          $(this).find(":submit").removeClass("btn-success");
          $(this).find(":submit").removeAttr("disabled");
    }
    });
}

function autohide() {
     //setTimeout(function () { $(".autohide").html(''); }, 8000);
     
}

