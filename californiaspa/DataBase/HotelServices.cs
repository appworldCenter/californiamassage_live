using californiaspa.Models.booking;
using californiaspa.Models.therapist;
using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace californiaspa.DataBase
{
    public class HotelServices
    {

        public string AddUpdateHotel(spaHotelItem _hotel, int? hotelid)
        {
            string str = "Hotel Image Deleted Succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                if (hotelid.HasValue)
                {
                    sortedLists.Add("@id", hotelid.Value);
                }
                sortedLists.Add("@name", (!string.IsNullOrEmpty(_hotel.name) ? _hotel.name : ""));
                sortedLists.Add("@available", _hotel.availfornew);
                sortedLists.Add("@type", _hotel.type);
                sortedLists.Add("@mapLocation", (!string.IsNullOrEmpty(_hotel.Map) ? _hotel.Map : ""));
                sortedLists.Add("@webaddress", (!string.IsNullOrEmpty(_hotel.WebPath) ? _hotel.WebPath : ""));
                sortedLists.Add("@payment", (!string.IsNullOrEmpty(_hotel.paymentType) ? _hotel.paymentType : ""));
                sortedLists.Add("@rate", (!string.IsNullOrEmpty(_hotel.rate) ? _hotel.rate : ""));
                sortedLists.Add("@address", (!string.IsNullOrEmpty(_hotel.address) ? _hotel.address : ""));
                sortedLists.Add("@description", (!string.IsNullOrEmpty(_hotel.descriptio) ? _hotel.descriptio : ""));
                sortedLists.Add("@bdescription", (!string.IsNullOrEmpty(_hotel.bookingdescriptio) ? _hotel.bookingdescriptio : ""));
                sortedLists.Add("@starttime", (!string.IsNullOrEmpty(_hotel.startTime) ? _hotel.startTime : ""));
                sortedLists.Add("@endtime", (!string.IsNullOrEmpty(_hotel.EndTime) ? _hotel.EndTime : ""));
                if (!string.IsNullOrEmpty(_hotel.ImgPath))
                {
                    sortedLists.Add("@imagePath", _hotel.ImgPath);
                }
                str = sqlHelper.executeNonQueryWMessage("AddOrupdateHotel", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string deleteHotel(int id = 0)
        {
            string str = "Hotel Deleted Succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {

                sqlHelper.executeNonQuery("", string.Concat("Delete from tblSpaHotal where id=", id), sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public string deleteImage(int imageid)
        {
            string str = "Hotel Image Deleted Succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                sqlHelper.executeNonQuery("", string.Concat("Delete from tblHotelimages where id=", imageid), null);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public hotelterm gethotelterm(int hid)
        {
            SortedList _list = new SortedList();
            hotelterm _spaHotel = new hotelterm();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                _list.Add("@id", hid);
                DataTable dataTable = sqlHelper.fillDataTable("GetAllHotelWithoutShowwithid", "", _list);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        _spaHotel.des = dataTable.Rows[i]["bdescription"].ToString();
                        _spaHotel.name = dataTable.Rows[i]["Name"].ToString();
                        _spaHotel.type = Convert.ToInt32(dataTable.Rows[i]["type"].ToString());
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _spaHotel;
        }


        public List<int> getoutsidehotels()
        {
            SortedList _list = new SortedList();
            List<int> _spaHotel = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("GetAlloutsidehotel", "", null);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        _spaHotel.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()));
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _spaHotel;
        }



        public spaHotel GetAllHotels(bool isshow = false, bool ishownew = true)
        {
            spaHotel _spaHotel = new spaHotel();
            List<spaHotelItem> spaHotelItems = new List<spaHotelItem>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                //  var proc = ishownew ? "GetAlloldHotel" : "GetAllHotelWithoutShow";

                var proc = "GetAllHotel";
                if(!isshow)
                {
                    if (ishownew)
                    {
                        proc = "GetAllnewHotel";
                    }
                    else
                    {
                        proc = "GetAlloldHotel";
                    }
                }
                DataTable dataTable = sqlHelper.fillDataTable(proc, "", null);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {//isshow

                        spaHotelItem _item = new spaHotelItem();

                        _item.address = dataTable.Rows[i]["Address"].ToString();
                        _item.bookingdescriptio = dataTable.Rows[i]["bdescription"].ToString();
                        _item.descriptio = dataTable.Rows[i]["description"].ToString();
                        _item.hotalId = Convert.ToInt32(dataTable.Rows[i]["Id"].ToString());
                        _item.type = Convert.ToInt32(dataTable.Rows[i]["type"].ToString());
                        _item.availfornew = Convert.ToBoolean(dataTable.Rows[i]["AvailForNewClient"].ToString());
                        _item.ImgPath = dataTable.Rows[i]["Imagepath"].ToString();
                        _item.Map = dataTable.Rows[i]["MapLocation"].ToString();
                        _item.name = dataTable.Rows[i]["Name"].ToString();
                        _item.WebPath = dataTable.Rows[i]["WebAddress"].ToString();
                        _item.isshow = dataTable.Rows[i]["isshow"].ToString();
                        _item.todayavailabe = this.GethotelAvailbilityScheduleId(Convert.ToInt32(dataTable.Rows[i]["Id"].ToString()), false);
                      
                            spaHotelItems.Add(_item);
                    }
                }
                _spaHotel._hotel = spaHotelItems;
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _spaHotel;
        }

        public Dictionary<int, string> GetAllHotelsForDropDown()
        {
            List<spaHotelItem> spaHotelItems = new List<spaHotelItem>();
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> nums = new Dictionary<int, string>();
            try
            {

                DataTable dataTable = sqlHelper.fillDataTable("GetAllHotel", "", null);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        nums.Add(Convert.ToInt32(dataTable.Rows[i]["Id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return nums;
        }

        public List<imagesItem> GetAllHotelsImages()
        {
            List<imagesItem> imagesItems = new List<imagesItem>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("GetHotelImages", "", null);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        imagesItems.Add(new imagesItem()
                        {
                            ImgId = Convert.ToInt32(dataTable.Rows[i]["HotelId"].ToString()),
                            path = dataTable.Rows[i]["ImagePath"].ToString(),
                            iId = Convert.ToInt32(dataTable.Rows[i]["Id"].ToString())
                        });
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return imagesItems;
        }

        private string GetAvailablityWithhotelAppointment(int thid, string startTime, string endTime, string date = "")
        {
            DateTime currentTimeByTimeZone;
            int day;
            DateTime dateTime;
            object str;
            string str1 = "";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            if (string.IsNullOrEmpty(date))
            {
                object[] month = new object[5];
                currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                month[0] = currentTimeByTimeZone.Month;
                month[1] = "-";
                if (currentTimeByTimeZone.Day.ToString().Length == 1)
                {
                    day = currentTimeByTimeZone.Day;
                    str = string.Concat("0", day.ToString());
                }
                else
                {
                    day = currentTimeByTimeZone.Day;
                    str = day.ToString();
                }
                month[2] = str;
                month[3] = "-";
                day = currentTimeByTimeZone.Year;
                month[4] = day.ToString().Substring(2);
                date = string.Concat(month);
            }
            sortedLists.Add("@date", date);
            sortedLists.Add("@thid", thid);
            DataTable dataTable = sqlHelper.fillDataTable("GetConfirmedAppointementTimeBYhid", "", sortedLists);
            DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            DateTime currentTimeByTimeZone1 = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    str1 = string.Concat(str1, "Available ");
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        DateTime dateTime1 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", startTime));
                        currentTimeByTimeZone1 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                        currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                        string str2 = currentTimeByTimeZone.AddMinutes(-150).ToString("hh:mm tt");
                        if ((dateTime1 < currentTimeByTimeZone1) & (Convert.ToDateTime(startTime) < Convert.ToDateTime(str2)))
                        {
                            str1 = string.Concat(new object[] { str1, "<br/>", startTime, "-", str2 });
                        }
                        currentTimeByTimeZone = (currentTimeByTimeZone.AddHours(2.5).Date > DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? currentTimeByTimeZone : currentTimeByTimeZone.AddHours(2.5));
                        startTime = currentTimeByTimeZone.ToString("hh:mm tt");
                        currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                        dateTime = (currentTimeByTimeZone.AddHours(2.5).Date > DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? currentTimeByTimeZone : currentTimeByTimeZone.AddHours(2.5));
                        DateTime currentTimeByTimeZone2 = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        DateTime datetime_notcurrent = DateTime.Parse(string.Concat(date, " ", dateTime.ToString("hh:mm tt"))); ;
                        if (datetime_notcurrent < currentTimeByTimeZone2)
                        {
                            startTime = currentTimeByTimeZone2.ToString("hh:mm tt");
                        }
                    }
                    if (Convert.ToDateTime(startTime) > Convert.ToDateTime(endTime) && (string.IsNullOrEmpty(str1) || str1 == "Available "))
                    {
                        str1 = string.Concat(new string[] { "Booked Today", "<br/>" });
                    }
                    else if (!(Convert.ToDateTime(date).Date == DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date) || !(Convert.ToDateTime(startTime) > Convert.ToDateTime(endTime)))
                    {
                        if (Convert.ToDateTime(startTime) > Convert.ToDateTime(endTime))
                        {
                            //  str1 = string.Concat(new string[] { str1, "<br/>", endTime, "-", startTime });

                        }
                        else
                        {
                            str1 = string.Concat(new string[] { str1, "<br/>", startTime, "-", endTime });

                        }

                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                dataTable.Dispose();
            }
            str1 = (str1 == "Available " ? "Booked Today" : str1);
            return str1;
        }


        private DateTime GetCurrentTimeByTimeZone(string Timezone = "India Standard Time")
        {
            return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow.AddMinutes(4), TimeZoneInfo.FindSystemTimeZoneById(Timezone));
        }

        public string GethotelAvailbilityScheduleId(int thid, bool forEdit = false)
        {
            DateTime dateTime;
            string str;
            DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            DateTime dateTime1 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", "10:00 AM"));
            var out_hotels = getoutsidehotels();
            if (thid == 4 || out_hotels.Contains(thid))
            {
                if (currentTimeByTimeZone < dateTime1)
                {
                    str = "10:00 AM";
                }
                else
                {
                    dateTime = currentTimeByTimeZone.AddHours(1).AddMinutes(4);
                    str = dateTime.ToString("hh:mm tt");
                }
                return string.Concat("Available <br/>", str, "- 10:00 PM");
            }
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string availablityWithhotelAppointment = "";
            try
            {
                sortedLists.Add("@id", thid);
                sortedLists.Add("@IsEdit", forEdit);
                sortedLists.Add("@date", currentTimeByTimeZone.Date);
                DataTable dataTable = sqlHelper.fillDataTable("GetAUTOScheduleByhotelId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        DateTime currentTimeByTimeZone1 = currentTimeByTimeZone;
                        DateTime dateTime2 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[0]["Starttime"].ToString()));
                        DateTime dateTime3 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[0]["EndTime"].ToString()));
                        DateTime dateTime4 = currentTimeByTimeZone1;
                        if (dateTime4 > dateTime3 && !forEdit)
                        {
                            availablityWithhotelAppointment = "Not Available Today";
                        }
                        else if (dateTime4 < dateTime2 && !forEdit)
                        {
                            availablityWithhotelAppointment = this.GetAvailablityWithhotelAppointment(thid, dataTable.Rows[0]["Starttime"].ToString(), dataTable.Rows[0]["EndTime"].ToString(), "");
                            if (availablityWithhotelAppointment == "")
                            {
                                availablityWithhotelAppointment = string.Concat("Available </br>", dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString());
                            }
                        }
                        else if (!((dateTime4 >= dateTime3 ? true : !(dateTime4 > dateTime2)) | forEdit))
                        {
                            dateTime = currentTimeByTimeZone1.AddHours(1).AddMinutes(4);
                            string str1 = dateTime.ToString("hh:mm tt");
                            availablityWithhotelAppointment = this.GetAvailablityWithhotelAppointment(thid, str1, dataTable.Rows[0]["EndTime"].ToString(), "");
                            if (availablityWithhotelAppointment == "")
                            {
                                availablityWithhotelAppointment = string.Concat("Available </br>", str1, "-", dataTable.Rows[0]["EndTime"].ToString());
                            }
                        }
                        else
                        {
                            availablityWithhotelAppointment = (forEdit ? string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()) : string.Concat("Available </br>", dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return availablityWithhotelAppointment;
        }


        public string GethotelAvailbilityScheduleIdbydate2(int thid, DateTime currentTimeByTimeZone, bool forEdit = false)
        {
            DateTime dateTime;
            string str;
            DateTime dateTime1 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", "10:00 AM"));
            var out_hotels = getoutsidehotels();
            if (thid == 4 || out_hotels.Contains(thid))
            {
                if (currentTimeByTimeZone < dateTime1)
                {
                    str = "10:00 AM";
                }
                else
                {
                    dateTime = currentTimeByTimeZone.AddHours(1).AddMinutes(4);
                    str = dateTime.ToString("hh:mm tt");
                }
                return string.Concat("Available <br/>", str, "- 10:00 PM");
            }
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string availablityWithhotelAppointment = "";
            try
            {
                sortedLists.Add("@id", thid);
                sortedLists.Add("@IsEdit", forEdit);
                sortedLists.Add("@date", currentTimeByTimeZone.Date);
                DataTable dataTable = sqlHelper.fillDataTable("GetAUTOScheduleByhotelId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        DateTime currentTimeByTimeZone2 = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");

                        DateTime dateTime2 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[0]["Starttime"].ToString()));
                        DateTime dateTime3 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[0]["EndTime"].ToString()));
                        DateTime dateTime4 = currentTimeByTimeZone;
                        if (dateTime4.Date == currentTimeByTimeZone2.Date)
                        {
                            dateTime4 = currentTimeByTimeZone2;
                            currentTimeByTimeZone = currentTimeByTimeZone2;
                        }
                        if (dateTime4 > dateTime3 && !forEdit)
                        {
                            availablityWithhotelAppointment = "Not Available Today";
                        }
                        else if (dateTime4 < dateTime2 && !forEdit)
                        {
                            object[] month = new object[5];
                            int day = 1;
                            month[0] = currentTimeByTimeZone.Month;
                            month[1] = "-";
                            if (currentTimeByTimeZone.Day.ToString().Length == 1)
                            {
                                day = currentTimeByTimeZone.Day;
                                str = string.Concat("0", day.ToString());
                            }
                            else
                            {
                                day = currentTimeByTimeZone.Day;
                                str = day.ToString();
                            }
                            month[2] = str;
                            month[3] = "-";
                            day = currentTimeByTimeZone.Year;
                            month[4] = day.ToString().Substring(2);
                            string date = string.Concat(month);
                            availablityWithhotelAppointment = this.GetAvailablityWithhotelAppointment(thid, dataTable.Rows[0]["Starttime"].ToString(), dataTable.Rows[0]["EndTime"].ToString(), date);
                            if (availablityWithhotelAppointment == "")
                            {
                                availablityWithhotelAppointment = string.Concat("Available </br>", dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString());
                            }
                        }
                        else if (!((dateTime4 >= dateTime3 ? true : !(dateTime4 > dateTime2)) | forEdit))
                        {
                            dateTime = currentTimeByTimeZone.AddHours(1).AddMinutes(4);
                            string str1 = dateTime.ToString("hh:mm tt");
                            availablityWithhotelAppointment = this.GetAvailablityWithhotelAppointment(thid, str1, dataTable.Rows[0]["EndTime"].ToString(), "");
                            if (availablityWithhotelAppointment == "")
                            {
                                availablityWithhotelAppointment = string.Concat("Available </br>", str1, "-", dataTable.Rows[0]["EndTime"].ToString());
                            }
                        }
                        else
                        {
                            availablityWithhotelAppointment = (forEdit ? string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()) : string.Concat("Available </br>", dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return availablityWithhotelAppointment;
        }

        public string GethotelAvailbilityScheduleIdbydate3(int thid, DateTime currentTimeByTimeZone, string start, string end, bool forEdit = false)
        {
            DateTime dateTime;
            string str;
            DateTime dateTime1 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", "10:00 AM"));
            var out_hotels = getoutsidehotels();
            if (thid == 4 || out_hotels.Contains(thid))
            {
                if (currentTimeByTimeZone < dateTime1)
                {
                    str = "10:00 AM";
                }
                else
                {
                    dateTime = currentTimeByTimeZone.AddHours(1).AddMinutes(4);
                    str = dateTime.ToString("hh:mm tt");
                }
                return string.Concat("Available <br/>", str, "- 10:00 PM");
            }
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string availablityWithhotelAppointment = "";
            try
            {
                sortedLists.Add("@id", thid);
                sortedLists.Add("@IsEdit", forEdit);
                sortedLists.Add("@date", currentTimeByTimeZone.Date);
                DataTable dataTable = sqlHelper.fillDataTable("GetAUTOScheduleByhotelId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        DateTime currentTimeByTimeZone1 = currentTimeByTimeZone;
                        DateTime dateTime2 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", start));
                        DateTime dateTime3 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", end));
                        DateTime dateTime4 = currentTimeByTimeZone1;
                        if (dateTime4 > dateTime3 && !forEdit)
                        {
                            availablityWithhotelAppointment = "Not Available Today";
                        }
                        else if (dateTime4 < dateTime2 && !forEdit)
                        {
                            availablityWithhotelAppointment = this.GetAvailablityWithhotelAppointment(thid, start, end, "");
                            if (availablityWithhotelAppointment == "")
                            {
                                availablityWithhotelAppointment = string.Concat("Available </br>", start, "-", end);
                            }
                        }
                        else if (!((dateTime4 >= dateTime3 ? true : !(dateTime4 > dateTime2)) | forEdit))
                        {
                            dateTime = currentTimeByTimeZone1.AddHours(1).AddMinutes(4);
                            string str1 = dateTime.ToString("hh:mm tt");
                            availablityWithhotelAppointment = this.GetAvailablityWithhotelAppointment(thid, str1, end, "");
                            if (availablityWithhotelAppointment == "")
                            {
                                availablityWithhotelAppointment = string.Concat("Available </br>", str1, "-", end);
                            }
                        }
                        else
                        {
                            availablityWithhotelAppointment = (forEdit ? string.Concat(start, "-", end) : string.Concat("Available </br>", start, "-", end));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return availablityWithhotelAppointment;
        }


        public string GethotelAvailbilityScheduleIdwithdate(int thid, DateTime currentTimeByTimeZone, bool forEdit = false)
        {
            DateTime dateTime;
            string str;
            DateTime dateTime1 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", "10:00 AM"));
            var out_hotels = getoutsidehotels();
            if (thid == 4 || out_hotels.Contains(thid))
            {
                if (currentTimeByTimeZone < dateTime1)
                {
                    str = "10:00 AM";
                }
                else
                {
                    dateTime = currentTimeByTimeZone.AddHours(1).AddMinutes(4);
                    str = dateTime.ToString("hh:mm tt");
                }
                return string.Concat("Available <br/>", str, "- 10:00 PM");
            }
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string availablityWithhotelAppointment = "";
            try
            {
                sortedLists.Add("@id", thid);
                sortedLists.Add("@IsEdit", forEdit);
                sortedLists.Add("@date", currentTimeByTimeZone.Date);
                DataTable dataTable = sqlHelper.fillDataTable("GetAUTOScheduleByhotelId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        DateTime dateTime2 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[0]["Starttime"].ToString()));
                        if (dateTime2 <= currentTimeByTimeZone)
                        {
                            dateTime2 = currentTimeByTimeZone;
                        }
                        DateTime dateTime3 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[0]["EndTime"].ToString()));
                        DateTime dateTime4 = currentTimeByTimeZone;
                        if (dateTime4 > dateTime3 && !forEdit)
                        {
                            availablityWithhotelAppointment = "Not Available Today";
                        }
                        else if (dateTime4 < dateTime2 && !forEdit)
                        {
                            availablityWithhotelAppointment = this.GetAvailablityWithhotelAppointment(thid, dataTable.Rows[0]["Starttime"].ToString(), dataTable.Rows[0]["EndTime"].ToString(), "");
                            if (availablityWithhotelAppointment == "")
                            {
                                availablityWithhotelAppointment = string.Concat("Available </br>", dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString());
                            }
                        }
                        else if (!((dateTime4 >= dateTime3 ? true : !(dateTime4 > dateTime2)) | forEdit))
                        {
                            dateTime = currentTimeByTimeZone.AddHours(1).AddMinutes(4);
                            string str1 = dateTime.ToString("hh:mm tt");
                            availablityWithhotelAppointment = this.GetAvailablityWithhotelAppointment(thid, str1, dataTable.Rows[0]["EndTime"].ToString(), "");
                            if (availablityWithhotelAppointment == "")
                            {
                                availablityWithhotelAppointment = string.Concat("Available </br>", str1, "-", dataTable.Rows[0]["EndTime"].ToString());
                            }
                        }
                        else
                        {
                            availablityWithhotelAppointment = (forEdit ? string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()) : string.Concat("Available </br>", dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return availablityWithhotelAppointment;
        }

        public List<Events> GethotelEvents(int hid, DateTime start, DateTime end)
        {
            DateTime currentTimeByTimeZone;
            List<Events> events = new List<Events>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            DateTime dateTime = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            try
            {
                sortedLists.Add("@id", hid);
                DataTable dataTable = sqlHelper.fillDataTable("GetScheduleByhotelId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    List<HotelServices.ScheduleModel> scheduleModels = new List<HotelServices.ScheduleModel>();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        scheduleModels.Add(new HotelServices.ScheduleModel()
                        {
                            _type = dataTable.Rows[i]["Type"].ToString(),
                            _start = dataTable.Rows[i]["Starttime"].ToString(),
                            _end = dataTable.Rows[i]["EndTime"].ToString(),
                            _date = Convert.ToDateTime(dataTable.Rows[i]["aptDate"].ToString())
                        });
                    }
                    dataTable.Dispose();
                    object[] month = new object[5];
                    DateTime date = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                    month[0] = date.Month;
                    month[1] = "/";
                    month[2] = date.Day;
                    month[3] = "/";
                    month[4] = date.Year;
                    string.Concat(month);
                    DateTime dateTime1 = date;
                    DateTime dateTime2 = date;
                    string str = "";
                    string str1 = "";
                    int num = 0;
                    DateTime date1 = new DateTime();
                    DateTime dateTime3 = new DateTime();
                    DateTime dateTime4 = new DateTime();
                    for (DateTime j = start; j < end; j = j.AddDays(1))
                    {
                        string[] strArrays = new string[5];
                        int day = j.Month;
                        strArrays[0] = day.ToString();
                        strArrays[1] = "-";
                        day = j.Day;
                        strArrays[2] = (j.Day.ToString().Length > 1 ? day.ToString() : string.Concat("0", day.ToString()));
                        strArrays[3] = "-";
                        day = j.Year;
                        strArrays[4] = day.ToString().Substring(2);
                        str1 = string.Concat(strArrays);
                        num = Convert.ToInt32(sqlHelper.executeScaler(string.Concat(new object[] { "select count(1) from tblappointement with(nolock) join tbllocation on tbllocation.id=locationid  where bookingDate='", str1, "'  and status ='Confirmed' and spahotelid=", hid })).ToString());
                        IEnumerable<HotelServices.ScheduleModel> list = scheduleModels.Where<HotelServices.ScheduleModel>((HotelServices.ScheduleModel a) =>
                        {
                            if (a._date.Date != j.Date)
                            {
                                return false;
                            }
                            return a._type == "Manual";
                        });
                        if (list == null || list.Count<HotelServices.ScheduleModel>() == 0)
                        {
                            str = (num > 0 ? "Click here to check Avail" : "");
                            list = (
                                from a in scheduleModels
                                where a._type == "Auto"
                                select a).ToList<HotelServices.ScheduleModel>();
                            string str2 = string.Concat(new object[] { j.Month, "/", j.Day, "/", j.Year });
                            date1 = Convert.ToDateTime(str2).Date;
                            dateTime1 = Convert.ToDateTime(string.Concat(str2, " ", list.FirstOrDefault<HotelServices.ScheduleModel>()._start));
                            dateTime2 = Convert.ToDateTime(string.Concat(str2, " ", list.FirstOrDefault<HotelServices.ScheduleModel>()._end));
                            if (dateTime.Date == date1)
                            {
                                date = dateTime.Date;
                                dateTime3 = DateTime.Parse(string.Concat(date.ToShortDateString(), " ", list.FirstOrDefault<HotelServices.ScheduleModel>()._start));
                                dateTime4 = DateTime.Parse(string.Concat(date.ToShortDateString(), " ", list.FirstOrDefault<HotelServices.ScheduleModel>()._end));
                                if (dateTime > dateTime4)
                                {
                                    events.Add(new Events()
                                    {
                                        id = "1",
                                        start = dateTime1.ToString("s"),
                                        end = dateTime2.ToString("s"),
                                        title = "Not Available",
                                        allDay = false,
                                        className = (dateTime1.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "")
                                    });
                                }
                                else if (!(dateTime > dateTime3) || !(dateTime < dateTime4))
                                {
                                    events.Add(new Events()
                                    {
                                        id = "1",
                                        title = str,
                                        start = dateTime1.ToString("s"),
                                        end = dateTime2.ToString("s"),
                                        allDay = false,
                                        className = (dateTime1.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "")
                                    });
                                }
                                else
                                {
                                    currentTimeByTimeZone = this.GetCurrentTimeByTimeZone("India Standard Time");
                                    dateTime1 = currentTimeByTimeZone.AddHours(1).AddMinutes(4);
                                    dateTime2 = Convert.ToDateTime(string.Concat(dateTime.ToShortDateString(), " ", list.FirstOrDefault<HotelServices.ScheduleModel>()._end));
                                    events.Add(new Events()
                                    {
                                        id = "1",
                                        start = dateTime1.ToString("s"),
                                        end = dateTime2.ToString("s"),
                                        title = str,
                                        allDay = false,
                                        className = (dateTime1.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "")
                                    });
                                }
                            }
                            else
                            {
                                events.Add(new Events()
                                {
                                    id = "1",
                                    title = str,
                                    start = dateTime1.ToString("s"),
                                    end = dateTime2.ToString("s"),
                                    allDay = false,
                                    className = (dateTime1.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "")
                                });
                            }
                        }
                        else
                        {
                            str = "Edited";
                            str = (num > 0 ? "Click here to check Avail" : "Edited");
                            string str3 = string.Concat(new object[] { j.Month, "/", j.Day, "/", j.Year });
                            DateTime date2 = Convert.ToDateTime(str3).Date;
                            dateTime1 = Convert.ToDateTime(string.Concat(str3, " ", list.FirstOrDefault<HotelServices.ScheduleModel>()._start));
                            dateTime2 = Convert.ToDateTime(string.Concat(str3, " ", list.FirstOrDefault<HotelServices.ScheduleModel>()._end));
                            if (dateTime.Date == date2)
                            {
                                date = dateTime.Date;
                                dateTime3 = DateTime.Parse(string.Concat(date.ToShortDateString(), " ", list.FirstOrDefault<HotelServices.ScheduleModel>()._start));
                                dateTime4 = DateTime.Parse(string.Concat(date.ToShortDateString(), " ", list.FirstOrDefault<HotelServices.ScheduleModel>()._end));
                                if (dateTime > dateTime4)
                                {
                                    events.Add(new Events()
                                    {
                                        id = "1",
                                        start = dateTime1.ToString("s"),
                                        end = dateTime2.ToString("s"),
                                        title = "Not Available",
                                        allDay = false,
                                        className = (dateTime1.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "")
                                    });
                                }
                                else if (!(dateTime > dateTime3) || !(dateTime < dateTime4))
                                {
                                    events.Add(new Events()
                                    {
                                        id = "1",
                                        start = dateTime1.ToString("s"),
                                        end = dateTime2.ToString("s"),
                                        title = str,
                                        allDay = false,
                                        className = (dateTime1.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "")
                                    });
                                }
                                else
                                {
                                    currentTimeByTimeZone = this.GetCurrentTimeByTimeZone("India Standard Time");
                                    dateTime1 = currentTimeByTimeZone.AddHours(1).AddMinutes(4);
                                    dateTime2 = Convert.ToDateTime(string.Concat(dateTime.ToShortDateString(), " ", list.FirstOrDefault<HotelServices.ScheduleModel>()._end));
                                    events.Add(new Events()
                                    {
                                        id = "1",
                                        start = dateTime1.ToString("s"),
                                        end = dateTime2.ToString("s"),
                                        title = str,
                                        allDay = false,
                                        className = (dateTime1.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "")
                                    });
                                }
                            }
                            else
                            {
                                events.Add(new Events()
                                {
                                    id = "1",
                                    start = dateTime1.ToString("s"),
                                    end = dateTime2.ToString("s"),
                                    title = str,
                                    allDay = false,
                                    className = (dateTime1.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "")
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return events;
        }

        public spaHotelItem GetHotelsById(int id)
        {
            spaHotelItem _spaHotelItem = new spaHotelItem();
            SqlHelper sqlHelper = new SqlHelper();
            Crypto crypto = new Crypto();
            SortedList sortedLists = new SortedList()
            {
                { "@Id", id }
            };
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("GetAllHotel", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        _spaHotelItem.address = dataTable.Rows[i]["Address"].ToString();
                        _spaHotelItem.bookingdescriptio = dataTable.Rows[i]["bdescription"].ToString();
                        _spaHotelItem.descriptio = dataTable.Rows[i]["description"].ToString();
                        _spaHotelItem.hotalId = Convert.ToInt32(dataTable.Rows[i]["Id"].ToString());
                        _spaHotelItem.type = Convert.ToInt32(dataTable.Rows[i]["type"].ToString());
                        _spaHotelItem.ImgPath = dataTable.Rows[i]["Imagepath"].ToString();
                        _spaHotelItem.Map = dataTable.Rows[i]["MapLocation"].ToString();
                        _spaHotelItem.name = dataTable.Rows[i]["Name"].ToString();
                        _spaHotelItem.WebPath = dataTable.Rows[i]["WebAddress"].ToString();
                        _spaHotelItem.paymentType = dataTable.Rows[i]["paymentType"].ToString();
                        _spaHotelItem.startTime = dataTable.Rows[i]["startTime"].ToString();
                        _spaHotelItem.EndTime = dataTable.Rows[i]["Endtime"].ToString();
                        _spaHotelItem.rate = dataTable.Rows[i]["rate"].ToString();
                        _spaHotelItem.token = crypto.EncryptStringAES(dataTable.Rows[i]["Id"].ToString());
                        _spaHotelItem.availfornew = Convert.ToBoolean(dataTable.Rows[i]["AvailForNewClient"].ToString());
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _spaHotelItem;
        }

        public string GethotelschedulebyId(int id)
        {
            string str = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {

                DataTable dataTable = sqlHelper.fillDataTable("GetAUTOScheduleByhotalId", "", null);
                if (dataTable.Rows.Count > 0)
                {
                    str = string.Concat(dataTable.Rows[0]["startTime"].ToString(), "-", dataTable.Rows[0]["endtime"].ToString());
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        private DateTime GetIndianTimeFromPST(DateTime _date)
        {
            DateTime staticdatetime = Convert.ToDateTime("04/13/2020");
            string Timezone = "Pacific Standard Time";
            if (_date > staticdatetime)
            {
                Timezone = "UTC";
                _date = _date.AddMinutes(4);
            }
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_date, Timezone, "India Standard Time");
        }

        private DateTime GetPSTFromIndianTime(DateTime _date)
        {
            DateTime staticdatetime = Convert.ToDateTime("04/13/2020");
            string Timezone = "Pacific Standard Time";
            if (_date > staticdatetime)
            {
                Timezone = "UTC";
                _date = _date.AddMinutes(4);
            }
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_date, "India Standard Time", Timezone);
        }

        public void updateStatusbyhId(int hid, string show)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                sqlHelper.executeNonQuery("", string.Concat(new object[] { "update tblSpaHotal set isshow='", show, "' where id=", hid }), null);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
        }

        public class ScheduleModel
        {
            public DateTime _date
            {
                get;
                set;
            }

            public string _end
            {
                get;
                set;
            }

            public string _start
            {
                get;
                set;
            }

            public string _type
            {
                get;
                set;
            }

            public string _working
            {
                get;
                set;
            }
        }
    }
}