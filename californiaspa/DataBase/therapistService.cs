using californiaspa.Models;
using californiaspa.Models.Appointment;
using californiaspa.Models.therapist;
using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web;

namespace californiaspa.DataBase
{
    public class therapistService
    {
        public string AddDiscount(int thid, int clientId, int Aid)
        {
            int num;
            string str;
            string str1 = "Discount Added";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@AId", Aid);
                sortedLists.Add("@clientId", clientId);
                sortedLists.Add("@thid", thid);
                DataTable dataTable = sqlHelper.fillDataTable("GetAppointmentRate", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    decimal num1 = new decimal();
                    decimal num2 = Convert.ToDecimal(dataTable.Rows[0]["AppointmentRate"].ToString());
                    if (!string.IsNullOrEmpty(dataTable.Rows[0]["Dayss"].ToString()))
                    {
                        str = dataTable.Rows[0]["Dayss"].ToString();
                    }
                    else
                    {
                        str = null;
                    }
                    if (str == "Days")
                    {
                        num = 20;
                        num1 = (num2 * num) / new decimal(100);
                    }
                    else
                    {
                        num = 10;
                        num1 = (num2 * new decimal(10)) / new decimal(100);
                    }
                    decimal num3 = num2 - num1;
                    this.UpdateAppointmentRate(Aid, num, num3);
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str1 = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str1;
        }

        public bool checkServiceTaken(int thid)
        {
            bool flag = false;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@thid", thid);
                flag = (Convert.ToInt32(sqlHelper.executeNonQueryWMessage("GetServiceTakenbythid_v2", "", sortedLists).ToString()) > 0 ? true : false);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return flag;
        }

        public string DeleteTherapistById(int thid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "";
            try
            {
                sortedLists.Add("@id", thid);
                sqlHelper.executeNonQuery("DeletTherapistbyId", "", sortedLists);
                str = "Account Deleted Succesfully";
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string DeleteTherapistImageById(int thid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "";
            try
            {
                sortedLists.Add("@id", thid);
                sqlHelper.executeNonQuery("DeleteImageById", "", sortedLists);
                str = "Image Deleted Succesfully";
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string DeleteUserById(int thid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "";
            try
            {
                sortedLists.Add("@id", thid);
                sqlHelper.executeNonQuery("DeletuserbyId", "", sortedLists);
                str = "Account Deleted Succesfully";
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string DeleteUserPhotoIdByUserId(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "";
            try
            {
                sortedLists.Add("@id", id);
                sqlHelper.executeNonQuery("DeleteUplodedIdByUserId", "", sortedLists);
                str = "Uploded Id Deleted Succesfully";
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        private string getaavailtime(int thid)
        {
            string str = "";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@thid", thid);
                str = sqlHelper.executeNonQueryWMessage("GetEndtimebythid", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public List<AppointmentModel_item> GetAllConfirmAppointments(string srearch_type, string date, int h, int t, int c)
        {
            List<AppointmentModel_item> appointmentModelItems = new List<AppointmentModel_item>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            if (!string.IsNullOrEmpty(date))
            {
                sortedLists.Add("@date", date);
            }
            if (h != 0)
            {
                sortedLists.Add("@h", h);
            }
            if (t != 0)
            {
                sortedLists.Add("@t", t);
            }
            if (c != 0)
            {
                sortedLists.Add("@c", c);
            }
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable(srearch_type, "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        AppointmentModel_item appointmentModelItem = new AppointmentModel_item()
                        {
                            tokenId = crypto.EncryptStringAES(dataTable.Rows[i]["Aid"].ToString()),
                            startDate = dataTable.Rows[i]["BookingDate"].ToString()
                        };
                        int num = Convert.ToInt32(dataTable.Rows[0]["SpaHotelId"].ToString());
                        appointmentModelItem.address = (num == 0 ? dataTable.Rows[i]["location"].ToString() : dataTable.Rows[i]["Address"].ToString());
                        appointmentModelItem.comments = dataTable.Rows[i]["Comments"].ToString();
                        appointmentModelItem.therapistName = dataTable.Rows[i]["therapistName"].ToString();
                        appointmentModelItem.ClientName = dataTable.Rows[i]["Name"].ToString();
                        appointmentModelItem.startTime = dataTable.Rows[i]["ConfirmTime"].ToString();
                        appointmentModelItems.Add(appointmentModelItem);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return appointmentModelItems;
        }

        public List<AppointmentModel_item> getAllPendingAppointment()
        {
            List<AppointmentModel_item> appointmentModelItems = new List<AppointmentModel_item>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("GetAllPendingAppointment", "", null);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        AppointmentModel_item appointmentModelItem = new AppointmentModel_item()
                        {
                            tokenId = crypto.EncryptStringAES(dataTable.Rows[i]["Aid"].ToString()),
                            startDate = dataTable.Rows[i]["BookingDate"].ToString()
                        };
                        int num = Convert.ToInt32(dataTable.Rows[i]["SpaHotelId"].ToString());
                        appointmentModelItem.address = (num == 0 ? dataTable.Rows[i]["location"].ToString() : dataTable.Rows[i]["Address"].ToString());
                        appointmentModelItem.arrivalTime = dataTable.Rows[i]["eArrivalTime"].ToString();
                        appointmentModelItem.finishTime = dataTable.Rows[i]["lArrivaltime"].ToString();
                        appointmentModelItem.therapistName = dataTable.Rows[i]["therapistName"].ToString();
                        appointmentModelItem.ClientName = dataTable.Rows[i]["Name"].ToString();
                        appointmentModelItems.Add(appointmentModelItem);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return appointmentModelItems;
        }

        public List<AppointmentModel_item> GetAllPunctualityReport(int startindex, int endindex, ref int total, int id = 0, string startDate = null, string endDate = null)
        {
            List<AppointmentModel_item> appointmentModelItems = new List<AppointmentModel_item>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            if (id != 0)
            {
                sortedLists.Add("@therapistId", id);
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                sortedLists.Add("@startDate", startDate);
                sortedLists.Add("@endDate", endDate);
            }
            total = 0;
            sortedLists.Add("@startindex", startindex);
            sortedLists.Add("@endindex", endindex);
            Crypto crypto = new Crypto();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("GetAllPunctualityReport", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        AppointmentModel_item appointmentModelItem = new AppointmentModel_item()
                        {
                            tokenId = crypto.EncryptStringAES(dataTable.Rows[i]["Aid"].ToString()),
                            therapistName = dataTable.Rows[i]["therapistName"].ToString(),
                            ClientName = dataTable.Rows[i]["Name"].ToString(),
                            appTime = dataTable.Rows[i]["AppTime"].ToString(),
                            startTime = dataTable.Rows[i]["startTime"].ToString(),
                            variation = string.Concat(dataTable.Rows[i]["MinuteDiff"].ToString(), " Min")
                        };
                        DateTime dateTime = Convert.ToDateTime(dataTable.Rows[i]["bookingDate"].ToString());
                        appointmentModelItem.startDate = dateTime.ToString("dd-MMM-yy");
                        appointmentModelItem.TotalVaritaion = Convert.ToInt32(dataTable.Rows[i]["MinuteDiff"].ToString());
                        appointmentModelItem.Aid = Convert.ToInt32(dataTable.Rows[i]["Aid"].ToString());
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString());
                        appointmentModelItem.srno = Convert.ToInt32(dataTable.Rows[i]["RowNum"].ToString());
                        appointmentModelItems.Add(appointmentModelItem);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return appointmentModelItems;
        }

        public _therapist GetAllTherapist(bool showHideprofiles = true,bool isnewuser=false)
        {
            _therapist __therapist = new _therapist();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            RatingHelper ratingHelper = new RatingHelper();
            Crypto crypto = new Crypto();
            List<therapistprofile> therapistprofiles = new List<therapistprofile>();
            try
            {
                string str = (showHideprofiles ? "tbltherapistGetAll" : "tbltherapistGetAll1");

                int num = 0;
                DataTable dataTable = sqlHelper.fillDataTable(str, "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        List<comments> _comments = new List<comments>();
                        therapistprofile _therapistprofile = new therapistprofile()
                        {
                            id = Convert.ToInt32(dataTable.Rows[i]["tid"].ToString())
                        };
                        int num1 = _therapistprofile.id;
                        _therapistprofile.token = crypto.EncryptStringAES(num1.ToString());
                        _therapistprofile.Age = Convert.ToInt32(dataTable.Rows[i]["age"].ToString());
                        _therapistprofile.showprofile = Convert.ToBoolean(dataTable.Rows[i]["Show"].ToString());
                        _therapistprofile.ClientRating = ratingHelper.GetAverageRating(_therapistprofile.id, ref num);
                        _therapistprofile.ImageCount = Convert.ToInt32(dataTable.Rows[i]["imagecount"].ToString());
                        _therapistprofile.imagePath = (dataTable.Rows[i]["imagePath"].ToString() == "" ? "~/assets/images/role.jpg" : dataTable.Rows[i]["imagePath"].ToString());
                        _therapistprofile.languages = dataTable.Rows[i]["languages"].ToString();
                        _therapistprofile.location = dataTable.Rows[i]["location"].ToString();
                        _therapistprofile.name = dataTable.Rows[i]["Name"].ToString();
                        _therapistprofile.MassageStyles = dataTable.Rows[i]["MassageStyles"].ToString();
                        _therapistprofile.rate = Convert.ToDecimal(dataTable.Rows[i]["rate"].ToString());
                        _therapistprofile.TodaysAvailabilty = this.GetTherapistAvailbilityScheduleId(_therapistprofile.id, false);
                        _therapistprofile.bookingLimit = Convert.ToInt32(dataTable.Rows[i]["DailyCount"].ToString());
                        _comments.Add(new comments()
                        {
                            Comment = dataTable.Rows[i]["cmnt"].ToString()
                        });
                        _therapistprofile.Comments = _comments;
                        if(isnewuser&&(_therapistprofile.TodaysAvailabilty.Contains("Booked")|| _therapistprofile.TodaysAvailabilty.ToLower().Contains("not")))
                        {

                        }
                        else
                        {
                            therapistprofiles.Add(_therapistprofile);
                        }
                          
                    }
                }
                __therapist.profile = therapistprofiles;
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                __therapist.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return __therapist;
        }

        public therapistprofile GetAllTherapistById(int thid, bool IsEdit = false)
        {
            therapistprofile _therapistprofile = new therapistprofile();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            RatingHelper ratingHelper = new RatingHelper();
            try
            {
                sortedLists.Add("@id", thid);
                DataTable dataTable = sqlHelper.fillDataTable("tbltherapistGetById", "", sortedLists);
                int num = 0;
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        _therapistprofile.showprofile = Convert.ToBoolean(dataTable.Rows[i]["IsShow"].ToString());
                        _therapistprofile.name = dataTable.Rows[i]["Name"].ToString();
                        _therapistprofile.id = Convert.ToInt32(dataTable.Rows[i]["tid"].ToString());
                        _therapistprofile.bookingLimit = Convert.ToInt32(dataTable.Rows[i]["DailyCount"].ToString());
                        int num1 = _therapistprofile.id;
                        _therapistprofile.token = crypto.EncryptStringAES(num1.ToString());
                        _therapistprofile.Age = Convert.ToInt32(dataTable.Rows[i]["age"].ToString());
                        _therapistprofile.ClientRating = ratingHelper.GetAverageRating(_therapistprofile.id, ref num);
                        _therapistprofile.ImageCount = Convert.ToInt32(dataTable.Rows[i]["ImageCount"].ToString());
                        _therapistprofile.imagePath = dataTable.Rows[i]["imagePath"].ToString();
                        _therapistprofile.Username = dataTable.Rows[i]["username"].ToString();
                        _therapistprofile.mobile = dataTable.Rows[i]["mobile"].ToString();
                        _therapistprofile.Password = dataTable.Rows[i]["password"].ToString();
                        _therapistprofile.languages = dataTable.Rows[i]["languages"].ToString();
                        _therapistprofile.location = dataTable.Rows[i]["location"].ToString();
                        _therapistprofile.Adminreview = dataTable.Rows[i]["cmnt"].ToString();
                        _therapistprofile.MassageStyles = dataTable.Rows[i]["MassageStyles"].ToString();
                        _therapistprofile.TodaysAvailabilty = this.GetTherapistAvailbilityScheduleId(_therapistprofile.id, IsEdit);
                        string therapistAvailbilityScheduleId = this.GetTherapistAvailbilityScheduleId(_therapistprofile.id, true);
                        _therapistprofile.FirstAptTime = therapistAvailbilityScheduleId.Split(new char[] { '-' })[0];
                        _therapistprofile.LastAptTime = therapistAvailbilityScheduleId.Split(new char[] { '-' })[1];
                        _therapistprofile.rate = Convert.ToDecimal(dataTable.Rows[i]["rate"].ToString());
                        _therapistprofile.Comments = this.Gettop10Comments(_therapistprofile.id);
                        _therapistprofile.TotalRating = num;
                        string[] strArrays = this.GetTherapistcallScheduleId(_therapistprofile.id).Split(new char[] { '-' });
                        _therapistprofile.ErliestcallTime = strArrays[0];
                        _therapistprofile.latestcallTime = strArrays[1];
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _therapistprofile;
        }

        public List<imagesItem> GetAllTherapistImages()
        {
            List<imagesItem> imagesItems = new List<imagesItem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("tbltherapistGetAllUImages", "", null);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        imagesItems.Add(new imagesItem()
                        {
                            ImgId = Convert.ToInt32(dataTable.Rows[i]["ImageId"].ToString()),
                            thId = Convert.ToInt32(dataTable.Rows[i]["TherapistId"].ToString()),
                            path = (dataTable.Rows[i]["ImagePath"].ToString() == "" ? "~/assets/images/role.jpg" : dataTable.Rows[i]["ImagePath"].ToString())
                        });
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return imagesItems;
        }

        private int GetAvailablityCountWithAppointment(int thid, string date)
        {
            int num = 0;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                num = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("GetConfirmedAppointementTimeBYthid", "", new SortedList()
                    {
                        { "@date", date },
                        { "@thid", thid }
                    }).ToString());
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return num;
        }

        private string GetAvailablityWithAppointment(int thid, string startTime, string endTime, string date = "", int hour = 1)
        {
            DateTime currentTimeByTimeZone;
            string str;
            int day;
            DateTime dateTime;
            DateTime dateTime1;
            DateTime dateTime2;
            object obj;
            string str1 = "";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            if (string.IsNullOrEmpty(date))
            {
                object[] month = new object[5];
                currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                month[0] = currentTimeByTimeZone.Month;
                month[1] = "-";
                currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                if (currentTimeByTimeZone.Day.ToString().Length == 1)
                {
                    day = currentTimeByTimeZone.Day;
                    obj = string.Concat("0", day.ToString());
                }
                else
                {
                    day = currentTimeByTimeZone.Day;
                    obj = day.ToString();
                }
                month[2] = obj;
                month[3] = "-";
                currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                int year = currentTimeByTimeZone.Year;
                month[4] = year.ToString().Substring(2);
                date = string.Concat(month);
            }
            List<string> strs = new List<string>();
            sortedLists.Add("@date", date);
            sortedLists.Add("@thid", thid);
            DataTable dataTable = sqlHelper.fillDataTable("GetConfirmedAppointementTimeBYthid", "", sortedLists);
            DateTime currentTimeByTimeZone1 = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            bool flag = false;
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    str1 = string.Concat(str1, "Available ");
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        flag = false;
                        strs.Add((dataTable.Rows[i]["ConfirmedTime"].ToString().Length <= 7 ? string.Concat("0", dataTable.Rows[i]["ConfirmedTime"].ToString()) : dataTable.Rows[i]["ConfirmedTime"].ToString()));
                        startTime = (startTime.Length <= 7 ? string.Concat("0", startTime) : startTime);
                        currentTimeByTimeZone = Convert.ToDateTime(date);
                        DateTime dateTime3 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", startTime));
                        currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                        currentTimeByTimeZone1 = dateTime3.AddHours(1).AddMinutes(4);
                        if (dateTime3 > Convert.ToDateTime(date).AddHours(1).AddMinutes(4))
                        {
                            dateTime1 = dateTime3;
                        }
                        else
                        {
                            dateTime = Convert.ToDateTime(date);
                            dateTime1 = dateTime.AddHours(1).AddMinutes(4);
                        }
                        DateTime dateTime4 = dateTime1;
                        startTime = (dateTime4 < currentTimeByTimeZone1 ? dateTime4.ToString("hh:mm tt") : currentTimeByTimeZone1.ToString("hh:mm tt"));
                        dateTime = currentTimeByTimeZone.AddMinutes(-150);
                        string str2 = dateTime.ToString("hh:mm tt");
                        if (Convert.ToDateTime(startTime) < Convert.ToDateTime(str2) && Convert.ToDateTime(date).Date == DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date && currentTimeByTimeZone.AddMinutes(-150) != dateTime3)
                        {
                            if (dateTime3 < currentTimeByTimeZone1 && dateTime3 != currentTimeByTimeZone1 && (currentTimeByTimeZone1 - DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").AddHours(1).AddMinutes(4)).Minutes > 0)
                            {
                                str1 = string.Concat(new object[] { str1, "<br/>", startTime, "-", str2 });
                            }
                        }
                        else if (startTime != str2 && Convert.ToDateTime(startTime) < Convert.ToDateTime(str2))
                        {
                            str1 = string.Concat(new object[] { str1, "<br/>", startTime, "-", str2 });
                        }
                        currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        currentTimeByTimeZone = currentTimeByTimeZone.Date;
                        currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                        currentTimeByTimeZone = (currentTimeByTimeZone.AddHours(2.5).Date > DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? currentTimeByTimeZone : currentTimeByTimeZone.AddHours(2.5));
                        startTime = currentTimeByTimeZone.ToString("hh:mm tt");
                        currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        currentTimeByTimeZone = currentTimeByTimeZone.Date;
                        currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                        dateTime2 = (currentTimeByTimeZone.AddHours(2.5).Date > DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? currentTimeByTimeZone : currentTimeByTimeZone.AddHours(2.5));
                        DateTime currentTimeByTimeZone2 = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        if (dateTime2 < currentTimeByTimeZone2)
                        {
                            flag = true;
                            if (currentTimeByTimeZone2.AddHours(1).AddMinutes(4) >= Convert.ToDateTime(endTime))
                            {
                                str = currentTimeByTimeZone2.ToString("hh:mm tt");
                            }
                            else
                            {
                                currentTimeByTimeZone = currentTimeByTimeZone2.AddHours(1).AddMinutes(4);
                                str = currentTimeByTimeZone.ToString("hh:mm tt");
                            }
                            startTime = str;
                        }
                    }
                    if (flag || !string.IsNullOrEmpty(str1))
                    {
                        if (Convert.ToDateTime(startTime) > Convert.ToDateTime(endTime) && (string.IsNullOrEmpty(str1) || str1 == "Available "))
                        {
                            str1 = string.Concat(new string[] { "Booked Today", "<br/>" });
                        }
                        else if (!(Convert.ToDateTime(date).Date == DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date) || !(Convert.ToDateTime(startTime) > Convert.ToDateTime(endTime)))
                        {
                            if (Convert.ToDateTime(startTime).AddHours(2.5).Date <= Convert.ToDateTime(endTime).Date && (string.IsNullOrEmpty(str1) || str1 == "Available ") && Convert.ToDateTime(startTime).AddHours(2.5) < Convert.ToDateTime(endTime))
                            {
                                str1 = string.Concat(new string[] { str1, "<br/>", startTime, "-", endTime });
                            }
                            else if ((string.IsNullOrEmpty(str1) || str1 == "Available ") && Convert.ToDateTime(startTime) < Convert.ToDateTime(endTime) && Convert.ToDateTime(startTime).AddMinutes(1) < Convert.ToDateTime(endTime) && (strs.Count == 0 || !strs.Contains(startTime)))
                            {
                                str1 = string.Concat(new string[] { str1, "<br/>", startTime, "-", endTime });
                            }
                            else if (string.IsNullOrEmpty(str1) || str1 == "Available ")
                            {
                                str1 = string.Concat(new string[] { "Booked Today", "<br/>" });
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                dataTable.Dispose();
            }
            return str1;
        }

        public string GetAvailablityWithAppointmentByDate(int thid, string startTime, string endTime, string date)
        {
            DateTime dateTime;
            DateTime dateTime1;
            string str = "";
            SqlHelper sqlHelper = new SqlHelper();
            bool flag = false;
            DataTable dataTable = sqlHelper.fillDataTable("GetConfirmedAppointementTimeBYthid", "", new SortedList()
            {
                { "@date", date },
                { "@thid", thid }
            });
            DateTime dateTime2 = Convert.ToDateTime(date);
            try
            {
                List<string> strs = new List<string>();
                if (dataTable.Rows.Count > 0)
                {
                    int num = Convert.ToInt32(dataTable.Rows[0]["DailyCount"]);
                    if (dataTable.Rows.Count >= num)
                    {
                        str = string.Concat(str, "Booked Today ");
                    }
                    else
                    {
                        str = string.Concat(str, "Available ");
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            flag = false;
                            strs.Add((dataTable.Rows[i]["ConfirmedTime"].ToString().Length <= 7 ? string.Concat("0", dataTable.Rows[i]["ConfirmedTime"].ToString()) : dataTable.Rows[i]["ConfirmedTime"].ToString()));
                            startTime = (startTime.Length <= 7 ? string.Concat("0", startTime) : startTime);
                            DateTime dateTime3 = Convert.ToDateTime(date);
                            DateTime dateTime4 = DateTime.Parse(string.Concat(dateTime3.ToShortDateString(), " ", startTime));
                            dateTime3 = DateTime.Parse(string.Concat(dateTime3.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                            dateTime2 = dateTime4.AddHours(1).AddMinutes(4);
                            if (dateTime4 > Convert.ToDateTime(date).AddHours(1).AddMinutes(4))
                            {
                                dateTime1 = dateTime4;
                            }
                            else
                            {
                                dateTime = Convert.ToDateTime(date);
                                dateTime1 = dateTime.AddHours(1).AddMinutes(4);
                            }
                            DateTime dateTime5 = dateTime1;
                            startTime = (dateTime5 < dateTime2 ? dateTime5.ToString("hh:mm tt") : dateTime2.ToString("hh:mm tt"));
                            dateTime = dateTime3.AddMinutes(-150);
                            string str1 = dateTime.ToString("hh:mm tt");
                            if (Convert.ToDateTime(startTime) < Convert.ToDateTime(str1) && Convert.ToDateTime(date).Date == DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date && dateTime3.AddMinutes(-150) != dateTime4)
                            {
                                if (dateTime4 < dateTime2 && dateTime4 != dateTime2 && (dateTime2 - DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").AddHours(1).AddMinutes(4)).Minutes > 0)
                                {
                                    str = string.Concat(new object[] { str, "<br/>", startTime, "-", str1 });
                                }
                            }
                            else if (!startTime.Contains(str1) && Convert.ToDateTime(startTime) < Convert.ToDateTime(str1))
                            {
                                str = string.Concat(new object[] { str, "<br/>", startTime, "-", str1 });
                            }
                            dateTime3 = Convert.ToDateTime(date).Date;
                            dateTime3 = DateTime.Parse(string.Concat(dateTime3.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                            dateTime3 = (dateTime3.AddHours(2.5).Date > Convert.ToDateTime(date).Date ? dateTime3 : dateTime3.AddHours(2.5));
                            startTime = dateTime3.ToString("hh:mm tt");
                            dateTime3 = Convert.ToDateTime(date).Date;
                            dateTime3 = DateTime.Parse(string.Concat(dateTime3.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                            DateTime dateTime6 = (dateTime3.AddHours(2.5).Date > Convert.ToDateTime(date).Date ? dateTime3 : dateTime3.AddHours(2.5));
                            DateTime dateTime7 = Convert.ToDateTime(date);
                            if (Convert.ToDateTime(date).Date == DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date && dateTime6 < dateTime7)
                            {
                                flag = true;
                                dateTime3 = dateTime7.AddHours(1).AddMinutes(4);
                                startTime = dateTime3.ToString("hh:mm tt");
                            }
                        }
                        if (flag || !string.IsNullOrEmpty(str))
                        {
                            if (Convert.ToDateTime(startTime) > Convert.ToDateTime(endTime) && (string.IsNullOrEmpty(str) || str == "Available "))
                            {
                                str = string.Concat(new string[] { "Booked Today", "<br/>" });
                            }
                            else if (!(Convert.ToDateTime(date).Date == Convert.ToDateTime(date).Date) || !(Convert.ToDateTime(startTime) > Convert.ToDateTime(endTime)))
                            {
                                if (Convert.ToDateTime(startTime).AddHours(2.5).Date <= Convert.ToDateTime(endTime).Date && (string.IsNullOrEmpty(str) || str.Contains("Available ")) && Convert.ToDateTime(startTime).AddHours(2.4) < Convert.ToDateTime(endTime))
                                {
                                    str = string.Concat(new string[] { str, "<br/>", startTime, "-", endTime });
                                }
                                else if ((string.IsNullOrEmpty(str) || str == "Available ") && Convert.ToDateTime(startTime) < Convert.ToDateTime(endTime) && Convert.ToDateTime(startTime).AddMinutes(1) < Convert.ToDateTime(endTime) && (strs.Count == 0 || !strs.Contains(startTime)))
                                {
                                    str = string.Concat(new string[] { str, "<br/>", startTime, "-", endTime });
                                }
                                else if (string.IsNullOrEmpty(str) || str == "Available ")
                                {
                                    str = string.Concat(new string[] { "Booked Today", "<br/>" });
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                dataTable.Dispose();
            }
            return str;
        }

        public string GetAvailablityWithAppointmentByDatenew(int thid, string startTime, string endTime, string date)
        {
            string str = "";
            SqlHelper sqlHelper = new SqlHelper();
            bool flag = false;
            DataTable dataTable = sqlHelper.fillDataTable("GetConfirmedAppointementTimeBYthid", "", new SortedList()
            {
                { "@date", date },
                { "@thid", thid }
            });
            DateTime dateTime = Convert.ToDateTime(date);
            try
            {
                List<string> strs = new List<string>();
                if (dataTable.Rows.Count > 0)
                {
                    int num = Convert.ToInt32(dataTable.Rows[0]["DailyCount"]);
                    if (dataTable.Rows.Count >= num)
                    {
                        str = string.Concat(str, "Booked Today ");
                    }
                    else
                    {
                        str = string.Concat(str, "Available ");
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            flag = false;
                            strs.Add((dataTable.Rows[i]["ConfirmedTime"].ToString().Length <= 7 ? string.Concat("0", dataTable.Rows[i]["ConfirmedTime"].ToString()) : dataTable.Rows[i]["ConfirmedTime"].ToString()));
                            startTime = (startTime.Length <= 7 ? string.Concat("0", startTime) : startTime);
                            DateTime dateTime1 = Convert.ToDateTime(date);
                            DateTime dateTime2 = DateTime.Parse(string.Concat(dateTime1.ToShortDateString(), " ", startTime));
                            dateTime1 = DateTime.Parse(string.Concat(dateTime1.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                            dateTime = dateTime1.AddHours(1).AddMinutes(4);
                            DateTime dateTime3 = (dateTime2 > Convert.ToDateTime(date).AddHours(1).AddMinutes(4) ? dateTime2 : Convert.ToDateTime(date).AddHours(1).AddMinutes(4));
                            startTime = (dateTime3 < dateTime ? dateTime3.ToString("hh:mm tt") : dateTime.ToString("hh:mm tt"));
                            string str1 = dateTime1.ToString("hh:mm tt");
                            if (Convert.ToDateTime(date).Date == DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date)
                            {
                                if (dateTime2 < dateTime && dateTime2 != dateTime && (dateTime - DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").AddHours(1).AddMinutes(4)).Hours > 0)
                                {
                                    str = string.Concat(new object[] { str, "<br/>", startTime, "-", str1 });
                                }
                            }
                            else if (!startTime.Contains(str1))
                            {
                                str = string.Concat(new object[] { str, "<br/>", startTime, "-", str1 });
                            }
                            dateTime1 = Convert.ToDateTime(date).Date;
                            dateTime1 = DateTime.Parse(string.Concat(dateTime1.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                            dateTime1 = (dateTime1.AddHours(2.5).Date > Convert.ToDateTime(date).Date ? dateTime1 : dateTime1.AddHours(2.5));
                            startTime = dateTime1.ToString("hh:mm tt");
                            dateTime1 = Convert.ToDateTime(date).Date;
                            dateTime1 = DateTime.Parse(string.Concat(dateTime1.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                            DateTime dateTime4 = (dateTime1.AddHours(2.5).Date > Convert.ToDateTime(date).Date ? dateTime1 : dateTime1.AddHours(2.5));
                            DateTime dateTime5 = Convert.ToDateTime(date);
                            if (Convert.ToDateTime(date).Date == DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date && dateTime4 < dateTime5)
                            {
                                flag = true;
                                dateTime1 = dateTime5.AddHours(1).AddMinutes(4);
                                startTime = dateTime1.ToString("hh:mm tt");
                            }
                        }
                        if (flag || !string.IsNullOrEmpty(str))
                        {
                            if (Convert.ToDateTime(startTime) > Convert.ToDateTime(endTime) && (string.IsNullOrEmpty(str) || str == "Available "))
                            {
                                str = string.Concat(new string[] { "Booked Today", "<br/>" });
                            }
                            else if (!(Convert.ToDateTime(date).Date == Convert.ToDateTime(date).Date) || !(Convert.ToDateTime(startTime) > Convert.ToDateTime(endTime)))
                            {
                                if (Convert.ToDateTime(startTime).AddHours(2.5).Date <= Convert.ToDateTime(endTime).Date && (string.IsNullOrEmpty(str) || str == "Available ") && Convert.ToDateTime(startTime).AddHours(2.5) < Convert.ToDateTime(endTime))
                                {
                                    str = string.Concat(new string[] { str, "<br/>", startTime, "-", endTime });
                                }
                                else if ((string.IsNullOrEmpty(str) || str == "Available ") && Convert.ToDateTime(startTime) < Convert.ToDateTime(endTime) && Convert.ToDateTime(startTime).AddMinutes(1) < Convert.ToDateTime(endTime) && (strs.Count == 0 || !strs.Contains(startTime)))
                                {
                                    str = string.Concat(new string[] { str, "<br/>", startTime, "-", endTime });
                                }
                                else if (string.IsNullOrEmpty(str) || str == "Available ")
                                {
                                    str = string.Concat(new string[] { "Booked Today", "<br/>" });
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                dataTable.Dispose();
            }
            return str;
        }

        public string GetAvailablityWithAppointmentByDateold(int thid, string startTime, string endTime, string date)
        {
            DateTime currentTimeByTimeZone;
            int day;
            DateTime dateTime;
            DateTime dateTime1;
            object str;
            string str1 = "";
            SqlHelper sqlHelper = new SqlHelper();
            bool flag = false;
            if (string.IsNullOrEmpty(date))
            {
                object[] month = new object[5];
                currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                month[0] = currentTimeByTimeZone.Month;
                month[1] = "-";
                currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                if (currentTimeByTimeZone.Day.ToString().Length == 1)
                {
                    day = currentTimeByTimeZone.Day;
                    str = string.Concat("0", day.ToString());
                }
                else
                {
                    day = currentTimeByTimeZone.Day;
                    str = day.ToString();
                }
                month[2] = str;
                month[3] = "-";
                currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                int year = currentTimeByTimeZone.Year;
                month[4] = year.ToString().Substring(2);
                date = string.Concat(month);
            }
            DataTable dataTable = sqlHelper.fillDataTable("GetConfirmedAppointementTimeBYthid", "", new SortedList()
            {
                { "@date", date },
                { "@thid", thid }
            });
            DateTime currentTimeByTimeZone1 = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            try
            {
                List<string> strs = new List<string>();
                if (dataTable.Rows.Count > 0)
                {
                    int num = Convert.ToInt32(dataTable.Rows[0]["DailyCount"]);
                    if (dataTable.Rows.Count >= num)
                    {
                        str1 = string.Concat(str1, "Booked Today ");
                    }
                    else
                    {
                        str1 = string.Concat(str1, "Available ");
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            flag = false;
                            strs.Add((dataTable.Rows[i]["ConfirmedTime"].ToString().Length <= 7 ? string.Concat("0", dataTable.Rows[i]["ConfirmedTime"].ToString()) : dataTable.Rows[i]["ConfirmedTime"].ToString()));
                            startTime = (startTime.Length <= 7 ? string.Concat("0", startTime) : startTime);
                            currentTimeByTimeZone = Convert.ToDateTime(date);
                            DateTime dateTime2 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", startTime));
                            currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                            currentTimeByTimeZone1 = dateTime2.AddHours(1).AddMinutes(4);
                            if (dateTime2 > Convert.ToDateTime(date).AddHours(1).AddMinutes(4))
                            {
                                dateTime1 = dateTime2;
                            }
                            else
                            {
                                dateTime = Convert.ToDateTime(date);
                                dateTime1 = dateTime.AddHours(1).AddMinutes(4);
                            }
                            DateTime dateTime3 = dateTime1;
                            startTime = (dateTime3 < currentTimeByTimeZone1 ? dateTime3.ToString("hh:mm tt") : currentTimeByTimeZone1.ToString("hh:mm tt"));
                            dateTime = currentTimeByTimeZone.AddMinutes(-150);
                            string str2 = dateTime.ToString("hh:mm tt");
                            if (Convert.ToDateTime(startTime) < Convert.ToDateTime(str2) && Convert.ToDateTime(date).Date == DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date && currentTimeByTimeZone.AddMinutes(-150) != dateTime2)
                            {
                                if (dateTime2 < currentTimeByTimeZone1 && dateTime2 != currentTimeByTimeZone1 && (currentTimeByTimeZone1 - DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").AddHours(1).AddMinutes(4)).Minutes > 0)
                                {
                                    str1 = string.Concat(new object[] { str1, "<br/>", startTime, "-", str2 });
                                }
                            }
                            else if (Convert.ToDateTime(startTime) < Convert.ToDateTime(str2) && !startTime.Contains(str2))
                            {
                                str1 = string.Concat(new object[] { str1, "<br/>", startTime, "-", str2 });
                            }
                            currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                            currentTimeByTimeZone = currentTimeByTimeZone.Date;
                            currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                            currentTimeByTimeZone = (currentTimeByTimeZone.AddHours(2.5).Date > DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? currentTimeByTimeZone : currentTimeByTimeZone.AddHours(2.5));
                            startTime = currentTimeByTimeZone.ToString("hh:mm tt");
                            currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                            currentTimeByTimeZone = currentTimeByTimeZone.Date;
                            currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                            DateTime dateTime4 = (currentTimeByTimeZone.AddHours(2.5).Date > DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? currentTimeByTimeZone : currentTimeByTimeZone.AddHours(2.5));
                            DateTime currentTimeByTimeZone2 = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                            if (Convert.ToDateTime(date).Date == DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date && dateTime4 < currentTimeByTimeZone2)
                            {
                                flag = true;
                                currentTimeByTimeZone = currentTimeByTimeZone2.AddHours(1).AddMinutes(4);
                                startTime = currentTimeByTimeZone.ToString("hh:mm tt");
                            }
                        }
                        if (flag || !string.IsNullOrEmpty(str1))
                        {
                            if (Convert.ToDateTime(startTime) > Convert.ToDateTime(endTime) && (string.IsNullOrEmpty(str1) || str1 == "Available "))
                            {
                                str1 = string.Concat(new string[] { "Booked Today", "<br/>" });
                            }
                            else if (!(Convert.ToDateTime(date).Date == DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date) || !(Convert.ToDateTime(startTime) > Convert.ToDateTime(endTime)))
                            {
                                if (Convert.ToDateTime(startTime).AddHours(2.5).Date <= Convert.ToDateTime(endTime).Date && (string.IsNullOrEmpty(str1) || str1 == "Available ") && Convert.ToDateTime(startTime).AddHours(2.5) < Convert.ToDateTime(endTime))
                                {
                                    str1 = string.Concat(new string[] { str1, "<br/>", startTime, "-", endTime });
                                }
                                else if ((string.IsNullOrEmpty(str1) || str1 == "Available ") && Convert.ToDateTime(startTime) < Convert.ToDateTime(endTime) && Convert.ToDateTime(startTime).AddMinutes(1) < Convert.ToDateTime(endTime) && (strs.Count == 0 || !strs.Contains(startTime)))
                                {
                                    str1 = string.Concat(new string[] { str1, "<br/>", startTime, "-", endTime });
                                }
                                else if (string.IsNullOrEmpty(str1) || str1 == "Available ")
                                {
                                    str1 = string.Concat(new string[] { "Booked Today", "<br/>" });
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                dataTable.Dispose();
            }
            return str1;
        }

        public string GetAvailablityWithAppointmenthotelByDate(int thid, string startTime, string endTime, string date = "")
        {
            DateTime currentTimeByTimeZone;
            int day;
            object str;
            string str1 = "";
            SqlHelper sqlHelper = new SqlHelper();
            string str2 = "";
            string str3 = "";
            if (string.IsNullOrEmpty(date))
            {
                object[] month = new object[5];
                currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                month[0] = currentTimeByTimeZone.Month;
                month[1] = "-";
                if (currentTimeByTimeZone.Day.ToString().Length == 1)
                {
                    day = currentTimeByTimeZone.Day;
                    str = string.Concat("0", day.ToString());
                }
                else
                {
                    day = currentTimeByTimeZone.Day;
                    str = day.ToString();
                }
                month[2] = str;
                month[3] = "-";
                int year = currentTimeByTimeZone.Year;
                month[4] = year.ToString().Substring(2);
                date = string.Concat(month);
            }
            DataTable dataTable = sqlHelper.fillDataTable("GetConfirmedAppointementTimeBYhid", "", new SortedList()
            {
                { "@date", date },
                { "@thid", thid }
            });

            DateTime dateTime = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    str1 = string.Concat(str1, "Available ");

                    List<Tuple<DateTime, DateTime>> _time = new List<Tuple<DateTime, DateTime>>();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        bool willadd = true;
                        currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        DateTime dateTime1 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", startTime));
                        dateTime = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                        currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                        string str4 = currentTimeByTimeZone.AddMinutes(-150).ToString("hh:mm tt");
                        startTime = (startTime.Length <= 7 ? string.Concat("0", startTime) : startTime);
                        Tuple<DateTime, DateTime> _item = new Tuple<DateTime, DateTime>(currentTimeByTimeZone.AddMinutes(-150), currentTimeByTimeZone.AddHours(2.5));
                        _time.Add(_item);
                        if (i > 0)
                        {
                            foreach (var item in _time)
                            {
                                if (currentTimeByTimeZone >= item.Item1 & currentTimeByTimeZone <= item.Item2)
                                {
                                    willadd = false;
                                    startTime = item.Item2.ToString("hh:mm tt");
                                    break;
                                }
                            }
                        }
                        if (willadd == true)
                        {
                            if (Convert.ToDateTime(date).Date == DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date)
                            {
                                if (dateTime1 < dateTime && dateTime1 != dateTime)
                                {
                                    if (Convert.ToDateTime(startTime) >= Convert.ToDateTime(str4))
                                    {
                                        str2 = str4;
                                        str3 = startTime;
                                    }
                                    else
                                    {
                                        str2 = startTime;
                                        str3 = str4;
                                    }
                                    str1 = (string.IsNullOrEmpty(str1) ? string.Concat(new object[] { str1, "<br/>", str2, "-", str3 }) : string.Concat(new object[] { str1, "<br/>", str2, "-", str3, " And " }));
                                }
                            }
                            else if (Convert.ToDateTime(startTime) < Convert.ToDateTime(str4))
                            {
                                if (Convert.ToDateTime(startTime) >= Convert.ToDateTime(str4))
                                {
                                    str2 = str4;
                                    str3 = startTime;
                                }
                                else
                                {
                                    str2 = startTime;
                                    str3 = str4;
                                }
                                str1 = (string.IsNullOrEmpty(str1) ? string.Concat(new object[] { str1, "<br/>", str2, "-", str3 }) : string.Concat(new object[] { str1, "<br/>", str2, "-", str3, " And " }));
                            }
                            currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                            currentTimeByTimeZone = (currentTimeByTimeZone.AddHours(2.5).Date > DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? currentTimeByTimeZone : currentTimeByTimeZone.AddHours(2.5));
                            startTime = currentTimeByTimeZone.ToString("hh:mm tt");
                            currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                            DateTime dateTime2 = (currentTimeByTimeZone.AddHours(2.5).Date > DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? currentTimeByTimeZone : currentTimeByTimeZone.AddHours(2.5));
                            DateTime currentTimeByTimeZone1 = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                            if (Convert.ToDateTime(date).Date == DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date && dateTime2 < currentTimeByTimeZone1)
                            {
                                startTime = currentTimeByTimeZone1.ToString("hh:mm tt");
                            }
                        }
                    }
                    if (Convert.ToDateTime(startTime) < Convert.ToDateTime(endTime))
                    {
                        str1 = (string.IsNullOrEmpty(str1) ? string.Concat(new object[] { str1, "<br/>", startTime, "-", endTime }) : string.Concat(new object[] { str1, "<br/>", startTime, "-", endTime }));
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                dataTable.Dispose();
            }
            str1 = (str1 == "Available " ? "Booked Full Day" : str1);
            return str1;
        }

        private string GetAvailablityWithhotelAppointment(int thid, string startTime, string endTime, string date = "")
        {
            DateTime currentTimeByTimeZone;
            DateTime dateTime;
            string str = "";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            if (string.IsNullOrEmpty(date))
            {
                object[] month = new object[5];
                currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                month[0] = currentTimeByTimeZone.Month;
                month[1] = "-";
                month[2] = currentTimeByTimeZone.Day;
                month[3] = "-";
                month[4] = currentTimeByTimeZone.Year.ToString().Substring(2);
                date = string.Concat(month);
            }
            sortedLists.Add("@date", date);
            sortedLists.Add("@thid", thid);
            DataTable dataTable = sqlHelper.fillDataTable("GetConfirmedAppointementTimeBYhid", "", sortedLists);
            DateTime currentTimeByTimeZone1 = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    str = string.Concat(str, "Available ");
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        currentTimeByTimeZone = currentTimeByTimeZone.Date;
                        DateTime dateTime1 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", startTime));
                        currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date;
                        currentTimeByTimeZone1 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                        currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        currentTimeByTimeZone = currentTimeByTimeZone.Date;
                        currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                        currentTimeByTimeZone = currentTimeByTimeZone.AddHours(-2.5);
                        string str1 = currentTimeByTimeZone.ToString("hh:mm tt");
                        if (dateTime1 < currentTimeByTimeZone1)
                        {
                            str = string.Concat(new object[] { str, "<br/>", startTime, "-", str1 });
                        }
                        currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        currentTimeByTimeZone = currentTimeByTimeZone.Date;
                        currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                        currentTimeByTimeZone = (currentTimeByTimeZone.AddHours(2.5).Date > DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? currentTimeByTimeZone : currentTimeByTimeZone.AddHours(2.5));
                        startTime = currentTimeByTimeZone.ToString("hh:mm tt");
                        currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        currentTimeByTimeZone = currentTimeByTimeZone.Date;
                        currentTimeByTimeZone = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[i]["ConfirmedTime"]));
                        dateTime = (currentTimeByTimeZone.AddHours(2.5).Date > DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? currentTimeByTimeZone : currentTimeByTimeZone.AddHours(2.5));
                        DateTime currentTimeByTimeZone2 = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        if (dateTime < currentTimeByTimeZone2)
                        {
                            startTime = currentTimeByTimeZone2.ToString("hh:mm tt");
                        }
                    }
                    if (Convert.ToDateTime(startTime) < Convert.ToDateTime(endTime))
                    {
                        str = string.Concat(new string[] { str, "<br/>", startTime, "-", endTime });
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                dataTable.Dispose();
            }
            return str;
        }

        private DateTime GetCurrentTimeByTimeZone(string Timezone = "India Standard Time")
        {
            return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById(Timezone));
        }

        public List<Events> GetEvents(int thid, DateTime start, DateTime end, int byclient, int cnt)
        {
            string str;
            string str1;
            string str2;
            string str3;
            string str4;
            string str5;
            string str6;
            string str7;
            string str8;
            List<Events> events = new List<Events>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
            try
            {
                sortedLists.Add("@id", thid);
                DataTable dataTable = sqlHelper.fillDataTable("GetScheduleByTherapistId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    List<therapistService.ScheduleModel> scheduleModels = new List<therapistService.ScheduleModel>();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        scheduleModels.Add(new therapistService.ScheduleModel()
                        {
                            _type = dataTable.Rows[i]["Type"].ToString(),
                            _start = dataTable.Rows[i]["Starttime"].ToString(),
                            _end = dataTable.Rows[i]["EndTime"].ToString(),
                            _date = Convert.ToDateTime(dataTable.Rows[i]["aptDate"].ToString()),
                            _working = dataTable.Rows[i]["Working"].ToString()
                        });
                    }
                    dataTable.Dispose();
                    object[] month = new object[5];
                    DateTime date = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                    month[0] = date.Month;
                    month[1] = "/";
                    month[2] = date.Day;
                    month[3] = "/";
                    month[4] = date.Year;
                    DateTime dateTime = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                    DateTime currentTimeByTimeZone1 = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                    string str9 = "";
                    string str10 = "";
                    string str11 = "";
                    DataTable dataTable1 = new DataTable();
                    int num = 0;
                    DateTime date1 = new DateTime();
                    DateTime dateTime1 = new DateTime();
                    DateTime dateTime2 = new DateTime();
                    DateTime dateTime3 = new DateTime();
                    for (DateTime j = start; j < end; j = j.AddDays(1))
                    {
                        str11 = "";
                        string[] strArrays = new string[5];
                        int day = j.Month;
                        strArrays[0] = day.ToString();
                        strArrays[1] = "-";
                        if (j.Day.ToString().Length <= 1)
                        {
                            day = j.Day;
                            str = string.Concat("0", day.ToString());
                        }
                        else
                        {
                            day = j.Day;
                            str = day.ToString();
                        }
                        strArrays[2] = str;
                        strArrays[3] = "-";
                        day = j.Year;
                        strArrays[4] = day.ToString().Substring(2);
                        str10 = string.Concat(strArrays);
                        sortedLists.Clear();
                        sortedLists.Add("@tid", thid);
                        sortedLists.Add("@aptDate", str10);
                        DataTable dataTable2 = sqlHelper.fillDataTable("GetScheduledByAppointmnetDateorTherapistId", "", sortedLists);
                        num = Convert.ToInt32(dataTable2.Rows[0]["ConfirmedAppointmentTotal"].ToString());
                        if (num >= Convert.ToInt32(dataTable2.Rows[0]["DailyCount"].ToString()) && cnt == 0)
                        {
                            str11 = "Booked FullDay";
                        }
                        dataTable2.Dispose();
                        IEnumerable<therapistService.ScheduleModel> scheduleModels1 = scheduleModels.Where<therapistService.ScheduleModel>((therapistService.ScheduleModel a) =>
                        {
                            if (a._date.Date != j.Date)
                            {
                                return false;
                            }
                            return a._type == "Manual";
                        });
                        if (scheduleModels1 == null || scheduleModels1.Count<therapistService.ScheduleModel>() == 0)
                        {
                            str9 = "";
                            str9 = (num <= 0 || byclient <= 0 ? "" : "Click here to check Avail");
                            scheduleModels1 =
                                from a in scheduleModels
                                where a._type == "Auto"
                                select a;
                            string str12 = string.Concat(new object[] { j.Month, "/", j.Day, "/", j.Year });
                            date1 = Convert.ToDateTime(str12).Date;
                            dateTime = Convert.ToDateTime(string.Concat(str12, " ", scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._start));
                            currentTimeByTimeZone1 = Convert.ToDateTime(string.Concat(str12, " ", scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._end));
                            if (currentTimeByTimeZone.Date == date1)
                            {
                                date = currentTimeByTimeZone.Date;
                                dateTime2 = DateTime.Parse(string.Concat(date.ToShortDateString(), " ", scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._start));
                                date = currentTimeByTimeZone.Date;
                                dateTime3 = DateTime.Parse(string.Concat(date.ToShortDateString(), " ", scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._end));
                                if (currentTimeByTimeZone > dateTime3)
                                {
                                    events.Add(new Events()
                                    {
                                        id = "1",
                                        start = dateTime.ToString("s"),
                                        end = currentTimeByTimeZone1.ToString("s"),
                                        title = (str11 == "Booked FullDay" ? str11 : "Not Available"),
                                        allDay = false,
                                        className = (dateTime.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "")
                                    });
                                }
                                else if (!(currentTimeByTimeZone > dateTime2) || !(currentTimeByTimeZone < dateTime3))
                                {
                                    Events @event = new Events()
                                    {
                                        id = "1"
                                    };
                                    if (str11 != "Booked FullDay")
                                    {
                                        str2 = (scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._working == "Y" ? str9 : "Not Working");
                                    }
                                    else
                                    {
                                        str2 = str11;
                                    }
                                    @event.title = str2;
                                    @event.start = dateTime.ToString("s");
                                    @event.end = currentTimeByTimeZone1.ToString("s");
                                    @event.allDay = false;
                                    @event.className = (dateTime.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "");
                                    events.Add(@event);
                                }
                                else if (DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").AddHours(1).AddMinutes(4) <= currentTimeByTimeZone1)
                                {
                                    dateTime1 = this.GetCurrentTimeByTimeZone("India Standard Time");
                                    string str13 = dateTime1.ToString("hh:mm tt");
                                    dateTime = Convert.ToDateTime(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", str13));
                                    currentTimeByTimeZone1 = Convert.ToDateTime(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._end));
                                    Events event1 = new Events()
                                    {
                                        id = "1"
                                    };
                                    if (dateTime.AddHours(1).AddMinutes(4) >= currentTimeByTimeZone1)
                                    {
                                        str3 = dateTime.ToString("s");
                                    }
                                    else
                                    {
                                        date = dateTime.AddHours(1).AddMinutes(4);
                                        str3 = date.ToString("s");
                                    }
                                    event1.start = str3;
                                    event1.end = currentTimeByTimeZone1.ToString("s");
                                    if (str11 != "Booked FullDay")
                                    {
                                        str4 = (scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._working == "Y" ? str9 : "Not Working");
                                    }
                                    else
                                    {
                                        str4 = str11;
                                    }
                                    event1.title = str4;
                                    event1.allDay = false;
                                    event1.className = (dateTime.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "");
                                    events.Add(event1);
                                }
                                else
                                {
                                    dateTime = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                                    currentTimeByTimeZone1 = Convert.ToDateTime(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._end));
                                    Events event2 = new Events()
                                    {
                                        id = "1",
                                        start = dateTime.ToString("s"),
                                        end = currentTimeByTimeZone1.ToString("s"),
                                        title = "Not Available",
                                        allDay = false,
                                        className = "redEvent"
                                    };
                                }
                            }
                            else
                            {
                                Events event3 = new Events()
                                {
                                    id = "1"
                                };
                                if (str11 != "Booked FullDay")
                                {
                                    str1 = (scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._working == "Y" ? str9 : "Not Working");
                                }
                                else
                                {
                                    str1 = str11;
                                }
                                event3.title = str1;
                                event3.start = dateTime.ToString("s");
                                event3.end = currentTimeByTimeZone1.ToString("s");
                                event3.allDay = false;
                                event3.className = (dateTime.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "");
                                events.Add(event3);
                            }
                        }
                        else
                        {
                            str9 = "Edited";
                            str9 = (num <= 0 || byclient <= 0 ? "Edited" : "Click here to check Avail");
                            string str14 = string.Concat(new object[] { j.Month, "/", j.Day, "/", j.Year });
                            DateTime date2 = Convert.ToDateTime(str14).Date;
                            dateTime = Convert.ToDateTime(string.Concat(str14, " ", scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._start));
                            currentTimeByTimeZone1 = Convert.ToDateTime(string.Concat(str14, " ", scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._end));
                            if (currentTimeByTimeZone.Date == date2)
                            {
                                date = currentTimeByTimeZone.Date;
                                dateTime2 = DateTime.Parse(string.Concat(date.ToShortDateString(), " ", scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._start));
                                date = currentTimeByTimeZone.Date;
                                dateTime3 = DateTime.Parse(string.Concat(date.ToShortDateString(), " ", scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._end));
                                if (currentTimeByTimeZone > dateTime3)
                                {
                                    events.Add(new Events()
                                    {
                                        id = "1",
                                        start = dateTime.ToString("s"),
                                        end = currentTimeByTimeZone1.ToString("s"),
                                        title = (str11 == "booked" ? str11 : "Not Available"),
                                        allDay = false,
                                        className = (dateTime.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "")
                                    });
                                }
                                else if (!(currentTimeByTimeZone > dateTime2) || !(currentTimeByTimeZone < dateTime3))
                                {
                                    Events event4 = new Events()
                                    {
                                        id = "1",
                                        start = dateTime.ToString("s"),
                                        end = currentTimeByTimeZone1.ToString("s")
                                    };
                                    if (str11 != "Booked FullDay")
                                    {
                                        str6 = (scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._working == "Y" ? "Edited" : "Not Working");
                                        str6 = (num <= 0 || byclient <= 0 ? "Edited" : "Click here to check Avail");
                                    }
                                    else
                                    {
                                        str6 = str11;
                                    }
                                    event4.title = str6;
                                    event4.allDay = false;
                                    event4.className = (dateTime.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "");
                                    events.Add(event4);
                                }
                                else
                                {
                                    dateTime1 = this.GetCurrentTimeByTimeZone("India Standard Time");
                                    string str15 = dateTime1.ToString("hh:mm tt");
                                    dateTime = Convert.ToDateTime(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", str15));
                                    currentTimeByTimeZone1 = Convert.ToDateTime(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._end));
                                    Events event5 = new Events()
                                    {
                                        id = "1"
                                    };
                                    if (dateTime.AddHours(1).AddMinutes(4) >= currentTimeByTimeZone1)
                                    {
                                        str7 = dateTime.ToString("s");
                                    }
                                    else
                                    {
                                        date = dateTime.AddHours(1).AddMinutes(4);
                                        str7 = date.ToString("s");
                                    }
                                    event5.start = str7;
                                    event5.end = currentTimeByTimeZone1.ToString("s");
                                    if (str11 != "Booked FullDay")
                                    {
                                        str8 = (scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._working == "Y" ? str9 : "Not Working");
                                    }
                                    else
                                    {
                                        str8 = str11;
                                    }
                                    event5.title = str8;
                                    event5.allDay = false;
                                    event5.className = (dateTime.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "");
                                    events.Add(event5);
                                }
                            }
                            else
                            {
                                Events event6 = new Events()
                                {
                                    id = "1",
                                    start = dateTime.ToString("s"),
                                    end = currentTimeByTimeZone1.ToString("s")
                                };
                                if (str11 != "Booked FullDay")
                                {
                                    str5 = (scheduleModels1.FirstOrDefault<therapistService.ScheduleModel>()._working == "Y" ? str9 : "Not Working");
                                }
                                else
                                {
                                    str5 = str11;
                                }
                                event6.title = str5;
                                event6.allDay = false;
                                event6.className = (dateTime.Date < DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time").Date ? "redEvent" : "");
                                events.Add(event6);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return events;
        }

        public string GethotelScheduleId(int thid, DateTime date, bool forEdit = false)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string availablityWithAppointmenthotelByDate = "";
            try
            {
                sortedLists.Add("@id", thid);
                sortedLists.Add("@IsEdit", forEdit);
                sortedLists.Add("@date", date);
                DataTable dataTable = sqlHelper.fillDataTable("GetAUTOScheduleByhotelId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        DateTime dateTime = DateTime.Parse(string.Concat(date.ToShortDateString(), " ", dataTable.Rows[0]["Starttime"].ToString()));
                        DateTime dateTime1 = date.Date;
                        //selecteddate
                        DateTime dateTime2 = DateTime.Parse(string.Concat(dateTime1.ToShortDateString(), " ", dataTable.Rows[0]["EndTime"].ToString()));
                        DateTime dateTime3 = currentTimeByTimeZone;
                        if (dateTime3 > dateTime2 && !forEdit)
                        {
                            availablityWithAppointmenthotelByDate = "Not Available Today";
                        }
                        else if (dateTime3.Date != date.Date)
                        {
                            object[] month = new object[] { date.Month, "-", date.Day, "-", null };
                            int year = date.Year;
                            month[4] = year.ToString().Substring(2);
                            string str = string.Concat(month);
                            availablityWithAppointmenthotelByDate = this.GetAvailablityWithAppointmenthotelByDate(thid, dataTable.Rows[0]["Starttime"].ToString(), dataTable.Rows[0]["EndTime"].ToString(), str);
                            if (availablityWithAppointmenthotelByDate == "")
                            {
                                availablityWithAppointmenthotelByDate = string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString());
                            }
                        }
                        else if (dateTime3 < dateTime && !forEdit)
                        {
                            availablityWithAppointmenthotelByDate = this.GetAvailablityWithAppointmenthotelByDate(thid, dataTable.Rows[0]["Starttime"].ToString(), dataTable.Rows[0]["EndTime"].ToString(), "");
                            if (availablityWithAppointmenthotelByDate == "")
                            {
                                availablityWithAppointmenthotelByDate = string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString());
                            }
                        }
                        else if (!((dateTime3 >= dateTime2 ? true : !(dateTime3 > dateTime)) | forEdit))
                        {
                            string str1 = currentTimeByTimeZone.ToString("hh:mm tt");
                            availablityWithAppointmenthotelByDate = this.GetAvailablityWithAppointmenthotelByDate(thid, str1, dataTable.Rows[0]["EndTime"].ToString(), "").Replace("Available", "");
                            if (availablityWithAppointmenthotelByDate == "")
                            {
                                availablityWithAppointmenthotelByDate = string.Concat(str1, "-", dataTable.Rows[0]["EndTime"].ToString());
                            }
                        }
                        else
                        {
                            availablityWithAppointmenthotelByDate = (forEdit ? string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()) : string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return (new HtmlString(availablityWithAppointmenthotelByDate)).ToString();
        }

        public string GethotelScheduleIdwithdate(int thid, DateTime date, bool forEdit = false)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string availablityWithAppointmenthotelByDate = "";
            try
            {
                sortedLists.Add("@id", thid);
                sortedLists.Add("@IsEdit", forEdit);
                sortedLists.Add("@date", date);
                DataTable dataTable = sqlHelper.fillDataTable("GetAUTOScheduleByhotelId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                        DateTime dateTime = DateTime.Parse(string.Concat(date.ToShortDateString(), " ", dataTable.Rows[0]["Starttime"].ToString()));
                        DateTime dateTime1 = date.Date;
                        //selecteddate
                        DateTime dateTime2 = DateTime.Parse(string.Concat(dateTime1.ToShortDateString(), " ", dataTable.Rows[0]["EndTime"].ToString()));
                        DateTime dateTime3 = currentTimeByTimeZone;
                        if (dateTime3 > dateTime2 && !forEdit)
                        {
                            availablityWithAppointmenthotelByDate = "Not Available Today";
                        }
                        else if (dateTime3.Date != date.Date)
                        {
                            object[] month = new object[] { date.Month, "-", date.Day, "-", null };
                            int year = date.Year;
                            month[4] = year.ToString().Substring(2);
                            string str = string.Concat(month);
                            availablityWithAppointmenthotelByDate = this.GetAvailablityWithAppointmenthotelByDate(thid, dataTable.Rows[0]["Starttime"].ToString(), dataTable.Rows[0]["EndTime"].ToString(), str);
                            if (availablityWithAppointmenthotelByDate == "")
                            {
                                availablityWithAppointmenthotelByDate = string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString());
                            }
                        }
                        else if (dateTime3 < dateTime && !forEdit)
                        {
                            availablityWithAppointmenthotelByDate = this.GetAvailablityWithAppointmenthotelByDate(thid, dataTable.Rows[0]["Starttime"].ToString(), dataTable.Rows[0]["EndTime"].ToString(), "");
                            if (availablityWithAppointmenthotelByDate == "")
                            {
                                availablityWithAppointmenthotelByDate = string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString());
                            }
                        }
                        else if (!((dateTime3 >= dateTime2 ? true : !(dateTime3 > dateTime)) | forEdit))
                        {
                            string str1 = currentTimeByTimeZone.ToString("hh:mm tt");
                            availablityWithAppointmenthotelByDate = this.GetAvailablityWithAppointmenthotelByDate(thid, str1, dataTable.Rows[0]["EndTime"].ToString(), "").Replace("Available", "");
                            if (availablityWithAppointmenthotelByDate == "")
                            {
                                availablityWithAppointmenthotelByDate = string.Concat(str1, "-", dataTable.Rows[0]["EndTime"].ToString());
                            }
                        }
                        else
                        {
                            availablityWithAppointmenthotelByDate = (forEdit ? string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()) : string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return (new HtmlString(availablityWithAppointmenthotelByDate)).ToString();
        }


        private DateTime GetIndianTimeFromPST(DateTime _date)
        {
            DateTime staticdatetime = Convert.ToDateTime("04/13/2020");
            string Timezone = "Pacific Standard Time";
            if (_date > staticdatetime)
            {
                Timezone = "UTC";
                _date = _date.AddMinutes(4);
            }
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_date, Timezone, "India Standard Time");
        }

        private DateTime GetPSTFromIndianTime(DateTime _date)
        {
            DateTime staticdatetime = Convert.ToDateTime("04/13/2020");
            string Timezone = "Pacific Standard Time";
            if (_date > staticdatetime)
            {
                Timezone = "UTC";
                _date = _date.AddMinutes(4);
            }
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_date, "India Standard Time", Timezone);
        }

        public string GetTherapistAvailbilityScheduleId(int thid, bool forEdit = false)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string availablityWithAppointment = "";
            try
            {
                sortedLists.Add("@id", thid);
                sortedLists.Add("@IsEdit", forEdit);
                DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                sortedLists.Add("@date", currentTimeByTimeZone.Date);
                DataTable dataTable = sqlHelper.fillDataTable("GetAUTOScheduleByTherapistId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    int num = Convert.ToInt32(dataTable.Rows[0]["DailyCount"].ToString());
                    if (!(Convert.ToInt32(dataTable.Rows[0]["TodayCount"].ToString()) < num | forEdit))
                    {
                        availablityWithAppointment = "Booked Today";
                    }
                    else
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            if (!(!(dataTable.Rows[0]["working"].ToString() == "N") | forEdit))
                            {
                                availablityWithAppointment = "Not Working - ";
                            }
                            else
                            {
                                DateTime dateTime = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                                currentTimeByTimeZone = dateTime.Date;
                                DateTime dateTime1 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[0]["Starttime"].ToString()));
                                currentTimeByTimeZone = dateTime.Date;
                                DateTime dateTime2 = DateTime.Parse(string.Concat(currentTimeByTimeZone.ToShortDateString(), " ", dataTable.Rows[0]["EndTime"].ToString()));
                                DateTime dateTime3 = dateTime.AddHours(1).AddMinutes(4);
                                if (dateTime3 > dateTime2 && !forEdit)
                                {
                                    availablityWithAppointment = "Not Available Today";
                                }
                                else if (dateTime3 < dateTime1 && !forEdit)
                                {
                                    availablityWithAppointment = this.GetAvailablityWithAppointment(thid, dataTable.Rows[0]["Starttime"].ToString(), dataTable.Rows[0]["EndTime"].ToString(), "", 1);
                                    if (availablityWithAppointment == "")
                                    {
                                        availablityWithAppointment = string.Concat("Available </br>", dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString());
                                    }
                                }
                                else if (!((dateTime3 >= dateTime2 ? true : !(dateTime3 > dateTime1)) | forEdit))
                                {
                                    string str = ((dateTime3 == dateTime1 ? dateTime : dateTime.AddHours(1).AddMinutes(4))).ToString("hh:mm tt");
                                    availablityWithAppointment = this.GetAvailablityWithAppointment(thid, str, dataTable.Rows[0]["EndTime"].ToString(), "", 1);
                                    if (availablityWithAppointment == "")
                                    {
                                        availablityWithAppointment = string.Concat("Available </br>", str, "-", dataTable.Rows[0]["EndTime"].ToString());
                                    }
                                }
                                else
                                {
                                    availablityWithAppointment = (forEdit ? string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()) : string.Concat("Available </br>", dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return availablityWithAppointment;
        }

        public string GetTherapistcallScheduleId(int thid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "";
            try
            {
                sortedLists.Add("@id", thid);
                DataTable dataTable = sqlHelper.fillDataTable("GetcallScheduleByTherapistId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        str = string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString());
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string GetTherapistScheduleId(int thid, bool forEdit = false)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "";
            try
            {
                sortedLists.Add("@id", thid);
                sortedLists.Add("@IsEdit", forEdit);
                DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                sortedLists.Add("@date", currentTimeByTimeZone.Date);
                DataTable dataTable = sqlHelper.fillDataTable("GetAUTOScheduleByTherapistId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        str = (!(dataTable.Rows[0]["working"].ToString() == "N") | forEdit ? string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()) : "Not Working - ");
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string GetTherapistScheduleId(int thid, DateTime date, bool forEdit = false)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string availablityWithAppointmentByDate = "";
            try
            {
                sortedLists.Add("@id", thid);
                sortedLists.Add("@IsEdit", forEdit);
                sortedLists.Add("@date", date);
                DataTable dataTable = sqlHelper.fillDataTable("GetAUTOScheduleByTherapistId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    int num = Convert.ToInt32(dataTable.Rows[0]["DailyCount"].ToString());
                    if (!(Convert.ToInt32(dataTable.Rows[0]["TodayCount"].ToString()) < num | forEdit))
                    {
                        availablityWithAppointmentByDate = "Booked Today";
                    }
                    else
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            if (!(!(dataTable.Rows[0]["working"].ToString() == "N") | forEdit))
                            {
                                availablityWithAppointmentByDate = "Not Working - ";
                            }
                            else
                            {
                                DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                                DateTime dateTime = DateTime.Parse(string.Concat(date.ToShortDateString(), " ", dataTable.Rows[0]["Starttime"].ToString()));
                                DateTime dateTime1 = date.Date;
                                DateTime dateTime2 = DateTime.Parse(string.Concat(dateTime1.ToShortDateString(), " ", dataTable.Rows[0]["EndTime"].ToString()));
                                DateTime dateTime3 = currentTimeByTimeZone.AddHours(1).AddMinutes(4);
                                if (dateTime3 > dateTime2 && !forEdit)
                                {
                                    availablityWithAppointmentByDate = "Not Available Today";
                                }
                                else if (dateTime3.Date != date.Date)
                                {
                                    object[] month = new object[] { date.Month, "-", date.Day, "-", null };
                                    int year = date.Year;
                                    month[4] = year.ToString().Substring(2);
                                    string str = string.Concat(month);
                                    availablityWithAppointmentByDate = this.GetAvailablityWithAppointmentByDate(thid, dataTable.Rows[0]["Starttime"].ToString(), dataTable.Rows[0]["EndTime"].ToString(), str);
                                    if (availablityWithAppointmentByDate == "")
                                    {
                                        availablityWithAppointmentByDate = string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString());
                                    }
                                }
                                else if (dateTime3 < dateTime && !forEdit)
                                {
                                    object[] objArray = new object[] { dateTime3.Month, "-", dateTime3.Day, "-", null };
                                    int year1 = dateTime3.Year;
                                    objArray[4] = year1.ToString().Substring(2);
                                    string str1 = string.Concat(objArray);
                                    availablityWithAppointmentByDate = this.GetAvailablityWithAppointmentByDate(thid, dataTable.Rows[0]["Starttime"].ToString(), dataTable.Rows[0]["EndTime"].ToString(), str1);
                                    if (availablityWithAppointmentByDate == "")
                                    {
                                        availablityWithAppointmentByDate = string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString());
                                    }
                                }
                                else if (!((dateTime3 >= dateTime2 ? true : !(dateTime3 > dateTime)) | forEdit))
                                {
                                    string str2 = currentTimeByTimeZone.AddHours(1).AddMinutes(4).ToString("hh:mm tt");
                                    availablityWithAppointmentByDate = this.GetAvailablityWithAppointmentByDateold(thid, str2, dataTable.Rows[0]["EndTime"].ToString(), "").Replace("Available Available", "");
                                    if (availablityWithAppointmentByDate == "")
                                    {
                                        dateTime1 = currentTimeByTimeZone.AddHours(1).AddMinutes(4);
                                        str2 = dateTime1.ToString("hh:mm tt");
                                        availablityWithAppointmentByDate = string.Concat(str2, "-", dataTable.Rows[0]["EndTime"].ToString());
                                    }
                                }
                                else
                                {
                                    availablityWithAppointmentByDate = (forEdit ? string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()) : string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString()));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return (new HtmlString(availablityWithAppointmentByDate)).ToString();
        }

        public List<comments> Gettop10Comments(int thid)
        {
            List<comments> _comments = new List<comments>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@thid", thid);
                DataTable dataTable = sqlHelper.fillDataTable("GetCommentsByTherapistid", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        _comments.Add(new comments()
                        {
                            Comment = dataTable.Rows[i]["Comments"].ToString()
                        });
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _comments;
        }

        public List<comments> GettopAllComments(int thid, int startindex, int endindex, ref int total)
        {
            total = 0;
            List<comments> _comments = new List<comments>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@thid", thid);
                sortedLists.Add("@startindex", startindex);
                sortedLists.Add("@endindex", endindex);
                DataTable dataTable = sqlHelper.fillDataTable("GetALLCommentsByTherapistid", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        comments comment = new comments()
                        {
                            Comment = dataTable.Rows[i]["Comments"].ToString(),
                            name = dataTable.Rows[i]["name"].ToString(),
                            rating = Convert.ToInt32(dataTable.Rows[i]["rating"].ToString())
                        };
                        DateTime dateTime = Convert.ToDateTime(dataTable.Rows[i]["createdDate"].ToString());
                        comment.Commentdate = dateTime.ToString("dd-MMM-y");
                        dateTime = Convert.ToDateTime(dataTable.Rows[i]["bookingdate"].ToString());
                        comment.aptdate = dateTime.ToString("dd-MMM-y");
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString());
                        comment.srno = Convert.ToInt32(dataTable.Rows[i]["RowNum"].ToString());
                        _comments.Add(comment);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _comments;
        }

        public string UpdateAppointmentOnReport(int Aid)
        {
            string str = "Record Remove Succesfully from Report";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@Aid", Aid);
                sqlHelper.executeNonQuery("UpdateApponitementReport", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        private string UpdateAppointmentRate(int Aid, int percentage, decimal pendingRate)
        {
            string str = "Discount Added";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {

                string str1 = pendingRate.ToString();
                sortedLists.Add("@AId", Aid);
                sortedLists.Add("@pendingRate", str1);
                sortedLists.Add("@percentage", percentage);
                sqlHelper.executeNonQuery("UpdateAppointmentRate", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string UpdateClientNameById(int clientId, string name)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "";
            try
            {
                sortedLists.Add("@id", clientId);
                sortedLists.Add("@newUserName", name);
                sqlHelper.executeNonQuery("UpdateClientUserNameByAdmin", "", sortedLists);
                str = "Name Changed Succesfully";
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string UpdateSchedule(DateTime date, string start, string end, int tid, string working)
        {
            string str = "User info updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@date", date);
                sortedLists.Add("@NewStartTime", start);
                sortedLists.Add("@NewEndTime", end);
                sortedLists.Add("@tid", tid);
                sortedLists.Add("@working", working);
                sqlHelper.executeNonQuery("UpdateScheduleDate", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string UpdateSchedule(DateTime date, string start, string end, int tid)
        {
            string str = "User info updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@date", date);
                sortedLists.Add("@NewStartTime", start);
                sortedLists.Add("@NewEndTime", end);
                sortedLists.Add("@tid", tid);
                sqlHelper.executeNonQuery("UpdatehotelScheduleDate", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string UpdateScheduleForAll(DateTime date, string start, string end, int tid, string working)
        {
            string str = "Your Schedule time is updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@date", date);
                sortedLists.Add("@NewStartTime", start);
                sortedLists.Add("@NewEndTime", end);
                sortedLists.Add("@tid", tid);
                sortedLists.Add("@working", working);
                sqlHelper.executeNonQuery("UpdateScheduleDateForAll", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string UpdateScheduleForcallandmessage(string start, string end, int tid)
        {
            string str = "Your Schedule time for message and calls is updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@NewStartTime", start);
                sortedLists.Add("@NewEndTime", end);
                sortedLists.Add("@tid", tid);
                sqlHelper.executeNonQuery("UpdateScheduleDateFormessage", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public class ScheduleModel
        {
            public DateTime _date
            {
                get;
                set;
            }

            public string _end
            {
                get;
                set;
            }

            public string _start
            {
                get;
                set;
            }

            public string _type
            {
                get;
                set;
            }

            public string _working
            {
                get;
                set;
            }
        }
    }
}