﻿using californiaspa.Models;
using californiaspa.Models.Chat;
using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Web;
using Twilio;

namespace californiaspa.DataBase
{
    public class ChatServices
    {
        private TwilioRestClient twilio = new TwilioRestClient("AC262d4400f4061aa96d2690f29e23caa4", "a979aeab9dc1590454dceee35b3ae755");

        private string CreateForwardCallXmlFile(string ForwardNumber)
        {
            string str = DateTime.Now.Ticks.ToString();
            string str1 = string.Concat("<?xml version='1.0' encoding='utf-8' standalone='yes'?>  <Response>\r\n            <Say> Please wait while we are connecting </Say><Dial>", ForwardNumber, " </Dial></Response>");
            File.WriteAllText(HttpContext.Current.Server.MapPath(string.Concat("~/callXML/", str, ".xml")), str1);
            return str;
        }

        public void deleteChatMessage(int messageId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            try
            {
                sortedLists.Add("@messageId", messageId);
                sqlHelper.executeNonQuery("DelChatById", "", sortedLists);
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
        }

        public void deleteChatMessage(int[] messageIds)
        {
            int[] numArray = messageIds;
            for (int i = 0; i < (int)numArray.Length; i++)
            {
                this.deleteChatMessage(numArray[i]);
            }
        }

        public void deleteclientsurvey(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                sqlHelper.executeNonQuery("DeleteClientSurvey", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
        }

        public void deletetherapistsurvey(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                sqlHelper.executeNonQuery("DeletetherapistsurveyBYId", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
        }

        public string forwardCall(string to, string Twiliofrom, string therapist, int Aid = 0, string number = "0")
        {
            string str;
            try
            {
                string str1 = this.CreateForwardCallXmlFile(therapist);
                string str2 = string.Concat("https://www.californiamassage.in/callXML/", str1, ".xml");
                string str3 = string.Concat(new object[] { "http://www.californiamass.in/APITasks/statusCallBack.aspx?Aid=", Aid, "&number=", number });
                CallOptions callOption = new CallOptions();
                callOption.Url = str2;
                callOption.To = to;
                callOption.From = Twiliofrom;
                callOption.Method = "GET";
                callOption.StatusCallbackMethod = "Get";
                callOption.StatusCallbackEvents = new string[] { "ringing", "answered", "completed", "initiated" };
                callOption.StatusCallback = str3;
                callOption.Record = true;
                callOption.RecordingStatusCallbackMethod = "Get";
                callOption.RecordingStatusCallback = string.Concat("http://www.californiamass.in/APITasks/Record.aspx?Aid=", Aid);
                this.twilio.InitiateOutboundCall(callOption);
                str = "tesrt";
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();

                return "Error :";
            }
            return str;
        }

        public chatModel GetAdminChatByClientId(int ClientId, bool needchat = true)
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            DateTime dateTime = new DateTime();
            try
            {
                sortedLists.Add("@id", ClientId);

                DataTable dataTable = sqlHelper.fillDataTable("GetAdminChatByClientId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        chatModelitem _chatModelitem = new chatModelitem()
                        {
                            MessageId = Convert.ToInt32(dataTable.Rows[i]["MessageId"].ToString()),
                            LastMassage = dataTable.Rows[i]["Message"].ToString()
                        };
                        DateTime indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        _chatModelitem.CreatedDate = indianTimeFromPST.ToString();
                        _chatModelitem.time = dateTime.ToString("hh:mm tt");
                        _chatModelitem.SenderId = Convert.ToInt32(dataTable.Rows[i]["SenderId"].ToString());
                        _chatModelitem.TherapistName = dataTable.Rows[i]["Tname"].ToString();
                        _chatModelitem.ClientName = dataTable.Rows[i]["CName"].ToString();
                        _chatModelitem.TherapistId = Convert.ToInt32(dataTable.Rows[i]["TherapistId"].ToString());
                        _chatModelitem.ClientId = ClientId;
                        _chatModelitem._ttoken = crypto.EncryptStringAES(dataTable.Rows[i]["TherapistId"].ToString());
                        _chatModelitem._ctoken = crypto.EncryptStringAES(ClientId.ToString());
                        chatModelitems.Add(_chatModelitem);
                        if (!needchat)
                        {
                            break;
                        }
                    }
                }
                dataTable.Dispose();

            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public chatModel GetAdminChatByTherapistId(int thid)
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                sortedLists.Add("@id", thid);
                DataTable dataTable = sqlHelper.fillDataTable("GetAdminChatByTherapistId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        chatModelitem _chatModelitem = new chatModelitem()
                        {
                            MessageId = Convert.ToInt32(dataTable.Rows[i]["MessageId"].ToString()),
                            LastMassage = dataTable.Rows[i]["Message"].ToString()
                        };
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        _chatModelitem.CreatedDate = indianTimeFromPST.ToString();
                        _chatModelitem.time = indianTimeFromPST.ToString("hh:mm tt");
                        _chatModelitem.SenderId = Convert.ToInt32(dataTable.Rows[i]["SenderId"].ToString());
                        _chatModelitem.TherapistName = dataTable.Rows[i]["Tname"].ToString();
                        _chatModelitem.ClientName = dataTable.Rows[i]["CName"].ToString();
                        _chatModelitem.TherapistId = thid;
                        _chatModelitem.ClientId = Convert.ToInt32(dataTable.Rows[i]["clientid"].ToString());
                        _chatModelitem._ttoken = crypto.EncryptStringAES(thid.ToString());
                        _chatModelitem._ctoken = crypto.EncryptStringAES(dataTable.Rows[i]["clientid"].ToString());
                        chatModelitems.Add(_chatModelitem);
                    }
                }
                dataTable.Dispose();
            }

            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public chatModel GetAllChatByAdminIdForClient(int cId, string searchToken = "", bool isallow = false)
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                if (!string.IsNullOrEmpty(searchToken))
                {
                    sortedLists.Add("@client", searchToken);
                }
                sortedLists.Add("@id", cId);
                DataTable dataTable = sqlHelper.fillDataTable("GetAllChatByAdminIdFOrClient", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (!isallow)
                        {
                            break;
                        }
                        chatModelitem _chatModelitem = new chatModelitem()
                        {
                            MessageId = Convert.ToInt32(dataTable.Rows[i]["MessageId"].ToString()),
                            LastMassage = dataTable.Rows[i]["Message"].ToString()
                        };
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        _chatModelitem.CreatedDate = indianTimeFromPST.ToString();
                        _chatModelitem.time = indianTimeFromPST.ToString("hh:mm tt");
                        _chatModelitem.SenderId = Convert.ToInt32(dataTable.Rows[i]["SenderId"].ToString());
                        _chatModelitem.MessageId = Convert.ToInt32(dataTable.Rows[i]["MessageId"].ToString());
                        _chatModelitem.TherapistName = dataTable.Rows[i]["Tname"].ToString();
                        _chatModelitem.ClientName = dataTable.Rows[i]["CName"].ToString();
                        _chatModelitem.ClientId = Convert.ToInt32(dataTable.Rows[i]["clientId"].ToString());
                        _chatModelitem._ctoken = crypto.EncryptStringAES(dataTable.Rows[i]["clientId"].ToString());
                        chatModelitems.Add(_chatModelitem);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public chatModel GetAllChatByAdminIdForTherspist(int thid, string searchToken = "")
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                if (!string.IsNullOrEmpty(searchToken))
                {
                    sortedLists.Add("@Therapist", searchToken);
                }
                sortedLists.Add("@id", thid);
                DataTable dataTable = sqlHelper.fillDataTable("GetAllChatByAdminIdFOrTherapist", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {if(i==1)
                        {
                            break;
                        }
                        chatModelitem _chatModelitem = new chatModelitem()
                        {
                            MessageId = Convert.ToInt32(dataTable.Rows[i]["MessageId"].ToString()),
                            LastMassage = dataTable.Rows[i]["Message"].ToString()
                        };
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        _chatModelitem.CreatedDate = indianTimeFromPST.ToString();
                        _chatModelitem.time = indianTimeFromPST.ToString("hh:mm tt");
                        _chatModelitem.SenderId = Convert.ToInt32(dataTable.Rows[i]["SenderId"].ToString());
                        _chatModelitem.TherapistName = dataTable.Rows[i]["Tname"].ToString();
                        _chatModelitem.ClientName = dataTable.Rows[i]["CName"].ToString();
                        _chatModelitem.TherapistId = Convert.ToInt32(dataTable.Rows[i]["therapistId"].ToString());
                        _chatModelitem._ttoken = crypto.EncryptStringAES(dataTable.Rows[i]["therapistId"].ToString());
                        chatModelitems.Add(_chatModelitem);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public chatModel GetAllChatByAllClientIdForAdminBYKeywordSearch(string serachToken = "")
        {
            chatModel _chatModel = new chatModel();
            List<sercahChatItem> sercahChatItems = new List<sercahChatItem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                sortedLists.Add("@search", serachToken);
                DataTable dataTable = sqlHelper.fillDataTable("GetChatByKeywordofAllClientForAdmin", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        sercahChatItem _sercahChatItem = new sercahChatItem()
                        {
                            messageId = Convert.ToInt32(dataTable.Rows[i]["msgId"].ToString()),
                            message = dataTable.Rows[i]["message"].ToString()
                        };
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        _sercahChatItem.date = indianTimeFromPST.ToString();
                        _sercahChatItem.time = indianTimeFromPST.ToString("hh:mm tt");
                        _sercahChatItem.Clientid = Convert.ToInt32(dataTable.Rows[i]["clientId"].ToString());
                        _sercahChatItem.name = dataTable.Rows[i]["UserName"].ToString();
                        sercahChatItems.Add(_sercahChatItem);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._chatList = sercahChatItems;
            return _chatModel;
        }

        public chatModel GetAllChatByAllTherapistIdForAdminBYKeywordSearch(string serachToken = "")
        {
            chatModel _chatModel = new chatModel();
            List<sercahChatItem> sercahChatItems = new List<sercahChatItem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                sortedLists.Add("@search", serachToken);
                DataTable dataTable = sqlHelper.fillDataTable("GetChatByKeywordofAllTherapistForAdmin", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        sercahChatItem _sercahChatItem = new sercahChatItem()
                        {
                            messageId = Convert.ToInt32(dataTable.Rows[i]["msgId"].ToString()),
                            message = dataTable.Rows[i]["message"].ToString()
                        };
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        _sercahChatItem.date = indianTimeFromPST.ToString();
                        _sercahChatItem.time = indianTimeFromPST.ToString("hh:mm tt");
                        _sercahChatItem.Clientid = Convert.ToInt32(dataTable.Rows[i]["clientId"].ToString());
                        _sercahChatItem.name = dataTable.Rows[i]["UserName"].ToString();
                        sercahChatItems.Add(_sercahChatItem);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._chatList = sercahChatItems;
            return _chatModel;
        }

        public chatModel GetAllChatByClientId(int ClientId, string serachToken = "")
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                if (!string.IsNullOrEmpty(serachToken))
                {
                    sortedLists.Add("@clientName", serachToken);
                }
                sortedLists.Add("@id", ClientId);
                DataTable dataTable = sqlHelper.fillDataTable("GetAllChatByClientId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        chatModelitem _chatModelitem = new chatModelitem();
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        _chatModelitem.CreatedDate = indianTimeFromPST.ToString();
                        _chatModelitem.time = indianTimeFromPST.ToString("hh:mm tt");
                        _chatModelitem.MessageId = Convert.ToInt32(dataTable.Rows[i]["MessageId"].ToString());
                        _chatModelitem.LastMassage = dataTable.Rows[i]["Message"].ToString();
                        _chatModelitem.SenderId = Convert.ToInt32(dataTable.Rows[i]["SenderId"].ToString());
                        _chatModelitem.TherapistName = dataTable.Rows[i]["Tname"].ToString();
                        _chatModelitem.ClientName = dataTable.Rows[i]["CName"].ToString();
                        _chatModelitem.TherapistId = Convert.ToInt32(dataTable.Rows[i]["TherapistId"].ToString());
                        _chatModelitem.ClientId = ClientId;
                        _chatModelitem._ttoken = crypto.EncryptStringAES(dataTable.Rows[i]["TherapistId"].ToString());
                        _chatModelitem._ctoken = crypto.EncryptStringAES(ClientId.ToString());
                        chatModelitems.Add(_chatModelitem);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public chatModel GetAllChatByClientIdForAdminBYKeywordSearch(int ClientId, string serachToken = "")
        {
            chatModel _chatModel = new chatModel();
            List<sercahChatItem> sercahChatItems = new List<sercahChatItem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                sortedLists.Add("@search", serachToken);
                sortedLists.Add("@id", ClientId);
                DataTable dataTable = sqlHelper.fillDataTable("GetChatByKeywordofClientForAdmin", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        sercahChatItem _sercahChatItem = new sercahChatItem()
                        {
                            messageId = Convert.ToInt32(dataTable.Rows[i]["msgId"].ToString()),
                            message = dataTable.Rows[i]["message"].ToString()
                        };
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        _sercahChatItem.date = indianTimeFromPST.ToString();
                        _sercahChatItem.time = indianTimeFromPST.ToString("hh:mm tt");
                        _sercahChatItem.Clientid = ClientId;
                        sercahChatItems.Add(_sercahChatItem);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._chatList = sercahChatItems;
            return _chatModel;
        }

        public List<KeywordsSearchProfile> GetAllChatBySerachForAdmin(int thid, int clientId, string keyword, int startindex, int endindex)
        {
            string str;
            List<KeywordsSearchProfile> keywordsSearchProfiles = new List<KeywordsSearchProfile>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                sortedLists.Add("@startIndex", startindex);
                sortedLists.Add("@endIndex", endindex);
                if (!string.IsNullOrEmpty(keyword))
                {
                    sortedLists.Add("@Search", keyword);
                }
                if (clientId != 0)
                {
                    sortedLists.Add("@Clientid", clientId);
                }
                if (thid != 0)
                {
                    sortedLists.Add("@therapistId", thid);
                }
                DataTable dataTable = sqlHelper.fillDataTable("GetChatBySearch", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        KeywordsSearchProfile keywordsSearchProfile = new KeywordsSearchProfile();
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        keywordsSearchProfile.date = indianTimeFromPST.ToString();
                        keywordsSearchProfile.time = indianTimeFromPST.ToString("hh:mm tt");
                        KeywordsSearchProfile keywordsSearchProfile1 = keywordsSearchProfile;
                        if (dataTable.Rows[i]["therapistName"].ToString() != "Admin")
                        {
                            str = (dataTable.Rows[i]["clientname"].ToString() == "Admin" ? "Admin" : "");
                        }
                        else
                        {
                            str = "Admin";
                        }
                        keywordsSearchProfile1.Admin = str;
                        keywordsSearchProfile.therapist = (dataTable.Rows[i]["therapistName"].ToString() == "Admin" ? "" : dataTable.Rows[i]["therapistName"].ToString());
                        keywordsSearchProfile.client = (dataTable.Rows[i]["clientname"].ToString() == "Admin" ? "" : dataTable.Rows[i]["clientname"].ToString());
                        keywordsSearchProfile.Message = dataTable.Rows[i]["Message"].ToString();
                        keywordsSearchProfile.therapistId = Convert.ToInt32(dataTable.Rows[i]["therapistId"].ToString());
                        keywordsSearchProfile.clientId = Convert.ToInt32(dataTable.Rows[i]["ClientId"].ToString());
                        keywordsSearchProfile.Sender = Convert.ToInt32(dataTable.Rows[i]["SenderId"].ToString());
                        keywordsSearchProfiles.Add(keywordsSearchProfile);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return keywordsSearchProfiles;
        }

        public chatModel GetAllChatByTherapistId(int thid, string SearchToken = "")
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                if (!string.IsNullOrEmpty(SearchToken))
                {
                    sortedLists.Add("@Serach", SearchToken);
                }
                sortedLists.Add("@id", thid);
                DataTable dataTable = sqlHelper.fillDataTable("GetAllChatByTherapistId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        chatModelitem _chatModelitem = new chatModelitem()
                        {
                            MessageId = Convert.ToInt32(dataTable.Rows[i]["MessageId"].ToString()),
                            LastMassage = dataTable.Rows[i]["Message"].ToString()
                        };
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        _chatModelitem.CreatedDate = indianTimeFromPST.ToString();
                        _chatModelitem.time = indianTimeFromPST.ToString("hh:mm tt");
                        _chatModelitem.SenderId = Convert.ToInt32(dataTable.Rows[i]["SenderId"].ToString());
                        _chatModelitem.TherapistName = dataTable.Rows[i]["Tname"].ToString();
                        _chatModelitem.ClientName = dataTable.Rows[i]["CName"].ToString();
                        _chatModelitem.TherapistId = thid;
                        _chatModelitem.ClientId = Convert.ToInt32(dataTable.Rows[i]["clientId"].ToString());
                        _chatModelitem._ttoken = crypto.EncryptStringAES(thid.ToString());
                        _chatModelitem._ctoken = crypto.EncryptStringAES(dataTable.Rows[i]["clientId"].ToString());
                        chatModelitems.Add(_chatModelitem);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public chatModel GetAllChatByTherapistIdForAdminBYKeywordSearch(int tid, string serachToken = "")
        {
            chatModel _chatModel = new chatModel();
            List<sercahChatItem> sercahChatItems = new List<sercahChatItem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                sortedLists.Add("@search", serachToken);
                sortedLists.Add("@id", tid);
                DataTable dataTable = sqlHelper.fillDataTable("GetChatByKeywordofTherapistForAdmin", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        sercahChatItem _sercahChatItem = new sercahChatItem()
                        {
                            messageId = Convert.ToInt32(dataTable.Rows[i]["msgId"].ToString()),
                            message = dataTable.Rows[i]["message"].ToString()
                        };
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        _sercahChatItem.date = indianTimeFromPST.ToString();
                        _sercahChatItem.time = indianTimeFromPST.ToString("hh:mm tt");
                        _sercahChatItem.Clientid = tid;
                        sercahChatItems.Add(_sercahChatItem);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._chatList = sercahChatItems;
            return _chatModel;
        }

        public profileList GetAllClientProfiles()
        {
            profileList _profileList = new profileList();
            List<profiles> _profiles = new List<profiles>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("GetAllAllClient", "", null);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        _profiles.Add(new profiles()
                        {
                            Name = dataTable.Rows[i]["name"].ToString(),
                            _token = crypto.EncryptStringAES(dataTable.Rows[i]["id"].ToString()),
                            id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString())
                        });
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            _profileList._list = _profiles;
            return _profileList;
        }

        public chatModel GetAllClientTherapistChat()
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("GetAllClientTherapistChat", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        chatModelitem _chatModelitem = new chatModelitem()
                        {
                            LastMassage = dataTable.Rows[i]["Message"].ToString()
                        };
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        _chatModelitem.CreatedDate = indianTimeFromPST.ToString();
                        _chatModelitem.time = indianTimeFromPST.ToString("hh:mm tt");
                        _chatModelitem.SenderId = Convert.ToInt32(dataTable.Rows[i]["SenderId"].ToString());
                        _chatModelitem.TherapistName = dataTable.Rows[i]["Tname"].ToString();
                        _chatModelitem.ClientName = dataTable.Rows[i]["CName"].ToString();
                        _chatModelitem.TherapistId = Convert.ToInt32(dataTable.Rows[i]["therapistId"].ToString());
                        _chatModelitem.ClientId = Convert.ToInt32(dataTable.Rows[i]["clientId"].ToString());
                        chatModelitems.Add(_chatModelitem);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public chatModel GetAllClientTherapistChatForLoad()
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("GetAllClientTherapistChatForLoad", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if(i==1)
                        {
                           break;
                        }
                        chatModelitem _chatModelitem = new chatModelitem()
                        {
                            LastMassage = dataTable.Rows[i]["Message"].ToString()
                        };
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"]));
                        _chatModelitem.CreatedDate = indianTimeFromPST.ToString();
                        _chatModelitem.time = indianTimeFromPST.ToString("hh:mm tt");
                        _chatModelitem.SenderId = Convert.ToInt32(dataTable.Rows[i]["SenderId"].ToString());
                        _chatModelitem.TherapistName = dataTable.Rows[i]["Tname"].ToString();
                        _chatModelitem.ClientName = dataTable.Rows[i]["CName"].ToString();
                        _chatModelitem.TherapistId = Convert.ToInt32(dataTable.Rows[i]["therapistId"].ToString());
                        _chatModelitem.ClientId = Convert.ToInt32(dataTable.Rows[i]["clientId"].ToString());
                        chatModelitems.Add(_chatModelitem);
                    }
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public NotificationModel getAllNotifications(int startindex, int endindex, ref int total, int? ClientId, int? therapistId, string startDate, string endDate)
        {
            total = 0;
            NotificationModel notificationModel = new NotificationModel();
            List<AppointementReminder> appointementReminders = new List<AppointementReminder>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            try
            {
                if (ClientId.HasValue)
                {
                    sortedLists.Add("@clientid", ClientId.Value);
                }
                if (therapistId.HasValue && therapistId.Value != 0)
                {
                    sortedLists.Add("@Therapistid", therapistId.Value);
                }
                if (!string.IsNullOrEmpty(startDate))
                {
                    sortedLists.Add("@startDate", startDate);
                    sortedLists.Add("@endDate", endDate);
                }
                sortedLists.Add("@startindex", startindex);
                sortedLists.Add("@endindex", endindex);
                DataTable dataTable = sqlHelper.fillDataTable("GetAllNotification", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        AppointementReminder appointementReminder = new AppointementReminder();
                        indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["Sentdate"]));
                        appointementReminder.sendTime = indianTimeFromPST.ToString("dd-MMM-y ; hh:mm tt ");
                        appointementReminder.message = dataTable.Rows[i]["Message"].ToString();
                        appointementReminder.TherapistName = dataTable.Rows[i]["Tname"].ToString();
                        appointementReminder.ClientName = dataTable.Rows[i]["uName"].ToString().GetNameFromstring();
                        appointementReminder.MessageId = Convert.ToInt32(dataTable.Rows[i]["id"].ToString());
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString());
                        appointementReminder.srno = Convert.ToInt32(dataTable.Rows[i]["RowNum"].ToString());
                        appointementReminders.Add(appointementReminder);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                notificationModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            notificationModel._list = appointementReminders;
            return notificationModel;
        }

        public List<SummaryListModel> GetAllSummaryForAdmin(string startdate, string endDate, int startindex, int endindex)
        {
            DateTime pSTFromIndianTime;
            List<SummaryListModel> summaryListModels = new List<SummaryListModel>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@startIndex", startindex);
                sortedLists.Add("@endIndex", endindex);
                if (!string.IsNullOrEmpty(startdate))
                {
                    pSTFromIndianTime = this.GetPSTFromIndianTime(Convert.ToDateTime(startdate));
                    sortedLists.Add("@startDate", pSTFromIndianTime.ToString());
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    pSTFromIndianTime = this.GetPSTFromIndianTime(Convert.ToDateTime(endDate));
                    sortedLists.Add("@endDate", pSTFromIndianTime.ToString());
                }
                DataTable dataTable = sqlHelper.fillDataTable("GetSummryReportMain", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        summaryListModels.Add(new SummaryListModel()
                        {
                            Amount = Convert.ToDouble(dataTable.Rows[i]["Amount"].ToString()),
                            Total = Convert.ToInt32(dataTable.Rows[i]["Total"].ToString()),
                            Name = dataTable.Rows[i]["name"].ToString(),
                            tokenId = crypto.EncryptStringAES(dataTable.Rows[i]["therapistid"].ToString())
                        });
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return summaryListModels;
        }

        public List<SurveyViewModel> GetAllSurveyForClient(int startindex, int endindex, string startDate, string endDate, int therapistid = 0)
        {
            List<SurveyViewModel> surveyViewModels = new List<SurveyViewModel>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@startIndex", startindex);
                sortedLists.Add("@endIndex", endindex);
                if (!string.IsNullOrEmpty(startDate))
                {
                    startDate = DateTimeExtensions.GetMonth(startDate, '-', 0, 1, 2, false);
                    endDate = DateTimeExtensions.GetMonth(endDate, '-', 0, 1, 2, false);
                    sortedLists.Add("@startDate", startDate);
                    sortedLists.Add("@endDate", endDate);
                }
                if (therapistid != 0)
                {
                    sortedLists.Add("@therapistId", therapistid);
                }
                DataTable dataTable = sqlHelper.fillDataTable("GetServeyReportFroClient", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        SurveyViewModel surveyViewModel = new SurveyViewModel()
                        {
                            ClientName = dataTable.Rows[i]["Cname"].ToString(),
                            Total = Convert.ToInt32(dataTable.Rows[i]["Total"].ToString()),
                            Name = dataTable.Rows[i]["Tname"].ToString(),
                            rating = Convert.ToInt16(dataTable.Rows[i]["rating"].ToString()),
                            comments = dataTable.Rows[i]["Comments"].ToString()
                        };
                        DateTime indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"].ToString()));
                        DateTime dateTime = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["bookingdate"].ToString()));
                        surveyViewModel.feedbackDate = (indianTimeFromPST < dateTime ? dateTime.ToString("dd-MMM-yy") : indianTimeFromPST.ToString("dd-MMM-yy"));
                        surveyViewModel.bookingDate = dateTime.ToString("dd-MMM-yy");
                        surveyViewModel.token = crypto.EncryptStringAES(dataTable.Rows[i]["id"].ToString());
                        surveyViewModel.id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString());
                        surveyViewModels.Add(surveyViewModel);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return surveyViewModels;
        }

        public List<SurveyViewModel> GetAllSurveyForTherapist(int startindex, int endindex, string startDate, string endDate, int therapistid = 0, int clientid = 0)
        {
            List<SurveyViewModel> surveyViewModels = new List<SurveyViewModel>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@startIndex", startindex);
                sortedLists.Add("@endIndex", endindex);
                if (!string.IsNullOrEmpty(startDate))
                {
                    startDate = DateTimeExtensions.GetMonth(startDate, '-', 0, 1, 2, false);
                    endDate = DateTimeExtensions.GetMonth(endDate, '-', 0, 1, 2, false);
                    sortedLists.Add("@startDate", startDate);
                    sortedLists.Add("@endDate", endDate);
                }
                if (therapistid != 0)
                {
                    sortedLists.Add("@therapistId", therapistid);
                }
                if (clientid != 0)
                {
                    sortedLists.Add("@clientId", clientid);
                }
                DataTable dataTable = sqlHelper.fillDataTable("GetServeyReportFroTherapist", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        SurveyViewModel surveyViewModel = new SurveyViewModel()
                        {
                            ClientName = dataTable.Rows[i]["Cname"].ToString(),
                            Total = Convert.ToInt32(dataTable.Rows[i]["Total"].ToString()),
                            Name = dataTable.Rows[i]["Tname"].ToString(),
                            rating = Convert.ToInt16(dataTable.Rows[i]["rating"].ToString()),
                            comments = dataTable.Rows[i]["Comments"].ToString()
                        };
                        DateTime indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["CreatedDate"].ToString()));
                        DateTime dateTime = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["bookingdate"].ToString()));
                        surveyViewModel.feedbackDate = (indianTimeFromPST < dateTime ? dateTime.ToString("dd-MMM-yy") : indianTimeFromPST.ToString("dd-MMM-yy"));
                        surveyViewModel.bookingDate = dateTime.ToString("dd-MMM-yy");
                        surveyViewModel.token = crypto.EncryptStringAES(dataTable.Rows[i]["id"].ToString());
                        surveyViewModel.id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString());
                        surveyViewModels.Add(surveyViewModel);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return surveyViewModels;
        }

        public profileList GetAllTherspistProfiles()
        {
            profileList _profileList = new profileList();
            List<profiles> _profiles = new List<profiles>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("GetAllAllTherapists", "", null);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        _profiles.Add(new profiles()
                        {
                            Name = dataTable.Rows[i]["name"].ToString(),
                            _token = crypto.EncryptStringAES(dataTable.Rows[i]["tid"].ToString()),
                            id = Convert.ToInt32(dataTable.Rows[i]["tid"].ToString())
                        });
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            _profileList._list = _profiles;
            return _profileList;
        }

        private void GetAmountDetails(int therapistId, int clientId, ref int totalmsg, ref double tamount, ref double camount)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@therapistid", therapistId);
                sortedLists.Add("@clientId", clientId);
                DataTable dataTable = sqlHelper.fillDataTable("getAmountDetailByTherapistid", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        totalmsg = Convert.ToInt16(dataTable.Rows[i]["TotalMassage"].ToString());
                        tamount = Convert.ToDouble(dataTable.Rows[i]["tAmount"].ToString());
                        camount = Convert.ToDouble(dataTable.Rows[i]["cAmount"].ToString());
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
        }

        public string Getcallstatus(int aid, string NUMBER)
        {
            string str = "";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@aid", aid);
                sortedLists.Add("@number", NUMBER);
                str = (Convert.ToInt16(sqlHelper.executeNonQueryWMessage("checkStatus1", "", sortedLists).ToString()) > 0 ? "Y" : "N");
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        private void GetClientAmountDetails(int therapistId, int clientId, ref int totalmsg, ref double tamount, ref double camount)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@therapistid", therapistId);
                sortedLists.Add("@clientId", clientId);
                DataTable dataTable = sqlHelper.fillDataTable("getClientAmountDetailByTherapistid", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        totalmsg = Convert.ToInt16(dataTable.Rows[i]["TotalMassage"].ToString());
                        tamount = Convert.ToDouble(dataTable.Rows[i]["tAmount"].ToString());
                        camount = Convert.ToDouble(dataTable.Rows[i]["cAmount"].ToString());
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
        }

        public chatModel GetClientUpdateChat()
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("GetAdminClientChat", "", null);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        chatModelitems.Add(new chatModelitem()
                        {
                            MessageId = Convert.ToInt32(dataTable.Rows[i]["msgId"].ToString()),
                            LastMassage = dataTable.Rows[i]["Message"].ToString(),
                            ClientId = Convert.ToInt32(dataTable.Rows[i]["ClientId"].ToString())
                        });
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public List<SummaryViewModel> GetClientViewSummaryForAdmin(int ClientId, string startdate, string endDate, int startindex = 0, int endindex = 0, int print = 0)
        {
            DateTime pSTFromIndianTime;
            List<SummaryViewModel> summaryViewModels = new List<SummaryViewModel>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                if (!string.IsNullOrEmpty(startdate))
                {
                    pSTFromIndianTime = this.GetPSTFromIndianTime(Convert.ToDateTime(startdate));
                    sortedLists.Add("@startDate", pSTFromIndianTime.ToString());
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    pSTFromIndianTime = this.GetPSTFromIndianTime(Convert.ToDateTime(endDate));
                    sortedLists.Add("@endDate", pSTFromIndianTime.ToString());
                }
                if (ClientId != 0)
                {
                    sortedLists.Add("@Clientid", ClientId);
                }
                int num = 0;
                double num1 = 0;
                double num2 = 0;
                int num3 = 0;
                int num4 = 0;
                DataTable dataTable = sqlHelper.fillDataTable("GetClientSummryReportView", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        SummaryViewModel summaryViewModel = new SummaryViewModel()
                        {
                            Amount = Convert.ToDouble(dataTable.Rows[i]["Amount"].ToString()),
                            rate = Convert.ToDouble(dataTable.Rows[i]["rate"].ToString()),
                            DiscountAmount = Convert.ToDouble(dataTable.Rows[i]["DiscountAmount"].ToString()),
                            Total = Convert.ToInt32(dataTable.Rows[i]["Total"].ToString()),
                            Name = dataTable.Rows[i]["name"].ToString(),
                            ClientName = dataTable.Rows[i]["CLientName"].ToString(),
                            bookingDate = dataTable.Rows[i]["Bookingdate"].ToString(),
                            bookingtime = dataTable.Rows[i]["ConfirmedTime"].ToString()
                        };
                        if (print == 1)
                        {
                            pSTFromIndianTime = Convert.ToDateTime(summaryViewModel.bookingDate);
                            summaryViewModel.bookingDate = pSTFromIndianTime.ToString("dd-MMM-y");
                        }
                        num3 = Convert.ToInt32(dataTable.Rows[i]["ClientId"].ToString());
                        num4 = Convert.ToInt32(dataTable.Rows[i]["therapistid"].ToString());
                        this.GetClientAmountDetails(num4, num3, ref num, ref num1, ref num2);
                        summaryViewModel.totalamountPerClient = Convert.ToString(num2);
                        summaryViewModel.totalAmountpertherapist = num1;
                        summaryViewModel.totalMassage = num;
                        summaryViewModels.Add(summaryViewModel);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return summaryViewModels;
        }

        private DateTime GetIndianTimeFromPST(DateTime _date)
        {
            DateTime staticdatetime = Convert.ToDateTime("04/13/2020");
            string Timezone = "Pacific Standard Time";
            if (_date > staticdatetime)
            {
                Timezone = "UTC";
                _date = _date.AddMinutes(4);
            }
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_date, Timezone, "India Standard Time");
        }

        private DateTime GetPSTFromIndianTime(DateTime _date)
        {
            DateTime staticdatetime = Convert.ToDateTime("04/13/2020");
            string Timezone = "Pacific Standard Time";
            if (_date > staticdatetime)
            {
                Timezone = "UTC";
                _date = _date.AddMinutes(4);
            }
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_date, "India Standard Time", Timezone);
        }

        public string GettherapistAppointmentSummary(int clientid, int therapistId, string startdate, string enddate, ref DataTable dt1)
        {
            string str = "<table class='table-bordered table1 table dt-responsive'><tr>";
            str = string.Concat(str, "<thead class='backgroundskyblue'><th>Therapist name</th>");
            str = string.Concat(str, "<th>Client Name</th>");
            str = string.Concat(str, "<th>Appt Date</th>");
            str = string.Concat(str, "<th>Appt Time</th>");
            str = string.Concat(str, "<th>Therapist Rate</th>");
            str = string.Concat(str, "<th>Discount Amount</th>");
            str = string.Concat(str, "<th>Total Amount</th>");
            str = string.Concat(str, "</tr></thead>");
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@therapistid", therapistId);
                sortedLists.Add("@clientId", clientid);
                sortedLists.Add("@startdate", startdate);
                sortedLists.Add("@endDate", enddate);
                double num = 0;
                double num1 = 0;
                DataTable dataTable = sqlHelper.fillDataTable("GetAllAppointment", "", sortedLists);
                dt1 = dataTable;
                if (dataTable.Rows.Count > 0)
                {
                    str = string.Concat(str, "<tbody>");
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        str = string.Concat(str, "<tr>");
                        str = string.Concat(str, "<td>", dataTable.Rows[i]["name"].ToString(), "</td>");
                        str = string.Concat(str, "<td>", dataTable.Rows[i]["CLientName"].ToString(), "</td>");
                        DateTime dateTime = Convert.ToDateTime(dataTable.Rows[i]["Bookingdate"].ToString());
                        str = string.Concat(str, "<td>", dateTime.ToString("dd-MMM-y"), "</td>");
                        str = string.Concat(str, "<td>", dataTable.Rows[i]["ConfirmedTime"].ToString(), "</td>");
                        str = string.Concat(str, "<td>", dataTable.Rows[i]["rate"].ToString(), "</td>");
                        str = string.Concat(str, "<td>", dataTable.Rows[i]["DiscountAmount"].ToString(), "</td>");
                        str = string.Concat(str, "<td>", dataTable.Rows[i]["Amount"].ToString(), "</td>");
                        str = string.Concat(str, "</tr>");
                        num += Convert.ToDouble(dataTable.Rows[i]["rate"].ToString());
                        num1 += Convert.ToDouble(dataTable.Rows[i]["DiscountAmount"].ToString());
                    }
                    str = string.Concat(str, "</tbody>");
                    str = string.Concat(str, "<tfoot>");
                    str = string.Concat(str, "<tr>");
                    str = string.Concat(str, "<td colspan='5'></td>");
                    str = string.Concat(new object[] { str, "<td colspan='2'><span>Total Massgae :", dataTable.Rows.Count, "</span><br/><span> sub-total Amount :Rs ", num, "</span><br/><span>Discount : Rs ", num1, "</span><br/><span>Net Amount : Rs ", num, "- Rs ", num1, "= Rs ", num - num1, "</span> </td>" });
                    str = string.Concat(str, "</tr>");
                    str = string.Concat(str, "</tfoot>");
                }
                str = string.Concat(str, "</table>");
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public chatModel GetTherapistUpdateChat()
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("GetAdminTherapistChat", "", null);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        chatModelitems.Add(new chatModelitem()
                        {
                            MessageId = Convert.ToInt32(dataTable.Rows[i]["msgId"].ToString()),
                            LastMassage = dataTable.Rows[i]["Message"].ToString(),
                            TherapistId = Convert.ToInt32(dataTable.Rows[i]["TherapistId"].ToString())
                        });
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public chatModel GetUpdatedAdminChatByClientId(int ClientId)
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@clientId", ClientId);
                DataTable dataTable = sqlHelper.fillDataTable("GetUpdatedAdminChat", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        chatModelitems.Add(new chatModelitem()
                        {
                            MessageId = Convert.ToInt32(dataTable.Rows[i]["msgid"].ToString()),
                            LastMassage = dataTable.Rows[i]["message"].ToString()
                        });
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public chatModel GetUpdatedAdminChatByTherapistId(int therapistId)
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@therapistId", therapistId);
                DataTable dataTable = sqlHelper.fillDataTable("GetUpdatedAdminChatByTherapistId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        chatModelitems.Add(new chatModelitem()
                        {
                            MessageId = Convert.ToInt32(dataTable.Rows[i]["msgid"].ToString()),
                            LastMassage = dataTable.Rows[i]["message"].ToString()
                        });
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public chatModel GetUpdatedClientChatByClientId(int ClientId)
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@clientId", ClientId);
                DataTable dataTable = sqlHelper.fillDataTable("GetUpdatedClientChat", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        chatModelitems.Add(new chatModelitem()
                        {
                            MessageId = Convert.ToInt32(dataTable.Rows[i]["msgId"].ToString()),
                            LastMassage = dataTable.Rows[i]["Message"].ToString(),
                            TherapistId = Convert.ToInt32(dataTable.Rows[i]["TherapistId"].ToString()),
                            ClientId = ClientId
                        });
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public chatModel GetUpdatedTherapistChatByTherapistId(int therapistId)
        {
            chatModel _chatModel = new chatModel();
            List<chatModelitem> chatModelitems = new List<chatModelitem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@therapistId", therapistId);
                DataTable dataTable = sqlHelper.fillDataTable("GetUpdatedTherapistChatByTherapistId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        chatModelitems.Add(new chatModelitem()
                        {
                            MessageId = Convert.ToInt32(dataTable.Rows[i]["msgId"].ToString()),
                            LastMassage = dataTable.Rows[i]["Message"].ToString(),
                            ClientId = Convert.ToInt32(dataTable.Rows[i]["clientId"].ToString()),
                            TherapistId = therapistId
                        });
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _chatModel.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            _chatModel._list = chatModelitems;
            return _chatModel;
        }

        public List<SummaryViewModel> GetViewSummaryForAdmin(int therapistId, string startdate, string endDate, int startindex = 0, int endindex = 0, int print = 0)
        {
            DateTime pSTFromIndianTime;
            List<SummaryViewModel> summaryViewModels = new List<SummaryViewModel>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                if (!string.IsNullOrEmpty(startdate))
                {
                    pSTFromIndianTime = this.GetPSTFromIndianTime(Convert.ToDateTime(startdate));
                    sortedLists.Add("@startDate", pSTFromIndianTime.ToString());
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    pSTFromIndianTime = this.GetPSTFromIndianTime(Convert.ToDateTime(endDate));
                    sortedLists.Add("@endDate", pSTFromIndianTime.ToString());
                }
                if (therapistId != 0)
                {
                    sortedLists.Add("@therapistid", therapistId);
                }
                int num = 0;
                double num1 = 0;
                double num2 = 0;
                int num3 = 0;
                int num4 = 0;
                DataTable dataTable = sqlHelper.fillDataTable("GetSummryReportView", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        SummaryViewModel summaryViewModel = new SummaryViewModel()
                        {
                            Amount = Convert.ToDouble(dataTable.Rows[i]["Amount"].ToString()),
                            rate = Convert.ToDouble(dataTable.Rows[i]["rate"].ToString()),
                            DiscountAmount = Convert.ToDouble(dataTable.Rows[i]["DiscountAmount"].ToString()),
                            Total = Convert.ToInt32(dataTable.Rows[i]["Total"].ToString()),
                            Name = dataTable.Rows[i]["name"].ToString(),
                            ClientName = dataTable.Rows[i]["CLientName"].ToString(),
                            bookingDate = dataTable.Rows[i]["Bookingdate"].ToString(),
                            bookingtime = dataTable.Rows[i]["ConfirmedTime"].ToString()
                        };
                        if (print == 1)
                        {
                            pSTFromIndianTime = Convert.ToDateTime(summaryViewModel.bookingDate);
                            summaryViewModel.bookingDate = pSTFromIndianTime.ToString("dd-MMM-y");
                        }
                        num3 = Convert.ToInt32(dataTable.Rows[i]["ClientId"].ToString());
                        num4 = Convert.ToInt32(dataTable.Rows[i]["therapistid"].ToString());
                        summaryViewModel.clientId = num3;
                        summaryViewModel.therapistId = num4;
                        this.GetAmountDetails(num4, num3, ref num, ref num1, ref num2);
                        summaryViewModel.totalamountPerClient = Convert.ToString(num2);
                        summaryViewModel.totalAmountpertherapist = num1;
                        summaryViewModel.totalMassage = num;
                        summaryViewModels.Add(summaryViewModel);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return summaryViewModels;
        }

        public List<SummaryViewModel> GetViewSummaryForAdmin1(int therapistId, string startdate, string endDate, int startindex = 0, int endindex = 0, int print = 0, int clientId = 0, string type = "", string search = "")
        {
            List<SummaryViewModel> summaryViewModels = new List<SummaryViewModel>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                if (!string.IsNullOrEmpty(startdate))
                {
                    sortedLists.Add("@startDate", startdate);
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    sortedLists.Add("@endDate", endDate);
                }
                if (therapistId != 0)
                {
                    sortedLists.Add("@therapistid", therapistId);
                }
                if (clientId != 0)
                {
                    sortedLists.Add("@ClientId", clientId);
                }
                string pc = "GetSummryReportView2";

                if (!string.IsNullOrEmpty(type) && !string.IsNullOrEmpty(search))
                {
                    if (type == "1")
                    {
                        sortedLists.Add("@name", search);
                    }
                    else if (type == "2")
                    {
                        sortedLists.Add("@email", search);
                    }
                    else if (type == "3")
                    {
                        sortedLists.Add("@phone", search);
                    }
                }
                DataTable dataTable = sqlHelper.fillDataTable(pc, "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        SummaryViewModel summaryViewModel = new SummaryViewModel()
                        {
                            Name = dataTable.Rows[i]["name"].ToString(),
                            ClientName = dataTable.Rows[i]["CLientName"].ToString(),
                            clientId = Convert.ToInt32(dataTable.Rows[i]["ClientId"].ToString()),
                            therapistId = Convert.ToInt32(dataTable.Rows[i]["therapistid"].ToString()),
                            totalamountPerClient = dataTable.Rows[i]["Amount"].ToString().Split(new char[] { '.' })[0],
                            DiscountAmount = Convert.ToDouble(dataTable.Rows[i]["Discount"].ToString()),
                            Amount = Convert.ToDouble(dataTable.Rows[i]["TotalAmount"].ToString()),
                            bookingDate = dataTable.Rows[i]["bookingdate"].ToString(),
                            bookingDate_2 = Convert.ToDateTime(dataTable.Rows[i]["bookingdate"].ToString()),
                            bookingtime = dataTable.Rows[i]["ConfirmedTime"].ToString()
                        };
                        summaryViewModels.Add(summaryViewModel);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return summaryViewModels;
        }

        public List<SummaryViewModel> GetViewSummaryForclient1(int clientId, string startdate, string endDate, int startindex = 0, int endindex = 0, int print = 0)
        {
            DateTime pSTFromIndianTime;
            List<SummaryViewModel> summaryViewModels = new List<SummaryViewModel>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                if (!string.IsNullOrEmpty(startdate))
                {
                    pSTFromIndianTime = this.GetPSTFromIndianTime(Convert.ToDateTime(startdate));
                    sortedLists.Add("@startDate", pSTFromIndianTime.ToString());
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    pSTFromIndianTime = this.GetPSTFromIndianTime(Convert.ToDateTime(endDate));
                    sortedLists.Add("@endDate", pSTFromIndianTime.ToString());
                }
                if (clientId != 0)
                {
                    sortedLists.Add("@clientId", clientId);
                }
                int num = 0;
                int num1 = 0;
                DataTable dataTable = sqlHelper.fillDataTable("GetClientSummryReportView1", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        SummaryViewModel summaryViewModel = new SummaryViewModel()
                        {
                            Name = dataTable.Rows[i]["name"].ToString(),
                            ClientName = dataTable.Rows[i]["CLientName"].ToString()
                        };
                        num = Convert.ToInt32(dataTable.Rows[i]["ClientId"].ToString());
                        num1 = Convert.ToInt32(dataTable.Rows[i]["therapistid"].ToString());
                        summaryViewModel.clientId = num;
                        summaryViewModel.therapistId = num1;
                        summaryViewModel.totalamountPerClient = dataTable.Rows[i]["Amount"].ToString();
                        summaryViewModel.DiscountAmount = Convert.ToDouble(dataTable.Rows[i]["Discount"].ToString());
                        summaryViewModel.Amount = Convert.ToDouble(dataTable.Rows[i]["TotalAmount"].ToString());
                        summaryViewModel.totalMassage = Convert.ToInt32(dataTable.Rows[i]["massages"].ToString());
                        summaryViewModels.Add(summaryViewModel);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return summaryViewModels;
        }

        public string SaveAdminChat(int clientId, string text)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "0";
            try
            {
                sortedLists.Add("@clientId", clientId);
                sortedLists.Add("@text", text);
                str = sqlHelper.executeNonQueryWMessage("AddAdminChatByClient", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public string SaveAdminChatByClientId(int clientId, string text, string ChatReply = "")
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "0";
            try
            {
                sortedLists.Add("@clientId", clientId);
                sortedLists.Add("@text", text);
                str = sqlHelper.executeNonQueryWMessage("AddAdminChatByClientId", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public string SaveAdminChatByTherapist(int thid, string text)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "0";
            try
            {
                sortedLists.Add("@thid", thid);
                sortedLists.Add("@text", text);
                str = sqlHelper.executeNonQueryWMessage("AddAdminChatByTherapist", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public string SaveAdminChatByTherapistId(int thid, string text)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "0";
            try
            {
                sortedLists.Add("@thid", thid);
                sortedLists.Add("@text", text);
                str = sqlHelper.executeNonQueryWMessage("AddAdminChatByTherapistId", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public string SaveClientChat(int clientId, int therapistid, string text)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            try
            {
                sortedLists.Add("@clientId", clientId);
                sortedLists.Add("@therapistid", therapistid);
                sortedLists.Add("@text", text);
                empty = sqlHelper.executeNonQueryWMessage("AddClientChat_v2", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return empty;
        }

        public string SaveTherapistChat(int clientId, int therapistid, string text)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "0";
            try
            {
                sortedLists.Add("@clientId", clientId);
                sortedLists.Add("@therapistid", therapistid);
                sortedLists.Add("@text", text);
                str = sqlHelper.executeNonQueryWMessage("AddTherapistWithCLientChat_v2", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public void testforwardCall(string to, string Twiliofrom, string therapist)
        {
            try
            {
                string str = this.CreateForwardCallXmlFile(therapist);
                string str1 = string.Concat("https://www.californiamassage.in/callXML/", str, ".xml");
                CallOptions callOption = new CallOptions();
                callOption.Url = str1;
                callOption.To = to;
                callOption.From = Twiliofrom;
                callOption.Method = "GET";
                callOption.StatusCallbackMethod = "Get";
                callOption.StatusCallbackEvents = new string[] { "ringing", "answered", "completed", "busy", "no-answer", "canceled", "failed" };
                callOption.Record = false;
                this.twilio.InitiateOutboundCall(callOption);
            }
            catch (Exception exception)
            {
            }
        }

        public string UpdateNotification(int id = 0)
        {
            string str = "Notification Deleted Succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                sqlHelper.executeNonQuery("Updatenotification", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }
    }
}