using californiaspa.Models;
using californiaspa.Models.Account;
using californiaspa.Models.AdminAudio;
using californiaspa.Models.Client;
using californiaspa.Models.therapist;
using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Reflection;

namespace californiaspa.DataBase
{
    public class AdminServices
    {

        public string AddNewCustomer(RegisterModel model)
        {
            string empty = string.Empty;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@email", model.email);
                sortedLists.Add("@mobile", model.mobile);
                //JP Code Changes
                sortedLists.Add("@CountryCode", model.CountryCode);
                //End Changes

                sortedLists.Add("@Name", model.name);
                sortedLists.Add("@password", model.password);
                sortedLists.Add("@verify", model.mngrVerfiy);
                sortedLists.Add("@lth", (model.lth == null ? "" : model.lth));
                sortedLists.Add("@ltd", (model.ltd == null ? "" : model.ltd));
                sortedLists.Add("@HouseNumber", (model.HouseNumber == null ? "" : model.HouseNumber));
                sortedLists.Add("@AptNumber", (model.AptNumber == null ? "" : model.AptNumber));
                sortedLists.Add("@HotelName", (model.HotelName == null ? "" : model.HotelName));
                sortedLists.Add("@SpaHotelId", model.SpaHotelId);
                sortedLists.Add("@FloorNumber", (model.FloorNumber == null ? "" : model.FloorNumber));
                sortedLists.Add("@StreetName", (model.StreetName == null ? "" : model.StreetName));
                sortedLists.Add("@CrossStreetName", (model.CrossStreetName == null ? "" : model.CrossStreetName));
                sortedLists.Add("@Landmark", (model.Landmark == null ? "" : model.Landmark));
                sortedLists.Add("@LocalityArea", (model.LocalityArea == null ? "" : model.LocalityArea));
                sortedLists.Add("@AptName", (model.AptName == null ? "" : model.AptName));
                sortedLists.Add("@locationTypeId", model.locationtypeId);
                sortedLists.Add("@RoomNumber", (model.RoomNumber == null ? "" : model.RoomNumber));
                sortedLists.Add("@usertype", (model.usertype == null ? "" : model.usertype));
                empty = sqlHelper.executeNonQueryWMessage("tbluserSaveNewUserByAdmin", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return empty;
        }


        public string CreateAdminXML(AdminProfile _model, int createdBy = 1)
        {
            string str = "Admin details Saved Succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@createdBy", createdBy);
                sortedLists.Add("@email", _model.EmailAddress);
                sortedLists.Add("@name", _model.Name);
                sortedLists.Add("@password", crypto.EncryptStringAES(_model.Password));
                sortedLists.Add("@ROleId", _model.roleId);
                str = sqlHelper.executeNonQueryWMessage("AddNewAdminUser", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
               (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
                {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public string deleteUser(int id)
        {
            string str = "User remove succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            { 
                    sqlHelper.executeScaler(string.Concat("Delete from tbladminuser where id=", id));
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                    str = string.Concat("Error :", exception.Message);
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public List<AdminProfile> getalladmin()
        {
            List<AdminProfile> adminProfiles = new List<AdminProfile>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            { 
                    DataTable dataTable = sqlHelper.fillDataTable("getAllAdminUser", "", sortedLists);
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        AdminProfile adminProfile = new AdminProfile()
                        {
                            token = crypto.EncryptStringAES(dataTable.Rows[i]["Id"].ToString()),
                            rolename = dataTable.Rows[i]["rolename"].ToString(),
                            Name = dataTable.Rows[i]["Name"].ToString(),
                            Password = crypto.DecryptStringAES(dataTable.Rows[i]["Password"].ToString()),
                            EmailAddress = dataTable.Rows[i]["Email"].ToString()
                        };
                        adminProfiles.Add(adminProfile);
                    }
                    dataTable.Dispose();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return adminProfiles;
        }

        public List<AudioItem> GetAllAudioReports(int startindex,int endindex,ref int total,int id, string startDate, string endDate)
        {
            List<AudioItem> audioItems = new List<AudioItem>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            { 
                    if (id != 0)
                    {
                        sortedLists.Add("@therapistId", id);
                    }
                    if (!string.IsNullOrEmpty(startDate))
                    {
                        sortedLists.Add("@startDate", startDate);
                        sortedLists.Add("@endDate", endDate);
                    }
                sortedLists.Add("@startindex", startindex);
                sortedLists.Add("@endindex", endindex);
                total = 0;
                DataTable dataTable = sqlHelper.fillDataTable("GetAllAudioReports", "", sortedLists);
                    if (dataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            AudioItem audioItem = new AudioItem()
                            {
                                audioUrl = dataTable.Rows[i]["audioUrl"].ToString(),
                                Audioid = Convert.ToInt32(dataTable.Rows[i]["audioId"].ToString()),
                                therapistName = dataTable.Rows[i]["TherapistName"].ToString(),
                                ClientName = dataTable.Rows[i]["ClientName"].ToString().GetNameFromstring(),
                                srno= Convert.ToInt32(dataTable.Rows[i]["RowNum"].ToString())  
                                  };
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString());                            DateTime indianTimeFromPST = Convert.ToDateTime(dataTable.Rows[i]["recordingDate"].ToString()).GetIndianTimeFromPST();
                            audioItem.recordedDate = indianTimeFromPST.ToString("dd-MMM-yy");
                            audioItems.Add(audioItem);
                        }
                    }
                    dataTable.Dispose();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return audioItems;
        }

        public Profile GetAllClients()
        {
            Profile profile = new Profile();
            List<profile_item> profileItems = new List<profile_item>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            { 
                    DataTable dataTable = sqlHelper.fillDataTable("GetAllClientsWithoutIndex", "", null);
                    if (dataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            profileItems.Add(new profile_item()
                            {
                                name = dataTable.Rows[i]["Name"].ToString(),
                                BlockStatus = (Convert.ToBoolean(dataTable.Rows[i]["IsBlocked"].ToString()) ? "Yes" : "No"),
                                token = crypto.EncryptStringAES(dataTable.Rows[i]["id"].ToString()),
                                _xid = Convert.ToInt32(dataTable.Rows[i]["id"].ToString()),
                                reviewToken = "45678",
                                mobile = dataTable.Rows[i]["mobile"].ToString(),
                                email = dataTable.Rows[i]["Email"].ToString(),
                                clientReview = this.Gettop10ClientComments(Convert.ToInt16(dataTable.Rows[i]["id"].ToString())),
                                review = this.Gettop10Comments(Convert.ToInt16(dataTable.Rows[i]["id"].ToString())),
                                totalMassage = Convert.ToInt32(sqlHelper.executeScaler(string.Concat("Select Count(1) from tblAppointement WITH(NOLOCK) WHERE STatus = 'Finish' AND  Clientid =", Convert.ToInt16(dataTable.Rows[i]["id"].ToString())))),
                                therapistVisitedCount = Convert.ToInt32(sqlHelper.executeScaler(string.Concat("Select Count(1) from tblAppointement WITH(NOLOCK) WHERE  Clientid =", Convert.ToInt16(dataTable.Rows[i]["id"].ToString()))))
                            });
                        }
                    }
                    dataTable.Dispose();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                    profile.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            profile._profiles = profileItems;
            return profile;
        }

        public Profile GetAllClientsPageWise(ref int total, string type = "", string searchItem = "", int pageindex = 0, int pagesize = 10)
        {
            Profile profile = new Profile();
            List<profile_item> profileItems = new List<profile_item>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            { 
                    sortedLists.Add("@PageIndex", (pageindex == 0 ? 1 : pageindex));
                    sortedLists.Add("@PageSize", (pagesize == 0 ? 10 : pagesize));
                    searchItem = searchItem.Trim();
                    string str = "GetClinetPageWise";
                    if (type == "Name")
                    {
                        str = "GetClinetPageWiseByName";
                        sortedLists.Add("@searchBy", searchItem.Trim());
                    }
                    else if (type == "mobile")
                    {
                        str = "GetClinetPageWiseBymobile";
                        sortedLists.Add("@searchBy", searchItem.Trim());
                    }
                    else if (type == "Email")
                    {
                        str = "GetClinetPageWiseByEmail";
                        sortedLists.Add("@searchBy", searchItem.Trim());
                    }
                    DataTable dataTable = sqlHelper.fillDataTable(str, "", sortedLists);
                    if (dataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            profile_item profileItem = new profile_item()
                            {
                                name = dataTable.Rows[i]["Name"].ToString(),
                                BlockStatus = (Convert.ToBoolean(dataTable.Rows[i]["IsBlocked"].ToString()) ? "Yes" : "No"),
                                _xid = Convert.ToInt32(dataTable.Rows[i]["id"].ToString()),
                                counter = Convert.ToInt32(dataTable.Rows[i]["counter"].ToString()),
                                token = crypto.EncryptStringAES(dataTable.Rows[i]["id"].ToString()),
                                mobile = dataTable.Rows[i]["mobile"].ToString(),
                                email = dataTable.Rows[i]["Email"].ToString(),
                                totalMassage = Convert.ToInt32(dataTable.Rows[i]["totalMassage"].ToString()),
                                therapistVisitedCount = Convert.ToInt32(dataTable.Rows[i]["therapistVisitedCount"].ToString()),
                                total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString())
                                ,usertype= dataTable.Rows[i]["clienttype"].ToString()
                            };
                            total = Convert.ToInt32(dataTable.Rows[i]["alltotal"].ToString());
                            profileItem.alltotal = total;
                            profileItem.pgindex = (pageindex == 0 ? 1 : pageindex);
                            profileItems.Add(profileItem);
                        }
                    }
                    else
                    {
                        total = Convert.ToInt32(sqlHelper.executeScaler("Select count(1) from tblUser WITH(NOLOCK) WHERE ISACTIVE = 'Y'"));
                    }
                    dataTable.Dispose();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                    profile.ErrorText = string.Concat("Error  : ", exception.StackTrace, " ", exception.Message);
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            profile._profiles = profileItems;
            return profile;
        }

        public therapistprofile GetAllTherapistById(int thid)
        {
            therapistprofile _therapistprofile = new therapistprofile();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {  sortedLists.Add("@id", thid);
                    DataTable dataTable = sqlHelper.fillDataTable("tbltherapistGetById", "", sortedLists);
                    if (dataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            _therapistprofile.name = dataTable.Rows[i]["Name"].ToString();
                            _therapistprofile.id = Convert.ToInt32(dataTable.Rows[i]["tid"].ToString());
                            int num = _therapistprofile.id;
                            _therapistprofile.token = crypto.EncryptStringAES(num.ToString());
                            _therapistprofile.Age = Convert.ToInt32(dataTable.Rows[i]["age"].ToString());
                            _therapistprofile.ClientRating = (double)Convert.ToInt32(dataTable.Rows[i]["ClientRating"].ToString());
                            _therapistprofile.ImageCount = Convert.ToInt32(dataTable.Rows[i]["ImageCount"].ToString());
                            _therapistprofile.imagePath = dataTable.Rows[i]["imagePath"].ToString();
                            _therapistprofile.languages = dataTable.Rows[i]["languages"].ToString();
                            _therapistprofile.location = dataTable.Rows[i]["location"].ToString();
                            _therapistprofile.MassageStyles = dataTable.Rows[i]["MassageStyles"].ToString();
                            _therapistprofile.TodaysAvailabilty = this.GetTherapistScheduleId(_therapistprofile.id);
                            therapistprofile _therapistprofile1 = _therapistprofile;
                            _therapistprofile1.FirstAptTime = _therapistprofile1.TodaysAvailabilty.Split(new char[] { '-' })[0];
                            therapistprofile _therapistprofile2 = _therapistprofile;
                            _therapistprofile2.LastAptTime = _therapistprofile2.TodaysAvailabilty.Split(new char[] { '-' })[1];
                            _therapistprofile.rate = Convert.ToInt32(dataTable.Rows[i]["rate"].ToString());
                        }
                    }
                    dataTable.Dispose();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return _therapistprofile;
        }

        public List<imagesItem> GetImagesById(int thid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            List<imagesItem> imagesItems = new List<imagesItem>();
            try
            {  sortedLists.Add("@id", thid);
                    DataTable dataTable = sqlHelper.fillDataTable("GetImagesById", "", sortedLists);
                    if (dataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            imagesItems.Add(new imagesItem()
                            {
                                ImgId = Convert.ToInt32(dataTable.Rows[i]["ImageId"].ToString()),
                                path = dataTable.Rows[i]["ImagePath"].ToString()
                            });
                        }
                    }
                    dataTable.Dispose();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return imagesItems;
        }

        public string GetTherapistScheduleId(int thid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string str = "";
            try
            { sortedLists.Add("@id", thid);
                    sortedLists.Add("@date", DateTime.Now.Date);
                    DataTable dataTable = sqlHelper.fillDataTable("GetAUTOScheduleByTherapistId", "", sortedLists);
                    if (dataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            str = string.Concat(dataTable.Rows[0]["Starttime"].ToString(), "-", dataTable.Rows[0]["EndTime"].ToString());
                        }
                    }
                    dataTable.Dispose();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public List<comments> Gettop10ClientComments(int Client)
        {
            List<comments> _comments = new List<comments>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            { 
                    sortedLists.Add("@clientid", Client);
                    DataTable dataTable = sqlHelper.fillDataTable("GetClientsurveyCommentsByclientid", "", sortedLists);
                    if (dataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            _comments.Add(new comments()
                            {
                                Comment = dataTable.Rows[i]["Comments"].ToString()
                            });
                        }
                    }
                    dataTable.Dispose();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return _comments;
        }

        public List<comments> Gettop10Comments(int Client)
        {
            List<comments> _comments = new List<comments>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                 sortedLists.Add("@clientid", Client);
                    DataTable dataTable = sqlHelper.fillDataTable("GetCommentsByclientid", "", sortedLists);
                    if (dataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            _comments.Add(new comments()
                            {
                                Comment = dataTable.Rows[i]["Comments"].ToString()
                            });
                        }
                    }
                    dataTable.Dispose();
                    sortedLists = null;
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return _comments;
        }

        public AdminProfile readAdminxml(int adminId)
        {
            AdminProfile adminProfile = new AdminProfile();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            { sortedLists.Add("@id", adminId);
                    DataTable dataTable = sqlHelper.fillDataTable("getAdminUser", "", sortedLists);
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        adminProfile.Id = Convert.ToInt32(dataTable.Rows[i]["Id"].ToString());
                        adminProfile.Name = dataTable.Rows[i]["Name"].ToString();
                        adminProfile.Password = crypto.DecryptStringAES(dataTable.Rows[i]["Password"].ToString());
                        adminProfile.EmailAddress = dataTable.Rows[i]["Email"].ToString();
                    }
                    dataTable.Dispose();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return adminProfile;
        }

        public AdminProfile readAdminxml(string email)
        {
            AdminProfile adminProfile = new AdminProfile();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {       sortedLists.Add("@id", email);
                    DataTable dataTable = sqlHelper.fillDataTable("getAdminUserbyemail", "", sortedLists);
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        adminProfile.Id = Convert.ToInt32(dataTable.Rows[i]["Id"].ToString());
                        adminProfile.Name = dataTable.Rows[i]["Name"].ToString();
                        adminProfile.Password = crypto.DecryptStringAES(dataTable.Rows[i]["Password"].ToString());
                        adminProfile.EmailAddress = dataTable.Rows[i]["email"].ToString();
                    }
                    dataTable.Dispose();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return adminProfile;
        }

        public string updateAdminXML(AdminProfile _model)
        {
            string str = "Admin details Saved Succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@id", _model.Id);
                sortedLists.Add("@email", _model.EmailAddress);
                sortedLists.Add("@name", _model.Name);
                sortedLists.Add("@password", crypto.EncryptStringAES(_model.Password));
                str = sqlHelper.executeNonQueryWMessage("updateAdminUserwithoutrole", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                 (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            return str;
        }

        public string updateAdminXMLwithRole(AdminProfile _model)
        {
            string str = "Admin details Saved Succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@id", _model.Id);
                sortedLists.Add("@email", _model.EmailAddress);
                sortedLists.Add("@name", _model.Name);
                sortedLists.Add("@password", _model.Password);
                sortedLists.Add("@ROleId", _model.roleId);
                str = sqlHelper.executeNonQueryWMessage("updateAdminUser", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                 (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            return str;
        }

        public string UpdateAudioReport(int id = 0)
        {
            string str = "Recording Deleted Succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            { 
                    sortedLists.Add("@id", id);
                    sqlHelper.executeNonQuery("DeleteAudio", "", sortedLists);
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                    str = string.Concat("Error :", exception.Message);
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string updateCustomer(RegisterModel model, int userid)
        {
            string empty = string.Empty;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            { 
                    if (!string.IsNullOrEmpty(model.photoidurl))
                    {
                        this.updatePhotoId(userid, model.photoidurl);
                    }
                    sortedLists.Add("@email", model.email);
                    sortedLists.Add("@mobile", model.mobile);
                    sortedLists.Add("@Name", model.name);
                    sortedLists.Add("@password", model.password);
                    sortedLists.Add("@Userid", userid);
                    sortedLists.Add("@block", model.block);
                    sortedLists.Add("@verify", model.mngrVerfiy);
                    sortedLists.Add("@reason", (string.IsNullOrEmpty(model.blockReason) ? string.Empty : model.blockReason));
                    sortedLists.Add("@lth", (model.lth == null ? "" : model.lth));
                    sortedLists.Add("@ltd", (model.ltd == null ? "" : model.ltd));
                    sortedLists.Add("@HouseNumber", (model.HouseNumber == null ? "" : model.HouseNumber));
                    sortedLists.Add("@AptNumber", (model.AptNumber == null ? "" : model.AptNumber));
                    sortedLists.Add("@HotelName", (model.HotelName == null ? "" : model.HotelName));
                    sortedLists.Add("@SpaHotelId", model.SpaHotelId);
                    sortedLists.Add("@FloorNumber", (model.FloorNumber == null ? "" : model.FloorNumber));
                    sortedLists.Add("@StreetName", (model.StreetName == null ? "" : model.StreetName));
                    sortedLists.Add("@CrossStreetName", (model.CrossStreetName == null ? "" : model.CrossStreetName));
                    sortedLists.Add("@Landmark", (model.Landmark == null ? "" : model.Landmark));
                    sortedLists.Add("@LocalityArea", (model.LocalityArea == null ? "" : model.LocalityArea));
                    sortedLists.Add("@AptName", (model.AptName == null ? "" : model.AptName));
                    sortedLists.Add("@locationTypeId", model.locationtypeId);
                    sortedLists.Add("@RoomNumber", (model.RoomNumber == null ? "" : model.RoomNumber));
                sortedLists.Add("@usertype", (model.usertype == null ? "" : model.usertype));
                empty = sqlHelper.executeNonQueryWMessage("tbluserUpdateUserByAdmin", "", sortedLists).ToString();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                    empty = string.Concat("Error :", exception.Message);
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return empty;
        }

        public string UpdateHotelImages(string path, int id = 0)
        {
            string str = "Your Profile is updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            { 
                    sortedLists.Add("@id", id);
                    sortedLists.Add("@path", path);
                    str = sqlHelper.executeNonQueryWMessage("AddHotelImageById", "", sortedLists).ToString();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                    str = string.Concat("Error :", exception.Message);
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string UpdateImages(string path, int id = 0)
        {
            string str = "Your Profile is updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            { 
                    sortedLists.Add("@id", id);
                    sortedLists.Add("@path", path);
                    str = sqlHelper.executeNonQueryWMessage("AddImageById", "", sortedLists).ToString();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                    str = string.Concat("Error :", exception.Message);
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string UpdateOrCreate(therapistprofile _model, int id = 0)
        {
            string str = "Your Profile is updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {   if (!string.IsNullOrEmpty(_model.token))
                    {
                        sortedLists.Add("@id", id);
                    }
                    sortedLists.Add("@name", _model.name);
                    if (!string.IsNullOrEmpty(_model.imagePath))
                    {
                        sortedLists.Add("@imagePath", _model.imagePath);
                    }
                    sortedLists.Add("@Age", _model.Age);
                    sortedLists.Add("@Location", _model.location);
                    sortedLists.Add("@rate", _model.rate);
                    sortedLists.Add("@userName", _model.Username);
                    sortedLists.Add("@Password", _model.Password);
                    sortedLists.Add("@mobile", _model.mobile);
                    sortedLists.Add("@langugae", _model.languages);
                    sortedLists.Add("@MassageType", _model.MassageStyles);
                    sortedLists.Add("@firstAppointmentTime", _model.FirstAptTime);
                    sortedLists.Add("@lastAppointmentTime", _model.LastAptTime);
                    sortedLists.Add("@firstcallTime", _model.ErliestcallTime);
                    sortedLists.Add("@lastcallTime", _model.latestcallTime);
                    sortedLists.Add("@comment", (string.IsNullOrEmpty(_model.Adminreview) ? "" : _model.Adminreview));
                    sortedLists.Add("@bookingLimit", _model.bookingLimit);
                    sortedLists.Add("@isShow", _model.showprofile);
                    str = sqlHelper.executeNonQueryWMessage("UpdateTherapistProfile", "", sortedLists).ToString();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                    str = string.Concat("Error :", exception.Message);
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string UpdateOrCreateImage(string imagePath, int id)
        {
            string str = "Your Profile is updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            { 
                    sortedLists.Add("@id", id);
                    sortedLists.Add("@imagePath", imagePath);
                    str = sqlHelper.executeNonQueryWMessage("UpdateTherapistProfileimage", "", sortedLists).ToString();
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                    str = string.Concat("Error :", exception.Message);
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public void updatePhotoId(int userId, string url)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            { sortedLists.Add("@Url", url);
                    sortedLists.Add("@id", userId);
                    sqlHelper.executeNonQuery("UploadNewPhoto", "", sortedLists);
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                } 
            finally
            {
                sqlHelper.Dispose();
            }
        }
    }
}