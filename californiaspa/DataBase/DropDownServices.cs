using californiaspa.utilis;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Web.Mvc;

namespace californiaspa.DataBase
{
    public class DropDownServices
    {
       
        public SelectList bindClientDropDown(string selectedValue, int userId = 0)
        {
            SelectList selectList;
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            { 
                    DataTable dataTable = sqlHelper.fillDataTable("", "select id,name from tbluser with(nolock) WHERE ISActive='Y'", null);
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text="--All Client---";
                    selectListItem.Value="0";
                    selectListItems.Add(selectListItem);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            SelectListItem selectListItem1 = new SelectListItem();
                            selectListItem1.Text=dataTable.Rows[i]["name"].ToString();
                            selectListItem1.Value=dataTable.Rows[i]["id"].ToString();
                            selectListItems.Add(selectListItem1);
                        }
                    }
                    dataTable.Dispose();
                    return new SelectList(selectListItems, "Value", "Text", selectedValue);
                }
                catch (Exception exception)
                {
                     (new Logger(exception.Message, this.GetType().FullName, exception.Source, userId, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                    selectList = new SelectList(selectListItems, "Value", "Text", selectedValue);
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return selectList;
        }

        public SelectList bindTherapistDropDown(string selectedValue, int userId = 0)
        {
            SelectList selectList;
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            { 
                    DataTable dataTable = sqlHelper.fillDataTable("", "select tid,Name from tbltherapist  with(NOLOCK) Where ISActive='Y'", null);
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text="--All Therapist---";
                    selectListItem.Value="0";
                    selectListItems.Add(selectListItem);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            SelectListItem selectListItem1 = new SelectListItem();
                            selectListItem1.Text=dataTable.Rows[i]["Name"].ToString();
                            selectListItem1.Value=dataTable.Rows[i]["tid"].ToString();
                            selectListItems.Add(selectListItem1);
                        }
                    }
                    dataTable.Dispose();
                    return new SelectList(selectListItems, "Value", "Text", selectedValue);
                }
                catch (Exception exception)
                {
                    (new Logger(exception.Message, this.GetType().FullName, exception.Source, userId, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                    selectList = new SelectList(selectListItems, "Value", "Text", selectedValue);
                } 
            finally
            {
                sqlHelper.Dispose();
            }
            return selectList;
        }
    }
}