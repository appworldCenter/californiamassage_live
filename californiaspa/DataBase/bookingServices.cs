using californiaspa.Models;
using californiaspa.Models.Appointment;
using californiaspa.Models.booking;
using californiaspa.Models.therapist;
using californiaspa.utilis;
using Google.Apis.Requests;
using Google.Apis.Services;
using Google.Apis.Urlshortener.v1;
using Google.Apis.Urlshortener.v1.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;

namespace californiaspa.DataBase
{
    public class bookingServices
    {

        public string appointmentRequest(bookingModel _model, int cUserId, string imagePath)
        {
            string apartmentNUmber;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            try
            {
                string[] str = new string[5];
                string[] strArrays = _model.bookingdate.Split(new char[] { '-' });
                int month = this.GetMonth(strArrays[1].ToUpper());
                str[0] = month.ToString();
                str[1] = "-";
                str[2] = strArrays[0];
                str[3] = "-";
                str[4] = strArrays[2];
                _model.bookingdate = string.Concat(str);
                if (_model.AID != 0)
                {
                    sortedLists.Add("@id", _model.AID);
                }
                if (!string.IsNullOrEmpty(imagePath))
                {
                    imagePath = this.GetShortUrl(string.Concat("https://www.californiamassage.in/main/getId?name=", imagePath), false);
                    sortedLists.Add("@idImagePath", imagePath);
                }
                bookingModel _bookingModel = _model;
                if (_model.loc != "1")
                {
                    apartmentNUmber = _model.ApartmentNUmber;
                }
                else
                {
                    apartmentNUmber = (string.IsNullOrEmpty(_model.houseFlatNUmber) ? string.Empty : _model.houseFlatNUmber);
                }
                _bookingModel.ApartmentNUmber = apartmentNUmber;
                sortedLists.Add("@FloorNumber", this.GetFloorNumber(_model));
                sortedLists.Add("@pickuptime", (_model.locPick == "1" ? _model.pickUpTime : string.Empty));
                sortedLists.Add("@StreetName", this.GetStreetName(_model));
                sortedLists.Add("@CrossStreetName", this.GetCrosStreetName(_model));
                sortedLists.Add("@Landmark", this.GetLandmark(_model));
                sortedLists.Add("@LocalityArea", this.Getlocality(_model));
                sortedLists.Add("@locationTypeId", _model.loc);
                sortedLists.Add("@ClientId", cUserId);
                sortedLists.Add("@Thid", _model.profile.id);
                sortedLists.Add("@BookingDate", _model.bookingdate);
                sortedLists.Add("@EArrivalTime", _model.EArrivalTime);
                sortedLists.Add("@lArrivalTime", _model.LArrivalTime);
                sortedLists.Add("@CustomMessage", _model.CustomeMessage);
                sortedLists.Add("@status", Enums.AppointmentStatus.pending);
                sortedLists.Add("@VehicalType", (string.IsNullOrEmpty(_model.vehicalType) ? string.Empty : _model.vehicalType));
                sortedLists.Add("@color", (string.IsNullOrEmpty(_model.color) ? string.Empty : _model.color));
                sortedLists.Add("@make", (string.IsNullOrEmpty(_model.make) ? string.Empty : _model.make));
                sortedLists.Add("@licencesNumber", (string.IsNullOrEmpty(_model.licenceNumber) ? string.Empty : _model.licenceNumber));
                sortedLists.Add("@isVehical", (_model.locPick == "1" ? "Y" : "N"));
                sortedLists.Add("@modelNumber", (string.IsNullOrEmpty(_model.ModelNumber) ? "" : _model.ModelNumber));
                sortedLists.Add("@HouseNumber", (string.IsNullOrEmpty(_model.houseNUmber) ? string.Empty : _model.houseNUmber));
                sortedLists.Add("@AptNumber", (string.IsNullOrEmpty(_model.ApartmentNUmber) ? string.Empty : _model.ApartmentNUmber));
                sortedLists.Add("@RoomNumber", (string.IsNullOrEmpty(_model.HotelRoomNUmber) ? string.Empty : _model.HotelRoomNUmber));
                sortedLists.Add("@HotelName", (string.IsNullOrEmpty(_model.HotalName) ? string.Empty : _model.HotalName));
                sortedLists.Add("@SpaHotelId", _model.spaHotelId);
                sortedLists.Add("@ltd", _model.ltd);
                sortedLists.Add("@lth", _model.lth);
                sortedLists.Add("@AptName", (string.IsNullOrEmpty(_model.ApartmentName) ? string.Empty : _model.ApartmentName));
                empty = sqlHelper.executeNonQueryWMessage("SaveappointmentRequest_v2", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(string.Concat(exception.Message, _model.bookingdate.ToString()), this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return empty;
        }

        public string CancelAppointMentById(int RequestId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            try
            {
                sortedLists.Add("@Aid", RequestId);
                sqlHelper.executeNonQuery("CancelRequestByClentID", "", sortedLists);
                empty = "Appointment canceled by User is succesfully.";
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return empty;
        }

        public string CheckBookingDateIsExist(int thId, string bookingDate, string EArrivalTime, string lArrivalTime)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            string[] strArrays = bookingDate.Split(new char[] { '-' });
            utility _utility = new utility();
            string str = string.Concat(new object[] { _utility.ParseMonthToInt(strArrays[1]), "-", strArrays[0], "-", strArrays[2] });
            try
            {
                sortedLists.Add("@thId", thId);
                sortedLists.Add("@BookingDate", str);
                sortedLists.Add("@EArrivalTime", EArrivalTime);
                sortedLists.Add("@lArrivalTime", lArrivalTime);
                empty = sqlHelper.executeNonQueryWMessage("CheckBookingDateIsExist", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return empty;
        }

        public ClientSurveymodel CheckCLientPendingSurveryByAId(int aid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            ClientSurveymodel clientSurveymodel = new ClientSurveymodel();
            try
            {
                sortedLists.Add("@Aid", aid);
                DataTable dataTable = sqlHelper.fillDataTable("GetClientPendingSurvery", "", sortedLists);
                clientSurveymodel.tname = (!string.IsNullOrEmpty(dataTable.Rows[0]["Name"].ToString()) ? dataTable.Rows[0]["Name"].ToString() : string.Empty);
                clientSurveymodel.pendingServey = Convert.ToInt16(dataTable.Rows[0]["_count"].ToString()) <= 0;
                clientSurveymodel.therapistName = (!string.IsNullOrEmpty(dataTable.Rows[0]["tName"].ToString()) ? dataTable.Rows[0]["tName"].ToString() : string.Empty);
                DateTime dateTime = Convert.ToDateTime(dataTable.Rows[0]["bookingDate"].ToString());
                clientSurveymodel.appointment_date = dateTime.ToString("dd-MMM-y");
                clientSurveymodel.appointment_time = dataTable.Rows[0]["confirmedTime"].ToString();
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return clientSurveymodel;
        }

        public string checkempty(string val)
        {
            return val;
        }

        public string CheckhotelBookingDateIsExist(int thId, string bookingDate, string EArrivalTime, string lArrivalTime)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            string[] strArrays = bookingDate.Split(new char[] { '-' });
            utility _utility = new utility();
            object[] num = new object[] { _utility.ParseMonthToInt(strArrays[1]), "-", null, null, null };
            num[2] = (strArrays[0].Length == 1 ? string.Concat("0", strArrays[0]) : strArrays[0]);
            num[3] = "-";
            num[4] = strArrays[2];
            string str = string.Concat(num);
            try
            {
                sortedLists.Add("@hId", thId);
                sortedLists.Add("@BookingDate", str);
                sortedLists.Add("@EArrivalTime", EArrivalTime);
                sortedLists.Add("@lArrivalTime", lArrivalTime);
                empty = sqlHelper.executeNonQueryWMessage("CheckBookingDatehotelIsExist", "", sortedLists).ToString();
                if (empty.Contains("checkconfirmed"))
                {
                    empty = (new therapistService()).GetAvailablityWithAppointmenthotelByDate(thId, EArrivalTime, lArrivalTime, bookingDate);
                }
                else if (empty.Contains("checkavail"))
                {
                    string[] strArrays1 = empty.Split(new char[] { '\u005F' });
                    HotelServices _therapistService = new HotelServices();
                    empty = string.Concat(new string[] { "Error: Therapist available but ", strArrays1[1], " not available at that time it is available from ", _therapistService.GethotelAvailbilityScheduleIdbydate2(thId, Convert.ToDateTime(str), false).Replace("Available", "").Replace("<br/>", ""), " Or Try any other Hotel Apt  for that time Total stay is 2 hrs which is 90 min plus  30 min to change shower." });
                }
                empty = empty.Replace("And  Or", "Or");
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return empty;
        }

        public Surveymodel CheckPendingSurveryByAId(int aid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Surveymodel surveymodel = new Surveymodel();
            try
            {
                sortedLists.Add("@Aid", aid);
                DataTable dataTable = sqlHelper.fillDataTable("GetTherapistPendingSurvery", "", sortedLists);
                surveymodel.tname = (!string.IsNullOrEmpty(dataTable.Rows[0]["Name"].ToString()) ? dataTable.Rows[0]["Name"].ToString() : string.Empty);
                surveymodel.pendingServey = Convert.ToInt16(dataTable.Rows[0]["_count"].ToString()) <= 0;
                DateTime dateTime = Convert.ToDateTime(dataTable.Rows[0]["bookingDate"].ToString());
                surveymodel.appointment_date = dateTime.ToString("dd-MMM-y");
                surveymodel.appointment_time = dataTable.Rows[0]["confirmedTime"].ToString();
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return surveymodel;
        }

        public void editAppointmentRequest(int id, string reason)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string shortUrl = this.GetShortUrl("https://www.californiamassage.in/Account/retrunAppointement", false);
            try
            {
                sortedLists.Add("@id", id);
                sortedLists.Add("@reason", reason);
                sortedLists.Add("@url", shortUrl);
                sqlHelper.executeNonQuery("tblAppointementMakeActiveRetrun", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
        }

        public AppointmentModel GetAllAppointMentDetailByClientId(int Clientid)
        {
            AppointmentModel appointmentModel = new AppointmentModel();
            List<AppointmentModel_item> appointmentModelItems = new List<AppointmentModel_item>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            utility _utility = new utility();
            Crypto crypto = new Crypto();
            HotelServices hotelService = new HotelServices();
            try
            {
                sortedLists.Add("@Cid", Clientid);
                DataTable dataTable = sqlHelper.fillDataTable("GetAppointmentConfirmedDetailsByCLIENTId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        AppointmentModel_item appointmentModelItem = new AppointmentModel_item()
                        {
                            pickdrop = (dataTable.Rows[i]["IsVehicale"].ToString() == "Y" ? "Pay 2 way ola transport " : "Pick And drop"),
                            therapistName = dataTable.Rows[i]["Name"].ToString(),
                            ltd = dataTable.Rows[i]["ltd"].ToString(),
                            lth = dataTable.Rows[i]["lth"].ToString(),
                            ClientId = Convert.ToInt32(dataTable.Rows[i]["ClientId"].ToString()),
                            ClientName = dataTable.Rows[i]["CLientName"].ToString()
                        };
                        DateTime dateTime = Convert.ToDateTime(dataTable.Rows[i]["BookingDate"]);
                        appointmentModelItem.startDate = dateTime.ToString("dd-MMM-y");
                        appointmentModelItem.Aid = Convert.ToInt32(dataTable.Rows[i]["Aid"].ToString());
                        appointmentModelItem.vehical = this.getVehicalByAid(Convert.ToInt32(dataTable.Rows[i]["AID"].ToString()));
                        appointmentModelItem.token = crypto.EncryptStringAES(dataTable.Rows[i]["tid"].ToString());
                        appointmentModelItem.status = dataTable.Rows[i]["Status"].ToString();
                        appointmentModelItem.tokenId = crypto.EncryptStringAES(dataTable.Rows[i]["Aid"].ToString());
                        appointmentModelItem.arrivalTime = dataTable.Rows[i]["LArrivalTime"].ToString();
                        appointmentModelItem.startTime = dataTable.Rows[i]["EArrivalTime"].ToString();
                        appointmentModelItem.rate = dataTable.Rows[i]["rate"].ToString().Split(new char[] { '.' })[0].ToString();
                        appointmentModelItem.custommessage = dataTable.Rows[i]["CustomMessage"].ToString();
                        int num = Convert.ToInt32(dataTable.Rows[0]["SpaHotelId"].ToString());
                        appointmentModelItem.location = this.GetLocation((dataTable.Rows[0]["locationTypeId"].ToString() == "4" ? dataTable.Rows[0]["HotalName"].ToString() : dataTable.Rows[0]["locationTypeId"].ToString()), dataTable.Rows[0]["locationTypeId"].ToString(), num, hotelService);
                        appointmentModelItem.SpaMap = (num == 0 ? string.Empty : hotelService.GetHotelsById(num).Map);
                        appointmentModelItem.address = (num == 0 ? string.Empty : dataTable.Rows[i]["Address"].ToString());
                        appointmentModelItem.houseNumber = (dataTable.Rows[i]["houseNumber"] != null ? string.Empty : dataTable.Rows[i]["houseNumber"].ToString());
                        appointmentModelItem.aptNumber = (dataTable.Rows[i]["aptNumber"] != null ? string.Empty : dataTable.Rows[i]["aptNumber"].ToString());
                        appointmentModelItem.hotelName = (dataTable.Rows[i]["hotelName"] != null ? string.Empty : dataTable.Rows[i]["hotelName"].ToString());
                        appointmentModelItem.floorNumber = (dataTable.Rows[i]["floorNumber"] != null ? string.Empty : dataTable.Rows[i]["floorNumber"].ToString());
                        appointmentModelItem.streetName = (dataTable.Rows[i]["streetName"] != null ? string.Empty : dataTable.Rows[i]["streetName"].ToString());
                        appointmentModelItem.crossstreetName = (dataTable.Rows[i]["crossstreetName"] != null ? string.Empty : dataTable.Rows[i]["crossstreetName"].ToString());
                        appointmentModelItem.landmark = (dataTable.Rows[i]["landmark"] != null ? string.Empty : dataTable.Rows[i]["landmark"].ToString());
                        appointmentModelItem.localityArea = (dataTable.Rows[i]["localityArea"] != null ? string.Empty : dataTable.Rows[i]["localityArea"].ToString());
                        appointmentModelItem.roomNumber = (dataTable.Rows[i]["roomNumber"] != null ? string.Empty : dataTable.Rows[i]["roomNumber"].ToString());
                        appointmentModelItem.aptName = (dataTable.Rows[i]["aptName"] != null ? string.Empty : dataTable.Rows[i]["aptName"].ToString());
                        appointmentModelItems.Add(appointmentModelItem);
                    }
                }
                else
                {
                    appointmentModel.ErrorText = "No Pending Requests At Present";
                }
                dataTable.Dispose();
                appointmentModel.appt = appointmentModelItems;
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                appointmentModel.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return appointmentModel;
        }

        public bookingModel GetAppointMentById(int RequestId)
        {
            bookingModel _bookingModel = new bookingModel();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            HotelServices hotelService = new HotelServices();
            try
            {
                sortedLists.Add("@Aid", RequestId);
                DataSet dataSet = sqlHelper.fillDataSet("TblAppointmentGetAllDetails", "", sortedLists);
                if (dataSet.Tables.Count > 0)
                {
                    DataTable item = dataSet.Tables[0];
                    DataTable dataTable = dataSet.Tables[1];
                    DataTable item1 = dataSet.Tables[2];
                    _bookingModel.AID = RequestId;
                    DateTime dateTime = Convert.ToDateTime(item.Rows[0]["BookingDate"].ToString());
                    _bookingModel.bookingdate = dateTime.ToString("dd-MMM-y");
                    _bookingModel.CustomeMessage = item.Rows[0]["CustomMessage"].ToString();
                    _bookingModel.EArrivalTime = item.Rows[0]["EArrivalTime"].ToString();
                    _bookingModel.LArrivalTime = item.Rows[0]["LArrivalTime"].ToString();
                    _bookingModel.locPick = (dataTable.Rows[0]["IsVehicale"].ToString() == "Y" ? "1" : "2");
                    _bookingModel.vehicalType = dataTable.Rows[0]["VehicalType"].ToString();
                    if (_bookingModel.locPick == "1")
                    {
                        _bookingModel.color = (dataTable.Rows[0]["color"] != null ? dataTable.Rows[0]["color"].ToString() : string.Empty);
                        _bookingModel.make = (dataTable.Rows[0]["Make"] != null ? dataTable.Rows[0]["Make"].ToString() : string.Empty);
                        _bookingModel.licenceNumber = (dataTable.Rows[0]["LicenceNumber"] != null ? dataTable.Rows[0]["LicenceNumber"].ToString() : string.Empty);
                        _bookingModel.ModelNumber = (dataTable.Rows[0]["modelNumber"] != null ? dataTable.Rows[0]["ModelNumber"].ToString() : string.Empty);
                    }
                    _bookingModel.loc = item1.Rows[0]["locationTypeId"].ToString();
                    if (_bookingModel.loc == "1")
                    {
                        _bookingModel.housecrossStreetName = item1.Rows[0]["CrossStreetName"].ToString();
                        _bookingModel.houseFloorNUmber = item1.Rows[0]["FloorNumber"].ToString();
                        _bookingModel.housestreetname = item1.Rows[0]["StreetName"].ToString();
                        _bookingModel.houseNUmber = item1.Rows[0]["HouseNumber"].ToString();
                        _bookingModel.houselandmark = item1.Rows[0]["Landmark"].ToString();
                        _bookingModel.houselacalityArea = item1.Rows[0]["LocalityArea"].ToString();
                        _bookingModel.houseFlatNUmber = item1.Rows[0]["AptNumber"].ToString();
                    }
                    else if (_bookingModel.loc == "2")
                    {
                        _bookingModel.AptcrossStreetName = item1.Rows[0]["CrossStreetName"].ToString();
                        _bookingModel.AptFloorNUmber = item1.Rows[0]["FloorNumber"].ToString();
                        _bookingModel.Aptstreetname = item1.Rows[0]["StreetName"].ToString();
                        _bookingModel.ApartmentNUmber = item1.Rows[0]["AptNumber"].ToString();
                        _bookingModel.Aptlandmark = item1.Rows[0]["Landmark"].ToString();
                        _bookingModel.ApartmentName = item1.Rows[0]["AptName"].ToString();
                        _bookingModel.AptlacalityArea = item1.Rows[0]["LocalityArea"].ToString();
                    }
                    else if (_bookingModel.loc == "3")
                    {
                        _bookingModel.HotelStreetname = item1.Rows[0]["StreetName"].ToString();
                        _bookingModel.HotelRoomNUmber = item1.Rows[0]["RoomNumber"].ToString();
                        _bookingModel.HotelcrossStreetName = item1.Rows[0]["CrossStreetName"].ToString();
                        _bookingModel.Hotellandmark = item1.Rows[0]["Landmark"].ToString();
                        _bookingModel.HotalName = item1.Rows[0]["HotelName"].ToString();
                    }
                    else if (_bookingModel.loc == "4")
                    {
                        _bookingModel.spaHotelId = Convert.ToInt32(item1.Rows[0]["SpaHotelId"].ToString());
                        bookingModel _bookingModel1 = _bookingModel;
                        _bookingModel1.SpaMap = (_bookingModel1.spaHotelId == 0 ? string.Empty : hotelService.GetHotelsById(_bookingModel.spaHotelId).Map);
                    }
                    item.Dispose();
                    dataTable.Dispose();
                    item1.Dispose();
                }
                dataSet.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _bookingModel.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _bookingModel;
        }

        public bookingModel GetAppointMentBythidId(int RequestId)
        {
            bookingModel _bookingModel = new bookingModel();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            HotelServices hotelService = new HotelServices();
            try
            {
                sortedLists.Add("@Aid", RequestId);
                DataSet dataSet = sqlHelper.fillDataSet("TblAppointmentGetAllDetailsdesc", "", sortedLists);
                if (dataSet.Tables.Count > 0)
                {
                    DataTable item = dataSet.Tables[1];
                    if (dataSet.Tables.Count > 2)
                    {
                        DataTable dataTable = dataSet.Tables[2];
                        _bookingModel.locPick = (dataTable.Rows[0]["IsVehicale"].ToString() == "Y" ? "1" : "2");
                        _bookingModel.vehicalType = dataTable.Rows[0]["VehicalType"].ToString();
                        if (_bookingModel.locPick == "1")
                        {
                            _bookingModel.color = (dataTable.Rows[0]["color"] != null ? dataTable.Rows[0]["color"].ToString() : string.Empty);
                            _bookingModel.make = (dataTable.Rows[0]["Make"] != null ? dataTable.Rows[0]["Make"].ToString() : string.Empty);
                            _bookingModel.licenceNumber = (dataTable.Rows[0]["LicenceNumber"] != null ? dataTable.Rows[0]["LicenceNumber"].ToString() : string.Empty);
                            _bookingModel.ModelNumber = (dataTable.Rows[0]["modelNumber"] != null ? dataTable.Rows[0]["ModelNumber"].ToString() : string.Empty);
                        }
                        dataTable.Dispose();
                    }
                    _bookingModel.loc = item.Rows[0]["locationTypeId"].ToString();
                    if (_bookingModel.loc == "1")
                    {
                        _bookingModel.housecrossStreetName = this.checkempty(item.Rows[0]["CrossStreetName"].ToString());
                        _bookingModel.houseFloorNUmber = this.checkempty(item.Rows[0]["FloorNumber"].ToString());
                        _bookingModel.housestreetname = this.checkempty(item.Rows[0]["StreetName"].ToString());
                        _bookingModel.houseNUmber = this.checkempty(item.Rows[0]["HouseNumber"].ToString());
                        _bookingModel.houselandmark = this.checkempty(item.Rows[0]["Landmark"].ToString());
                        _bookingModel.houselacalityArea = this.checkempty(item.Rows[0]["LocalityArea"].ToString());
                        _bookingModel.houseFlatNUmber = this.checkempty(item.Rows[0]["AptNumber"].ToString());
                        _bookingModel.ltd = this.checkempty(item.Rows[0]["ltd"].ToString());
                        _bookingModel.lth = this.checkempty(item.Rows[0]["lth"].ToString());
                    }
                    else if (_bookingModel.loc == "3")
                    {
                        _bookingModel.AptcrossStreetName = this.checkempty(item.Rows[0]["CrossStreetName"].ToString());
                        _bookingModel.AptFloorNUmber = this.checkempty(item.Rows[0]["FloorNumber"].ToString());
                        _bookingModel.Aptstreetname = this.checkempty(item.Rows[0]["StreetName"].ToString());
                        _bookingModel.ApartmentNUmber = this.checkempty(item.Rows[0]["AptNumber"].ToString());
                        _bookingModel.Aptlandmark = this.checkempty(item.Rows[0]["Landmark"].ToString());
                        _bookingModel.ApartmentName = this.checkempty(item.Rows[0]["AptName"].ToString());
                        _bookingModel.AptlacalityArea = this.checkempty(item.Rows[0]["LocalityArea"].ToString());
                        _bookingModel.ltd = this.checkempty(item.Rows[0]["ltd"].ToString());
                        _bookingModel.lth = this.checkempty(item.Rows[0]["lth"].ToString());
                    }
                    else if (_bookingModel.loc == "2")
                    {
                        _bookingModel.HotelStreetname = this.checkempty(item.Rows[0]["StreetName"].ToString());
                        _bookingModel.HotelRoomNUmber = this.checkempty(item.Rows[0]["RoomNumber"].ToString());
                        _bookingModel.HotelcrossStreetName = this.checkempty(item.Rows[0]["CrossStreetName"].ToString());
                        _bookingModel.Hotellandmark = this.checkempty(item.Rows[0]["Landmark"].ToString());
                        _bookingModel.HotalName = this.checkempty(item.Rows[0]["HotelName"].ToString());
                        _bookingModel.ltd = this.checkempty(item.Rows[0]["ltd"].ToString());
                        _bookingModel.lth = this.checkempty(item.Rows[0]["lth"].ToString());
                    }
                    else if (_bookingModel.loc == "4")
                    {
                        _bookingModel.spaHotelId = Convert.ToInt32(item.Rows[0]["SpaHotelId"].ToString());
                        bookingModel _bookingModel1 = _bookingModel;
                        _bookingModel1.SpaMap = (_bookingModel1.spaHotelId == 0 ? string.Empty : hotelService.GetHotelsById(_bookingModel.spaHotelId).Map);
                    }
                    item.Dispose();
                }
                dataSet.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _bookingModel.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _bookingModel;
        }

        public AppointmentModel_item GetAppointMentDetailByClientId(int RequestId, bool isFormatRequired = true)
        {
            AppointmentModel_item appointmentModelItem = new AppointmentModel_item();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            utility _utility = new utility();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            HotelServices hotelService = new HotelServices();
            try
            {
                sortedLists.Add("@Clientid", RequestId);
                DataTable dataTable = sqlHelper.fillDataTable("GetAppointmentDetailsByUserid", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    appointmentModelItem.vehical = this.getVehicalByAid(Convert.ToInt32(dataTable.Rows[0]["AID"].ToString()));
                    appointmentModelItem.ClientPickUptime = dataTable.Rows[0]["PickUpTime"].ToString();
                    appointmentModelItem.pickdrop = (dataTable.Rows[0]["IsVehicale"].ToString() == "Y" ? "Pick And drop" : " Pay 2 way ola transport ");
                    appointmentModelItem.therapistName = dataTable.Rows[0]["Name"].ToString();
                    DateTime dateTime = Convert.ToDateTime(dataTable.Rows[0]["BookingDate"]);
                    appointmentModelItem.startDate = dateTime.ToString("dd-MMM-y");
                    indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[0]["createdDate"]));
                    appointmentModelItem.createdDate = (isFormatRequired ? Convert.ToDateTime(indianTimeFromPST).ToString("dd-MMM-y") : indianTimeFromPST.ToString());
                    appointmentModelItem.Aid = RequestId;
                    appointmentModelItem.ltd = dataTable.Rows[0]["ltd"].ToString();
                    appointmentModelItem.lth = dataTable.Rows[0]["lth"].ToString();
                    appointmentModelItem.tltd = dataTable.Rows[0]["tltd"].ToString();
                    appointmentModelItem.tlth = dataTable.Rows[0]["tlth"].ToString();
                    appointmentModelItem.comments = dataTable.Rows[0]["comments"].ToString();
                    appointmentModelItem.therapistTime = dataTable.Rows[0]["confirmedTime"].ToString();
                    appointmentModelItem.token = crypto.EncryptStringAES(dataTable.Rows[0]["tid"].ToString());
                    appointmentModelItem.tokenId = crypto.EncryptStringAES(RequestId.ToString());
                    appointmentModelItem.arrivalTime = dataTable.Rows[0]["LArrivalTime"].ToString();
                    appointmentModelItem.startTime = dataTable.Rows[0]["EArrivalTime"].ToString();
                    if (!string.IsNullOrEmpty(dataTable.Rows[0]["discountedRate"].ToString()))
                    {
                        appointmentModelItem.rate = (Convert.ToDouble(dataTable.Rows[0]["discountedRate"].ToString()) < 1 ? dataTable.Rows[0]["rate"].ToString() : dataTable.Rows[0]["discountedRate"].ToString());
                    }
                    else
                    {
                        appointmentModelItem.rate = dataTable.Rows[0]["rate"].ToString();
                    }
                    AppointmentModel_item str = appointmentModelItem;
                    str.rate = str.rate.Split(new char[] { '.' })[0].ToString();
                    appointmentModelItem.custommessage = dataTable.Rows[0]["CustomMessage"].ToString();
                    int num = Convert.ToInt32(dataTable.Rows[0]["SpaHotelId"].ToString());
                    appointmentModelItem.location = this.GetLocation((dataTable.Rows[0]["locationTypeId"].ToString() == "4" ? dataTable.Rows[0]["HotalName"].ToString() : dataTable.Rows[0]["locationTypeId"].ToString()), dataTable.Rows[0]["locationTypeId"].ToString(), num, hotelService);
                    appointmentModelItem.address = (num == 0 ? dataTable.Rows[0]["location"].ToString() : dataTable.Rows[0]["Address"].ToString());
                    appointmentModelItem.SpaMap = (num == 0 ? string.Empty : hotelService.GetHotelsById(num).Map);
                    appointmentModelItem.houseNumber = dataTable.Rows[0]["houseNumber"].ToString();
                    appointmentModelItem.aptNumber = dataTable.Rows[0]["aptNumber"].ToString();
                    appointmentModelItem.hotelName = dataTable.Rows[0]["hotelName"].ToString();
                    appointmentModelItem.floorNumber = dataTable.Rows[0]["floorNumber"].ToString();
                    appointmentModelItem.streetName = dataTable.Rows[0]["streetName"].ToString();
                    appointmentModelItem.crossstreetName = dataTable.Rows[0]["crossstreetName"].ToString();
                    appointmentModelItem.landmark = dataTable.Rows[0]["landmark"].ToString();
                    appointmentModelItem.localityArea = dataTable.Rows[0]["localityArea"].ToString();
                    appointmentModelItem.roomNumber = dataTable.Rows[0]["roomNumber"].ToString();
                    appointmentModelItem.aptName = dataTable.Rows[0]["aptName"].ToString();
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                appointmentModelItem.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return appointmentModelItem;
        }

        public AppointmentModel_item GetAppointMentDetailById(int RequestId, bool isFormatRequired = true)
        {
            AppointmentModel_item appointmentModelItem = new AppointmentModel_item();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            utility _utility = new utility();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            HotelServices hotelService = new HotelServices();
            try
            {
                sortedLists.Add("@Aid", RequestId);
                DataTable dataTable = sqlHelper.fillDataTable("GetAppointmentDetails", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    appointmentModelItem.vehical = this.getVehicalByAid(RequestId);
                    appointmentModelItem.ClientPickUptime = dataTable.Rows[0]["PickUpTime"].ToString();
                    appointmentModelItem.pickdrop = (dataTable.Rows[0]["IsVehicale"].ToString() == "Y" ? "Pick And drop" : " Pay 2 way ola transport ");
                    appointmentModelItem.therapistName = dataTable.Rows[0]["Name"].ToString();
                    appointmentModelItem.ClientName = dataTable.Rows[0]["CName"].ToString();
                    DateTime dateTime = Convert.ToDateTime(dataTable.Rows[0]["BookingDate"]);
                    appointmentModelItem.startDate = dateTime.ToString("dd-MMM-y");
                    indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[0]["createdDate"]));
                    appointmentModelItem.createdDate = (isFormatRequired ? Convert.ToDateTime(indianTimeFromPST).ToString("dd-MMM-y") : indianTimeFromPST.ToString());
                    appointmentModelItem.Aid = RequestId;
                    appointmentModelItem.ltd = dataTable.Rows[0]["ltd"].ToString();
                    appointmentModelItem.lth = dataTable.Rows[0]["lth"].ToString();
                    appointmentModelItem.tltd = dataTable.Rows[0]["tltd"].ToString();
                    appointmentModelItem.tlth = dataTable.Rows[0]["tlth"].ToString();
                    appointmentModelItem.comments = dataTable.Rows[0]["comments"].ToString();
                    appointmentModelItem.therapistTime = dataTable.Rows[0]["confirmedTime"].ToString();
                    appointmentModelItem.token = crypto.EncryptStringAES(dataTable.Rows[0]["tid"].ToString());
                    appointmentModelItem.thid = Convert.ToInt32(dataTable.Rows[0]["tid"].ToString());
                    appointmentModelItem.tokenId = crypto.EncryptStringAES(RequestId.ToString());
                    appointmentModelItem.arrivalTime = dataTable.Rows[0]["LArrivalTime"].ToString();
                    appointmentModelItem.startTime = dataTable.Rows[0]["EArrivalTime"].ToString();
                    if (!string.IsNullOrEmpty(dataTable.Rows[0]["discountedRate"].ToString()))
                    {
                        appointmentModelItem.rate = (Convert.ToDouble(dataTable.Rows[0]["discountedRate"].ToString()) < 1 ? dataTable.Rows[0]["rate"].ToString() : dataTable.Rows[0]["discountedRate"].ToString());
                    }
                    else
                    {
                        appointmentModelItem.rate = dataTable.Rows[0]["rate"].ToString();
                    }
                    AppointmentModel_item str = appointmentModelItem;
                    str.rate = str.rate.Split(new char[] { '.' })[0].ToString();
                    appointmentModelItem.custommessage = dataTable.Rows[0]["CustomMessage"].ToString();
                    int num = Convert.ToInt32(dataTable.Rows[0]["SpaHotelId"].ToString());
                    appointmentModelItem.SpaHotelid = num;
                    appointmentModelItem.location = this.GetLocation((dataTable.Rows[0]["locationTypeId"].ToString() == "4" ? dataTable.Rows[0]["HotalName"].ToString() : dataTable.Rows[0]["locationTypeId"].ToString()), dataTable.Rows[0]["locationTypeId"].ToString(), num, hotelService);
                    appointmentModelItem.address = (num == 0 ? dataTable.Rows[0]["location"].ToString() : dataTable.Rows[0]["Address"].ToString());
                    appointmentModelItem.SpaMap = (num == 0 ? string.Empty : hotelService.GetHotelsById(num).Map);
                    appointmentModelItem.houseNumber = dataTable.Rows[0]["houseNumber"].ToString();
                    appointmentModelItem.aptNumber = dataTable.Rows[0]["aptNumber"].ToString();
                    appointmentModelItem.hotelName = dataTable.Rows[0]["hotelName"].ToString();
                    appointmentModelItem.floorNumber = dataTable.Rows[0]["floorNumber"].ToString();
                    appointmentModelItem.streetName = dataTable.Rows[0]["streetName"].ToString();
                    appointmentModelItem.crossstreetName = dataTable.Rows[0]["crossstreetName"].ToString();
                    appointmentModelItem.landmark = dataTable.Rows[0]["landmark"].ToString();
                    appointmentModelItem.localityArea = dataTable.Rows[0]["localityArea"].ToString();
                    appointmentModelItem.roomNumber = dataTable.Rows[0]["roomNumber"].ToString();
                    appointmentModelItem.aptName = dataTable.Rows[0]["aptName"].ToString();
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                appointmentModelItem.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return appointmentModelItem;
        }

        public AppointmentModel GetAppointMentDetailByTherapistId(int thid)
        {
            AppointmentModel appointmentModel = new AppointmentModel();
            List<AppointmentModel_item> appointmentModelItems = new List<AppointmentModel_item>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            utility _utility = new utility();
            Crypto crypto = new Crypto();
            HotelServices hotelService = new HotelServices();
            try
            {
                sortedLists.Add("@thid", thid);
                DataTable dataTable = sqlHelper.fillDataTable("GetAppointmentDetailsByThidId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        AppointmentModel_item appointmentModelItem = new AppointmentModel_item()
                        {
                            pickdrop = (dataTable.Rows[i]["IsVehicale"].ToString().ToUpper() == "N" ? "Pay 2 way ola transport " : "Pick And drop"),
                            therapistName = dataTable.Rows[i]["Name"].ToString(),
                            ClientPickUptime = dataTable.Rows[i]["PickUpTime"].ToString(),
                            ClientName = dataTable.Rows[i]["CLientName"].ToString()
                        };
                        DateTime dateTime = Convert.ToDateTime(dataTable.Rows[i]["BookingDate"]);
                        appointmentModelItem.startDate = dateTime.ToString("dd-MMM-y");
                        dateTime = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["createdDate"]));
                        appointmentModelItem.createdDate = dateTime.ToString();
                        appointmentModelItem.Aid = Convert.ToInt32(dataTable.Rows[i]["Aid"].ToString());
                        appointmentModelItem.vehical = this.getVehicalByAid(Convert.ToInt32(dataTable.Rows[i]["AID"].ToString()));
                        appointmentModelItem.ClientId = Convert.ToInt32(dataTable.Rows[i]["ClientId"].ToString());
                        appointmentModelItem.token = crypto.EncryptStringAES(thid.ToString());
                        appointmentModelItem.tokenId = crypto.EncryptStringAES(dataTable.Rows[i]["Aid"].ToString());
                        appointmentModelItem.arrivalTime = dataTable.Rows[i]["LArrivalTime"].ToString();
                        appointmentModelItem.startTime = dataTable.Rows[i]["EArrivalTime"].ToString();
                        appointmentModelItem.ltd = dataTable.Rows[i]["ltd"].ToString();
                        appointmentModelItem.lth = dataTable.Rows[i]["lth"].ToString();
                        if (!string.IsNullOrEmpty(dataTable.Rows[i]["discountedRate"].ToString()))
                        {
                            appointmentModelItem.rate = dataTable.Rows[i]["discountedRate"].ToString();
                        }
                        else
                        {
                            appointmentModelItem.rate = dataTable.Rows[i]["rate"].ToString();
                        }
                        AppointmentModel_item str = appointmentModelItem;
                        str.rate = str.rate.Split(new char[] { '.' })[0].ToString();
                        appointmentModelItem.custommessage = dataTable.Rows[i]["CustomMessage"].ToString();
                        int num = Convert.ToInt32(dataTable.Rows[i]["SpaHotelId"].ToString());
                        appointmentModelItem.location = this.GetLocation((dataTable.Rows[i]["locationTypeId"].ToString() == "4" ? dataTable.Rows[i]["HotalName"].ToString() : dataTable.Rows[i]["locationTypeId"].ToString()), dataTable.Rows[i]["locationTypeId"].ToString(), num, hotelService);
                        appointmentModelItem.address = (num == 0 ? "" : dataTable.Rows[i]["Address"].ToString());
                        appointmentModelItem.SpaMap = (num == 0 ? string.Empty : hotelService.GetHotelsById(num).Map);
                        appointmentModelItem.houseNumber = dataTable.Rows[i]["houseNumber"].ToString();
                        appointmentModelItem.aptNumber = dataTable.Rows[i]["aptNumber"].ToString();
                        appointmentModelItem.hotelName = dataTable.Rows[i]["hotelName"].ToString();
                        appointmentModelItem.floorNumber = dataTable.Rows[i]["floorNumber"].ToString();
                        appointmentModelItem.streetName = dataTable.Rows[i]["streetName"].ToString();
                        appointmentModelItem.crossstreetName = dataTable.Rows[i]["crossstreetName"].ToString();
                        appointmentModelItem.landmark = dataTable.Rows[i]["landmark"].ToString();
                        appointmentModelItem.localityArea = dataTable.Rows[i]["localityArea"].ToString();
                        appointmentModelItem.roomNumber = dataTable.Rows[i]["roomNumber"].ToString();
                        appointmentModelItem.aptName = dataTable.Rows[i]["aptName"].ToString();
                        appointmentModelItems.Add(appointmentModelItem);
                    }
                    dataTable.Dispose();
                }
                else
                {
                    appointmentModel.ErrorText = "No Pending Requests At Present ";
                }
                appointmentModel.appt = appointmentModelItems;
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                appointmentModel.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return appointmentModel;
        }

        public string getclientsurvey(int id)
        {
            string str = "Appointment Is Confirmed";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@clientId", id);
                str = sqlHelper.executeNonQueryWMessage("GetclientSurveyFeedBack", "", sortedLists).ToString();
                if (str.Contains("Error"))
                {
                    string str1 = str.Split(new char[] { '\u005F' })[1];
                    string shortUrl = this.GetShortUrl(string.Concat(new object[] { "https://www.californiamassage.in/main/Survey?_xid=", id.ToString(), "&aid=", str1 }), false);
                    str = string.Concat(str.Split(new char[] { '\u005F' })[0], "_", shortUrl);
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public ClientSurveymodel GetClientSurveyByIdSurvey(int id, string token)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            ClientSurveymodel clientSurveymodel = new ClientSurveymodel();
            try
            {
                sortedLists.Add("@id", id);
                DataTable dataTable = sqlHelper.fillDataTable("GetServeyFroClientBySurveyid", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    clientSurveymodel.Pay = dataTable.Rows[0]["Paid"].ToString();
                    clientSurveymodel.Q1 = dataTable.Rows[0]["Q1radio"].ToString();
                    clientSurveymodel.Q2 = dataTable.Rows[0]["Q2radio"].ToString();
                    clientSurveymodel.Q3 = dataTable.Rows[0]["Q3radio"].ToString();
                    clientSurveymodel.Q4 = dataTable.Rows[0]["Q4radio"].ToString();
                    clientSurveymodel.Q5 = dataTable.Rows[0]["Q5radio"].ToString();
                    clientSurveymodel.Q6 = dataTable.Rows[0]["Q6radio"].ToString();
                    clientSurveymodel.Q7 = dataTable.Rows[0]["Q7radio"].ToString();
                    clientSurveymodel.Q8 = dataTable.Rows[0]["Q8radio"].ToString();
                    clientSurveymodel.Q9 = dataTable.Rows[0]["Q9radio"].ToString();
                    clientSurveymodel.Q10 = dataTable.Rows[0]["Q10radio"].ToString();
                    clientSurveymodel.Q11 = dataTable.Rows[0]["Q11radio"].ToString();
                    clientSurveymodel.Q12 = dataTable.Rows[0]["Q12radio"].ToString();
                    clientSurveymodel.Q1Comment = dataTable.Rows[0]["Qcomment1"].ToString();
                    clientSurveymodel.Q2Comment = dataTable.Rows[0]["Qcomment2"].ToString();
                    clientSurveymodel.Q3Comment = dataTable.Rows[0]["Qcomment3"].ToString();
                    clientSurveymodel.Q4Comment = dataTable.Rows[0]["Qcomment4"].ToString();
                    clientSurveymodel.Q5Comment = dataTable.Rows[0]["Qcomment5"].ToString();
                    clientSurveymodel.Q6Comment = dataTable.Rows[0]["Qcomment6"].ToString();
                    clientSurveymodel.Q7Comment = dataTable.Rows[0]["Qcomment7"].ToString();
                    clientSurveymodel.Q8Comment = dataTable.Rows[0]["Qcomment8"].ToString();
                    clientSurveymodel.Q9Comment = dataTable.Rows[0]["Qcomment9"].ToString();
                    clientSurveymodel.Q10Comment = dataTable.Rows[0]["Qcomment10"].ToString();
                    clientSurveymodel.Q11Comment = dataTable.Rows[0]["Qcomment11"].ToString();
                    clientSurveymodel.Q12Comment = dataTable.Rows[0]["Qcomment12"].ToString();
                    clientSurveymodel.tname = dataTable.Rows[0]["name"].ToString();
                    clientSurveymodel.therapistName = dataTable.Rows[0]["tName"].ToString();
                    clientSurveymodel.rating = Convert.ToInt16(dataTable.Rows[0]["rating"].ToString());
                    clientSurveymodel.token = token;
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return clientSurveymodel;
        }

        public AppointmentModel GetConfirmedAppointMentDetailByTherapistId(int thid)
        {
            AppointmentModel appointmentModel = new AppointmentModel();
            List<AppointmentModel_item> appointmentModelItems = new List<AppointmentModel_item>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            utility _utility = new utility();
            Crypto crypto = new Crypto();
            HotelServices hotelService = new HotelServices();
            try
            {
                sortedLists.Add("@thid", thid);
                DataTable dataTable = sqlHelper.fillDataTable("GetAppointmentConfirmedDetailsByThidId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        AppointmentModel_item appointmentModelItem = new AppointmentModel_item()
                        {
                            pickdrop = (dataTable.Rows[i]["IsVehicale"].ToString().ToUpper() == "N" ? "Pay 2 way ola transport " : "Pick And drop"),
                            therapistName = dataTable.Rows[i]["Name"].ToString(),
                            ClientPickUptime = dataTable.Rows[i]["PickUpTime"].ToString(),
                            ltd = dataTable.Rows[i]["ltd"].ToString(),
                            isstartmassage = !(dataTable.Rows[i]["startTime"].ToString() == ""),
                            isLeftCabTime = !(dataTable.Rows[i]["leftCabTime"].ToString() == ""),
                            lth = dataTable.Rows[i]["lth"].ToString(),
                            tltd = dataTable.Rows[i]["tltd"].ToString(),
                            tlth = dataTable.Rows[i]["tlth"].ToString(),
                            comments = dataTable.Rows[i]["comments"].ToString(),
                            therapistTime = dataTable.Rows[i]["confirmedTime"].ToString(),
                            ClientMobile = dataTable.Rows[i]["Clientmobile"].ToString(),
                            therapistMobile = dataTable.Rows[i]["therapistmobile"].ToString(),
                            ClientId = Convert.ToInt32(dataTable.Rows[i]["ClientId"].ToString()),
                            ClientName = dataTable.Rows[i]["CLientName"].ToString()
                        };
                        DateTime dateTime = Convert.ToDateTime(dataTable.Rows[i]["BookingDate"]);
                        appointmentModelItem.startDate = dateTime.ToString("dd-MMM-y");
                        dateTime = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[i]["createdDate"]));
                        appointmentModelItem.createdDate = dateTime.ToString();
                        appointmentModelItem.Aid = Convert.ToInt32(dataTable.Rows[i]["Aid"].ToString());
                        appointmentModelItem.vehical = this.getVehicalByAid(Convert.ToInt32(dataTable.Rows[i]["AID"].ToString()));
                        appointmentModelItem.token = crypto.EncryptStringAES(thid.ToString());
                        appointmentModelItem.tokenId = crypto.EncryptStringAES(dataTable.Rows[i]["Aid"].ToString());
                        appointmentModelItem.arrivalTime = dataTable.Rows[i]["LArrivalTime"].ToString();
                        appointmentModelItem.startTime = dataTable.Rows[i]["EArrivalTime"].ToString();
                        if (!string.IsNullOrEmpty(dataTable.Rows[i]["discountedRate"].ToString()))
                        {
                            appointmentModelItem.rate = (Convert.ToDouble(dataTable.Rows[i]["discountedRate"].ToString()) < 1 ? dataTable.Rows[i]["rate"].ToString() : dataTable.Rows[i]["discountedRate"].ToString());
                        }
                        else
                        {
                            appointmentModelItem.rate = dataTable.Rows[i]["rate"].ToString();
                        }
                        AppointmentModel_item appointmentModelItem1 = appointmentModelItem;
                        appointmentModelItem1.rate = appointmentModelItem1.rate.Split(new char[] { '.' })[0];
                        appointmentModelItem.custommessage = dataTable.Rows[i]["CustomMessage"].ToString();
                        int num = Convert.ToInt32(dataTable.Rows[i]["SpaHotelId"].ToString());
                        appointmentModelItem.SpaHotelid = num;
                        appointmentModelItem.location = this.GetLocation((dataTable.Rows[i]["locationTypeId"].ToString() == "4" ? dataTable.Rows[i]["HotalName"].ToString() : dataTable.Rows[i]["locationTypeId"].ToString()), dataTable.Rows[i]["locationTypeId"].ToString(), num, hotelService);
                        appointmentModelItem.address = (num == 0 ? string.Empty : dataTable.Rows[i]["Address"].ToString());
                        appointmentModelItem.SpaMap = (num == 0 ? string.Empty : hotelService.GetHotelsById(num).Map);
                        appointmentModelItem.houseNumber = dataTable.Rows[i]["houseNumber"].ToString();
                        appointmentModelItem.aptNumber = dataTable.Rows[i]["aptNumber"].ToString();
                        appointmentModelItem.hotelName = dataTable.Rows[i]["hotelName"].ToString();
                        appointmentModelItem.floorNumber = dataTable.Rows[i]["floorNumber"].ToString();
                        appointmentModelItem.streetName = dataTable.Rows[i]["streetName"].ToString();
                        appointmentModelItem.crossstreetName = dataTable.Rows[i]["crossstreetName"].ToString();
                        appointmentModelItem.landmark = dataTable.Rows[i]["landmark"].ToString();
                        appointmentModelItem.localityArea = dataTable.Rows[i]["localityArea"].ToString();
                        appointmentModelItem.roomNumber = dataTable.Rows[i]["roomNumber"].ToString();
                        appointmentModelItem.aptName = dataTable.Rows[i]["aptName"].ToString();
                        appointmentModelItems.Add(appointmentModelItem);
                    }
                }
                else
                {
                    appointmentModel.ErrorText = "No Confirmed Appt At this time";
                }
                dataTable.Dispose();
                appointmentModel.appt = appointmentModelItems;
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                appointmentModel.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return appointmentModel;
        }

        public string getconfirmTime(string ApptTime, int id)
        {
            string str = "Appointment Is Confirmed";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            if (ApptTime.Contains<char>('\u005F'))
            {
                string[] strArrays = ApptTime.Split(new char[] { '\u005F' });
                ApptTime = string.Concat(new string[] { strArrays[0], ":", strArrays[1], " ", strArrays[2] });
            }
            try
            {
                sortedLists.Add("@time", ApptTime);
                sortedLists.Add("@AID", id);
                str = sqlHelper.executeNonQueryWMessage("GetConfirmedTime_v2", "", sortedLists).ToString();
                if (str.Contains("plese check your schedule"))
                {
                    string[] strArrays1 = str.Split(new char[] { '\u005F' });
                    str = string.Concat("Error : Selected Time is not available ,Available Time is", (new therapistService()).GetTherapistScheduleId(Convert.ToInt32(strArrays1[0]), Convert.ToDateTime(strArrays1[1]), false));
                }
                else if (str.Contains("uncompleted"))
                {
                    string str1 = str.Split(new char[] { '\u005F' })[1];
                    string str2 = str.Split(new char[] { '\u005F' })[2];
                    string shortUrl = this.GetShortUrl(string.Concat(new object[] { "https://www.californiamassage.in/main/ClientSurvey?_xid=", str1.ToString(), "&aid=", str2.ToString() }), false);
                    str = string.Concat(str.Split(new char[] { '\u005F' })[0], ".Click below url to complete <a target='_blank' href='", shortUrl, "' class='btn btn-action'>Click here to complete </a>");
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string GetCrosStreetName(bookingModel _model)
        {
            string empty = string.Empty;
            string str = _model.loc;
            if (str == "1")
            {
                empty = _model.housecrossStreetName;
            }
            else if (str == "3")
            {
                empty = _model.AptcrossStreetName;
            }
            else if (str == "2")
            {
                empty = _model.HotelcrossStreetName;
            }
            if (string.IsNullOrEmpty(empty))
            {
                return string.Empty;
            }
            return empty;
        }

        public string GetFloorNumber(bookingModel _model)
        {
            string empty = string.Empty;
            string str = _model.loc;
            if (str == "1")
            {
                empty = _model.houseFloorNUmber;
            }
            else if (str == "3")
            {
                empty = _model.AptFloorNUmber;
            }
            if (string.IsNullOrEmpty(empty))
            {
                return string.Empty;
            }
            return empty;
        }

        private DateTime GetIndianTimeFromPST(DateTime _date)
        {
            DateTime staticdatetime = Convert.ToDateTime("04/13/2020");
            string Timezone = "Pacific Standard Time";
            if (_date > staticdatetime)
            {
                Timezone = "UTC";
                _date = _date.AddMinutes(4);
            }
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_date, Timezone, "India Standard Time");
        }

        public string GetLandmark(bookingModel _model)
        {
            string empty = string.Empty;
            string str = _model.loc;
            if (str == "1")
            {
                empty = _model.houselandmark;
            }
            else if (str == "3")
            {
                empty = _model.Aptlandmark;
            }
            else if (str == "2")
            {
                empty = _model.Hotellandmark;
            }
            if (string.IsNullOrEmpty(empty))
            {
                return string.Empty;
            }
            return empty;
        }

        public string Getlocality(bookingModel _model)
        {
            string empty = string.Empty;
            string str = _model.loc;
            if (str == "1")
            {
                empty = _model.houselacalityArea;
            }
            else if (str == "3")
            {
                empty = _model.AptlacalityArea;
            }
            else if (str == "2")
            {
                empty = _model.HotellacalityArea;
            }
            if (string.IsNullOrEmpty(empty))
            {
                return string.Empty;
            }
            return empty;
        }

        public string GetLocation(string TypeId, string locationTypeId, int hotelId, HotelServices _hser)
        {
            string str;
            if (TypeId == "1")
            {
                str = "Clients House";
            }
            else if (TypeId == "2")
            {
                str = "Clients Hotel";
            }
            else if (TypeId != "3")
            {
                str = (TypeId == "4" ? _hser.GetHotelsById(hotelId).name : _hser.GetHotelsById(hotelId).name);
            }
            else
            {
                str = "Clients Apartment";
            }
            return str;
        }

        public int GetMonth(string monthName)
        {
            int num = 0;
            switch (monthName)
            {
                case "JAN":
                    {
                        num = 1;
                        break;
                    }
                case "FEB":
                    {
                        num = 2;
                        break;
                    }
                case "MAR":
                    {
                        num = 3;
                        break;
                    }
                case "APR":
                    {
                        num = 4;
                        break;
                    }
                case "MAY":
                    {
                        num = 5;
                        break;
                    }
                case "JUN":
                    {
                        num = 6;
                        break;
                    }
                case "JUL":
                    {
                        num = 7;
                        break;
                    }
                case "AUG":
                    {
                        num = 8;
                        break;
                    }
                case "SEP":
                    {
                        num = 9;
                        break;
                    }
                case "OCT":
                    {
                        num = 10;
                        break;
                    }
                case "NOV":
                    {
                        num = 11;
                        break;
                    }
                case "DEC":
                    {
                        num = 12;
                        break;
                    }
            }
            return num;
        }

        private DateTime GetPSTFromIndianTime(DateTime _date)
        {
            DateTime staticdatetime = Convert.ToDateTime("04/13/2020");
            string Timezone = "Pacific Standard Time";
            if (_date > staticdatetime)
            {
                Timezone = "UTC";
                _date = _date.AddMinutes(4);
            }
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_date, "India Standard Time", Timezone);
        }


        public string GetShortUrltest(string longUrl, bool isByGoolge = false, bool isurlencoded = false)
        {
            bool error = false;
            string _url = longUrl;
            try
            {
                string url = isurlencoded == false ? HttpContext.Current.Server.UrlEncode(longUrl) : longUrl;
                string str3 = string.Concat("https://api-ssl.bitly.com/v3/shorten?access_token=f48ea3d945fec8608950cb2dde821d1420e27dbc&longUrl=", url);
                string str4 = (new WebClient()).DownloadString(str3);
                result _result = (new JavaScriptSerializer()).Deserialize<result>(str4);
                Thread.Sleep(3000);
                _url = _result.data.url;
            }
            catch (Exception ex)
            {
                error = true;
            }
            if (error == true)
            {
                try
                {
                    string url = isurlencoded == false ? HttpContext.Current.Server.UrlEncode(longUrl) : longUrl;
                    string str3 = string.Concat("https://api-ssl.bitly.com/v3/shorten?access_token=6f1474375cf882d156f442b7ceab8b4f85111b03&longUrl=", url);
                    string str4 = (new WebClient()).DownloadString(str3);
                    result _result = (new JavaScriptSerializer()).Deserialize<result>(str4);
                    Thread.Sleep(3000);
                    _url = _result.data.url;
                    error = false;
                }
                catch (Exception ex)
                {
                    error = true;
                }
            }
            if (error == true)
            {
                try
                {
                    string url = isurlencoded == false ? HttpContext.Current.Server.UrlEncode(longUrl) : longUrl;
                    string str3 = string.Concat("https://api-ssl.bitly.com/v3/shorten?access_token=b00fcb9b2e5eeafbc1fb0eab4d577f03a31d0103&longUrl=", url);
                    string str4 = (new WebClient()).DownloadString(str3);
                    result _result = (new JavaScriptSerializer()).Deserialize<result>(str4);
                    Thread.Sleep(3000);
                    _url = _result.data.url;
                    error = false;
                }
                catch (Exception ex)
                {
                    error = true;
                }
            }

            if (error == true)
            {
                try
                {
                    string url = isurlencoded == false ? HttpContext.Current.Server.UrlEncode(longUrl) : longUrl;
                    string str3 = string.Concat("https://api-ssl.bitly.com/v3/shorten?access_token=454e9aa20709730e3326b0861b3be87fb64cbecb&longUrl=", url);
                    string str4 = (new WebClient()).DownloadString(str3);
                    result _result = (new JavaScriptSerializer()).Deserialize<result>(str4);
                    Thread.Sleep(3000);
                    _url = _result.data.url;
                    error = false;
                }
                catch (Exception ex)
                {
                    error = true;
                }
            }
            if (error == true)
            {
                try
                {
                    string url = isurlencoded == false ? HttpContext.Current.Server.UrlEncode(longUrl) : longUrl;
                    string str3 = string.Concat("https://api-ssl.bitly.com/v3/shorten?access_token=ddb9c90bc9090e1c1c50343ab069fce030bee207&longUrl=", url);
                    string str4 = (new WebClient()).DownloadString(str3);
                    result _result = (new JavaScriptSerializer()).Deserialize<result>(str4);
                    Thread.Sleep(3000);
                    _url = _result.data.url;
                }
                catch (Exception ex)
                {
                }
            }
            if (error == true)
            {
                try
                {
                    string url = isurlencoded == false ? HttpContext.Current.Server.UrlEncode(longUrl) : longUrl;
                    string str3 = string.Concat("https://api-ssl.bitly.com/v3/shorten?access_token=486219cc97b2d2f60ca0b9d41294d8958af7c0e4&longUrl=", url);
                    string str4 = (new WebClient()).DownloadString(str3);
                    result _result = (new JavaScriptSerializer()).Deserialize<result>(str4);
                    Thread.Sleep(3000);
                    _url = _result.data.url;
                }
                catch (Exception ex)
                {
                }
            }
            if (error == true)
            {
                try
                {
                    string url = isurlencoded == false ? HttpContext.Current.Server.UrlEncode(longUrl) : longUrl;
                    string str3 = string.Concat("https://api-ssl.bitly.com/v3/shorten?access_token=b379fc17d58531dc269e4560cb7871425de9b00f&longUrl=", url);
                    string str4 = (new WebClient()).DownloadString(str3);
                    result _result = (new JavaScriptSerializer()).Deserialize<result>(str4);
                    Thread.Sleep(3000);
                    _url = _result.data.url;
                }
                catch (Exception ex)
                {
                }
            }
            return _url;
        }
        public class d
        {
            public string global_hash
            {
                get;
                set;
            }

            public string hash
            {
                get;
                set;
            }

            public string long_url
            {
                get;
                set;
            }

            public string new_hash
            {
                get;
                set;
            }

            public string url
            {
                get;
                set;
            }

        }

        public class result
        {
            public d data
            {
                get;
                set;
            }

            public string status_code
            {
                get;
                set;
            }

            public string status_txt
            {
                get;
                set;
            }

            public result()
            {
            }
        }

        public string GetShortUrl(string longUrl, bool isByGoolge = false, bool isencodeurl = false)
        {
            //BaseClientService.Initializer initializer = new BaseClientService.Initializer();
            //initializer.ApiKey = "AIzaSyBprUoQ4HFjM5EUd9A5CkxmtprnaZBDZGM";
            //initializer.ApplicationName = "URL Shortener API Project";
            //UrlshortenerService urlshortenerService = new UrlshortenerService(initializer);
            //Url url = new Url();
            //url.LongUrl = longUrl;
            //Url url1 = url;
            //return urlshortenerService.Url.Insert(url1).Execute().Id;
            return GetShortUrltest(longUrl, isByGoolge, isencodeurl);
        }

        public string GetStreetName(bookingModel _model)
        {
            string empty = string.Empty;
            string str = _model.loc;
            if (str == "1")
            {
                empty = _model.housestreetname;
            }
            else if (str == "3")
            {
                empty = _model.Aptstreetname;
            }
            else if (str == "2")
            {
                empty = _model.HotelStreetname;
            }
            if (string.IsNullOrEmpty(empty))
            {
                return string.Empty;
            }
            return empty;
        }

        public Surveymodel GetTerapistSurveyByIdSurvey(int id, string token)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Surveymodel surveymodel = new Surveymodel();
            try
            {
                sortedLists.Add("@id", id);
                DataTable dataTable = sqlHelper.fillDataTable("GetServeyFroTherapistBySurveyid", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    surveymodel.Pay = dataTable.Rows[0]["Paid"].ToString();
                    surveymodel.Q15Comment = dataTable.Rows[0]["Comments"].ToString();
                    surveymodel.Q1 = dataTable.Rows[0]["Q1radio"].ToString();
                    surveymodel.Q2 = dataTable.Rows[0]["Q2radio"].ToString();
                    surveymodel.Q3 = dataTable.Rows[0]["Q3radio"].ToString();
                    surveymodel.Q4 = dataTable.Rows[0]["Q4radio"].ToString();
                    surveymodel.Q5 = dataTable.Rows[0]["Q5radio"].ToString();
                    surveymodel.Q6 = dataTable.Rows[0]["Q6radio"].ToString();
                    surveymodel.Q7 = dataTable.Rows[0]["Q7radio"].ToString();
                    surveymodel.Q8 = dataTable.Rows[0]["Q8radio"].ToString();
                    surveymodel.Q9 = dataTable.Rows[0]["Q9radio"].ToString();
                    surveymodel.Q10 = dataTable.Rows[0]["Q10radio"].ToString();
                    surveymodel.Q11 = dataTable.Rows[0]["Q11radio"].ToString();
                    surveymodel.Q12 = dataTable.Rows[0]["Q12radio"].ToString();
                    surveymodel.Q13 = dataTable.Rows[0]["Q13radio"].ToString();
                    surveymodel.Q14 = dataTable.Rows[0]["Q14radio"].ToString();
                    surveymodel.Q1Comment = dataTable.Rows[0]["Qcomment1"].ToString();
                    surveymodel.Q2Comment = dataTable.Rows[0]["Qcomment2"].ToString();
                    surveymodel.Q3Comment = dataTable.Rows[0]["Qcomment3"].ToString();
                    surveymodel.Q4Comment = dataTable.Rows[0]["Qcomment4"].ToString();
                    surveymodel.Q5Comment = dataTable.Rows[0]["Qcomment5"].ToString();
                    surveymodel.Q6Comment = dataTable.Rows[0]["Qcomment6"].ToString();
                    surveymodel.Q7Comment = dataTable.Rows[0]["Qcomment7"].ToString();
                    surveymodel.Q8Comment = dataTable.Rows[0]["Qcomment8"].ToString();
                    surveymodel.Q9Comment = dataTable.Rows[0]["Qcomment9"].ToString();
                    surveymodel.Q10Comment = dataTable.Rows[0]["Qcomment10"].ToString();
                    surveymodel.Q11Comment = dataTable.Rows[0]["Qcomment11"].ToString();
                    surveymodel.Q12Comment = dataTable.Rows[0]["Qcomment12"].ToString();
                    surveymodel.Q13Comment = dataTable.Rows[0]["Qcomment13"].ToString();
                    surveymodel.Q14Comment = dataTable.Rows[0]["Qcomment14"].ToString();
                    surveymodel.tname = dataTable.Rows[0]["name"].ToString();
                    surveymodel.therapistName = dataTable.Rows[0]["CLientName"].ToString();
                    surveymodel.rating = Convert.ToInt16(dataTable.Rows[0]["rating"].ToString());
                    surveymodel.token = token;
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return surveymodel;
        }

        public VehicalModel getVehicalByAid(int AID)
        {
            VehicalModel vehicalModel = new VehicalModel();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@Aid", AID);
                DataTable dataTable = sqlHelper.fillDataTable("GetVehicalFromAid", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    vehicalModel.vcolor = dataTable.Rows[0]["Color"].ToString();
                    vehicalModel.vlicenceNumber = dataTable.Rows[0]["LicenceNumber"].ToString();
                    vehicalModel.vmake = dataTable.Rows[0]["Make"].ToString();
                    vehicalModel.vmodelNumber = dataTable.Rows[0]["ModelNumber"].ToString();
                    vehicalModel.vtype = dataTable.Rows[0]["VehicalType"].ToString();
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return vehicalModel;
        }

        public void rejectAppointmentRequestold(int id, string res, string url1, string adminurl)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string shortUrl = this.GetShortUrl(url1, false, true);
            Thread.Sleep(1000);
            string shortUrl1 = this.GetShortUrl(adminurl, false, true);
            try
            {
                sortedLists.Add("@id", id);
                sortedLists.Add("@reason", res);
                sortedLists.Add("@Url", shortUrl);
                sortedLists.Add("@Url1", shortUrl1);
                sqlHelper.executeNonQuery("tblAppointementMakereject_v2", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
        }

        public void rejectAppointmentRequestonly(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                sqlHelper.executeNonQuery("tblAppointementMakerejectonly", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
        }

        public void rejectAppointmentRequest(int id, string res, string url1, string adminurl)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string shortUrl = this.GetShortUrl(url1, false, true);
            Thread.Sleep(1000);
            string shortUrl1 = this.GetShortUrl(adminurl, false, true);
            try
            {
                sortedLists.Add("@id", id);
                sortedLists.Add("@reason", res);
                sortedLists.Add("@Url", shortUrl);
                sortedLists.Add("@Url1", shortUrl1);
                sqlHelper.executeNonQuery("tblAppointementMakerejectnew_v2", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
        }

        public string SaveClientSurvey(ClientSurveymodel _model)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            try
            {
                string str = string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat((!string.IsNullOrEmpty(_model.Q1Comment) ? _model.Q1Comment : string.Empty), (!string.IsNullOrEmpty(_model.Q2Comment) ? _model.Q2Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q3Comment) ? _model.Q3Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q4Comment) ? _model.Q4Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q5Comment) ? _model.Q5Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q6Comment) ? _model.Q6Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q7Comment) ? _model.Q7Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q8Comment) ? _model.Q8Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q9Comment) ? _model.Q9Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q10Comment) ? _model.Q10Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q11Comment) ? _model.Q11Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q12Comment) ? _model.Q12Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q13Comment) ? _model.Q13Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q14Comment) ? _model.Q14Comment : string.Empty));
                sortedLists.Add("@Aid", _model.AID);
                sortedLists.Add("@comment", str);
                sortedLists.Add("@Q1", _model.Q1 == "1");
                sortedLists.Add("@Q1c", (!string.IsNullOrEmpty(_model.Q1Comment) ? _model.Q1Comment : string.Empty));
                sortedLists.Add("@payment", "0");
                sortedLists.Add("@rating", _model.rating);
                sortedLists.Add("@Q2", _model.Q2 == "1");
                sortedLists.Add("@Q2c", (!string.IsNullOrEmpty(_model.Q2Comment) ? _model.Q2Comment : string.Empty));
                sortedLists.Add("@Q3", _model.Q3 == "1");
                sortedLists.Add("@Q3c", (!string.IsNullOrEmpty(_model.Q3Comment) ? _model.Q3Comment : string.Empty));
                sortedLists.Add("@Q4", _model.Q4 == "1");
                sortedLists.Add("@Q4c", (!string.IsNullOrEmpty(_model.Q4Comment) ? _model.Q4Comment : string.Empty));
                sortedLists.Add("@Q5", _model.Q5 == "1");
                sortedLists.Add("@Q5c", (!string.IsNullOrEmpty(_model.Q5Comment) ? _model.Q5Comment : string.Empty));
                sortedLists.Add("@Q6", _model.Q6 == "1");
                sortedLists.Add("@Q6c", (!string.IsNullOrEmpty(_model.Q6Comment) ? _model.Q6Comment : string.Empty));
                sortedLists.Add("@Q7", _model.Q7 == "1");
                sortedLists.Add("@Q7c", (!string.IsNullOrEmpty(_model.Q7Comment) ? _model.Q7Comment : string.Empty));
                sortedLists.Add("@Q8", _model.Q8 == "1");
                sortedLists.Add("@Q8c", (!string.IsNullOrEmpty(_model.Q8Comment) ? _model.Q8Comment : string.Empty));
                sortedLists.Add("@Q9", false);
                sortedLists.Add("@Q9c", (!string.IsNullOrEmpty(_model.Q9Comment) ? _model.Q9Comment : string.Empty));
                sortedLists.Add("@Q10", false);
                sortedLists.Add("@Q10c", (!string.IsNullOrEmpty(_model.Q10Comment) ? _model.Q10Comment : string.Empty));
                sortedLists.Add("@Q11", false);
                sortedLists.Add("@Q11c", (!string.IsNullOrEmpty(_model.Q11Comment) ? _model.Q11Comment : string.Empty));
                sortedLists.Add("@Q12", false);
                sortedLists.Add("@Q12c", (!string.IsNullOrEmpty(_model.Q12Comment) ? _model.Q12Comment : string.Empty));
                sortedLists.Add("@Q13", false);
                sortedLists.Add("@Q13c", (!string.IsNullOrEmpty(_model.Q13Comment) ? _model.Q13Comment : string.Empty));
                sortedLists.Add("@Q14", false);
                sortedLists.Add("@Q14c", (!string.IsNullOrEmpty(_model.Q14Comment) ? _model.Q14Comment : string.Empty));
                empty = sqlHelper.executeNonQueryWMessage("AddCLientSurvey", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return empty;
        }

        public string SaveEndMassageNotification(int id)
        {
            string str = "";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                int num = Convert.ToInt32(sqlHelper.executeScaler(string.Concat("Select Clientid From tblappointement WITH(NOLOCK) WHERE AID=", id)).ToString());
                int num1 = Convert.ToInt32(sqlHelper.executeScaler(string.Concat("Select TherapistId From tblappointement WITH(NOLOCK) WHERE AID=", id)).ToString());
                string shortUrl = this.GetShortUrl(string.Concat(new object[] { "https://www.californiamassage.in/main/Survey?_xid=", num.ToString(), "&aid=", id }), false);
                string shortUrl1 = this.GetShortUrl(string.Concat(new object[] { "https://www.californiamassage.in/main/ClientSurvey?_xid=", num1.ToString(), "&aid=", id }), false);
                sortedLists.Add("@Aid", id);
                sortedLists.Add("@url", shortUrl);
                sortedLists.Add("@url1", shortUrl1);
                str = sqlHelper.executeNonQueryWMessage("EndMassageNotification_v2", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public void SaveStartMassageNotification(int id, string startTime)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@Aid", id);
                sortedLists.Add("@startTime", startTime);
                sqlHelper.executeNonQuery("StartMassageNotification", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
        }

        public string SaveSurvey(Surveymodel _model)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            try
            {
                string str = (!string.IsNullOrEmpty(_model.Q15Comment) ? _model.Q15Comment : string.Empty);
                sortedLists.Add("@Aid", _model.AID);
                sortedLists.Add("@comment", str);
                sortedLists.Add("@Q1", _model.Q1 == "1");
                sortedLists.Add("@Q1c", (!string.IsNullOrEmpty(_model.Q1Comment) ? _model.Q1Comment : string.Empty));
                sortedLists.Add("@payment", (!_model.Pay.Contains("Rs") ? string.Concat(_model.Pay, " Rs") : _model.Pay));
                sortedLists.Add("@rating", _model.rating);
                sortedLists.Add("@Q2", _model.Q2 == "1");
                sortedLists.Add("@Q2c", (!string.IsNullOrEmpty(_model.Q2Comment) ? _model.Q2Comment : string.Empty));
                sortedLists.Add("@Q3", _model.Q3 == "1");
                sortedLists.Add("@Q3c", (!string.IsNullOrEmpty(_model.Q3Comment) ? _model.Q3Comment : string.Empty));
                sortedLists.Add("@Q4", _model.Q4 == "1");
                sortedLists.Add("@Q4c", (!string.IsNullOrEmpty(_model.Q4Comment) ? _model.Q4Comment : string.Empty));
                sortedLists.Add("@Q5", _model.Q5 == "1");
                sortedLists.Add("@Q5c", (!string.IsNullOrEmpty(_model.Q5Comment) ? _model.Q5Comment : string.Empty));
                sortedLists.Add("@Q6", _model.Q6 == "1");
                sortedLists.Add("@Q6c", (!string.IsNullOrEmpty(_model.Q6Comment) ? _model.Q6Comment : string.Empty));
                sortedLists.Add("@Q7", _model.Q7 == "1");
                sortedLists.Add("@Q7c", (!string.IsNullOrEmpty(_model.Q7Comment) ? _model.Q7Comment : string.Empty));
                sortedLists.Add("@Q8", _model.Q8 == "1");
                sortedLists.Add("@Q8c", (!string.IsNullOrEmpty(_model.Q8Comment) ? _model.Q8Comment : string.Empty));
                sortedLists.Add("@Q9", _model.Q9 == "1");
                sortedLists.Add("@Q9c", (!string.IsNullOrEmpty(_model.Q9Comment) ? _model.Q9Comment : string.Empty));
                sortedLists.Add("@Q10", _model.Q10 == "1");
                sortedLists.Add("@Q10c", (!string.IsNullOrEmpty(_model.Q10Comment) ? _model.Q10Comment : string.Empty));
                sortedLists.Add("@Q11", _model.Q11 == "1");
                sortedLists.Add("@Q11c", (!string.IsNullOrEmpty(_model.Q11Comment) ? _model.Q11Comment : string.Empty));
                sortedLists.Add("@Q12", _model.Q12 == "1");
                sortedLists.Add("@Q12c", (!string.IsNullOrEmpty(_model.Q12Comment) ? _model.Q12Comment : string.Empty));
                sortedLists.Add("@Q13", _model.Q13 == "1");
                sortedLists.Add("@Q13c", (!string.IsNullOrEmpty(_model.Q13Comment) ? _model.Q13Comment : string.Empty));
                sortedLists.Add("@Q14", _model.Q14 == "1");
                sortedLists.Add("@Q14c", (!string.IsNullOrEmpty(_model.Q14Comment) ? _model.Q14Comment : string.Empty));
                empty = sqlHelper.executeNonQueryWMessage("AddSurvey", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return empty;
        }
        public AppointmentModel_item GetAppointMentDetailByIdforwhatsapp(int RequestId, bool isFormatRequired = true)
        {
            AppointmentModel_item appointmentModelItem = new AppointmentModel_item();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            utility _utility = new utility();
            Crypto crypto = new Crypto();
            DateTime indianTimeFromPST = new DateTime();
            HotelServices hotelService = new HotelServices();
            try
            {
                sortedLists.Add("@Aid", RequestId);
                DataTable dataTable = sqlHelper.fillDataTable("GetAppointmentDetails_v2", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    appointmentModelItem.therapistName = dataTable.Rows[0]["Name"].ToString();
                    appointmentModelItem.ClientName = dataTable.Rows[0]["CName"].ToString();
                    appointmentModelItem.locationurl = dataTable.Rows[0]["locationurl"].ToString();
                    DateTime dateTime = Convert.ToDateTime(dataTable.Rows[0]["BookingDate"]);
                    appointmentModelItem.startDate = dateTime.ToString("dd-MMM-y");
                    indianTimeFromPST = this.GetIndianTimeFromPST(Convert.ToDateTime(dataTable.Rows[0]["createdDate"]));
                    appointmentModelItem.createdDate = (isFormatRequired ? Convert.ToDateTime(indianTimeFromPST).ToString("dd-MMM-y") : indianTimeFromPST.ToString());
                    appointmentModelItem.ltd = dataTable.Rows[0]["ltd"].ToString();
                    appointmentModelItem.thid = Convert.ToInt32(dataTable.Rows[0]["tid"].ToString());
                    appointmentModelItem.lth = dataTable.Rows[0]["lth"].ToString();
                    appointmentModelItem.therapistTime = dataTable.Rows[0]["confirmedTime"].ToString();
                    appointmentModelItem.arrivalTime = dataTable.Rows[0]["LArrivalTime"].ToString();
                    appointmentModelItem.startTime = dataTable.Rows[0]["EArrivalTime"].ToString();
                    int num = Convert.ToInt32(dataTable.Rows[0]["SpaHotelId"].ToString());
                    appointmentModelItem.SpaHotelid = num;
                    appointmentModelItem.address = (num == 0 ? dataTable.Rows[0]["location"].ToString() : dataTable.Rows[0]["Address"].ToString());

                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                appointmentModelItem.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return appointmentModelItem;
        }
        public string getsession(int therapistid, ref bool allow)
        {
            string str = "";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            DateTime msgstarttime = DateTime.Now;
            try
            {
                XmlHelper xmlHelper = new XmlHelper();
                sortedLists.Add("@tid", therapistid);
                string str1 = "";
                allow = xmlHelper.allowTocommunicate(therapistid, ref msgstarttime);
                if (allow)
                {
                    var dt = sqlHelper.fillDataTable("getSession_v2", "", sortedLists);
                    if (dt.Rows.Count > 0)
                    {
                        DateTime starttime = DateTime.Now;
                        DateTime endtime = DateTime.Now;
                        DateTime currentTimeByTimeZone = DateTime.Now.AddMinutes(810);
                        str1 = string.Concat(new object[] { currentTimeByTimeZone.Month, "/", currentTimeByTimeZone.Day, "/", currentTimeByTimeZone.Year });
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            starttime = Convert.ToDateTime(string.Concat(str1, " ", dt.Rows[i]["confirmedtime"].ToString()));
                            DateTime _currentdatetime = DateTime.Now.AddMinutes(810);
                            endtime = starttime.AddHours(2.5);
                            if (_currentdatetime >= starttime && _currentdatetime <= endtime)
                            {
                                str = endtime.ToString("dd-MMM-y ; hh:mm tt");
                                break;
                            }
                        }
                        // 
                    }
                    dt.Dispose();
                }
                else
                {
                    str = msgstarttime.AddDays(1).AddHours(1).ToString("dd-MMM-y ; hh:mm tt");
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;

        }

        public string getapptdetails(int aid, ref int therapistid)
        {
            string str = @"<br/> Date Request Sent:@date <br/>";
            str = str + " Time Request Sent:@time <br/>THIS IS NOT APPT TIME<br/> *************************** <br/>";
            str = str + " Client Name:@cname <br/>";
            str = str + " Therapist Name:@tname <br/>";
            str = str + " Appt Date:@adate <br/>";
            str = str + " Appt time:@atime <br/>";
            try
            {
                var dt = GetAppointMentDetailByIdforwhatsapp(aid, false);
                var date = Convert.ToDateTime(dt.createdDate);
                therapistid = dt.thid;
                str = str.Replace("@atime", dt.arrivalTime).Replace("@adate", dt.startDate).Replace("@tname", dt.therapistName).Replace("@cname", dt.ClientName).Replace("@date", date.ToString("dd-MMM-y")).Replace("@time", date.ToString("hh:mm tt"));
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {

            }
            return str;
        }
        public void SendAppointmentRequest(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                int tid = 0;
                var message = getapptdetails(id, ref tid);
                bool allow = false;
                var session = getsession(tid, ref allow);
                sortedLists.Add("@id", id);
                sortedLists.Add("@message", message);
                if (!string.IsNullOrEmpty(session))
                {
                    sortedLists.Add("@allow", allow ? 1 : 0);
                    sortedLists.Add("@sessiontime", session);
                }
                sqlHelper.executeNonQuery("tblAppointementMakeActive_v2", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
        }
        //public void SendAppointmentRequest(int id)
        //{
        //    SqlHelper sqlHelper = new SqlHelper();
        //    SortedList sortedLists = new SortedList();
        //    try
        //    {

        //        sortedLists.Add("@id", id);

        //        sqlHelper.executeNonQuery("tblAppointementMakeActive", "", sortedLists);
        //    }
        //    catch (Exception exception)
        //    {
        //        (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
        //    }
        //    finally
        //    {
        //        sqlHelper.Dispose();
        //    }
        //}

        public void SendCabNotification(int id, string leftCabTime)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            int num = (new Random(400)).Next(4000) + id;
            string shortUrl = this.GetShortUrl(string.Concat("https://www.californiamassage.in/TAccount/Notification?token=", num.ToString()), false);
            try
            {
                sortedLists.Add("@Aid", id);
                sortedLists.Add("@leftCabTime", leftCabTime);
                sortedLists.Add("@NotifyUrl", shortUrl);
                sqlHelper.executeNonQuery("cableftNotification_v2", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
        }

        public void setNotification(int id, string url)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@therapistId", id);
                if (Convert.ToInt32(sqlHelper.executeNonQueryWMessage("SendlocationNotification", "", sortedLists).ToString()) > 0)
                {
                    url = this.GetShortUrl(url, false);
                    sortedLists.Add("@url", url);
                    sqlHelper.executeNonQuery("UpdateLocationNotification_v2", "", sortedLists);
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
        }

        public string UpdateClientSurvey(ClientSurveymodel _model, int serveyId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            try
            {
                string str = string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat(string.Concat((!string.IsNullOrEmpty(_model.Q1Comment) ? _model.Q1Comment : string.Empty), (!string.IsNullOrEmpty(_model.Q2Comment) ? _model.Q2Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q3Comment) ? _model.Q3Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q4Comment) ? _model.Q4Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q5Comment) ? _model.Q5Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q6Comment) ? _model.Q6Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q7Comment) ? _model.Q7Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q8Comment) ? _model.Q8Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q9Comment) ? _model.Q9Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q10Comment) ? _model.Q10Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q11Comment) ? _model.Q11Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q12Comment) ? _model.Q12Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q13Comment) ? _model.Q13Comment : string.Empty)), (!string.IsNullOrEmpty(_model.Q14Comment) ? _model.Q14Comment : string.Empty));
                sortedLists.Add("@id", serveyId);
                sortedLists.Add("@comment", str);
                sortedLists.Add("@Q1", _model.Q1 == "1");
                sortedLists.Add("@Q1c", (!string.IsNullOrEmpty(_model.Q1Comment) ? _model.Q1Comment : string.Empty));
                sortedLists.Add("@payment", " Rs 0");
                sortedLists.Add("@rating", _model.rating);
                sortedLists.Add("@Q2", _model.Q2 == "1");
                sortedLists.Add("@Q2c", (!string.IsNullOrEmpty(_model.Q2Comment) ? _model.Q2Comment : string.Empty));
                sortedLists.Add("@Q3", _model.Q3 == "1");
                sortedLists.Add("@Q3c", (!string.IsNullOrEmpty(_model.Q3Comment) ? _model.Q3Comment : string.Empty));
                sortedLists.Add("@Q4", _model.Q4 == "1");
                sortedLists.Add("@Q4c", (!string.IsNullOrEmpty(_model.Q4Comment) ? _model.Q4Comment : string.Empty));
                sortedLists.Add("@Q5", _model.Q5 == "1");
                sortedLists.Add("@Q5c", (!string.IsNullOrEmpty(_model.Q5Comment) ? _model.Q5Comment : string.Empty));
                sortedLists.Add("@Q6", _model.Q6 == "1");
                sortedLists.Add("@Q6c", (!string.IsNullOrEmpty(_model.Q6Comment) ? _model.Q6Comment : string.Empty));
                sortedLists.Add("@Q7", _model.Q7 == "1");
                sortedLists.Add("@Q7c", (!string.IsNullOrEmpty(_model.Q7Comment) ? _model.Q7Comment : string.Empty));
                sortedLists.Add("@Q8", _model.Q8 == "1");
                sortedLists.Add("@Q8c", (!string.IsNullOrEmpty(_model.Q8Comment) ? _model.Q8Comment : string.Empty));
                sortedLists.Add("@Q9", false);
                sortedLists.Add("@Q9c", (!string.IsNullOrEmpty(_model.Q9Comment) ? _model.Q9Comment : string.Empty));
                sortedLists.Add("@Q10", false);
                sortedLists.Add("@Q10c", (!string.IsNullOrEmpty(_model.Q10Comment) ? _model.Q10Comment : string.Empty));
                sortedLists.Add("@Q11", false);
                sortedLists.Add("@Q11c", (!string.IsNullOrEmpty(_model.Q11Comment) ? _model.Q11Comment : string.Empty));
                sortedLists.Add("@Q12", false);
                sortedLists.Add("@Q12c", (!string.IsNullOrEmpty(_model.Q12Comment) ? _model.Q12Comment : string.Empty));
                sortedLists.Add("@Q13", false);
                sortedLists.Add("@Q13c", (!string.IsNullOrEmpty(_model.Q13Comment) ? _model.Q13Comment : string.Empty));
                sortedLists.Add("@Q14", false);
                sortedLists.Add("@Q14c", (!string.IsNullOrEmpty(_model.Q14Comment) ? _model.Q14Comment : string.Empty));
                sqlHelper.executeNonQuery("UpdateClientSurvey", "", sortedLists);
                empty = "Survey Done";
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return empty;
        }

        //public string UpdateConfirmedSTatusByAID(int Aid, string landmark = "", string lth = "", string ltd = "", string time = "", string pickupTime = "")
        //{
        //    string str = "Appointment Is Confirmed, You can start Chat Now";
        //    SqlHelper sqlHelper = new SqlHelper();
        //    SortedList sortedLists = new SortedList();
        //    utility _utility = new utility();
        //    Crypto crypto = new Crypto();
        //    string str1 = string.Concat("https://www.californiamassage.in/Admin/Getrequest?token=", crypto.EncryptStringAES(Aid.ToString()), "&name=C");
        //    int num = (new Random(400)).Next(4000) + Aid;
        //    string str2 = sqlHelper.executeScaler(string.Concat("select clientId  from tblappointement with(nolock) where Aid=", Aid)).ToString();
        //    string shortUrl = this.GetShortUrl(string.Concat("https://www.californiamassage.in/Appointment/GetrequestById?_xid=", Aid), false);
        //    string shortUrl1 = this.GetShortUrl(string.Concat("https://www.californiamassage.in/Taccount/confirmed_request?token=", num.ToString(), str2.ToString()), false);
        //    string shortUrl2 = this.GetShortUrl(str1, false);
        //    try
        //    {
        //        sortedLists.Add("@AID", Aid);
        //        sortedLists.Add("@lth", lth);
        //        sortedLists.Add("@ltd", ltd);
        //        sortedLists.Add("@landmark", landmark);
        //        sortedLists.Add("@time", time);
        //        sortedLists.Add("@curl", shortUrl);
        //        sortedLists.Add("@aurl", shortUrl2);
        //        if (!string.IsNullOrEmpty(pickupTime))
        //        {
        //            sortedLists.Add("@pickupTime", pickupTime);
        //        }
        //        sortedLists.Add("@turl", shortUrl1);
        //        str = sqlHelper.executeNonQueryWMessage("UpdateAppointentStatus", "", sortedLists).ToString();
        //    }
        //    catch (Exception exception)
        //    {
        //        (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
        //        str = string.Concat("Error :", exception.Message);
        //    }
        //    finally
        //    {
        //        sqlHelper.Dispose();
        //    }
        //    return str;
        //}
        public string getconfirmdetails(int aid, string message)
        {
            string str = @"<br/>Date Initiated:@date <br/>";
            str = str + "Time Initiated:@time <br/>";
            str = str + "Client Name:@cname <br/>";
            str = str + "THIS IS NOT CONFIRM TIME <br/> THIS IS TIME APPT<br/> REQUEST SENT BY CLIENT <br/>***************<br/>";
            str = str + "Therapist Name:@tname <br/>";
            str = str + "Appt Date:@adate <br/>";
            str = str + "Appt time:@atime <br/>";
            str = str + "Client locality:@clocality <br/>";
            str = str + "link: Click to view Map <br/>@map <br/><br/> Click to login to Web";
            try
            {
                var dt = GetAppointMentDetailByIdforwhatsapp(aid, false);
                var map = GetShortUrltest(dt.SpaHotelid > 0 ? dt.locationurl : "https://www.google.com/maps/place/" + dt.ltd + "," + dt.lth + "/@" + dt.ltd + "," + dt.lth + ",17z");
                var date = Convert.ToDateTime(dt.createdDate);
                str = str.Replace("@map", map).Replace("@clocality", dt.address).Replace("@atime", message).Replace("@adate", dt.startDate).Replace("@tname", dt.therapistName).Replace("@cname", dt.ClientName).Replace("@date", date.ToString("dd-MMM-y")).Replace("@time", date.ToString("hh:mm tt"));
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {

            }
            return str;
        }

        public string UpdateConfirmedSTatusByAID(int Aid, string landmark = "", string lth = "", string ltd = "", string time = "", string pickupTime = "")
        {
            string str = "Appointment Is Confirmed, You can start Chat Now";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            utility _utility = new utility();
            Crypto crypto = new Crypto();
            var message = getconfirmdetails(Aid, time);
            string str1 = string.Concat("https://californiamassage.in/Admin/Getrequest?token=", crypto.EncryptStringAES(Aid.ToString()), "&name=C");
            int num = (new Random(400)).Next(4000) + Aid;
            string str2 = sqlHelper.executeScaler(string.Concat("select clientId  from tblappointement with(nolock) where Aid=", Aid)).ToString();
            string shortUrl = this.GetShortUrl(string.Concat("https://californiamassage.in/Appointment/GetrequestById?_xid=", Aid), false);
            string shortUrl1 = this.GetShortUrl(string.Concat("https://californiamassage.in/Taccount/confirmed_request?token=", num.ToString(), str2.ToString()), false);
            string shortUrl2 = this.GetShortUrl(str1, false);
            try
            {
                sortedLists.Add("@AID", Aid);
                sortedLists.Add("@message", message);
                sortedLists.Add("@lth", lth);
                sortedLists.Add("@ltd", ltd);
                sortedLists.Add("@landmark", landmark);
                sortedLists.Add("@time", time);
                sortedLists.Add("@curl", shortUrl);
                sortedLists.Add("@aurl", shortUrl2);
                if (!string.IsNullOrEmpty(pickupTime))
                {
                    sortedLists.Add("@pickupTime", pickupTime);
                }
                sortedLists.Add("@turl", shortUrl1);
                str = sqlHelper.executeNonQueryWMessage("UpdateAppointentStatus_v2", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public void updateStatusbyThId(int thid, bool show)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                sqlHelper.executeNonQuery("", string.Concat(new object[] { "update tbltherapist set isshow='", show.ToString(), "' where tid=", thid }), null);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
        }

        public string UpdateSurvey(Surveymodel _model, int serveyId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            try
            {
                string str = (!string.IsNullOrEmpty(_model.Q15Comment) ? _model.Q15Comment : string.Empty);
                sortedLists.Add("@id", serveyId);
                sortedLists.Add("@comment", str);
                sortedLists.Add("@Q1", _model.Q1 == "1");
                sortedLists.Add("@Q1c", (!string.IsNullOrEmpty(_model.Q1Comment) ? _model.Q1Comment : string.Empty));
                sortedLists.Add("@payment", (!_model.Pay.Contains("Rs") ? string.Concat(_model.Pay, " Rs") : _model.Pay));
                sortedLists.Add("@rating", _model.rating);
                sortedLists.Add("@Q2", _model.Q2 == "1");
                sortedLists.Add("@Q2c", (!string.IsNullOrEmpty(_model.Q2Comment) ? _model.Q2Comment : string.Empty));
                sortedLists.Add("@Q3", _model.Q3 == "1");
                sortedLists.Add("@Q3c", (!string.IsNullOrEmpty(_model.Q3Comment) ? _model.Q3Comment : string.Empty));
                sortedLists.Add("@Q4", _model.Q4 == "1");
                sortedLists.Add("@Q4c", (!string.IsNullOrEmpty(_model.Q4Comment) ? _model.Q4Comment : string.Empty));
                sortedLists.Add("@Q5", _model.Q5 == "1");
                sortedLists.Add("@Q5c", (!string.IsNullOrEmpty(_model.Q5Comment) ? _model.Q5Comment : string.Empty));
                sortedLists.Add("@Q6", _model.Q6 == "1");
                sortedLists.Add("@Q6c", (!string.IsNullOrEmpty(_model.Q6Comment) ? _model.Q6Comment : string.Empty));
                sortedLists.Add("@Q7", _model.Q7 == "1");
                sortedLists.Add("@Q7c", (!string.IsNullOrEmpty(_model.Q7Comment) ? _model.Q7Comment : string.Empty));
                sortedLists.Add("@Q8", _model.Q8 == "1");
                sortedLists.Add("@Q8c", (!string.IsNullOrEmpty(_model.Q8Comment) ? _model.Q8Comment : string.Empty));
                sortedLists.Add("@Q9", _model.Q9 == "1");
                sortedLists.Add("@Q9c", (!string.IsNullOrEmpty(_model.Q9Comment) ? _model.Q9Comment : string.Empty));
                sortedLists.Add("@Q10", _model.Q10 == "1");
                sortedLists.Add("@Q10c", (!string.IsNullOrEmpty(_model.Q10Comment) ? _model.Q10Comment : string.Empty));
                sortedLists.Add("@Q11", _model.Q11 == "1");
                sortedLists.Add("@Q11c", (!string.IsNullOrEmpty(_model.Q11Comment) ? _model.Q11Comment : string.Empty));
                sortedLists.Add("@Q12", _model.Q12 == "1");
                sortedLists.Add("@Q12c", (!string.IsNullOrEmpty(_model.Q12Comment) ? _model.Q12Comment : string.Empty));
                sortedLists.Add("@Q13", _model.Q13 == "1");
                sortedLists.Add("@Q13c", (!string.IsNullOrEmpty(_model.Q13Comment) ? _model.Q13Comment : string.Empty));
                sortedLists.Add("@Q14", _model.Q14 == "1");
                sortedLists.Add("@Q14c", (!string.IsNullOrEmpty(_model.Q14Comment) ? _model.Q14Comment : string.Empty));
                sqlHelper.executeNonQuery("UpdateSurvey", "", sortedLists);
                empty = "Survey Done";
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return empty;
        }

        public string UpdateTherapistShedule(DateTime confirmDate, string startTime, string endTime, int thid)
        {
            string str = "Appointment Is Confirmed";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@startTime", startTime);
                sortedLists.Add("@endTime", endTime);
                sortedLists.Add("@confirmDate", confirmDate);
                sortedLists.Add("@thid", thid);
                sqlHelper.executeNonQuery("UpdateTherapistShedule", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string UpdateTherapistSheduleByAid(DateTime confirmDate, string startTime, string endTime, int Aid)
        {
            string str = "Appointment Is Confirmed";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@startTime", startTime);
                sortedLists.Add("@endTime", endTime);
                sortedLists.Add("@confirmDate", confirmDate);
                sortedLists.Add("@Aid", Aid);
                sqlHelper.executeNonQuery("UpdateTherapistSheduleByAid", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }
    }
}