﻿using californiaspa.Models.Account;
using californiaspa.Models.user;
using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace californiaspa.DataBase
{
    public class userService
    {
        public bool BulkInsert(DataTable ActionTable, string TableName)
        {
            ConnectionHelper connectionHelper = new ConnectionHelper();
            if (connectionHelper.con.State == ConnectionState.Closed)
            {
                connectionHelper.con.Open();
            }
            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connectionHelper.con))
            {
                sqlBulkCopy.DestinationTableName = TableName;
                sqlBulkCopy.WriteToServer(ActionTable);
            }
            return false;
        }

        public List<permissionitem> getAllPermissions()
        {
            List<permissionitem> permissionitems = new List<permissionitem>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {

                DataTable dataTable = sqlHelper.fillDataTable("GetAllPermission", "", null);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        permissionitem _permissionitem = new permissionitem()
                        {
                            id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString()),
                            name = dataTable.Rows[i]["permission"].ToString(),
                            type = dataTable.Rows[i]["type"].ToString()
                        };
                        permissionitems.Add(_permissionitem);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return permissionitems;
        }

        public List<Roles> getAllRoles()
        {
            List<Roles> roles = new List<Roles>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("GetAllrole", "", null);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        Roles role = new Roles()
                        {
                            roleId = Convert.ToInt32(dataTable.Rows[i]["id"].ToString()),
                            name = dataTable.Rows[i]["RoleName"].ToString()
                        };
                        roles.Add(role);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return roles;
        }

        public List<int> getPermissionByRoleId(int roleId)
        {
            List<int> nums = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {

                sortedLists.Add("@roleId", roleId);
                DataTable dataTable = sqlHelper.fillDataTable("GetAllPermissionbyroleId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        nums.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()));
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return nums;
        }

        public List<int> getPermissionByuserIdd(int userId)
        {
            List<int> nums = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@roleId", userId);
                DataTable dataTable = sqlHelper.fillDataTable("GetAllPermissionbyuserId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        nums.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()));
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return nums;
        }

        public string updatePermissionByRole(int[] _model, int roleId)
        {
            string str = "Role Permission updated Succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            DataTable dataTable = new DataTable("tblrolepermission");
            DataRow dataRow = null;
            dataTable.Columns.Add("id", typeof(int));
            dataTable.Columns.Add("RoleName", typeof(string));
            dataTable.Columns.Add("PermissionId", typeof(int));
            dataTable.Columns.Add("RoleId", typeof(int));
            try
            {
                for (int i = 0; i <= (int)_model.Length - 1; i++)
                {
                    dataRow = dataTable.NewRow();
                    dataRow[0] = 0;
                    dataRow[3] = roleId;
                    dataRow[2] = _model[i];
                    dataTable.Rows.Add(dataRow);
                }
                sqlHelper.executeNonQuery("", string.Concat("Delete from tblrolepermission where RoleId=", roleId), null);
                this.BulkInsert(dataTable, "tblrolepermission");
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }


        public otpmodel getotpmodel(int startindex,int endindex, ref int total, string search = "", int searchtype = -1)
        {
            List<otpnumbers> nums = new List<otpnumbers>();
            otpmodel _model = new Models.Account.otpmodel();
            _model.searchvalue = search;
            _model.searchtype = searchtype;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                if (searchtype != -1 && !string.IsNullOrEmpty(search))
                {
                    if (searchtype == 0)
                    {
                        sortedLists.Add("@mobile", search);
                    }
                    else if (searchtype == 1)
                    {
                        sortedLists.Add("@email", search);
                    }
                }
                total = 0;
                sortedLists.Add("@startindex", startindex);
                sortedLists.Add("@endindex", endindex);
                DataTable dataTable = sqlHelper.fillDataTable("GetOtpsms", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        otpnumbers _itm = new Models.Account.otpnumbers();
                        _itm.body = dataTable.Rows[i]["body"].ToString();
                        _itm.phone = dataTable.Rows[i]["mobilenumber"].ToString();
                        total=Convert.ToInt32(dataTable.Rows[i]["total"].ToString());
                        nums.Add(_itm);
                    }
                    _model.body = nums;
                }
                else
                {
                    _model.ErrorText = "No Match found";
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _model;
        }
    }
}