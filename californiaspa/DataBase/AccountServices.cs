using californiaspa.Models;
using californiaspa.Models.Account;
using californiaspa.Models.Appointment;
using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace californiaspa.DataBase
{
    public class AccountServices
    {

        public string AddNewCustomer(RegisterModel model, string emailVerificationCode, string mobileVerificationCode)
        {
            string empty = string.Empty;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@email", model.email);
                sortedLists.Add("@mobile", model.mobile);
                sortedLists.Add("@ltd", model.ltd);
                sortedLists.Add("@lth", model.lth);
                sortedLists.Add("@Name", model.name);
                sortedLists.Add("@CountryCode", model.CountryCode);
                sortedLists.Add("@hearabout", model.hearAbout);
                sortedLists.Add("@emailVerificationCode", emailVerificationCode);
                sortedLists.Add("@mobileVerificationCode", mobileVerificationCode);
                empty = sqlHelper.executeNonQueryWMessage("tbluserSaveNewUser", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return empty;
        }

        public string AddNewTherapist(TRegisterModel model, string mobileVerificationCode)
        {
            string empty = string.Empty;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@email", model.email);
                sortedLists.Add("@mobile", model.mobile);
                sortedLists.Add("@Name", model.name);
                sortedLists.Add("@mobileVerificationCode", mobileVerificationCode);
                sortedLists.Add("@massageStyle", model.massageStyle);
                sortedLists.Add("@rate", model.rate);
                sortedLists.Add("@languages", model.languages);
                sortedLists.Add("@location", model.location);
                sortedLists.Add("@age", model.age);
                empty = sqlHelper.executeNonQueryWMessage("tblTherapistSaveNewUser", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return empty;
        }

        public bool checkstatus(int id)
        {
            bool flag = false;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                if (id == 0)
                {
                    flag = true;
                }
                else
                {
                    flag = Convert.ToBoolean(sqlHelper.executeScaler("Select isnull(managerverified,'0') as managerverified from tbluser with(NOLOCK) where id=" + id).ToString());
                }
            }
            catch (Exception exception)
            {
                new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return flag;
        }

        public string Deleterequest(int id)
        {
            string str = "Request Deleted Succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                sqlHelper.executeNonQuery("DeleteRequest", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public List<AppointmentModel_item> GetAllAdminRejectedAppointment(int startindex, int endindex, ref int total)
        {
            List<AppointmentModel_item> appointmentModelItems = new List<AppointmentModel_item>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@startindex", startindex);
                sortedLists.Add("@endindex", endindex);
                total = 0;
                DataTable dataTable = sqlHelper.fillDataTable("GetAllRejectedAppointmentByadmin", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        AppointmentModel_item appointmentModelItem = new AppointmentModel_item()
                        {
                            tokenId = crypto.EncryptStringAES(dataTable.Rows[i]["Aid"].ToString()),
                            token = crypto.EncryptStringAES(dataTable.Rows[i]["therapistId"].ToString())
                        };
                        DateTime dateTime = Convert.ToDateTime(dataTable.Rows[i]["BookingDate"].ToString());
                        appointmentModelItem.startDate = dateTime.ToString("dd-MMM-yy");
                        int num = Convert.ToInt32(dataTable.Rows[i]["SpaHotelId"].ToString());
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString());
                        appointmentModelItem.srno = Convert.ToInt32(dataTable.Rows[i]["RowNum"].ToString());
                        appointmentModelItem.address = (num == 0 ? dataTable.Rows[i]["location"].ToString() : dataTable.Rows[i]["Address"].ToString());
                        appointmentModelItem.comments = dataTable.Rows[i]["Comments"].ToString();
                        appointmentModelItem.therapistName = dataTable.Rows[i]["therapistName"].ToString();
                        appointmentModelItem.ClientName = dataTable.Rows[i]["Name"].ToString();
                        appointmentModelItems.Add(appointmentModelItem);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return appointmentModelItems;
        }

        public List<AppointmentModel_item> GetAllRejectedAppointmentByUserId(int userId,int startindex,int endindex,ref int total)
        {
            List<AppointmentModel_item> appointmentModelItems = new List<AppointmentModel_item>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@userId", userId);
                sortedLists.Add("@startindex", startindex);
                sortedLists.Add("@endindex", endindex);
                DataTable dataTable = sqlHelper.fillDataTable("GetAllRejectedAppointmentByUserId", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        AppointmentModel_item appointmentModelItem = new AppointmentModel_item()
                        {
                            tokenId = crypto.EncryptStringAES(dataTable.Rows[i]["Aid"].ToString()),
                            token = crypto.EncryptStringAES(dataTable.Rows[i]["therapistId"].ToString())
                        };
                        DateTime dateTime = Convert.ToDateTime(dataTable.Rows[i]["BookingDate"].ToString());
                        appointmentModelItem.startDate = dateTime.ToString("dd-MMM-yy");
                        int num = Convert.ToInt32(dataTable.Rows[i]["SpaHotelId"].ToString());
                        total = Convert.ToInt32(dataTable.Rows[i]["total"].ToString());
                        appointmentModelItem.srno = Convert.ToInt32(dataTable.Rows[i]["RowNum"].ToString());
                        appointmentModelItem.address = (num == 0 ? dataTable.Rows[i]["location"].ToString() : dataTable.Rows[i]["Address"].ToString());
                        appointmentModelItem.comments = dataTable.Rows[i]["Comments"].ToString();
                        appointmentModelItem.therapistName = dataTable.Rows[i]["therapistName"].ToString();
                        appointmentModelItem.ClientName = dataTable.Rows[i]["Name"].ToString();
                        appointmentModelItems.Add(appointmentModelItem);
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return appointmentModelItems;
        }

        public AccountModel GetCustomer(int UserId, bool edit = false)
        {
            AccountModel accountModel = new AccountModel();
            List<Appontments> appontments = new List<Appontments>();
            List<Appontments> appontments1 = new List<Appontments>();
            List<Reviews> reviews = new List<Reviews>();
            List<Reviews> reviews1 = new List<Reviews>();
            RatingHelper ratingHelper = new RatingHelper();
            SqlHelper sqlHelper = new SqlHelper();
            Crypto crypto = new Crypto();
            SortedList sortedLists = new SortedList();
            int num = 0;
            try
            {
                accountModel.rating = ratingHelper.GetAverageRatingByClientId1(UserId, ref num);
                sortedLists.Add("@userId", UserId);
                DataSet dataSet = sqlHelper.fillDataSet("tbluserdetailGetCustomerById", "", sortedLists);
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    accountModel.Id = UserId;
                    accountModel.usertype = dataSet.Tables[0].Rows[0]["clienttype"].ToString();
                    accountModel.AdminUserName = dataSet.Tables[0].Rows[0]["AdminUserName"].ToString();
                    accountModel.Address = dataSet.Tables[0].Rows[0]["Address"].ToString();
                    accountModel._exid = crypto.EncryptStringAES(UserId.ToString());
                    accountModel.emailAddress = dataSet.Tables[0].Rows[0]["EMail"].ToString();
                    accountModel.mobile = dataSet.Tables[0].Rows[0]["mobile"].ToString();
                    accountModel.userName = dataSet.Tables[0].Rows[0]["UserName"].ToString();
                    accountModel.block = Convert.ToBoolean(dataSet.Tables[0].Rows[0]["IsBlocked"].ToString());
                    accountModel.serviceTaken = Convert.ToBoolean(dataSet.Tables[0].Rows[0]["serviceTaken"].ToString());
                    accountModel.Verifiedbymg = Convert.ToBoolean(dataSet.Tables[0].Rows[0]["managerVerified"].ToString());
                    accountModel.blockResaon = dataSet.Tables[0].Rows[0]["BlockedResaon"].ToString();
                    accountModel.password = dataSet.Tables[0].Rows[0]["password"].ToString();
                    accountModel.Name = dataSet.Tables[0].Rows[0]["Name"].ToString();
                    accountModel.Tharepisttried = dataSet.Tables[0].Rows[0]["Tharepisttried"].ToString();
                    accountModel.MassagesCount = Convert.ToInt32(dataSet.Tables[0].Rows[0]["MassageCount"].ToString());
                    accountModel.counter = Convert.ToInt32(dataSet.Tables[0].Rows[0]["counter"].ToString());
                    accountModel.lth = dataSet.Tables[0].Rows[0]["lth"].ToString();
                    accountModel.ltd = dataSet.Tables[0].Rows[0]["ltd"].ToString();
                    accountModel.locationtypeId = Convert.ToInt32(dataSet.Tables[0].Rows[0]["locationtypeId"].ToString());
                    string str = dataSet.Tables[0].Rows[0]["hoteladdress"].ToString();
                    if (accountModel.locationtypeId == 4)
                    {
                        accountModel.Address = "";
                        accountModel.spahotelAddress = (str.Contains<char>('$') ? str.Split(new char[] { '$' })[1] : "");
                        accountModel.spahotelname = (str.Contains<char>('$') ? str.Split(new char[] { '$' })[0] : "");
                        accountModel.Map = (str.Contains<char>('$') ? str.Split(new char[] { '$' })[2] : "");
                    }
                    accountModel.SpaHotelId = Convert.ToInt32(dataSet.Tables[0].Rows[0]["spahotelid"].ToString());
                    if (!string.IsNullOrEmpty(accountModel.Address) && accountModel.Address.Contains("%"))
                    {
                        string[] strArrays = accountModel.Address.Split(new char[] { '%' });
                        accountModel.houseNumber = strArrays[0].ToString();
                        accountModel.aptNumber = strArrays[1].ToString();
                        accountModel.hotelName = strArrays[2].ToString();
                        accountModel.floorNumber = strArrays[3].ToString();
                        accountModel.streetName = strArrays[4].ToString();
                        accountModel.crossstreetName = strArrays[5].ToString();
                        accountModel.landmark = strArrays[6].ToString();
                        accountModel.localityArea = strArrays[7].ToString();
                        accountModel.roomNumber = strArrays[8].ToString();
                        accountModel.aptName = strArrays[9].ToString();
                        accountModel.lth = (!string.IsNullOrEmpty(strArrays[11].ToString()) ? strArrays[11].ToString() : dataSet.Tables[0].Rows[0]["lth"].ToString());
                        accountModel.ltd = (!string.IsNullOrEmpty(strArrays[10].ToString()) ? strArrays[10].ToString() : dataSet.Tables[0].Rows[0]["ltd"].ToString());
                        if (edit)
                        {
                            accountModel.Address = string.Concat(new string[] { strArrays[0].ToString(), " ", strArrays[1].ToString(), " ", strArrays[2].ToString(), " ", strArrays[3].ToString(), " ", strArrays[4].ToString(), " ", strArrays[5].ToString(), " ", strArrays[6].ToString(), " ", strArrays[7].ToString(), " ", strArrays[8].ToString(), " ", strArrays[9].ToString() });
                        }
                    }
                }
                if (dataSet.Tables[4].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[4].Rows.Count; i++)
                    {
                        reviews.Add(new Reviews()
                        {
                            Name = dataSet.Tables[4].Rows[i]["Name"].ToString(),
                            tokenId = crypto.EncryptStringAES(dataSet.Tables[4].Rows[i]["Id"].ToString()),
                            Review = dataSet.Tables[4].Rows[i]["Comments"].ToString(),
                            rating = Convert.ToInt32(dataSet.Tables[4].Rows[i]["rating"].ToString())
                        });
                    }
                }
                if (dataSet.Tables[5].Rows.Count > 0)
                {
                    for (int j = 0; j < dataSet.Tables[5].Rows.Count; j++)
                    {
                        reviews1.Add(new Reviews()
                        {
                            Name = dataSet.Tables[5].Rows[j]["Name"].ToString(),
                            tokenId = crypto.EncryptStringAES(dataSet.Tables[5].Rows[j]["Id"].ToString()),
                            Review = dataSet.Tables[5].Rows[j]["Comments"].ToString(),
                            rating = Convert.ToInt32(dataSet.Tables[5].Rows[j]["rating"].ToString())
                        });
                    }
                }
                accountModel.Reviews = reviews;
                if (dataSet.Tables[3].Rows.Count > 0)
                {
                    string[] strArrays1 = new string[100];
                    for (int k = 0; k < dataSet.Tables[3].Rows.Count; k++)
                    {
                        strArrays1[k] = string.Concat(new string[] { " ", dataSet.Tables[3].Rows[k]["name"].ToString(), "(", dataSet.Tables[3].Rows[k]["MassageCount"].ToString(), ")" });
                    }
                    accountModel.Tharepisttried = string.Join(",",
                        from s in strArrays1
                        where !string.IsNullOrEmpty(s)
                        select s);
                }
                if (dataSet.Tables[1].Rows.Count > 0)
                {
                    for (int l = 0; l < dataSet.Tables[1].Rows.Count; l++)
                    {
                        Appontments appontment = new Appontments()
                        {
                            address = dataSet.Tables[1].Rows[l]["Address"].ToString()
                        };
                        if (!string.IsNullOrEmpty(dataSet.Tables[1].Rows[l]["discountedRate"].ToString()))
                        {
                            appontment.amount = (Convert.ToDecimal(dataSet.Tables[1].Rows[l]["discountedRate"].ToString()) < decimal.One ? Convert.ToDecimal(dataSet.Tables[1].Rows[l]["rate"].ToString()) : Convert.ToDecimal(dataSet.Tables[1].Rows[l]["discountedRate"].ToString()));
                        }
                        else
                        {
                            appontment.amount = Convert.ToDecimal(dataSet.Tables[1].Rows[l]["rate"].ToString());
                        }
                        appontment.date = dataSet.Tables[1].Rows[l]["BookingDate"].ToString();
                        appontment.Id = Convert.ToInt32(dataSet.Tables[1].Rows[l]["AID"].ToString());
                        appontment.token = crypto.EncryptStringAES(dataSet.Tables[1].Rows[l]["AID"].ToString());
                        appontment.tokenId = crypto.EncryptStringAES(dataSet.Tables[1].Rows[l]["therapistId"].ToString());
                        appontment.Name = dataSet.Tables[1].Rows[l]["name"].ToString();
                        appontment.comments = dataSet.Tables[1].Rows[l]["comments"].ToString();
                        appontment.time = dataSet.Tables[1].Rows[l]["LArrivalTime"].ToString();
                        appontments.Add(appontment);
                    }
                }
                if (dataSet.Tables[2].Rows.Count > 0)
                {
                    for (int m = 0; m < dataSet.Tables[2].Rows.Count; m++)
                    {
                        Appontments appontment1 = new Appontments()
                        {
                            address = dataSet.Tables[2].Rows[m]["Address"].ToString()
                        };
                        if (!string.IsNullOrEmpty(dataSet.Tables[2].Rows[m]["discountedRate"].ToString()))
                        {
                            appontment1.amount = (Convert.ToDecimal(dataSet.Tables[2].Rows[m]["discountedRate"].ToString()) < decimal.One ? Convert.ToDecimal(dataSet.Tables[2].Rows[m]["rate"].ToString()) : Convert.ToDecimal(dataSet.Tables[2].Rows[m]["discountedRate"].ToString()));
                        }
                        else
                        {
                            appontment1.amount = Convert.ToDecimal(dataSet.Tables[2].Rows[m]["rate"].ToString());
                        }
                        appontment1.date = dataSet.Tables[2].Rows[m]["BookingDate"].ToString();
                        appontment1.Id = Convert.ToInt32(dataSet.Tables[2].Rows[m]["AID"].ToString());
                        appontment1.Name = dataSet.Tables[2].Rows[m]["name"].ToString();
                        appontment1.time = dataSet.Tables[2].Rows[m]["confirmedTime"].ToString();
                        appontment1.token = crypto.EncryptStringAES(dataSet.Tables[2].Rows[m]["AID"].ToString());
                        appontment1.tltd = dataSet.Tables[2].Rows[m]["tltd"].ToString();
                        appontment1.tlth = dataSet.Tables[2].Rows[m]["tlth"].ToString();
                        appontment1.comments = dataSet.Tables[2].Rows[m]["comments"].ToString();
                        appontments1.Add(appontment1);
                    }
                }
                accountModel.pendingAppointment = appontments;
                accountModel.appointmentList = appontments1;
                accountModel.ClientReviews = reviews1;
                dataSet.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                accountModel.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            accountModel.photoidurl = this.GetPhotoIdByUserId(UserId);
            return accountModel;
        }

        public AccountModel GetCustomeraccount(int UserId, bool edit = false)
        {
            AccountModel accountModel = new AccountModel();
            SqlHelper sqlHelper = new SqlHelper();
            Crypto crypto = new Crypto();
            SortedList sortedLists = new SortedList();
            RatingHelper ratingHelper = new RatingHelper();
            int num = 0;
            try
            {
                accountModel.rating = ratingHelper.GetAverageRatingByClientId1(UserId, ref num);
                sortedLists.Add("@userId", UserId);
                DataSet dataSet = sqlHelper.fillDataSet("GetaccountCustomerById", "", sortedLists);
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    accountModel.Id = UserId;
                    accountModel.clientId = UserId;
                    accountModel.AdminUserName = dataSet.Tables[0].Rows[0]["AdminUserName"].ToString();
                    accountModel.userName = dataSet.Tables[0].Rows[0]["UserName"].ToString();
                    accountModel.block = Convert.ToBoolean(dataSet.Tables[0].Rows[0]["IsBlocked"].ToString());
                    accountModel.emailAddress = dataSet.Tables[0].Rows[0]["EMail"].ToString();
                    accountModel.password = dataSet.Tables[0].Rows[0]["password"].ToString();
                    accountModel.mobile = dataSet.Tables[0].Rows[0]["Mobile"].ToString();
                    accountModel.MassagesCount = Convert.ToInt32(dataSet.Tables[0].Rows[0]["MassageCount"].ToString());
                    accountModel.CountryCode = dataSet.Tables[0].Rows[0]["CountryCode"].ToString();

                }
                if (dataSet.Tables[1].Rows.Count > 0)
                {
                    string[] strArrays1 = new string[100];
                    for (int k = 0; k < dataSet.Tables[1].Rows.Count; k++)
                    {
                        strArrays1[k] = string.Concat(new string[] { " ", dataSet.Tables[1].Rows[k]["name"].ToString(), "(", dataSet.Tables[1].Rows[k]["MassageCount"].ToString(), ")" });
                    }
                    accountModel.Tharepisttried = string.Join(",",
                        from s in strArrays1
                        where !string.IsNullOrEmpty(s)
                        select s);
                }
                dataSet.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                accountModel.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            accountModel.photoidurl = this.GetPhotoIdByUserId(UserId);
            return accountModel;
        }

        public AccountModel GetCustomerpendingappt(int UserId, int startindex, int endindex, ref int total)
        {
            total = 0;
            AccountModel accountModel = new AccountModel();
            List<Appontments> appontments = new List<Appontments>();
            SqlHelper sqlHelper = new SqlHelper();
            Crypto crypto = new Crypto();
            SortedList sortedLists = new SortedList();
            int num = 0;
            try
            {
                sortedLists.Add("@userId", UserId);
                sortedLists.Add("@startindex", startindex);
                sortedLists.Add("@endindex", endindex);
                DataSet dataSet = sqlHelper.fillDataSet("GetpendingapptCustomerById", "", sortedLists);
                if (dataSet.Tables[1].Rows.Count > 0)
                {
                    for (int l = 0; l < dataSet.Tables[1].Rows.Count; l++)
                    {
                        Appontments appontment = new Appontments()
                        {
                            address = dataSet.Tables[1].Rows[l]["Address"].ToString()
                        };
                        if (!string.IsNullOrEmpty(dataSet.Tables[1].Rows[l]["discountedRate"].ToString()))
                        {
                            appontment.amount = (Convert.ToDecimal(dataSet.Tables[1].Rows[l]["discountedRate"].ToString()) < decimal.One ? Convert.ToDecimal(dataSet.Tables[1].Rows[l]["rate"].ToString()) : Convert.ToDecimal(dataSet.Tables[1].Rows[l]["discountedRate"].ToString()));
                        }
                        else
                        {
                            appontment.amount = Convert.ToDecimal(dataSet.Tables[1].Rows[l]["rate"].ToString());
                        }
                        appontment.date = dataSet.Tables[1].Rows[l]["BookingDate"].ToString();
                        appontment.Id = Convert.ToInt32(dataSet.Tables[1].Rows[l]["AID"].ToString());
                        appontment.srno = Convert.ToInt32(dataSet.Tables[1].Rows[l]["RowNum"].ToString());
                        total = Convert.ToInt32(dataSet.Tables[1].Rows[l]["total"].ToString());
                        appontment.token = crypto.EncryptStringAES(dataSet.Tables[1].Rows[l]["AID"].ToString());
                        appontment.tokenId = crypto.EncryptStringAES(dataSet.Tables[1].Rows[l]["therapistId"].ToString());
                        appontment.Name = dataSet.Tables[1].Rows[l]["name"].ToString();
                        appontment.comments = dataSet.Tables[1].Rows[l]["comments"].ToString();
                        appontment.time = dataSet.Tables[1].Rows[l]["LArrivalTime"].ToString();
                        appontments.Add(appontment);
                    }
                }
                accountModel.pendingAppointment = appontments;
                dataSet.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                accountModel.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return accountModel;
        }

        public AccountModel GetCustomerpendingapprovalappt(int UserId, int startindex, int endindex, ref int total)
        {
            total = 0;
            AccountModel accountModel = new AccountModel();
            List<Appontments> appontments = new List<Appontments>();
            SqlHelper sqlHelper = new SqlHelper();
            Crypto crypto = new Crypto();
            SortedList sortedLists = new SortedList();
            int num = 0;
            try
            {
                sortedLists.Add("@userId", UserId);
                sortedLists.Add("@startindex", startindex);
                sortedLists.Add("@endindex", endindex);
                DataSet dataSet = sqlHelper.fillDataSet("GetpendingapptapprovalCustomerById", "", sortedLists);
                if (dataSet.Tables[1].Rows.Count > 0)
                {
                    for (int l = 0; l < dataSet.Tables[1].Rows.Count; l++)
                    {
                        Appontments appontment = new Appontments()
                        {
                            address = dataSet.Tables[1].Rows[l]["Address"].ToString()
                        };
                        if (!string.IsNullOrEmpty(dataSet.Tables[1].Rows[l]["discountedRate"].ToString()))
                        {
                            appontment.amount = (Convert.ToDecimal(dataSet.Tables[1].Rows[l]["discountedRate"].ToString()) < decimal.One ? Convert.ToDecimal(dataSet.Tables[1].Rows[l]["rate"].ToString()) : Convert.ToDecimal(dataSet.Tables[1].Rows[l]["discountedRate"].ToString()));
                        }
                        else
                        {
                            appontment.amount = Convert.ToDecimal(dataSet.Tables[1].Rows[l]["rate"].ToString());
                        }
                        appontment.date = dataSet.Tables[1].Rows[l]["BookingDate"].ToString();
                        appontment.Id = Convert.ToInt32(dataSet.Tables[1].Rows[l]["AID"].ToString());
                        appontment.srno = Convert.ToInt32(dataSet.Tables[1].Rows[l]["RowNum"].ToString());
                        total = Convert.ToInt32(dataSet.Tables[1].Rows[l]["total"].ToString());
                        appontment.token = crypto.EncryptStringAES(dataSet.Tables[1].Rows[l]["AID"].ToString());
                        appontment.tokenId = crypto.EncryptStringAES(dataSet.Tables[1].Rows[l]["therapistId"].ToString());
                        appontment.Name = dataSet.Tables[1].Rows[l]["name"].ToString();
                        appontment.uname = dataSet.Tables[1].Rows[l]["uname"].ToString();
                        appontment.comments = dataSet.Tables[1].Rows[l]["comments"].ToString();
                        appontment.time = dataSet.Tables[1].Rows[l]["LArrivalTime"].ToString();
                        appontments.Add(appontment);
                    }
                }
                accountModel.pendingAppointment = appontments;
                dataSet.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                accountModel.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return accountModel;
        }


        public AccountModel GetCustomerconfirmappt(int UserId, int startindex, int endindex, ref int total)
        {
            AccountModel accountModel = new AccountModel();
            total = 0;
            List<Appontments> appontments1 = new List<Appontments>();
            SqlHelper sqlHelper = new SqlHelper();
            Crypto crypto = new Crypto();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@userId", UserId);
                sortedLists.Add("@startindex", startindex);
                sortedLists.Add("@endindex", endindex);
                DataSet dataSet = sqlHelper.fillDataSet("GetconfirmapptCustomerById", "", sortedLists);
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    for (int m = 0; m < dataSet.Tables[0].Rows.Count; m++)
                    {
                        Appontments appontment1 = new Appontments()
                        {
                            address = dataSet.Tables[0].Rows[m]["Address"].ToString()
                        };
                        if (!string.IsNullOrEmpty(dataSet.Tables[0].Rows[m]["discountedRate"].ToString()))
                        {
                            appontment1.amount = (Convert.ToDecimal(dataSet.Tables[0].Rows[m]["discountedRate"].ToString()) < decimal.One ? Convert.ToDecimal(dataSet.Tables[0].Rows[m]["rate"].ToString()) : Convert.ToDecimal(dataSet.Tables[0].Rows[m]["discountedRate"].ToString()));
                        }
                        else
                        {
                            appontment1.amount = Convert.ToDecimal(dataSet.Tables[0].Rows[m]["rate"].ToString());
                        }
                        appontment1.date = dataSet.Tables[0].Rows[m]["BookingDate"].ToString();
                        appontment1.Id = Convert.ToInt32(dataSet.Tables[0].Rows[m]["AID"].ToString());
                        appontment1.srno = Convert.ToInt32(dataSet.Tables[0].Rows[m]["RowNum"].ToString());
                        total = Convert.ToInt32(dataSet.Tables[0].Rows[m]["total"].ToString());
                        appontment1.Name = dataSet.Tables[0].Rows[m]["name"].ToString();
                        appontment1.time = dataSet.Tables[0].Rows[m]["confirmedTime"].ToString();
                        appontment1.token = crypto.EncryptStringAES(dataSet.Tables[0].Rows[m]["AID"].ToString());
                        appontment1.tltd = dataSet.Tables[0].Rows[m]["tltd"].ToString();
                        appontment1.tlth = dataSet.Tables[0].Rows[m]["tlth"].ToString();
                        appontment1.comments = dataSet.Tables[0].Rows[m]["comments"].ToString();
                        appontments1.Add(appontment1);
                    }
                }
                accountModel.appointmentList = appontments1;
                dataSet.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                accountModel.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return accountModel;
        }

        public AccountModel GetCustomerreviews(int UserId, int startindex, int endindex, ref int total)
        {
            AccountModel accountModel = new AccountModel();
            List<Reviews> reviews = new List<Reviews>();
            RatingHelper ratingHelper = new RatingHelper();
            SqlHelper sqlHelper = new SqlHelper();
            Crypto crypto = new Crypto();
            SortedList sortedLists = new SortedList();
            total = 0;
            try
            {
                sortedLists.Add("@userId", UserId);
                sortedLists.Add("@startindex", startindex);
                sortedLists.Add("@endindex", endindex);
                DataSet dataSet = sqlHelper.fillDataSet("GetreviewsCustomerById", "", sortedLists);
                if (dataSet.Tables[1].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[1].Rows.Count; i++)
                    {
                        reviews.Add(new Reviews()
                        {
                            Name = dataSet.Tables[1].Rows[i]["Name"].ToString(),
                            tokenId = crypto.EncryptStringAES(dataSet.Tables[1].Rows[i]["Id"].ToString()),
                            Review = dataSet.Tables[1].Rows[i]["Comments"].ToString(),
                            rating = Convert.ToInt32(dataSet.Tables[1].Rows[i]["rating"].ToString()),
                            srno = Convert.ToInt32(dataSet.Tables[1].Rows[i]["RowNum"].ToString())
                        });
                        total = Convert.ToInt32(dataSet.Tables[1].Rows[i]["total"].ToString());
                    }
                }
                accountModel.Reviews = reviews;
                dataSet.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                accountModel.ErrorText = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return accountModel;
        }

        public void getOldOTP(string mobile, string email, ref string emailVerificationCode, ref string mobileVerificationCode)
        {
            string empty = string.Empty;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@email", email);
                sortedLists.Add("@mobile", mobile);
                DataTable dataTable = sqlHelper.fillDataTable("getpreviousOZTP", "", sortedLists);
                if (dataTable.Rows.Count <= 0)
                {
                    emailVerificationCode = "";
                    mobileVerificationCode = "";
                }
                else
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        emailVerificationCode = dataTable.Rows[i]["emailverifycode"].ToString();
                        mobileVerificationCode = dataTable.Rows[i]["MobileVerifyCode"].ToString();
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
        }

        public string GetPassword(int Userid)
        {
            string str = "0";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                str = sqlHelper.executeScaler(string.Concat("Select Password from tbluser WITH(NOLOCK) WHere id =", Userid)).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string getPasswordByUserName(loginmodel _login)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            try
            {
                sortedLists.Add("@userName", _login.username);
                empty = sqlHelper.executeNonQueryWMessage("tbluserforgetPassword", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return empty;
        }

        public string GetPhotoIdByUserId(int id)
        {
            string empty = string.Empty;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@UserId", id);
                DataTable dataTable = sqlHelper.fillDataTable("GetuplodedIdByUserId", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    empty = dataTable.Rows[i]["idUrl"].ToString();
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return empty;
        }

        public TRegisterModel GetTherapistById(int Id)
        {
            TRegisterModel tRegisterModel = new TRegisterModel();
            List<images> _images = new List<images>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto crypto = new Crypto();
            try
            {
                sortedLists.Add("@id", Id);
                DataTable dataTable = sqlHelper.fillDataTable("tbltherapistById", "", sortedLists);
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        tRegisterModel.name = dataTable.Rows[i]["Name"].ToString();
                        tRegisterModel.token = crypto.EncryptStringAES(Id.ToString());
                        tRegisterModel.age = Convert.ToInt32(dataTable.Rows[i]["age"].ToString());
                        tRegisterModel.imagePath = dataTable.Rows[i]["imagePath"].ToString();
                        tRegisterModel.languages = dataTable.Rows[i]["languages"].ToString();
                        tRegisterModel.location = dataTable.Rows[i]["location"].ToString();
                        tRegisterModel.massageStyle = dataTable.Rows[i]["MassageStyles"].ToString();
                        tRegisterModel.rate = Convert.ToDecimal(dataTable.Rows[i]["rate"].ToString());
                    }
                }
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            tRegisterModel.profileImages = _images;
            return tRegisterModel;
        }

        public string getTPasswordByUserName(loginmodel _login)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            try
            {
                sortedLists.Add("@userName", _login.username);
                empty = sqlHelper.executeNonQueryWMessage("tbltherapistforgetPassword", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return empty;
        }

        public string getUserLocationById(int id)
        {
            string str = "User info updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                str = sqlHelper.executeScaler(string.Concat("Select isnull(lth,'')+'_'+isnull(ltd,'') from tbluser with(NOLOCK) WHere id=", id)).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return str;
        }

        public string UpdateCustomer(RegisterModel model)
        {
            long num;
            string str = "User info updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            smshelper _smshelper = new smshelper();
            if (!long.TryParse(model.userName, out num))
            {
                return "Error : Username must be your mobile number";
            }
            //if (model.userName.Length > 10 || model.userName.Length < 10)
            //{
            //    return "Error : Username must be your mobile number";
            //}
            try
            {
                sortedLists.Add("@Password", model.password);
                sortedLists.Add("@UserName", model.userName);
                sortedLists.Add("@emailVerificationCode", model.emailVerification);
                sortedLists.Add("@mobileVerificationCode", model.mobileVerification);
                str = sqlHelper.executeNonQueryWMessage("tbluserUpdateInfo", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public string updatePassword(string old, string newpass, int UserId, string confirm)
        {
            string str = "User info updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                if (newpass == confirm)
                {
                    sortedLists.Add("@newPass", newpass);
                    sortedLists.Add("@oldPass", old);
                    sortedLists.Add("@Userid", UserId);
                    str = sqlHelper.executeNonQueryWMessage("VerifyPassword", "", sortedLists).ToString();
                }
                else
                {
                    str = "Error : password and confirm password doesnt match";
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public string UpdateTCustomer(TRegisterModel model)
        {
            string str = "User info updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@UserName", model.userName);
                sortedLists.Add("@mobileVerificationCode", model.mobileVerification);
                str = sqlHelper.executeNonQueryWMessage("tblherapistUpdateInfo", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public string UpdateUserName(AccountModel model)
        {
            long num;
            string str = "User info updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            smshelper _smshelper = new smshelper();
            if (!long.TryParse(model.userName, out num))
            {
                return "Error : Username must be your mobile number";
            }
            //if (model.userName.Length > 10 || model.userName.Length < 10)
            //{
            //    return "Error : Username must be your mobile number";
            //}
            try
            {
                sortedLists.Add("@userId", model.Id);
                sortedLists.Add("@EMail", model.emailAddress);
                sortedLists.Add("@UserName", model.userName);
                sortedLists.Add("@emailVerificationCode", model.emailVerification);
                sortedLists.Add("@mobileVerificationCode", model.mobileVerification);
                str = sqlHelper.executeNonQueryWMessage("tbluserUpdateUserName", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }

        public string validateAdminLogin(loginmodel _login)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            try
            {
                if (_login.username != "Admin" && _login.password != "Admin123")
                {
                    empty = " Error : your password does'not match ";
                }
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return empty;
        }

        public string validateLogin(loginmodel _login)
        {
            long num;
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            smshelper _smshelper = new smshelper();
            if (!long.TryParse(_login.username, out num))
            {
                return "Error : Username must be your mobile number";
            }
            //if (_login.username.Length > 10 || _login.username.Length < 10)
            //{
            //    return "Error : Username must be your mobile number";
            //}
            try
            {
                sortedLists.Add("@userName", _login.username);
                sortedLists.Add("@password", _login.password);
                empty = sqlHelper.executeNonQueryWMessage("tbluserLogin", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return empty;
        }

        public string validateTLogin(loginmodel _login)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string empty = string.Empty;
            try
            {
                sortedLists.Add("@userName", _login.username);
                sortedLists.Add("@password", _login.password);
                empty = sqlHelper.executeNonQueryWMessage("tblTherapistLogin", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                empty = string.Concat("Error : ", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return empty;
        }

        public string verifyEmail(string email, string mobile, int UserId)
        {
            string str = "User info updated";
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@email", email);
                sortedLists.Add("@mobile", mobile);
                sortedLists.Add("@Userid", UserId);
                str = sqlHelper.executeNonQueryWMessage("tblUpdateNewUserVerify", "", sortedLists).ToString();
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                str = string.Concat("Error :", exception.Message);
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return str;
        }
    }
}