﻿using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace californiaspa.APITasks
{
    public partial class Record : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string RecordingUrl = Request.Params["RecordingUrl"].ToString();
            int Aid = Convert.ToInt32(Request.QueryString["Aid"].ToString());
            AddUrl(RecordingUrl, Aid);
        }

        private void AddUrl(string url, int Aid)
        {
            try
            {
                SqlHelper _sql = new SqlHelper();
                SortedList _list = new SortedList();
                _list.Add("@Aid", Aid);
                _list.Add("@Url", url);
                _sql.executeNonQuery("AddAudioFromTwilio", "", _list);
            }
            catch (Exception ex)
            {
                Logger _log = new Logger(ex.Message, "", "", 2, "", Enums.LogType.Info);
                _log.LogWrite();
            }
        }
    }
}