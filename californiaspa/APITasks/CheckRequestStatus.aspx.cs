﻿using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Twilio;

namespace californiaspa.APITasks
{
    public partial class CheckRequestStatus : System.Web.UI.Page
    {
        private TwilioRestClient twilio = new TwilioRestClient("AC262d4400f4061aa96d2690f29e23caa4", "a979aeab9dc1590454dceee35b3ae755");

        private string turl = "https://www.californiamassage.in/APITasks/textTospeech.aspx?SID=";

        protected void Page_Load(object sender, EventArgs e)
        {
            //  XmlHelper xmlHelper = new XmlHelper();
            //  string str = xmlHelper.readRequestCheckerXML();
            try
            {
                try
                {
                    //DateTime currentTimeByTimeZone = DateTime.Now.GetCurrentTimeByTimeZone("India Standard Time");
                    //string str1 = string.Concat(new object[] { currentTimeByTimeZone.Month, "/", currentTimeByTimeZone.Day, "/", currentTimeByTimeZone.Year });
                    //DateTime dateTime = Convert.ToDateTime(string.Concat(str1, " 8:00 AM"));
                    //DateTime dateTime1 = Convert.ToDateTime(string.Concat(str1, " 10:00 PM"));
                    //if (str == "T" && currentTimeByTimeZone > dateTime && currentTimeByTimeZone < dateTime1)
                    //if (str == "T")
                    //{
                    //  //  xmlHelper.CreateRequestCheckerXML("F");
                    //  //  this.SendSMS(str);
                    //}
                }
                catch (Exception exception)
                {
                }
            }
            finally
            {
                //  xmlHelper.CreateRequestCheckerXML("T");
            }
        }

        private void SendSMS(string IsStart)
        {
            SqlHelper sqlHelper = new SqlHelper();
            DataTable dataTable = new DataTable();
            XmlHelper xmlHelper = new XmlHelper();
            try
            {
                try
                {
                    if (IsStart == "T")
                    {
                        dataTable = sqlHelper.fillDataTable("GetAppointmentNotification", "", null);
                        if (dataTable.Rows.Count > 0)
                        {
                            SortedList sortedLists = new SortedList();
                            smshelper _smshelper = new smshelper();
                            int num = 0;
                            int id = 0;
                            string str = "";
                            string str1 = "";
                            string str2 = "CALIFORNIA SPA :Appointment Reminder- You have got an Appointment Request before few minutes, please respond";
                            string empty = string.Empty;
                            for (int i = 0; i < dataTable.Rows.Count; i++)
                            {
                                id = Convert.ToInt32(dataTable.Rows[i]["therapistId"]);
                                if (xmlHelper.allowTocommunicate(id))
                                {
                                    sortedLists.Clear();
                                    num = Convert.ToInt32(dataTable.Rows[i]["Aid"]);
                                    str = dataTable.Rows[i]["mobile"].ToString();
                                    DateTime indianTimeFromPST = Convert.ToDateTime(dataTable.Rows[i]["CReatedDate"].ToString()).GetIndianTimeFromPST();
                                    str1 = indianTimeFromPST.ToString("dd-MMM-y ; hh:mm tt");
                                    str2 = dataTable.Rows[i]["Msg"].ToString().Replace("@time", str1);
                                    _smshelper.SendSms(str, str2, "+14177089506");
                                    sortedLists.Add("@aid", num);
                                    sortedLists.Add("@text", str2);
                                    int num1 = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("UpdateAppointmentNotify", "", sortedLists).ToString());
                                    Thread.Sleep(10000);
                                    this.turl = string.Concat(this.turl, num1);
                                    str = (str.Length < 11 ? string.Concat("+91", str) : str);
                                    _smshelper.SendSms("7760555500", str2, "+14177089506");
                                    this.twilio.InitiateOutboundCall("+19282373824", str, this.turl);
                                    Thread.Sleep(10000);
                                }
                            }
                        }
                    }
                }
                catch (ThreadAbortException threadAbortException)
                {
                }
            }
            finally
            {
                xmlHelper.CreateRequestCheckerXML("T");
                dataTable.Dispose();
                sqlHelper.Dispose();
            }
        }
    }
}