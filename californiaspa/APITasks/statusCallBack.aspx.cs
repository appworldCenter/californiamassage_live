﻿using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace californiaspa.APITasks
{
    public partial class statusCallBack : System.Web.UI.Page
    {
        private void AddUrl(string status, int Aid, string Number)
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();
                SortedList sortedLists = new SortedList()
                {
                    { "@Aid", Aid },
                    { "@status", Number }
                };
                sqlHelper.executeNonQuery("UpdateStatus", "", sortedLists);
            }
            catch (Exception exception)
            {
                (new Logger(exception.Message, "", "", 2, "", Enums.LogType.Info)).LogWrite();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string status = base.Request.Params["CallStatus"].ToString();
            int num = Convert.ToInt32(base.Request.QueryString["Aid"].ToString());
            string number = base.Request.QueryString["number"].ToString();
            this.AddUrl(status, num, number);
        }
    }
}