﻿using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace californiaspa.APITasks
{
    public partial class SMSSender : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlHelper _xml = new utilis.XmlHelper();
            var status = _xml.readSMSSenderXML();
            try
            {
                if (status == "T")
                {
                    //  _xml.CreateSmsSenderXML("F");
                    // SendSMS(status);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                _xml.CreateSmsSenderXML("T");
            }

        }

        private void SendSMS(string IsStart)
        {

            SqlHelper _sql = new utilis.SqlHelper();
            DataTable dt = new DataTable();
            XmlHelper _xml = new utilis.XmlHelper();
            try
            {
                if (IsStart == "T")
                {
                    dt = _sql.fillDataTable("GetSmsForSent", "", null);
                    if (dt.Rows.Count > 0)
                    {
                        SortedList _list = new SortedList();
                        smshelper _smshelper = new utilis.smshelper();
                        int id = 0;
                        string mobile = "";
                        string text = "";  
                        string status = string.Empty;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            _list.Clear();
                            id = Convert.ToInt32(dt.Rows[i]["Id"]);
                            mobile = dt.Rows[i]["MobileNumber"].ToString();
                            text = dt.Rows[i]["body"].ToString(); 
                            status = _smshelper.SendSms( mobile, text);
                            _list.Add("@Id", id);
                            _list.Add("@Status", status);
                            _sql.executeNonQuery("UpdateSmsStatus", "", _list);
                            Thread.Sleep(3000);
                        }
                    }
                }
            }
            catch (ThreadAbortException)
            {

            }

            finally
            {
                _xml.CreateSmsSenderXML("T");
                dt.Dispose();
                _sql.Dispose();
            }


        }
    
}
}