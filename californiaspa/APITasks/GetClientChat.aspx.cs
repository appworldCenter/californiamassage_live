﻿using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Twilio;

namespace californiaspa.APITasks
{
    public partial class GetClientChat : System.Web.UI.Page
    {
        private TwilioRestClient twilio = new TwilioRestClient("AC262d4400f4061aa96d2690f29e23caa4", "a979aeab9dc1590454dceee35b3ae755");

        private string turl = "https://www.californiamassage.in/APITasks/textTospeech.aspx?SID=";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (base.Request.QueryString["test"] != null)
            {
                //    this.turl = string.Concat(this.turl, "987");
                //   this.twilio.InitiateOutboundCall("+19282373824", "+919813151090", this.turl);
            }
            XmlHelper xmlHelper = new XmlHelper();
            string str = xmlHelper.readChatRequestCheckerXML();
            try
            {
                try
                {
                    if (str == "T")
                    {
                        //   xmlHelper.CreateChatRequestCheckerXML("F");
                        //  this.SendSMS(str);
                    }
                }
                catch (Exception exception)
                {
                }
            }
            finally
            {
                // xmlHelper.CreateChatRequestCheckerXML("T");
            }
        }

        private void SendSMS(string IsStart)
        {
            SqlHelper sqlHelper = new SqlHelper();
            DataTable dataTable = new DataTable();
            SortedList sortedLists = new SortedList();
            XmlHelper xmlHelper = new XmlHelper();
            smshelper _smshelper = new smshelper();
            int num = 0;
            string str = "";
            string str1 = "";
            string str2 = "";
            int num1 = 0;
            try
            {
                try
                {
                    int id = 0;
                    if (IsStart == "T")
                    {
                        dataTable = sqlHelper.fillDataTable("GetClientChat", "", null);
                        if (dataTable.Rows.Count > 0)
                        {
                            for (int i = 0; i < dataTable.Rows.Count; i++)
                            { id = Convert.ToInt32(dataTable.Rows[i]["therapistId"]);
                                if (xmlHelper.allowTocommunicate(id))
                                {
                                    sortedLists.Clear();
                                    num = Convert.ToInt32(dataTable.Rows[i]["clientId"].ToString());
                                    num1 = Convert.ToInt32(dataTable.Rows[i]["therapistid"].ToString());
                                    DateTime indianTimeFromPST = Convert.ToDateTime(dataTable.Rows[i]["CReatedDate"].ToString()).GetIndianTimeFromPST();
                                    str = indianTimeFromPST.ToString("dd-MMM-y ; hh:mm tt");
                                    str2 = dataTable.Rows[i]["mobile"].ToString();
                                    str1 = dataTable.Rows[i]["Msg"].ToString().Replace("@time", str);
                                    _smshelper.SendSms(str2, str1, "+14177089506");
                                    sortedLists.Add("@ClientId", num);
                                    sortedLists.Add("@therapistId", num1);
                                    sortedLists.Add("@text", str1);
                                    int num2 = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("UpdateChatReplyTime", "", sortedLists).ToString());
                                    Thread.Sleep(10000);
                                    this.turl = string.Concat(this.turl, num2);
                                    str2 = (str2.Length < 11 ? string.Concat("+91", str2) : str2);
                                    this.twilio.InitiateOutboundCall("+19282373824", str2, this.turl);
                                    _smshelper.SendSms("7760555500", str1, "+14177089506");
                                    Thread.Sleep(10000);
                                }
                            }
                        }
                    }
                }
                catch (ThreadAbortException threadAbortException)
                {
                }
            }
            finally
            {
                xmlHelper.CreateRequestCheckerXML("T");
                dataTable.Dispose();
                sqlHelper.Dispose();
            }
        }
    }
}