﻿using californiaspa.DataBase;
using californiaspa.utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Twilio;

namespace californiaspa.APITasks
{
    public partial class gettherapistChat1 : System.Web.UI.Page
    {
        private TwilioRestClient twilio = new TwilioRestClient("AC262d4400f4061aa96d2690f29e23caa4", "a979aeab9dc1590454dceee35b3ae755");
        private string turl = "https://www.californiamassage.in/APITasks/textTospeech.aspx?SID=";
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlHelper xmlHelper = new XmlHelper();
            string str = xmlHelper.readtherapistRequestCheckerXML2();
            try
            {
                if (str == "T")
                {
                    //  xmlHelper.CreatetherapistRequestCheckerXML2("F");
                    //  this.SendSMS(str);
                }
            }
            catch (Exception exception)
            {
            }
            finally
            {
                //  xmlHelper.CreatetherapistRequestCheckerXML2("T");
            }
        }

        private void SendSMS(string IsStart)
        {
            SqlHelper sqlHelper = new SqlHelper();
            DataTable dataTable = new DataTable();
            SortedList sortedLists = new SortedList();
            XmlHelper xmlHelper = new XmlHelper();
            smshelper _smshelper = new smshelper();
            int num = 0;
            string str = "";
            string str1 = "";
            string str2 = "";
            int num1 = 0;
            try
            {
                int id = 0;
                if (IsStart == "T")
                {
                    dataTable = sqlHelper.fillDataTable("GettherapistChat1", "", null);
                    if (dataTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            sortedLists.Clear();
                            num = Convert.ToInt32(dataTable.Rows[i]["clientId"].ToString());
                            num1 = Convert.ToInt32(dataTable.Rows[i]["therapistid"].ToString());
                            DateTime indianTimeFromPST = Convert.ToDateTime(dataTable.Rows[i]["CReatedDate"].ToString()).GetIndianTimeFromPST();
                            str = indianTimeFromPST.ToString("dd-MMM-y ; hh:mm tt");
                            str2 = dataTable.Rows[i]["mobile"].ToString();
                            str1 = dataTable.Rows[i]["Msg"].ToString().Replace("@time", str);
                            sortedLists.Add("@therapistId", num1);
                            sortedLists.Add("@clientid", num);
                            sortedLists.Add("@text", str1);
                            int num2 = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("UpdateChatReplyTime1", "", sortedLists).ToString());
                            this.turl = string.Concat(new object[] { this.turl, num2, "&tid=", num2 });
                            str2 = (str2.Length < 11 ? string.Concat("+91", str2) : str2);
                            this.twilio.InitiateOutboundCall("+19282373824", str2, this.turl);
                            Thread.Sleep(10000); 
                        }
                    }
                }
            }
            catch (ThreadAbortException threadAbortException)
            {
            }
            finally
            {
                xmlHelper.CreatetherapistRequestCheckerXML2("T");
                dataTable.Dispose();
                sqlHelper.Dispose();
            }
        }
    }
}